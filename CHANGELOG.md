## [1.6.5](https://gitlab.dev.bling.sh/adar/bling/project_cor_e/compare/v1.6.4...v1.6.5) (2019-06-05)


### Bug Fixes

* **chartController:** fix obliczania średniej dla boxplota + zmiana komunikatu na info o nowych danych ([0dc4ea3](https://gitlab.dev.bling.sh/adar/bling/project_cor_e/commit/0dc4ea3))
* **MR:** formatowanie sie zjebalo -,- ([8de0fd1](https://gitlab.dev.bling.sh/adar/bling/project_cor_e/commit/8de0fd1))

## [1.6.4](https://gitlab.dev.bling.sh/adar/bling/project_cor_e/compare/v1.6.3...v1.6.4) (2019-03-11)


### Bug Fixes

* ogarniecie bugow - wykresy weather i availability ([13963ac](https://gitlab.dev.bling.sh/adar/bling/project_cor_e/commit/13963ac))

## [1.6.3](https://gitlab.dev.bling.sh/adar/bling/project_cor_e/compare/v1.6.2...v1.6.3) (2019-03-04)


### Bug Fixes

* PEAK values in nexWeek only between 9 and 20 hour ([0aca13a](https://gitlab.dev.bling.sh/adar/bling/project_cor_e/commit/0aca13a))

## [1.6.2](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.6.1...v1.6.2) (2019-02-26)


### Bug Fixes

* **boxplot & prices:** fixed dots colors ([e2b3183](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/e2b3183))
* **boxplot & prices:** mr fixes ([5bd4f7c](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/5bd4f7c))

## [1.6.1](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.6.0...v1.6.1) (2019-02-26)


### Bug Fixes

* **boxplot:** centering labels to gridLines ([251bcbb](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/251bcbb))

# [1.6.0](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.5.1...v1.6.0) (2019-02-26)


### Features

* **bigTable:** prevent backwards changing data ([aa03ba3](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/aa03ba3))
* **env:** added env to .gitignore ([839dcf7](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/839dcf7))

## [1.5.1](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.5.0...v1.5.1) (2019-02-25)


### Bug Fixes

* **bigTable:** fixed displaying countries two times ([ee1255f](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/ee1255f))

# [1.5.0](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.4.0...v1.5.0) (2019-02-25)


### Features

* **bigTable:** mr fix ([f5cebab](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/f5cebab))
* **bigTable:** sticky header & left column ([f48b7a8](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/f48b7a8))

# [1.4.0](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.3.1...v1.4.0) (2019-02-22)


### Bug Fixes

* **komentarze:** usuniecie niepotrzebnych komentarzy ([d07b132](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/d07b132))


### Features

* **weather i boxplot:** dodanie histohramu do wykresu temperatury (weather) i dodanie do boxplota realised date + dodanie filtrowania danych na buttonach prices ([10d8df5](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/10d8df5))
* **weather i boxplot:** dodanie histohramu do wykresu temperatury (weather) i dodanie do boxplota realised date + dodanie filtrowania danych na buttonach prices ([354d4ec](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/354d4ec))

## [1.3.1](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.3.0...v1.3.1) (2019-02-22)


### Bug Fixes

* **charts:** more margin between chart buttons ([a2786ec](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/a2786ec))
* **prices:** prices chart arrows fix ([a1ac6b4](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/a1ac6b4))

# [1.3.0](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.2.4...v1.3.0) (2019-02-21)


### Bug Fixes

* **datatable:** visible scroll on data table ([76bd888](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/76bd888))


### Features

* **bigtable:** checking if there is next/prev data available ([9ab9c94](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/9ab9c94))

## [1.2.4](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.2.3...v1.2.4) (2019-02-19)


### Bug Fixes

* **availability:** changed color of brown coal ([6818334](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/6818334))

## [1.2.3](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.2.2...v1.2.3) (2019-02-18)


### Bug Fixes

* **prices:** centered prev/next buttons ([293b14a](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/293b14a))

## [1.2.2](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.2.1...v1.2.2) (2019-02-18)


### Bug Fixes

* **datatable:** changes in csv export ([e13034a](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/e13034a))

## [1.2.1](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.2.0...v1.2.1) (2019-02-18)


### Bug Fixes

* **prices:** unsubscribing models and countries changes on destroy ([6b71843](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/6b71843))

# [1.2.0](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.1.3...v1.2.0) (2019-02-13)


### Features

* **charts:** pr fix ([d790640](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/d790640))
* **charts & bigtable:** added typing ([36c009d](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/36c009d))
* **charts & bigtable:** changing data + distinguishing model runDates ([9dc09b5](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/9dc09b5))
* **charts & bigtable:** merge request fixes ([6d7f56b](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/6d7f56b))
* **charts & bigtable:** merge request fixes 2 ([cb186a4](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/cb186a4))
* **charts & bigtable:** pr fix ([96b1ad5](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/96b1ad5))
* **charts & bigtable:** remove console.log ([d6cfa98](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/d6cfa98))
* **prices:** chart fullscreen bigger font ([cf35369](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/cf35369))

## [1.1.3](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.1.2...v1.1.3) (2019-02-06)


### Bug Fixes

* **prices:** fixed empty queryParams ([1ad9d1a](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/1ad9d1a))

## [1.1.2](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.1.1...v1.1.2) (2019-02-06)


### Bug Fixes

* **app:** fixed saving data to local storage ([afeadd9](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/afeadd9))
* **charts:** clearing chart state after logout/destroy ([0729be3](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/0729be3))
* **login:** fixed errors in password reset ([4539b50](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/4539b50))

## [1.1.1](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.1.0...v1.1.1) (2019-02-04)


### Bug Fixes

* **prices:** realised data UTC ([3ed204c](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/3ed204c))

# [1.1.0](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.0.1...v1.1.0) (2019-01-30)


### Features

* **pricesChart:** displaying one date when there is 1d selected ([c80f89e](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/c80f89e))

## [1.0.1](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/compare/v1.0.0...v1.0.1) (2019-01-30)


### Bug Fixes

* **prices realised:** new method to set styles ([73e26e4](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/73e26e4))

# 1.0.0 (2019-01-25)


### Features

* **commits:** dodanie semantycznego commitowania ([e6381c2](https://gitlab.dev.bling.sh/bling-projects/project_cor_e/commit/e6381c2))


### BREAKING CHANGES

* **commits:** semantic commits & releases
