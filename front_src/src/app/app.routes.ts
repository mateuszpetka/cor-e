import { PreviewComponent } from './preview.component';

import { UserprofileComponent } from './containers/userprofile/userprofile.component';
import { LogoutComponent } from './containers/logout/logout.component';
import { ChartlistComponent } from './containers/chartlist/chartlist.component';
import { LoginComponent } from './containers/login/login.component';

// USER-CODE-O
import { RecoverPasswordComponent } from './components/recover-password/recover-password.component';
// USER-CODE-C

export const AppRoutes = [
  // USER-CODE-O
  {
    path: 'auth/recovery-password',
    children: [{
      path: ':token',
      component: RecoverPasswordComponent
    }]
  },
  // USER-CODE-C
  {
    path: 'enable-preview',
    component: PreviewComponent,
  },
  {
    path: 'userprofile',
    component: UserprofileComponent,
  },
  {
    path: 'auth/logout',
    component: LogoutComponent,
  },
  {
    path: 'dashboard',
    component: ChartlistComponent,
  },
  {
    path: 'auth/login',
    component: LoginComponent,
  },

  // USER-CODE-O// USER-CODE-C
];
