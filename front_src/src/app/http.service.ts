import { Injectable } from '@angular/core';
import { RequestOptionsArgs } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { pick } from 'lodash';

import { AppSettings } from './app.settings';

@Injectable()
export class HttpService {

  constructor(
    private http: HttpClient,
    private settings: AppSettings
  ) { }

  /**
   * Send GET request.
   *
   * @param {string} url
   * @param {Object} [options]
   * @returns {Observable<any>}
   * @memberof HttpService
   */
  public get(url: string, options?: Object): Observable<any> {
    if (options === undefined) {
      options = {};
    }

    options['headers'] = new HttpHeaders({
      'php-auth-digest': localStorage.getItem('frontAuthToken') || this.settings.authGuestToken,
    });

    if (this.isPreviewMode()) {
      if (options['search'] === undefined || options['search'] === '') {
        options['search'] = {};
      }

      if (typeof options['search'] === 'object') {
        Object.assign(options['search'], {previewMode: true});
      } else {
        options['search'] += '&previewMode=true';
      }

      options['headers'] = new HttpHeaders({
        'php-auth-digest': sessionStorage.getItem('previewAuthToken'),
      });
    }

    return this.http.get(this.settings.apiUrl + '/' + url, options)
      .pipe(
        map( (res: any) => this.mapListResponse(res.data, options))
      );
  }

  /**
   * Send POST request.
   *
   * @param {string} url
   * @param {*} body
   * @param {Object} [options]
   * @returns {Observable<any>}
   * @memberof HttpService
   */
  public post(url: string, body: any, options?: Object): Observable<any> {
    if (options === undefined) {
      options = {};
    }

    options['headers'] = new HttpHeaders({
      'php-auth-digest': localStorage.getItem('frontAuthToken') || this.settings.authGuestToken,
    });

    return this.http.post(this.settings.apiUrl + '/' + url, body, options)
      .pipe(
        map( (res: any) => {
          return !res['_body'] ? res : res.data;
        })
      );
  }

  /**
   * Check if current request is in preview mode.
   *
   * @returns {boolean}
   * @memberof HttpService
   */
  public isPreviewMode(): boolean {
    if (sessionStorage.getItem('previewMode') && sessionStorage.getItem('previewAuthToken')) {
      return true;
    }

    return false;
  }

  /**
   * Return proper auth token.
   *
   * @returns {string}
   * @memberof HttpService
   */
  private getAuthToken(): string {
    if (this.isPreviewMode()) {
      return JSON.parse(sessionStorage.getItem('previewAuthToken') || '{}');
    }

    return localStorage.getItem('frontAuthToken');
  }

  /**
   * Zmapowanie rekordów listy jeśli wysłano zapytanie z parametrem withJoin=true.
   *
   * @param {any[]} records
   * @param {RequestOptionsArgs} options
   * @returns {any[]}
   */
  protected mapListResponse(records: any, options: RequestOptionsArgs) {
    let totalCount;

    if (options.params && !!options.params['limit']) {
      totalCount = records['totalCount'];
      records = records['items'];
    }

    if (
      (options.params && options.params['withJoin'] === null)
      || records.length === 0
      || !this.settings['mapResponseJoins']
      || !Array.isArray(records)
    ) {
      return records;
    }

    const joins = {};
    const unJoinedFields = Object.keys(records[0]).filter(key => !key.startsWith('__'));

    for (const key in records[0]) {
      if (records[0].hasOwnProperty(key) && key.startsWith('__')) {
        const propertyNames = key.slice(2).split('_');

        if (!joins.hasOwnProperty(propertyNames[0])) {
          joins[propertyNames[0]] = [];
          unJoinedFields.push(propertyNames[0])
        }

        joins[propertyNames[0]].push(propertyNames[1]);
      }
    }

    records = records.map(record => {
      for (const entityJoin in joins) {
        if (joins.hasOwnProperty(entityJoin)) {
          record[entityJoin] = {};

          for (const fieldJoin of joins[entityJoin]) {
            record[entityJoin][fieldJoin] = record[`__${entityJoin}_${fieldJoin}`]
          }
        }
      }

      return pick(record, unJoinedFields);
    });

    if (options.params && !!options.params['limit']) {
      records = {
        'items': records,
        'totalCount': totalCount
      }
    }

    return records;
  }
}
