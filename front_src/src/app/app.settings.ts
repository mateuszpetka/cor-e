import { environment } from 'environments/environment';

export class AppSettings {
  public apiIp = environment.apiIp;
  public apiUrl = this.apiIp + '/api';
  public authGuestToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjE1MjIzOTk4NjEsImV4cCI6MzA5OTE5OTg2MSwiaWQiOjAsImxvZ2luIjoiZ3Vlc3QiLCJncm91cElkIjoxMH0.OI3d4QHiRR-scgcX4pB6X_Yml1uZ3K883W7leL62Hcs';
  public captchaKey = '6Lf94jQUAAAAAK9qAkJQV2mEEJPHoDvno992kVtK';

  constructor() {
    if (location.protocol === 'https:') {
      this.apiIp = 'https://' + location.hostname;
    }
  }
}
