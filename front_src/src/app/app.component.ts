import {Component, HostBinding, OnInit, ElementRef} from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

import { AppSettings } from 'app/app.settings';
import { HttpService } from './http.service';

// USER-CODE-O
import { AuthService } from './components/auth/auth.service';
import { TokenService } from './components/auth/token.service';
import { SharedService } from './shared/shared.service';
import { AppService } from './app.service';
import { BehaviorSubject, Observable } from "rxjs";
// USER-CODE-C

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  // USER-CODE-O
  // USER-CODE-C
})
export class AppComponent implements OnInit {
  // USER-CODE-O
  @HostBinding('class') themeName: string;

  public previewMode = false;

  constructor(
    private router: Router,
    private httpService: HttpService,
    // USER-CODE-O
    private authService: AuthService,
    private tokenService: TokenService,
    private _sharedService: SharedService,
    private _appService: AppService,
    private elemetRef: ElementRef,
    // USER-CODE-C
  ) {
    if (this.httpService.isPreviewMode()) {
      this.previewMode = true;
    }
    // USER-CODE-O
    // Potrzebne do logowania
    const authToken = localStorage.getItem('authToken');
    if (location.pathname.includes('recovery-password')) {
      this.router.navigate([location.pathname]);
      this.router.initialNavigation();
    } else if (authToken === null) {
      this.router.navigate(['/auth/login']);
    } else if (authToken === 'undefined') {
      this.router.navigate(['/auth/logout']);
    } else {

      const expirationTime = Math.floor(this.tokenService.getTimeLeftToTokenExpiration(authToken) / 10);
      if (expirationTime <= 0) {
        this.router.navigate(['/auth/logout']);
      } else {
        this.authService.refreshTokenPeriodically();
        this._sharedService.setInitialFilters();
        this._appService.passChartStatesToSharedService();
        this.router.navigate(['/dashboard']);
      }
      this.router.initialNavigation();
    }
    // USER-CODE-C
  }

  // USER-CODE-O
  ngOnInit() {
    this.changeTheme();
  }
  /** Przypisuje nowy wyglad szablonu
   *
   * @returns void
   */
  changeTheme(): void {
    if (!!this._appService.getUserFromLocalStorage()) {
      this.themeName =  this._appService.getUserFromLocalStorage().customFields ?
      this._appService.getUserFromLocalStorage().customFields.themeDisplay : 'default_theme';

      this._appService.newTheme$.subscribe(theme => {
        if (theme) {
          this.themeName = theme;
        }
      });

    } else {
      this._appService.newTheme$.subscribe(theme => {
        this.themeName = theme;
      });
    }
  }
  // USER-CODE-C
}
