import { Chart, ChartData } from 'chart.js';
import * as moment from 'moment';
import { cloneDeep } from 'lodash';

interface BackgroundColor {
  main: string;
  opac: string;
}

// boxPlot Chart
export interface ChartDataBox {
  name: string;
  model: string;
  run_date: string;
  series: DataSetData[];
}

export class DataSetData {
  public min: number;
  public median: number;
  public max: number;
  public q1: number;
  public q3: number;
  public day: string;
  public whiskerMax: number;
  public whiskerMin: number;
  public x: string;

  constructor(data) {
    this.day = data.day;
    this.min = data.min;
    this.median = data.med;
    this.max = data.max;
    this.q1 = data.q1;
    this.q3 = data.q3;
    this.whiskerMax = data.max;
    this.whiskerMin = data.min;
    this.x = data.day;
  }
}

// bubble Chart
export interface ChartDataBubble {
  series: DataSetBubble[];
}

export interface BubbleSeriesItem {
  x: string;
  y: number;
  r: number;
}

export class DataSetBubble implements BubbleSeriesItem {
  public x: string;
  public y: number;
  public r: number;
  public min: number;
  public max: number;

  constructor(data: BubbleSeriesItem) {
    this.x = data.x;
    this.y = data.y;
    this.r = data.r;
    this.min = Number(data.y);
    this.max = Number(data.y);
  }
}

export class DataSets {
  public data: DataSetData[] = [];
  public fill: boolean = false;
  public borderColor: string = '';
  public backgroundColor: string = '';
  public borderWidth: number = 1;
  public hidden: boolean = false;
  public label: string = '';
  public runDate: string = '';

  constructor(
    series: any[],
    runDate: string,
    model: string,
    color: BackgroundColor
  ) {
    this.label = model;
    this.runDate = runDate;
    this.borderColor = color ? color.main : '#195f9a';
    this.backgroundColor = color ? color.opac : '#195f9a';
    series.forEach(seriesItem => {
      const newDataSetData: DataSetData = new DataSetData(seriesItem);
      this.data.push(newDataSetData);
    });
  }
}

export class DataSetsBubble {
  public type: string = 'bubble';
  public data: DataSetBubble[] = [];
  public padding: number = 10;
  public borderColor: string;
  public borderWidth: number = 2;
  public backgroundColor: string = 'transparent';
  public hidden: boolean = false;
  public hoverBorderWidth: number = 2;
  public hoverRadius: number = 2;

  constructor(
    series: BubbleSeriesItem[]
  ) {
    series.forEach(item => {
      if (!!item.y) {
        this.data.push(new DataSetBubble(item));
      }
    });
  }
}

export class BoxplotChartCustomOption {
  type: string = 'boxplot';
  data = {
    datasets: []
  };
  options = {
    maintainAspectRatio: false,
    scaleShowValues: true,
    responsive: false,
    legend: false,
    scales: {
      xAxes: [{
        gridLines: {
          color: 'rgba(0,0,0,0.1)',
          offsetGridLines: true
        },
        ticks: {
          autoSkip: false,
          min: 0,
          minor: {
            fontColor: 'rgba(0,0,0,0.6)'
          },
          source: 'auto',
          callback: value => {
            return value; // formatowanie jednostek wykresu
          }
        },
        display: true,
        scaleLabel: {
          display: true,
        },
        elements: {
          line: {
            tension: 0 // wyłączenie bezier curves
          },
          point: {
            radius: 1 // wyłączenie punktów pomiarowych - zamula strasznie
          }
        },
        type: 'time',
        offset: true,
        time: {
          stepSize: 1, // kroki na osi X, domyślnie 7 dni.
          unit: 'day',
          displayFormats: {
            day: 'MMM DD'
          }
        },
        position: 'center',
        callback: value => {
          return moment(value).utc().format('YYYY-MM-DD');
        }
      }],
      yAxes: [{
        gridLines: {
          color: 'rgba(0,0,0,0.1)',
          offsetGridLines: false
        },
        ticks: {
          minor: {
            fontColor: 'rgba(0,0,0,0.6)',
          },
          callback: value => {
            return this.formatYAxis(value, this.chartType); // formatowanie jednostek wykresu
          }
        }
      }]
    },
    tooltips: {
      displayColors: false,
      backgroundColor: 'rgba(255,255,255)',
      titleFontColor: '#000',
      bodyFontColor: '#000',
      borderColor: '#BCBCBC',
      borderWidth: 1,
      xPadding: 10,
      yPadding: 10,
      titleFontSize: 11,
      bodyFontSize: 11,
      callbacks: {
        title: () => {},
        beforeLabel: (tooltipItem, data) => {
          if (data.datasets[tooltipItem.datasetIndex].type === 'bubble') {
            return 'Realised Data';
          } else {
            const run_date = moment(data.datasets[tooltipItem.datasetIndex].runDate).format('HH');
            const model = data.datasets[tooltipItem.datasetIndex].label;
            return `${model} ${run_date}`;
          }
        },
        label: (tooltipItem, data) => {
          if (data.datasets[tooltipItem.datasetIndex].type === 'bubble') {
            const value = parseFloat(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].y).toFixed(2);
            const date = moment(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].x).format('YYYY-MM-DD');
            return `date: ${date}; value: ${value}`;
          } else {
            const min = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].min;
            const max = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].max;
            const median = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].median;
            const q1 = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].q1;
            const q3 = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].q3;
            return [`min: ${min}; q1: ${q1}; median: ${median}; q3: ${q3}; max: ${max}`]
          }
        }
      }
    }
  };

  readonly chartType: string = '';

  constructor(
    chartData: ChartDataBox[] | ChartDataBox,
    type: string
  ) {
    Chart.defaults.global.defaultFontFamily = ' Overpass';
    Chart.defaults.global.defaultFontSize = 10;

    if (Array.isArray(chartData)) {
      chartData.forEach((chartDataItem: ChartData, index) => {
        this.createDataSet(chartDataItem);
      });
    } else {
      this.createDataSet(chartData);
    }
    this.chartType = type;
  }

  /**
   * Creates new datasets
   *
   * @param  {BoxDataDb[]} seriesDb
   * @param  {string} name
   * @returns void
   */
  createDataSet(chartData: ChartData): void {
    const labels: string[] = [];

    chartData.series.forEach(singleSeries => {
      for (const key in singleSeries) {
        if (key !== 'day') {
          singleSeries[key] = parseFloat(singleSeries[key]);
        }
      }
      labels.push(moment(singleSeries.day).format('MMM DD'));
    });

    const datasetColors: BackgroundColor = this.checkColor(chartData.model);
    const dataset = new DataSets(chartData.series, chartData.run_date, chartData.model, datasetColors);
    this.data.datasets.push(dataset);
  }

  /**
   * Checks dataseries color based on model
   *
   * @param  {string} model
   * @returns string
   */
  checkColor(name: string): BackgroundColor {
    if (name.toLowerCase().includes('gfs')) {
      return {
        main: '#c1342c',
        opac: 'rgba(193, 52, 44, 0.8)'
      };
    } else if (name.toLowerCase().includes('hres')) {
      return {
        main: '#195f9a',
        opac: 'rgba(25, 95, 154, 0.8)'
      };
    } else {
      return {
        main: '#195f9a',
        opac: 'rgba(25, 95, 154, 0.8)'
      };
    }
  }

  /**
   * Przełączanie danych na po naciśnięciu przycisku
   *
   * @param  {string} key
   * @returns void
   */
  toggle(key: string): void {
    this.data.datasets.forEach(dataset => {
      dataset.hidden = (dataset.label === key) ? !dataset.hidden : dataset.hidden;
    });
  }

  /**
   * Formatuje jednostko osi Y w zależności od wykresu
   *
   * @param  {number} value
   * @param  {string} title
   * @returns string
   */
  formatYAxis(value: number, title: string, chartSubType?: string): string {
    // TODO uzupełnic o pozostałe casy
    const deg = '\u00b0C';
    const eur = '\u20AC';
    switch (title) {
      case 'Risk Management': {
        const formatedValue = this.formatNumber(value);
        return `${formatedValue}  €`;
      }
      default: {
        const formatedValue = this.formatNumber(value);
        return `${formatedValue}`;
      }
    }
  }

  /**
   * Formatowanie wartości osi Y uwzględniając przedrostki SI
   *
   * @param  {number} value
   * @returns number
   */
  formatNumber(value: number): number | string {
    const range = { divider: 1000, suffix: 'k' };
    if (value >= range.divider) {
      return `${(value / range.divider)}${range.suffix} `
    }
    return value;
  }
}
