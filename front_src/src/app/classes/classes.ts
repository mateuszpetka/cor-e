import { PhoneNumber } from './classes';
import { UserFormService } from './../components/userform/userform.service';

import * as moment from 'moment';
import { Chart } from 'chart.js';
import { ChartData } from './chart';

export class ChartQueryParams {
  iso: string = 'fr';
  model: string = 'hres';
  days: number = 1;
  offset: number = 0;
  subModel?: number;
  runDate?: string = null;
  dateFrom?: string = moment().add(1, 'days').format('YYYY-MM-DD'); // default value - today
  dateTo?: string = moment().add(30, 'days').format('YYYY-MM-DD'); // default value - today

  constructor(offset?: number, runDate?: string, iso?: string) {
    this.offset = !!offset ? offset : this.offset;
    this.runDate = !!runDate ? runDate : null;
    this.iso = iso;
  }
}

export interface AvailabilityTableData {
  installed: string;
  remaining: string;
  start_date: string;
  end_date: string;
  type_outage_planned: string;
  unit_name: string;
}

export class Country {
  name: string;
  iso: string;
  phonePrefix?: string;

  constructor(name: string, iso: string, phonePrefix?: string) {
    this.name = name;
    this.iso = iso;
    this.phonePrefix = phonePrefix;
  }
}

export interface Kind {
  kind: string;
}

export interface CountryDb {
  countryName: string;
  countryIsoCode: string;
}

export interface CountryWithTitle {
  country: Country;
  title: string;
}

export interface LoggedUserInitialData {
  id: number;
  cmsAccess: boolean;
  groupId: number;
  email: string;
  languageId: number;
  name: string;
  login: string;
  token: string;
  customFields?: CustomFields;
  avatar?: string;
}

export class CustomFields {
  firstName?: string;
  lastName?: string;
  street?: string;
  zipCode?: string;
  city?: string;
  countryIso?: string;
  phoneNumber?: string;
  filterModels?: string;
  filterCountries?: string;
  themeDisplay?: string;
}

export interface PhoneNumber {
  prefix: string;
  number: number;
}

export class PatchedData {
  email: string;
  firstName?: string;
  lastName?: string;
  street?: string;
  zipCode?: string;
  city?: string;
  countryIso?: string;
  phoneNumber?: string;
  phonePrefix?: string;
  avatar?: string;
  token?: string;
  themeDisplay?: string;

  constructor(data: any, private _service: UserFormService) {
    data = Object.assign({}, ...data); // convert Array -> Object
    const searchedCountry: Country = this._service.searchCountryByName(data.country); // find country iso code
    this.email = !!data.email ? data.email : null;
    this.firstName = data.firstName;
    this.lastName = data.lastName;
    this.street = data.street;
    this.zipCode = data.zipCode;
    this.city = data.city;
    this.countryIso = searchedCountry ? searchedCountry.iso : '';
    this.phoneNumber = data.phoneNumber;
    this.phonePrefix = data.phonePrefix;
    this.themeDisplay = data.themeDisplay;
  }
}

export class Model {
  modelName: string;
  modelDisplayedName: string;

  constructor(name: string) {
    this.modelName = name;
    this.modelDisplayedName = name.toLowerCase() === 'gfs' ? 'gfs op' : 'ec op';
  }
}

export class DashboardLayouts {
  id: number;
  status: number;
  layoutImage: string;
  layoutName: string;
}

export interface RunDates {
  run_dates: string[];
  index: number;
}

export class BtnLabel {
  name: string;
  value: string;

  constructor(name: string) {
    this.name = name.toLocaleUpperCase();
    this.value = name;
  }
}

export interface TableData {
  model: string;
  country: string;
  data: any[];
}

export interface MergedTableData {
  model: string;
  country: string;
  series: any[];
}

export interface DataSeries {
  name: string;
  data: any[];
}

export interface CsvData {
  models: string[];
  tables: string[];
  countries: string[];
}

export class Params {
  country: string;
  model: string;
  runDate: string;
  offset: number;

  constructor(country, model, offset, runDate) {
    this.country = country;
    this.model = model;
    this.runDate = runDate;
    this.offset = offset;
  }
}


export class ChartState {
  last: boolean = null;
  prev: boolean = null;
  runDate: string = null;
  viewFullRange: boolean = null;
  country: string = null;
  kind?: string = null;

  constructor(last: boolean, prev: boolean, runDate: string, viewFullRange: boolean, country: string, kind?: string) {
    this.last = last;
    this.prev = prev;
    this.runDate = runDate;
    this.viewFullRange = viewFullRange;
    this.country = country;
    this.kind = kind;
  }
}

export class ChartAvailabilityOrPricesState extends ChartState {
  currentDay: string;
  kinds: string[];
  selectedOffset: number;

  constructor(country: string, currentDay: string, selectedOffset: number, kind?: string, runDate?: string) {
    super(null, null, runDate, false, country, kind);
    this.currentDay = currentDay;
    this.selectedOffset = selectedOffset;
  }
}

export class TableState {
  hourly: boolean = null;
  kinds: string[] = null;

  constructor(hourly: boolean, kinds: string[]) {
    this.hourly = hourly;
    this.kinds = kinds;
  }
}

export interface MapPath {
  file_path: string;
  model: string;
}

export class ChartDisplay {
  name: string;
  settings: any;

  constructor(name: string, settings: any) {
    this.name = name;
    this.settings = settings;
  }
}

export interface BrowserSettings {
  currentBrowser: string;
  majorBrowserVersion: number;
}

export interface RunDatesResponse {
  runDates: string[];
  kind: string;
  model: string;
}

export interface PricesDates {
  start: string | Date | moment.Moment;
  end: string | Date | moment.Moment;
  firstDay: string | Date | moment.Moment;
  lastDay: string | Date | moment.Moment;
  startFormatted: string;
  endFormatted: string;
  showBeforeArrow: boolean;
  showNextArrow: boolean;
}