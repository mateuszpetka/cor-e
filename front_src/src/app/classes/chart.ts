import * as d3 from 'd3';
import * as moment from 'moment';

// Interfajce danych pobranych z bazy
export interface ChartDbData {
  status: boolean;
  data: ChartData;
}

// Interfejs przedstawiający dane wykresu - typ + seria pomiarowa
export interface ChartData {
  name: string;
  series: DbDataSeries[];
  model: string;
  runDate?: string;
  offset?: number;
  kind?: string;
}

// Interfejs odpowiadająca za serie pomiarową z bazy
export interface DbDataSeries {
  name: string;
  value: any;
  flow_date?: string;
}

// Interfejs od RunDate, dla odróżnienia modeli dane w gfs i hres
export interface ModelRunDates {
  gfs?: string[];
  hres?: string[];
}

// Klasa odpowiadająca za serie pomiarową do wykresów
export class DataSeries {
  x: string = null;
  y: any = null;
}

// Klasa odpowiadająca za serie pomiarową wykresu - dame x,y + label + dodatkowe opcje jak wypełnienie, czy grubość linii
export class DataSets {
  public label: string = '';
  public data: DataSeries[] = [];
  public fill: any = false;
  public borderColor: string;
  public backgroundColor?: string = '';
  public borderWidth: number = 2;
  public hidden: boolean = false;

  public runDate: string = '';
  public model: string = '';
  public offset: number = 0; // domyślny offset 0 - last
  public kind: string = '';
  public asOf: boolean;

  public pointRadius: number;
  public pointStyle: string;
  public pointBorderColor: string;
  public pointBorderWidth: number;
  public pointHoverRadius: number;

  constructor(label: string, data: DbDataSeries[], model: string, kind?: string, runDate?: string,
      offset?: number, color?, asOf?: boolean) {
    this.label = label;
    this.model = this.renameModel(model);
    this.kind = kind;
    this.asOf = asOf;
    this.runDate = runDate;
    this.offset = offset;
    this.borderColor = color;
    this.data = this.convertData(data);
  }

  /**
   * Renames models name
   *
   * @param  {string} model
   * @returns string
   */
  renameModel(model: string): string {
    switch (model) {
      case 'gfs':
        return 'GFS OP';
      case 'hres':
        return'EC OP';
      default:
        return model;
    }
  }

  /**
   * Konwertuje dane z bazy do formatu
   *
   * @param  {DbDataSeries[]} data
   * @returns DataSeries
   */
  convertData(data: DbDataSeries[]): DataSeries[] {
    return data.map(tempData => {
      return ({
        x: tempData.name,
        y: parseFloat(tempData.value).toFixed(2)
      });
    });
  }

  /**
   * Ustawianie podanych w obiekcie styli
   *
   * @param  {} styles
   * @returns void
   */
  setStyles(styles): void {
    for (const style in styles) {
      if (styles.hasOwnProperty(style)) {
        this[style] = styles[style];
      }
    }
  }
}

// Interfejs głównych opcji wykresy - typ np. Renewables, kraj, typ danych (wind, solar itp. TYMCZASOWO), dni
export class ChartDataParams {
  chartType?: string;
  country: string;
  dataType: string;
  days?: number;
}

// Klasa podstawowych opcji wykresu liniowego
export class ChartCustomOption {
  type: string = 'line';
  data = {
    labels: [],
    datasets: [],
  };
  options = {
    maintainAspectRatio: false,
    responsive:  false,
    scaleShowValues: true,
    legend: false,
    animation: {
      duration: 0,
      onAnimationProgress: () => {
        'easeOutCirc'
      }
    },
    title: {
      display: false,
      text: '',
      position: 'top',
      align: 'left',
      fontSize: 11
    },
    scales: {
      xAxes: [{
        offset: false,
        gridLines: {
          color: 'rgba(0,0,0,0.1)',
          zeroLineColor: 'rgba(0,0,0,0.1)'
        },
        type:  'time',
        stacked: false,
        time: {
          stepSize: 1, // kroki na osi X, domyślnie 7 dni.
          unit: 'hour',
          displayFormats: {
            day: 'MMM DD HH'
          }
        },
        display: true,
        ticks: {
          autoSkip: false,
          min: 0,
          minor: {
            fontColor : 'rgba(0,0,0,0.6)'
          },
          callback: value => {
            return this.formatXAxis(value, this.chartType);
          }
        },
        scaleLabel: {
          display: true,
        },
        callback: value => {
          return  moment(value).utc().format('YYYY-MM-DD HH:mm');
        }
      }],
      yAxes: [{
        offset: false,
        gridLines: {
          color: 'rgba(0,0,0,0.1)',
          zeroLineColor: 'rgba(0,0,0,0.1)'
        },
        stacked: false,
        ticks: {
          // beginAtZero: true,
          minor: {
            fontColor : 'rgba(0,0,0,0.6)',
          },
          callback: value => {
            return this.formatYAxis(value, this.chartType); // formatowanie jednostek wykresu
          }
        },
      }],
      pointLabels: {
        fontStyle: 'bold',
        fontSize: 12
      }
    },
    hover: {
      mode: 'x',
      animationDuration: 0
    },
    elements: {
      line: {
        tension: 0 // wyłączenie bezier curves
      },
      point: {
        radius: 1 // wyłączenie punktów pomiarowych - zamula strasznie
      }
    },
    responsiveAnimationDuration: 0,
    tooltips: {
      displayColors: false,
      backgroundColor: 'rgb(255,255,255)',
      titleFontColor: '#000',
      titleFontStyle: 'normal',
      bodyFontColor: '#000',
      borderColor: '#BCBCBC',
      borderWidth: 1,
      xPadding: 10,
      yPadding: 10,
      titleFontSize: 11,
      bodyFontSize: 11,
      callbacks: {
        title: () => {},
        beforeLabel: (tooltipItem, data) => {
          let runDate: string;
          if (this.chartType === 'Availability') {
            runDate = moment(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].x).format('YYYY-MM-DD');
          } else {
            runDate = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].x;
          }
          return runDate;
        },
        label: (tooltipItem, data) => {
          const label = data.datasets[tooltipItem.datasetIndex].label;
          const run_date = moment(data.datasets[tooltipItem.datasetIndex].runDate).format('HH');
          const model = !!data.datasets[tooltipItem.datasetIndex].model ? data.datasets[tooltipItem.datasetIndex].model : '';
          const kind = data.datasets[tooltipItem.datasetIndex].kind;
          const value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].y;
          let legend: string;

          if (!!model) {
            legend = `${model} ${run_date} ${value}`;
          } else {
            legend = `${kind} ${value}`;
          }

          if (kind === 'Isotherm') {
            legend = `${label} ${model} ${run_date} ${value}`;
          }

          if (kind === 'Run of River') {
            legend = `${kind} ${run_date} ${value}`;
          }

          if (kind === 'Temperature' && model === '') {
            legend = `Normal Temperature ${value}`;
          }

          return [legend];
        },
      }
    }
  };

  private chartType: string = '';
  public chartSubType: string;

  constructor(chartsData: ChartData[] | ChartData, type: string, colors?: string[] | string) {
    if (Array.isArray(chartsData)) {
      chartsData.forEach((chartData: ChartData, index) => {
        const tempColor = '#195f9a';
        const dataset = new DataSets(chartData.name, chartData.series, chartData.model, chartData.kind,
          chartData.runDate, 0, tempColor);
        this.data.datasets.push(dataset);
      });
    } else {
      const dataset = new DataSets(chartsData.name, chartsData.series, chartsData.model, chartsData.kind, chartsData.runDate, 0, colors);
      this.data.datasets.push(dataset);
    }
    this.chartType = type;
  }

  /**
  * Przełączanie danych po naciśnięciu przycisku
  *
  * @param  {string} key
  * @returns void
  */
   toggle(key: string): void {
    this.chartSubType = key;
     this.data.datasets.forEach(dataset => {
        dataset.hidden = !!(dataset.label.includes(key)) ? !dataset.hidden : true;
     });
   }

  /**
  * Przełączanie danych na po naciśnięciu przycisku
  *
  * @param  {string} key
  * @returns void
  */
   toggleAvaibilityData(key: string): void {
     this.data.datasets.forEach(dataset => {
        dataset.hidden = !!(dataset.label === key) ? !dataset.hidden : dataset.hidden;
      });
   }

  /**
   * Formatuje jednostko osi Y w zależności od wykresu
   *
   * @param  {number} value
   * @param  {string} title
   * @param  {string} chartSubType
   * @returns string
   */
  formatYAxis(value: number, title: string, chartSubType?: string): string {
    // TODO uzupełnic o pozostałe casy
    const deg = '\u00b0C';
    const eur = '\u20AC';
    switch (title) {
      case 'Weather': {
        const formatedValue = this.formatNumber(value);
        return this.chartSubType === 'Temperature' || this.chartSubType === 'Calculated Difference' ?
          `${formatedValue} ${deg}` : `${formatedValue} m`;
      }
      case 'Availability': {
        const formatedValue = this.formatNumber(value);
        return `${formatedValue} MW `;
      }
      case 'Prices': {
        const formatedValue = this.formatNumber(value);
        return `${formatedValue} ${eur}`;
      }
      case 'Demand': {
        const formatedValue = this.formatNumber(value);
        return `${formatedValue} MW `;
      }
      case 'Renewables': {
        const formatedValue = this.formatNumber(value);
        return `${formatedValue} MW `;
      }
      case 'Hydro': {
        const formatedValue = this.formatNumber(value);
        return `${formatedValue}  MW`;
      }
      default: {
        const formatedValue = this.formatNumber(value);
        return `${formatedValue}`;
      }
    }
  }

  /**
   * Formatuje jednostki osi X
   *
   * @param  {number} value
   * @param  {string} title
   * @returns string
   */
  formatXAxis(value: number, title?: string): string {
    const formatedValue: string = this.formatNumber(value).toString().replace('-',' ');
    return formatedValue;
  }

  /**
   * Formatowanie wartości osi Y uwzględniając przedrostki SI
   *
   * @param  {number} value
   * @returns number
   */
  formatNumber(value: number): number | string {
    const range = { divider: 1000, suffix: 'k' };
    if (value >= range.divider) {
      return `${(value / range.divider)}${range.suffix}`;
    }
    return value;
  }
}
