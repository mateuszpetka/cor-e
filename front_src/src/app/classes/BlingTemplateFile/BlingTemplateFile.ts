
export class BlingTemplateFile {
  public id?: number;
  public templateId?: number;
  public template?: any;
  public fileId?: number;
  public file?: any;
  public position?: number;


  constructor(objectData) {
    Object.assign(this, objectData);
  }
}
