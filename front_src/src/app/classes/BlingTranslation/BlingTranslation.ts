
export class BlingTranslation {
  public id?: number;
  public languageId?: number;
  public language?: any;
  public translationKey?: string;
  public translation?: string;
  public description?: string;


  constructor(objectData) {
    Object.assign(this, objectData);
  }
}
