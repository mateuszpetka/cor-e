
export interface BlingTemplateFile {
  id?: number;
  templateId?: number;
  template?: any;
  fileId?: number;
  file?: any;
  position?: number;

}
