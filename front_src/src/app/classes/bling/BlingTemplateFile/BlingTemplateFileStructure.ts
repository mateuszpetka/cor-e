export const BlingTemplateFileStructure = Object.freeze({
  id: 'BlingTemplateFile.`id`',
  templateName: 'BlingTemplate.`name`',
  templateDisplayName: 'BlingTemplate.`displayName`',
  templateDescription: 'BlingTemplate.`description`',
  templateTags: 'BlingTemplate.`tags`',
  templateHtmlContent: 'BlingTemplate.`htmlContent`',
  templateKind: 'BlingTemplate.`kind`',
  templateParentId: 'BlingTemplate.`parentId`',
  templateSubject: 'BlingTemplate.`subject`',
  position: 'BlingTemplateFile.`position`',

});
