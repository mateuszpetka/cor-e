import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { BlingTemplateFile } from './BlingTemplateFile';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class BlingTemplateFileService<T> extends Service<BlingTemplateFile> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('BlingTemplateFile', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
