
export interface BlingUser {
  id?: number;
  hydroState?: string;
  pricesState?: string;
  availabilityState?: string;
  tableState?: string;
  countryIso?: string;
  phonePrefix?: string;
  themeDisplay?: string;
  weatherState?: string;
  demandState?: string;
  street?: string;
  zipCode?: string;
  city?: string;
  phoneNumber?: string;
  filterCountries?: string;
  filterModels?: string;
  lastName?: string;
  firstName?: string;
  renewablesState?: string;
  activeJwt?: string;

}
