
export interface BlingRoute {
  id?: number;
  name?: string;
  path?: string;
  icon?: string;
  title?: string;
  appId?: number;
  app?: any;
  parentId?: number;
  parent?: any;
  position?: number;
  hasLanguagePrefix?: number;
  params?: object;

}
