import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { BlingRoute } from './BlingRoute';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class BlingRouteService<T> extends Service<BlingRoute> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('BlingRoute', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
