export const BlingLanguageStructure = Object.freeze({
  id: 'BlingLanguage.`id`',
  code: 'BlingLanguage.`code`',
  active: 'BlingLanguage.`active`',
  visible: 'BlingLanguage.`visible`',
  isDefault: 'BlingLanguage.`isDefault`',
  position: 'BlingLanguage.`position`',

});
