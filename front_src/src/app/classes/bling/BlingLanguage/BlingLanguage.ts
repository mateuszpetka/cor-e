
export interface BlingLanguage {
  id?: number;
  appId?: number;
  app?: any;
  code?: string;
  active?: number;
  visible?: number;
  isDefault?: number;
  position?: number;

}
