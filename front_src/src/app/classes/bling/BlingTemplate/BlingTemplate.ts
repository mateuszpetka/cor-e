
export interface BlingTemplate {
  id?: number;
  name?: string;
  displayName?: string;
  description?: string;
  tags?: object;
  htmlContent?: string;
  kind?: string;
  parentId?: number;
  parent?: any;
  subject?: string;

}
