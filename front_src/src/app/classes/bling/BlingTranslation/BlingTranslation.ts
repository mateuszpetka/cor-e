
export interface BlingTranslation {
  id?: number;
  languageId?: number;
  language?: any;
  translationKey?: string;
  translation?: string;
  description?: string;

}
