
export class BlingLanguage {
  public id?: number;
  public appId?: number;
  public app?: any;
  public code?: string;
  public active?: number;
  public visible?: number;
  public isDefault?: number;
  public position?: number;


  constructor(objectData) {
    Object.assign(this, objectData);
  }
}
