
export class BlingUser {
  public id?: number;
  public hydroState?: string;
  public pricesState?: string;
  public availabilityState?: string;
  public tableState?: string;
  public countryIso?: string;
  public phonePrefix?: string;
  public themeDisplay?: string;
  public weatherState?: string;
  public demandState?: string;
  public street?: string;
  public zipCode?: string;
  public city?: string;
  public phoneNumber?: string;
  public filterCountries?: string;
  public filterModels?: string;
  public lastName?: string;
  public firstName?: string;
  public renewablesState?: string;
  public activeJwt?: string;


  constructor(objectData) {
    Object.assign(this, objectData);
  }
}
