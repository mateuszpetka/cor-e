import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


import { HttpService } from '../../http.service';
import { QueryParams } from '../QueryParams';

import { BlingUser } from './BlingUser';
import { BlingUserStructure } from './BlingUserStructure';

// USER-CODE-O
// USER-CODE-C

interface LimitedResults {
  readonly totalCount: number;
  readonly items: Array<BlingUser>;
}

@Injectable()
export class BlingUserService {

  constructor(
    private httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {}

  /**
   * Wysłanie zapytania o pobranie danych
   *
   * @param {string} lang
   * @param {QueryParams} [params]
   * @returns {Observable<Array<BlingUser> & LimitedResults>}
   */
  public get(lang: string = 'pl', params?: QueryParams): Observable<Array<BlingUser> & LimitedResults> {
    const queryOptions = {
      search: params ? params.getParams() : {}
    };
    return this.httpService.get(lang + '/BlingUser', queryOptions);
  }

  /**
   * Wysłanie zapytania o pobranie danych dla jednego rekordu po ID
   *
   * @param {number} id
   * @param {string} lang
   * @param {boolean} join
   * @param {boolean} withPrevNext
   * @returns {Observable<BlingUser>}
   */
  public getOne(id: number, lang: string = 'pl', join: boolean = false, withPrevNext: boolean = false): Observable<BlingUser> {
    const params = new QueryParams();

    params.join(join);
    params.withPrevNext(withPrevNext);

    const queryOptions = {
      search: params.getParams()
    };

    return this.httpService.get(lang + '/BlingUser/' + id, queryOptions);
  }


  // USER-CODE-O
  // USER-CODE-C
}

