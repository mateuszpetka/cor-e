import { pickBy } from 'lodash';

export class QueryParams {
  private whereElements: Object = {};
  private orderValue: Array<Object> = [];
  private orderValueObject: Object = {};
  private orderRandValue: boolean;
  private offsetValue: number;
  private limitValue: number;
  private joinValue: boolean;
  private prevNextValue: boolean;
  private prevNextFieldsValue: Array<string> = [];
  private prevNextFieldsValueObject: Object = {};
  private withAllLangSlugsValue: boolean;

  constructor() { }

  /**
   * Warunki do pobrania danych
   *
   * @param {string} column
   * @param {any} parameters
   */
  public where(column: string, parameters: any) {
    this.whereElements[column] = parameters;
  }

  /**
   * Określenie kolumn po których sortować dane wraz z kierunkiem sortowania (DESC/ASC)
   *
   * @param {string} column
   * @param direction
   */
  public order(column: string, direction: string = 'DESC') {
    this.orderValue.push({ field: column, dir: direction });
  }

  /**
   * Określenie, czy rekordy mają zostać posortowane losowo.
   *
   * @param {boolean} orderRand
   */
  public orderRand(orderRand: boolean = false) {
    this.orderRandValue = orderRand;
  }

  /**
   * Określenie przesunięcia wyników
   *
   * @param {number} value
   */
  public offset(value: number) {
    this.offsetValue = value;
  }

  /**
   * Określenie liczby wyników do pobrania
   *
   * @param {number} value
   */
  public limit(value: number) {
    this.limitValue = value;
  }

  /**
   * Ustawienia złączenia
   *
   * @param {boolean} value
   */
  public join(value: boolean) {
    this.joinValue = value;
  }

  /**
   * Ustawienia pobrania ID poprzedniego i kolejnego rekordu
   *
   * @param {boolean} value
   */
  public withPrevNext(value: boolean) {
    this.prevNextValue = value;
  }

  /**
   * Określenie, które pola mają zostać pobrane podczas dołączenia powiązanego rekordu
   *
   * @param fields
   */
  public prevNextFields(fields: string[]) {
    fields.forEach( field => {
      this.prevNextFieldsValue.push(field);
    })
  }

  /**
   * Ustawienie pobrania slugów dla wszystkich języków
   *
   * @param {boolean} value
   */
  public withAllLangSlugs(value: boolean) {
    this.withAllLangSlugsValue = value;
  }

  /**
   * Sparsowanie dowolnego obiektu lub tablicy do query stringa
   *
   * @param  {Array<any>|Object} data
   * @param  {any} prefix=undefined
   * @returns string
   */
  public toQueryString(data: Array<any> | Object, prefix: any = undefined): string {
    let str = [], p;

    for (p in data) {
      if (data[p]) {
        if (data.hasOwnProperty(p)) {
          const k = prefix
            ? prefix + '[' + p + ']'
            : p;
            const v = data[p];

          str.push((v !== null && typeof v === 'object') ? this.toQueryString(v, k) : k + '=' + v);
        }
      } else {
        continue;
      }
    }

    str = str.filter(value => value !== '');

    return str.join('&');
  }

  /**
   * Sparsowanie parametrów z inputa lub routingu dla komponentu listy
   *
   * @param {any} urlData
   * @param {any} inputs
   * @returns {void}
   */
  public paramsForListComponent(urlData, ...inputs): void {
    let inputsValid = false;

    /**
     * this.where z paramsForListComponent(...)
     */
    if (inputs[0]) {
      for (const name in inputs[0]) {
        if (inputs[0].hasOwnProperty(name)) {
          this.where(name, inputs[0][name]);
        }
      }

      inputsValid = true;
    }

    /**
     * this.order z paramsForListComponent(...)
     */
    if (typeof inputs[1] === 'object') {
      inputs[1].forEach( ([column, dir]) => {
        this.order(column, dir);
      });
    }

    /**
     * this.offset z paramsForListComponent(...)
     */
    if (inputs[2] > 0) {
      this.offset(inputs[2]);
      inputsValid = true;
    }

    /**
     * this.limit z paramsForListComponent(...)
     */
    if (inputs[3] > 0) {
      this.limit(inputs[3]);
      inputsValid = true;
    }

    /**
     * this.join z paramsForListComponent(...)
     */
    if (typeof inputs[4] === 'boolean') {
      this.join(inputs[4]);
      inputsValid = true;
    }

    /**
     * this.random z paramsForListComponent(...)
     */
    if (typeof inputs[5] === 'boolean') {
      this.orderRand(inputs[5]);
      inputsValid = true;
    }

    // pobranie parametrów z routingu
    if (inputsValid === false && urlData !== null) {
      if (urlData.where) {
        for (const name in urlData.where) {
          if (urlData.where.hasOwnProperty(name)) {
            this.where(name, urlData.where[name]);
          }
        }
      }

      if (urlData.offset > 0) {
        this.offset(urlData.offset);
      }

      if (urlData.limit > 0) {
        this.limit(urlData.limit);
      }
    }

    return;
  }

  /**
   * Parsowanie tablicy do queryString
   *
   * @param {string} queryKey
   * @param elements
   * @param prefix
   * @returns {{}}
   */
  public parseArrayToQuery(queryKey: string, elements: any, prefix?) {
    let obj = {};

    Object.keys(elements).forEach( key => {
      const k = prefix ? `${prefix}[${key}]` : `[${key}]`, v = elements[key];

      if (typeof v === 'object' && v !== null) {
        obj = {...obj, ...this.parseArrayToQuery(queryKey, v, k)};
      } else {
        obj[`${queryKey}${k}`] = v;
      }
    });

    return obj;

  }

  /**
   * Pobranie wszystkich parametrów zapytania jako obiekt gotowy do przekazania w zapytaniu.
   *
   * @returns {any}
   */
  public getParams(): any {

    if (!!Object.keys(this.whereElements).length) {
      this.whereElements = this.parseArrayToQuery('where', this.whereElements);
    }

    if (!!Object.keys(this.prevNextFieldsValue).length) {
      this.prevNextFieldsValueObject = this.parseArrayToQuery('prevNextFields', this.prevNextFieldsValue);
    }

    if (!!Object.keys(this.orderValue).length) {
      this.orderValueObject = this.parseArrayToQuery('order', this.orderValue);
    }

    const data = {
      ...this.orderValueObject,
      orderRand: this.orderRandValue,
      offset: this.offsetValue,
      limit: this.limitValue,
      withJoin: this.joinValue,
      withPrevNext: this.prevNextValue,
      ...this.prevNextFieldsValueObject,
      withAllLangSlugs: this.withAllLangSlugsValue,
      ...this.whereElements
    };

    return pickBy(data, value => Array.isArray(value) ? !!value.length : (!!value || value === 0));
  }
}
