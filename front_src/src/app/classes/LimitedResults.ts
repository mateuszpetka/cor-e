export interface LimitedResults<T> {
  readonly totalCount: number;
  readonly items: T[];
}
