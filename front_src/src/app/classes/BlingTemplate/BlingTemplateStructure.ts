export const BlingTemplateStructure = Object.freeze({
  id: 'BlingTemplate.`id`',
  name: 'BlingTemplate.`name`',
  displayName: 'BlingTemplate.`displayName`',
  description: 'BlingTemplate.`description`',
  tags: 'BlingTemplate.`tags`',
  htmlContent: 'BlingTemplate.`htmlContent`',
  kind: 'BlingTemplate.`kind`',
  parentName: 'BlingTemplate.`name`',
  parentDisplayName: 'BlingTemplate.`displayName`',
  parentDescription: 'BlingTemplate.`description`',
  parentTags: 'BlingTemplate.`tags`',
  parentHtmlContent: 'BlingTemplate.`htmlContent`',
  parentKind: 'BlingTemplate.`kind`',
  parentParentId: 'BlingTemplate.`parentId`',
  parentSubject: 'BlingTemplate.`subject`',
  subject: 'BlingTemplate.`subject`',

});
