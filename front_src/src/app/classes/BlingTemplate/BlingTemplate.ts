
export class BlingTemplate {
  public id?: number;
  public name?: string;
  public displayName?: string;
  public description?: string;
  public tags?: object;
  public htmlContent?: string;
  public kind?: string;
  public parentId?: number;
  public parent?: any;
  public subject?: string;


  constructor(objectData) {
    Object.assign(this, objectData);
  }
}
