
import {map} from 'rxjs/operators';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { CarouselComponent, SlickCarouselItem } from './tools/carousel.component';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routes';
import { AppService } from './app.service';
import { AppSettings } from './app.settings';
import { PipesModule } from './tools/pipes.module';
import { HttpService } from './http.service';
import { NavigationComponent } from './components/navigation/navigation.component';
import { PreviewComponent } from './preview.component';
import { TranslationService } from './translation.service';

import { UserprofileComponent } from './containers/userprofile/userprofile.component';
import { LogoutComponent } from './containers/logout/logout.component';
import { ChartlistComponent } from './containers/chartlist/chartlist.component';
import { LoginComponent } from './containers/login/login.component';

import { BlingLanguageItemComponent } from './components/bling/blinglanguage-item/blinglanguage-item.component';
import { BlingLanguageListComponent } from './components/bling/blinglanguage-list/blinglanguage-list.component';
import { BlingTranslationItemComponent } from './components/bling/blingtranslation-item/blingtranslation-item.component';
import { BlingTranslationListComponent } from './components/bling/blingtranslation-list/blingtranslation-list.component';
import { BlingUserItemComponent } from './components/bling/blinguser-item/blinguser-item.component';
import { BlingUserListComponent } from './components/bling/blinguser-list/blinguser-list.component';
import { BlingTemplateItemComponent } from './components/bling/blingtemplate-item/blingtemplate-item.component';
import { BlingTemplateListComponent } from './components/bling/blingtemplate-list/blingtemplate-list.component';
import { BlingTemplateFileItemComponent } from './components/bling/blingtemplatefile-item/blingtemplatefile-item.component';
import { BlingTemplateFileListComponent } from './components/bling/blingtemplatefile-list/blingtemplatefile-list.component';


// USER-CODE-O
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';
import { MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatTableModule } from '@angular/material';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { ChartService } from './components/chart/chart.service';
import { ChartComponent } from './components/chart/chart.component';
import { SampleComponent } from './components/sample/sample.component';

import { AuthService } from './components/auth/auth.service';
import { TokenService } from './components/auth/token.service';
import { RecaptchaDirective } from './components/auth/recaptcha.directive';
import { ChartButtonComponent } from './components/chart/chart-button/chart-button.component';
import { ChartMenuComponent } from './components/chart/chart-menu/chart-menu.component';
import { ChartBoxplotComponent } from './components/chart-boxplot/chart-boxplot.component';
import { UserformComponent } from './components/userform/userform.component';
import { UserFormService } from './components/userform/userform.service';
import { ChartListHttpService } from './containers/chartlist/chartlist-http.service'
import { CustomHttpService } from './shared/customHttp.service';
import { GlobalFiltersComponent } from './components/globalfilters/globalfilters.component';
import { SharedService } from './shared/shared.service';
import { HighlightDirective } from './shared/highlight.directive';
import { ClickOutsideDirective } from './shared/clickOutsite.directive';
import { BannerComponent } from './components/banner/banner.component';
import { ChartAvailabilityComponent } from './components/chart-availability/chart-availability.component';
import { ChartWeatherComponent } from './components/chart-weather/chart-weather.component';
import { ChartWeatherService } from './components/chart-weather/chart-weather.service';
import { ChartPricesComponent } from './components/chart-prices/chart-prices.component';
import { ChartPricesService } from './components/chart-prices/chart-prices.service';
import { ChartDemandComponent } from './components/chart-demand/chart-demand.component';
import { ChartDemandService } from './components/chart-demand/chart-demand.service';
import { ChartHydroComponent } from './components/chart-hydro/chart-hydro.component';
import { ChartHydroService } from './components/chart-hydro/chart-hydro.service';
import { OwlCarouselComponent } from './tools/owl.carousel.component';
import { OwlCarouselItem } from './tools/owl.carousel.component';
import { DataTableComponent } from './components/data-table/data-table.component';
import { ChartAvailabilityService } from './components/chart-availability/chart-availability.service';
import { HourlyTableComponent } from './components/data-table/hourly-table/hourly-table.component';
import { RecoverPasswordComponent } from './components/recover-password/recover-password.component';
import { ChartsStateService } from './shared/chars-state.service';
import { UserFormSettingsComponent } from './components/userform/user-form-settings/user-form-settings.component';
import { UserSavedSettingsComponent } from './components/userform/user-saved-settings/user-saved-settings.component';
import { DialogComponent } from './containers/dialog/dialog.component';
import { BrowserStateService } from './shared/browser-state.service';
import { ChartListService } from './containers/chartlist/chartlist.service';
// USER-CODE-C

@NgModule({
  declarations: [
    AppComponent,
    PreviewComponent,
    NavigationComponent,
    CarouselComponent,
    SlickCarouselItem,
    UserprofileComponent,
    LogoutComponent,
    ChartlistComponent,
    LoginComponent,

    BlingLanguageItemComponent,
    BlingLanguageListComponent,
    BlingTranslationItemComponent,
    BlingTranslationListComponent,
    BlingUserItemComponent,
    BlingUserListComponent,
    BlingTemplateItemComponent,
    BlingTemplateListComponent,
    BlingTemplateFileItemComponent,
    BlingTemplateFileListComponent,

    // USER-CODE-O
    ChartComponent,
    LoginComponent,
    LogoutComponent,
    SampleComponent,
    RecaptchaDirective,
    ChartButtonComponent,
    ChartMenuComponent,
    ChartBoxplotComponent,
    UserformComponent,
    GlobalFiltersComponent,
    HighlightDirective,
    ClickOutsideDirective,
    BannerComponent,
    ChartAvailabilityComponent,
    ChartWeatherComponent,
    ChartPricesComponent,
    ChartDemandComponent,
    ChartHydroComponent,
    OwlCarouselComponent,
    OwlCarouselItem,
    DataTableComponent,
    HourlyTableComponent,
    RecoverPasswordComponent,
    UserFormSettingsComponent,
    UserSavedSettingsComponent,
    DialogComponent
    // USER-CODE-C
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(AppRoutes, { initialNavigation: false }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [AppService]
      }
    }),
    PipesModule,
    // USER-CODE-O
    MatChipsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatButtonToggleModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatDialogModule
    // USER-CODE-C
  ],
  providers: [
    AppSettings,
    AppService,
    HttpService,
    TranslationService,
    // USER-CODE-O
    ChartService,
    ChartDemandService,
    AuthService,
    TokenService,
    UserFormService,
    ChartListHttpService,
    CustomHttpService,
    SharedService,
    ChartHydroService,
    ChartWeatherService,
    ChartAvailabilityService,
    ChartPricesService,
    ChartsStateService,
    BrowserStateService,
    ChartListService
    // USER-CODE-C
  ],
  entryComponents: [
    // USER-CODE-O
    DialogComponent
    // USER-CODE-C
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor() {
    // if (localStorage.getItem('frontAuthToken') === null) {
      // const settings = new AppSettings();

      // localStorage.setItem('frontAuthToken', settings.authGuestToken);
    // }
  }
}

export class TranslationLoader implements TranslateLoader {

  constructor(private db: AppService) { }

  public getTranslation(lang): any {
    return this.db.getTranslations().pipe(map(res => res[lang])) as Observable<any>;
  }
}

export function HttpLoaderFactory(db: AppService) {
  return new TranslationLoader(db);
}

