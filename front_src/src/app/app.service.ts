import { AppSettings } from './app.settings';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { HttpService } from './http.service';

// USER-CODE-O
import {
  LoggedUserInitialData, ChartState, TableState, ChartAvailabilityOrPricesState
} from './classes/classes';
import {
  ChartsStateService
} from './shared/chars-state.service';
import {
  Observable
,  BehaviorSubject } from 'rxjs';
// USER-CODE-C

@Injectable()
export class AppService {
  // USER-CODE-O
  private authToken: string;
  private newThemeSrc: BehaviorSubject<string> = new BehaviorSubject('');
  public newTheme$ = this.newThemeSrc.asObservable();
  // USER-CODE-C
  constructor(
    private appSettings: AppSettings,
    private httpService: HttpService,
    // USER-CODE-O
    private _chartStateService: ChartsStateService,
    // USER-CODE-C
  ) {
    // USER-CODE-O
    // USER-CODE-C
  }

  public getTranslations() {
    const url = 'BlingTranslation';
    const queryOptions = {
      params: {
        'withJoin': 1
      }
    };

    return this.httpService.get(url, queryOptions).pipe(map(translations => {
      const parsedTranslations = [];

      translations.map(translation => {
        const keyValue = {[translation.translationKey]: translation.translation};
        const languageCode = this.appSettings['mapResponseJoins'] ? translation.language.code : translation.__language_code;

        if (parsedTranslations.hasOwnProperty(languageCode)) {
          parsedTranslations[languageCode] = {...parsedTranslations[languageCode], ...keyValue}
        } else {
          parsedTranslations[languageCode] = keyValue;
        }
      });

      return parsedTranslations;
    }));
  }

  // USER-CODE-O

  /**
   *
   *
   * @param  {string} authToken
   * @returns void
   */
  public setAuthToken(authToken: string): void {
    this.authToken = authToken;
  }

  /**
   * Pobiera token
   *
   * @returns string
   */
  public getAuthToken(): string {
    return this.authToken;
  }

  /**
   * Zapisuje usera do local storage
   *
   * @param  {any} user
   * @returns void
   */
  public saveUserToLocalStorage(activeUser: LoggedUserInitialData): void {
    const localStorageUser = localStorage.getItem('activeUser') ? JSON.parse(localStorage.getItem('activeUser')) : {};

    for (const prop in activeUser) {
      if (activeUser.hasOwnProperty(prop)) {
        localStorageUser[prop] = activeUser[prop];
      }
    }

    const user = JSON.stringify(localStorageUser);
    localStorage.setItem('activeUser', user);
  }

  /**
   * Zwraca zalogowanego usera
   *
   * @returns ActiveUser
   */
  public getUserFromLocalStorage(): LoggedUserInitialData {
    const user = JSON.parse(localStorage.getItem('activeUser') || '{}');
    return user;
  }

  /**
   * Pobiera z local storage dane użytkownika, inicjuje metode checkFields
   *
   * @returns void
   */
  public passChartStatesToSharedService(): void {
    const user = JSON.parse(localStorage.getItem('activeUser') || '{}');
    const customFields = user.customFields;
    if (!!customFields) {
      Object.keys(customFields).forEach(key => {
        if (key.includes('State')) {
          this.checkFields(key, customFields[key]);
        }
      });
    }
  }

  /**
   * Sprawdza klucz danego customowego pola, tworzy obiekty typu ChartState lub TableState i zapisuje je do serwisu ChartsStateService
   *
   * @param  {string} key
   * @param  {string} customField
   */
  checkFields(key: string, customField: string) {
    let chartState = null;
    const customFieldObj = !!customField ? JSON.parse(customField) : null;

    if (!!customFieldObj) {
      if (key.includes('tableState')) {
        chartState = this.createTableStateObject(customFieldObj);
      } else if (key.includes('availabilityState') || key.includes('pricesState')) {
        chartState = this.createAvaialbilityOrPricesStateObject(customFieldObj);
      } else {
        chartState = this.createChartStateObject(customFieldObj);
      }
    }

    if (!!chartState) {
      switch (key) {
        case 'renewablesState':
          this._chartStateService.saveChartState('renewables', chartState);
          break;
        case 'weatherState':
          this._chartStateService.saveChartState('weather', chartState);
          break;
        case 'demandState':
          this._chartStateService.saveChartState('demand', chartState);
          break;
        case 'hydroState':
          this._chartStateService.saveChartState('hydro', chartState);
          break;
        case 'pricesState':
          this._chartStateService.saveChartState('prices', chartState);
          break;
        case 'availabilityState':
          this._chartStateService.saveChartState('availability', chartState);
          break;
        case 'tableState':
          this._chartStateService.saveChartState('table', chartState);
          break;
      }
    }
  }

  /**
   * Tworzy obiekt typu ChartState
   *
   * @param  {} customFields
   * @returns ChartState
   */
  private createChartStateObject(customField): ChartState {
    return new ChartState(customField.last, customField.prev, customField.runDate,
      customField.viewFullRange, customField.country, customField.kind);
  }

  /**
   * Tworzy obiekt typu ChartState
   *
   * @param  {} customFields
   * @returns ChartState
   */
  private createAvaialbilityOrPricesStateObject(customField): ChartAvailabilityOrPricesState {
    return new ChartAvailabilityOrPricesState(customField.country, customField.currentDay,
      customField.selectedOffset, customField.kind, customField.runDate);
  }

  /**
   * Tworzy obiekt typu TableState
   *
   * @param  {} customField
   * @returns TableState
   */
  private createTableStateObject(customField): TableState {
    return new TableState(customField.hourly, customField.kinds);
  }

  /** Przypisuje nowy wyglad szablonu
   *
   * @param {string} currentTheme
   * @returns void
   */
  public changeThemeClass(currentTheme: string): void {
    this.newThemeSrc.next(currentTheme);
  }

  /**
   * Pobiera aktualną wartość z behavior subjecta z wygladem
   *
   * @returns string
   */
  public getThemeClass(): string {
    return this.newThemeSrc.getValue();
  }

  /** Przypisuje wyglad szablonu podczas inicjalizacji
   *
   * @returns void
   */
  public customThemeInit(): void {
    const localStorageUser = JSON.parse(localStorage.getItem('activeUser') || '{}');

    const themeName = localStorageUser.customFields.themeDisplay && localStorageUser.customFields.themeDisplay !== null ? localStorageUser.customFields.themeDisplay : 'default_theme';
    this.newThemeSrc.next(themeName);
  }
  // USER-CODE-C
}
