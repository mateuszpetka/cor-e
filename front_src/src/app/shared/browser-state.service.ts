import { Injectable } from '@angular/core';
import { BrowserSettings } from '../classes/classes';

/**
 * Przechowuje tymczasowy stan wewwnętrznych filtrow każdego wykresu oraz tabeli.
 * Nazewnistwo pol tej klasy musi być takie samo jak pola w bazie danych!!
 * Przy dodaniu nowego wykresu, należy w encji BlingUsers dodać nowe pole przechowujace stan wykresu.
 *
 */
@Injectable()
export class BrowserStateService {

  /**
   * Sprawdzanie przegladarki i jej wersji
   *
   * @returns void
   */
  checkBrowser(): BrowserSettings {
    const nAgt = navigator.userAgent;
    let browserName  = navigator.appName;
    let fullVersion  = '' + parseFloat(navigator.appVersion);
    let majorVersion = parseInt(navigator.appVersion, 10);
    let nameOffset, verOffset, ix;

    switch (true) {
      case nAgt.includes('OPR/'):
        verOffset = nAgt.indexOf('OPR/');
        browserName = 'Opera';
        fullVersion = nAgt.substring(verOffset + 4);
      break;
      case nAgt.includes('Opera'):
        verOffset = nAgt.indexOf('Opera');
        browserName = 'Opera';
        fullVersion = nAgt.substring(verOffset + 6);
        if (nAgt.includes('Version')) {
          fullVersion = nAgt.substring(verOffset + 8);
        }
      break;
      case nAgt.includes('MSIE'):
        verOffset = nAgt.indexOf('MSIE');
        browserName = 'Microsoft Internet Explorer';
        fullVersion = nAgt.substring(verOffset + 5);
      break;
      case nAgt.includes('Chrome'):
        verOffset = nAgt.indexOf('Chrome');
        browserName = 'Google Chrome';
        fullVersion = nAgt.substring(verOffset + 7);
      break;
      case nAgt.includes('Safari'):
        verOffset = nAgt.indexOf('Safari');
        browserName = 'Safari';
        fullVersion = nAgt.substring(verOffset + 7);
          if (nAgt.includes('Version')) {
            fullVersion = nAgt.substring(verOffset + 8);
          }
      break;
      case nAgt.includes('Firefox'):
       verOffset = nAgt.indexOf('Firefox');
        browserName = 'Mozilla Firefox';
        fullVersion = nAgt.substring(verOffset + 8);
      break;
      case (nAgt.lastIndexOf(' ') + 1) < (nAgt.lastIndexOf('/')):
        nameOffset = nAgt.lastIndexOf(' ') + 1;
        verOffset = nAgt.indexOf('/');
        browserName = nAgt.substring(nameOffset, verOffset);
        fullVersion = nAgt.substring(verOffset + 1);
        if (browserName.toLowerCase() === browserName.toUpperCase()) {
          browserName = navigator.appName;
        }
      break;
    }

    if ((fullVersion.indexOf(';')) !== -1) {
      ix = fullVersion.indexOf(';');
      fullVersion = fullVersion.substring(0, ix);
    }
    if ((fullVersion.indexOf(' ')) !== -1) {
      ix = fullVersion.indexOf(' ');
      fullVersion = fullVersion.substring(0, ix);
    }

    majorVersion = parseInt('' + fullVersion, 10);
    if (isNaN(majorVersion)) {
      fullVersion  = '' + parseFloat(navigator.appVersion);
      majorVersion = parseInt(navigator.appVersion, 10);
    }

    const currentBrowser = browserName.toLowerCase().replace(/ /gi, '');
    const majorBrowserVersion = majorVersion;

    const browserData: BrowserSettings = {
      currentBrowser: currentBrowser,
      majorBrowserVersion: majorBrowserVersion
    };
    return browserData;
  }
}
