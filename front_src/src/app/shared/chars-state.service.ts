import { Injectable } from '@angular/core';
import { ChartState, TableState } from './../classes/classes';

/**
 * Przechowuje tymczasowy stan wewwnętrznych filtrow każdego wykresu oraz tabeli.
 * Nazewnistwo pol tej klasy musi być takie samo jak pola w bazie danych!!
 * Przy dodaniu nowego wykresu, należy w encji BlingUsers dodać nowe pole przechowujace stan wykresu.
 *
 */
@Injectable()
export class ChartsStateService {
  // Aktualny state wykresow
  private renewablesState: ChartState = null; // stan wykresu Renewables
  private weatherState: ChartState = null; // stan wykresu Weather
  private hydrosState: ChartState = null; // stan wykresu Hydro
  private demandState: ChartState = null; // stan wykresu Demand
  private pricesState: ChartState = null; // stan wykresu Prices
  private availabilityState: ChartState = null; // stan wykresu Availability
  private tableState: TableState = null; // stan wykresu Tables

  // ELEMENTY chartStatesList MUSZĄ MIEĆ TAKIE SAMO NAZEWNICTWO JAK POLA W ENCJI BLINGUSER!!!!!
  public chartStatesList: string[] = ['renewablesState', 'weatherState', 'hydroState', 'demandState',
    'pricesState', 'availabilityState', 'tableState']; // list aktualnie przechowywanych stanow wykresow.
  // potrzebne przy zapisie danych w komponencie banner.component.ts

  constructor() {}

    /**
   * Zapisuje stan wykresu do lokalnych zmiennych
   *
   * @param  {string} chartName
   * @param  {ChartState|TableState} state
   * @returns void
   */
  public saveChartState(chartName: string, state: ChartState | TableState): void {
    this[`${chartName.toLowerCase()}State`] = state;
  }

  /**
   * Zwraca wybrany stan wykresu
   *
   * @param  {string} chartName
   * @returns void
   */
  public getChartState(stateName: string): ChartState | TableState {
    return this[`${stateName}`];
  }

  /**
   * Czyszczenie stanów wykresów
   *
   * @returns void
   */
  public clearChartStates(): void {
    this.chartStatesList.forEach(chartName => {
      this[chartName] = null;
    });
  }
}
