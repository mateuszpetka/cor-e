
import {of as observableOf,  BehaviorSubject ,  Observable ,  Subject } from 'rxjs';

import {map,  debounceTime } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { without } from 'lodash';

import { AppService } from '../app.service';
import { Country, CountryDb, CsvData, Model, ChartState, TableState, PricesDates } from './../classes/classes';
import { CustomHttpService } from './customHttp.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogComponent } from '../containers/dialog/dialog.component';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class SharedService {
  private selectedCountriesSrc: Subject<Country[]> = new BehaviorSubject<Country[]>([]); // selected countries from global filters
  public selectedCountries$: Observable<Country[]> = this.selectedCountriesSrc.asObservable().pipe(
    debounceTime(100)
  ); // observable created from selectedCountriesSrc

  private activeCountrySrc: BehaviorSubject<Country> = new BehaviorSubject<Country>(null); // first element from selected countries - used for rendering chart-button with kind = country
  public activeCountry$: Observable<Country> = this.activeCountrySrc.asObservable(); // observable created from activeCountrySrc

  private selectedModelSrc: Subject<string> = new Subject<string>(); // selected models from global filters
  public selectedModel$: Observable<string> = this.selectedModelSrc.asObservable(); // observable created from selectedModelSrc

  private colapseFiltersSrc: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false); // collapse menu state
  public colapseFilters$: Observable<boolean> = this.colapseFiltersSrc.asObservable(); // observables created from  colapseFiltersSrc

  private moodelsSrc: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]); // selected models from global filters
  public currentModels$: Observable<string[]> = this.moodelsSrc.asObservable().pipe(
    debounceTime(100)
  ); // observable created from selectedModelSrc

  // MAIN LOADERS SRC AND OBSERVABLES
  private loadingRenewablesSrc: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public loadingRenewables$: Observable<boolean> = this.loadingRenewablesSrc.asObservable().pipe(
    debounceTime(100)
  );

  private loadingAvailabilitySrc: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public loadingAvailability$: Observable<boolean> = this.loadingAvailabilitySrc.asObservable().pipe(
    debounceTime(100)
  );

  private loadingWeatherSrc: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public loadingWeather$: Observable<boolean> = this.loadingWeatherSrc.asObservable().pipe(
    debounceTime(100)
  );

  private loadingPricesSrc: Subject<boolean> = new Subject<boolean>();
  public loadingPrices$: Observable<boolean> = this.loadingPricesSrc.asObservable().pipe(
    debounceTime(100)
  );

  private tableDataLoaderSrc: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public tableDataLoader$: Observable<boolean> = this.tableDataLoaderSrc.asObservable().pipe(
    debounceTime(100)
  );

  private loadingDemandSrc: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public loadingDemand$: Observable<boolean> = this.loadingDemandSrc.asObservable().pipe(
    debounceTime(100)
  );

  private loadingRiskManagmentSrc: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public loadingRiskManagment$: Observable<boolean> = this.loadingRiskManagmentSrc.asObservable().pipe(
    debounceTime(100)
  );

  private loadingHydroSrc: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public loadingHydro$: Observable<boolean> = this.loadingHydroSrc.asObservable().pipe(
    debounceTime(100)
  );

  private dataTableStatusSrc: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public dataTableStatus$: Observable<boolean> = this.dataTableStatusSrc.asObservable().pipe(
    debounceTime(100)
  );


  private fullscreenChartSrc: Subject<string> = new Subject<string>();
  public fullscreenChart$: Observable<string> = this.fullscreenChartSrc.asObservable();

  private fullscreenOffChartSrc: Subject<string> = new Subject<string>();
  public fullscreenOffChart$: Observable<string> = this.fullscreenOffChartSrc.asObservable();

  private chartListStateSrc: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public chartListState$: Observable<boolean> = this.chartListStateSrc.asObservable();

  private pricesSubModelSrc: BehaviorSubject<number> = new BehaviorSubject<number>(17);
  public pricesCurrentSubModel$: Observable<number> = this.pricesSubModelSrc.asObservable();

  private parametersChartSrc: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public parametersChart$: Observable<boolean> = this.parametersChartSrc.asObservable();

  private loadNewBoxPlotDataSrc: Subject<any> = new Subject<any>();
  public newBoxPlotOffset$: Observable<any> = this.loadNewBoxPlotDataSrc.asObservable();

  private parametersUpdateSrc: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public parametersUpdate$: Observable<boolean> = this.parametersUpdateSrc.asObservable();

  private updateDashSrc: Subject<boolean> = new Subject<boolean>();
  public updateDashboard$: Observable<boolean> = this.updateDashSrc.asObservable();

  public updateDashTemplateSrc: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public updateDashTemplate$: Observable<string> = this.updateDashTemplateSrc.asObservable();

  public userDataSrc: BehaviorSubject<any> = new BehaviorSubject<any>('');
  public userData$: Observable<any> = this.userDataSrc.asObservable();

  public chartLegendHiddenFiltersSrc: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
  public chartLegendHiddenFilters$: Observable<string[]> = this.chartLegendHiddenFiltersSrc.asObservable();

  public dialogState: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public dialogState$: Observable<boolean> = this.dialogState.asObservable();

  public pricesDates: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  public pricesDates$: Observable<string> = this.pricesDates.asObservable();

  private currentModel: string = '';

  constructor(
    private _http: CustomHttpService,
    private _appService: AppService,
    private dialog: MatDialog,
    private router: Router
  ) {}

  /**
   * Gets countries list
   *
   * @param  {string='pl'} lang
   * @returns Observable
   */
  getCountries(lang: string = 'en'): Observable<Country[]> {
    return this._http.get(lang + '/Country').pipe(map(countriesRes => {
     return countriesRes.sort((a, b) => {
       return a.countryName.localeCompare(b.countryName); })
     .map(country => {
       return this.convertCountryType(country);
     });
    }));
  }

  /**
   * Dodaje/usuwa modele z tablicy
   *
   * @param  {string} currentModel
   * @returns void
   */
  setModels(currentModel: string): void {
    const models = [...this.moodelsSrc.value];
    const searchedIndex = models.findIndex(model => model === currentModel);

    searchedIndex !== -1
      ? models.splice(searchedIndex, 1)
      : models.push(currentModel);
    this.selectedModelSrc.next(models[0]);
    this.moodelsSrc.next(models);
  }

  /**
   * Sets initial models based on user saved settings
   *
   * @param  {string[]} models
   * @returns void
   */
  setInitialModels(models: string[]): void {
    this.moodelsSrc.next(models);
  }

  /**
   * Returns current model
   *
   * @returns string
   */
  getCurrentModel(): string {
    return this.currentModel;
  }

  /**
   * Converts country type from CountryDb to Country
   *
   * @param  {CountryDb} country
   * @returns Country
   */
  convertCountryType(country: CountryDb): Country {
    return new Country(country.countryName, country.countryIsoCode);
  }


  /**
   * Gets availability kinds list
   *
   * @param  {string='en'} lang
   * @returns Observable
   */
  getAvailabilityKinds(lang: string = 'en'): Observable<string[]> {
    return this._http.get(lang + '/AvailabilityKind').pipe(map(res => res.map(kind => kind.kind)));
  }


  /**
   * Gets models list
   *
   * @param  {string='en'} lang
   * @returns Observable
   */
  getModels(lang: string = 'en'): Observable<Model[]> {
    return this._http.get(lang + '/MeteoModel');
  }

  /**
   * Gets isotherm kinds list
   *
   * @param  {string='en'} lang
   * @returns Observable
   */
  getIsothermKinds(lang: string = 'en'): Observable<any[]> {
    return observableOf([]);
    // this._http.get(lang + '/IsothermKind').map(res => res.map(kind => kind.kind));
  }


  /**
   * Pushes value to selectedCountriesSrc subject
   *
   * @param  {Country[]} countries
   * @returns void
   */
  setSelectedCountries(countries: Country[]): void {
    this.selectedCountriesSrc.next(countries);
  }

  /**
   * Pushes value to activeCountry subject
   *
   * @param  {Country} country
   * @returns void
   */
  setActiveCountry(country: Country): void {
    this.activeCountrySrc.next(country);
  }

  /**
   * Pushes value to coplaseFilters subject
   *
   * @param  {boolean} state
   * @returns void
   */
  colapseFiltersMenu(state: boolean): void {
    this.colapseFiltersSrc.next(state);
  }

  /**
   * Pushes value to fullscreenChart subject
   *
   * @param  {string} chartName
   * @returns void
   */
  setFullscreenChart(chartName: string): void {
    this.fullscreenChartSrc.next(chartName);
  }

  /**
   * Pushes value to fullscreenOffChart subject
   *
   * @param  {string} chartName
   * @returns void
   */
  setFullscreenOffChart(chartName: string): void {
    this.fullscreenOffChartSrc.next(chartName);
  }

  /**
   * Pushes value to fullscreenParametrsChart subject
   *
   * @param  {string} chartName
   * @returns void
   */
  setFullscreenParameters(state: boolean): void {
    this.parametersChartSrc.next(state);
  }

  /**
   * Pushes value to fullscreenOffChart subject
   *
   * @param  {boolean} state
   * @returns void
   */
  setChartListVisibility(state: boolean): void {
    this.chartListStateSrc.next(state);
  }

  /**
   * Sets current submodel for prices/boxplot chart
   *
   * @param  {number} subModel
   * @returns void
   */
  setCurrentSubModel(subModel: number): void {
    this.pricesSubModelSrc.next(subModel);
  }

  /**
   * Sets
   *
   * @param  {number} offset
   * @returns void
   */
  setBoxPlotOffset(data: any): void {
    this.loadNewBoxPlotDataSrc.next(data);
  }

  /**
   * Sets upload state
   *
   * @param  {boolean} state
   * @returns void
   */
  setUpdateState(state: boolean): void {
    this.parametersUpdateSrc.next(state);
  }

  /**
   * Sets upload state
   *
   * @param  {boolean} state
   * @returns void
   */
  updateDashState(state: boolean): void {
    this.updateDashSrc.next(state);
  }

  /**
   * Sets loader state
   *
   * @param  {boolean} state
   * @returns void
   */
  changeLoaderState(chartType: string, state: boolean): void {
    switch (chartType) {
      case 'renewables': {
        this.loadingRenewablesSrc.next(state);
        break;
      }
      case 'availability': {
        this.loadingAvailabilitySrc.next(state);
        break;
      }
      case 'weather': {
        this.loadingWeatherSrc.next(state);
        break;
      }
      case 'prices': {
        this.loadingPricesSrc.next(state);
        break;
      }
      case 'demand': {
        this.loadingDemandSrc.next(state);
        break;
      }
      case 'risk': {
        this.loadingRiskManagmentSrc.next(state);
        break;
      }
      case 'hydro': {
        this.loadingHydroSrc.next(state);
        break;
      }
      case 'table': {
        this.tableDataLoaderSrc.next(state);
      }
    }
  }

  /**
   * Changes table data visibility
   *
   * @param  {boolean} status
   * @returns void
   */
  changeTableVisibility(status: boolean): void {
    this.dataTableStatusSrc.next(status);
  }

  /**
   * Clears global filters for countries and meteo models
   *
   * @returns void
   */
  clearFilters(): void {
    const userDataFields = this._appService.getUserFromLocalStorage().customFields;
    if (!(!!userDataFields.filterCountries) && !(!!userDataFields.filterModels)) {
      this.moodelsSrc.next([]);
      this.selectedCountriesSrc.next([]);
    }
    this.clearLoaders();
  }

  /**
   * Sets default main loaders state
   *
   * @returns void
   */
  clearLoaders(): void {
    const state = true;
    this.loadingRenewablesSrc.next(state);
    this.loadingAvailabilitySrc.next(state);
    this.loadingWeatherSrc.next(state);
    this.loadingPricesSrc.next(state);
    this.loadingDemandSrc.next(state);
    this.loadingRiskManagmentSrc.next(state);
    this.loadingHydroSrc.next(state);
    this.tableDataLoaderSrc.next(state);
  }


  /**
   * Sets initial fitlers based on logged user data
   *
   * @returns void
   */
  setInitialFilters(): void {
    const userDataFields = this._appService.getUserFromLocalStorage().customFields;
    if (!!userDataFields.filterCountries && !!userDataFields.filterModels) {
      const parsedCountries = JSON.parse(userDataFields.filterCountries);
      const parsedModels: string[] = JSON.parse(userDataFields.filterModels);

      this.setSelectedCountries(parsedCountries);
      // this.setActiveCountry(parsedCountries[0]);
      this.setInitialModels(parsedModels);
    }
  }

  /**
   * Saves table data to csv
   *
   * @param  {CsvData} data
   * @returns Observable
   */
  saveToCsv(data: CsvData): Observable<any> {
    const dataStr = this.toQueryString(data);
    return this._http.getBlob('ChartData/bigTableToCSV?' + dataStr, { responseType: 'blob' });
  }

  /**
   * Saves hourly table data to csv
   *
   * @param  {CsvData} data
   * @returns Observable
   */
  saveHourlyDataToCsv(data: CsvData): Observable<any> {
    const dataStr = this.toQueryString(data);
    return this._http.getBlob('ChartData/hourlyDataToCSV?' + dataStr, { responseType: 'blob' });
  }

  /**
   * Sparsowanie dowolnego obiektu lub tablicy do query stringa
   *
   * @param  {Array<any>|object} data
   * @param  {any} prefix=undefined
   * @returns string
   */
  public toQueryString(data: Array<any> | object, prefix: any = undefined): string {
    let str = [], property;

    for (property in data) {
      if (data[property] || data[property] === 0 && data.hasOwnProperty(property)) {
        const key = prefix ? prefix + `[${property}]` : property, value = data[property];

        if (!!value && typeof value === 'object') {
          str.push(this.toQueryString(value, key));
        } else {
          str.push(encodeURIComponent(key) + '=' + encodeURIComponent(value));
        }
      } else {
        continue;
      }
    }
    str = without(str, '');
    return str.join('&');
  }

  /**
   * Sprawdza czy użykownik jest już zalogowany na innym urządzeniu
   *
   * @param  {HttpErrorResponse} errorResponse
   * @returns void
   */
  checkIfUsersIsAlreadyLoggedIn(errorResponse: HttpErrorResponse): void {
    if (errorResponse.error.data && errorResponse.error.data.code === 4030) {
      this.openDialog('userLoggedIn');
    } else if (errorResponse.error.msg === 'Expired token') {
      this.openDialog('expiredToken');
    } else {
      console.error(errorResponse.error.msg || errorResponse.message);
    }
  }

  /**
   * Otwiera modal z informacją i wykonuje akcje po jego zamknieciu
   *
   * @param {string} kind
   * @returns void
   */
  openDialog(kind: string): void {
    if (!this.checkIfDialogIsOpen()) {
      this.dialogState.next(true);
      const dialogRef = this.dialog.open(DialogComponent, {
        width: '300px',
        data: kind,
        hasBackdrop: true,
        disableClose: true
      });

      dialogRef.beforeClose().subscribe(result => {
        if (result === 'redirect-account') {
          this.setChartListVisibility(false);
        } else if (result === 'userLoggedIn' || result === 'expiredToken') {
          this.router.navigate(['/auth/login']);
        }
      });

      dialogRef.afterOpen().subscribe(result => {
        this.dialogState.next(true);
      });

      dialogRef.afterClosed().subscribe(val => {
        this.dialogState.next(false);
      });
    }
  }

  /**
   * Checks if dialog is open
   *
   * @returns boolean
   */
  checkIfDialogIsOpen(): boolean {
    return this.dialogState.getValue();
  }

  /**
   * Przekazuje wylaczone filtry
   * @param {array} filters
   */
  singleChartLegendFilters(filters: string[]): void {
    this.chartLegendHiddenFiltersSrc.next(filters);
  }
}
