
import { Directive, OnInit, HostListener, Output, EventEmitter, ElementRef } from '@angular/core';

@Directive({
  selector: '[clickOutside]'
})
export class ClickOutsideDirective {
  @Output() public clickEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {}

  @HostListener('mouseleave') clickedOutside(ev) {
    this.clickEvent.emit(true);
  }
}
