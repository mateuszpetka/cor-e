
import { Directive, ElementRef, HostListener, Renderer2, Input, OnInit, AfterViewInit, AfterContentInit, OnChanges } from '@angular/core';
import { Country } from './../classes/classes';
import { SharedService } from './shared.service';

@Directive({
  selector: '[highlight]'
})
export class HighlightDirective  {
  @Input() selectedCountry: Country;
  @Input()
  public set selectedCountries(selectedCountries: Country[]) {
    if (!!selectedCountries) {
      this.checkIfExists(selectedCountries);
      this._selectedCountries = selectedCountries;
    }
  }

  public _selectedCountries;
  private state: boolean = false;
  private highlightColor: string = '#f58220';
  private basicColor: string = '#353535';

  constructor(private _el: ElementRef, private _renderer: Renderer2,
    private _sharedService: SharedService) {
  }


  @HostListener('click') onClick() {
    this.state = !this.state;
    this.changeColor();
  }

  /**
   * Checks if country should be already selected
   * 
   * @param  {Country[]} countries
   * @returns void
   */
  checkIfExists(countries: Country[]): void {
    countries.forEach(country => {
       if(country.iso.toLowerCase() === this.selectedCountry.iso.toLowerCase()) {
         this.state = true;
         this._renderer.setStyle(this._el.nativeElement, 'color', this.highlightColor);
         this._renderer.addClass(this._el.nativeElement, 'selectedCountry');
       }
    });
  }

 /**
   * Changes color of selected item
   *
   * @returns void
   */
  changeColor(): void {
    if (this.state) {
      this._renderer.setStyle(this._el.nativeElement, 'color', this.highlightColor);
      this._renderer.addClass(this._el.nativeElement, 'selectedCountry');
      const tempCountries  = this._selectedCountries.slice();
      tempCountries.push(this.selectedCountry)
      this._sharedService.setSelectedCountries(tempCountries);
    } else if(!this.state) {
      this._renderer.setStyle(this._el.nativeElement, 'color', this.basicColor);
      this._renderer.removeClass(this._el.nativeElement, 'selectedCountry');
      this.removeCountry();
    }
  }

  /**
   * Removes country
   *
   * @returns void
   */
  removeCountry(): void {
    let tempCountries  = this._selectedCountries.slice();
    tempCountries = tempCountries.filter(country => {
      return country.iso.toLowerCase() !== this.selectedCountry.iso.toLowerCase()
    })
    this._sharedService.setSelectedCountries(tempCountries);
  }
}
