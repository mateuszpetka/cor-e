
import { map, tap } from 'rxjs/operators';
import { AppService } from './../app.service';


import { Http, Headers, RequestOptionsArgs } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { AppSettings } from './../app.settings';
import { HttpService } from './../http.service';
import { SharedService } from './shared.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { pick } from 'lodash';

@Injectable()
export class CustomHttpService extends HttpService {
  constructor(
    private _http: HttpClient,
    private _settings: AppSettings,
    private _appService: AppService
  ) {
    super(_http, _settings);
  }


  /**
   * Updates user based on token
   *
   * @param  {string} url
   * @param  {any} body
   * @param  {} token
   * @param  {Object} options?
   * @returns Observable
   */
  public post(url: string, body: any, token?, options?: Object): Observable<any> {
    if (options === undefined) {
      options = {};
    }

    options['headers'] = new HttpHeaders({
      'php-auth-digest': !!localStorage.getItem('authToken') ?
        this.parseToken() : this._appService.getAuthToken()
    });

    return this._http.post(this._settings.apiUrl + '/' + url, body, options)
      .pipe(
        map((res: any) => {
          return !res['_body'] ? res : res.data;
        })
      );
  }

  /**
   * Parses token
   *
   * @returns string
   */
  parseToken(): string {
    const token = localStorage.getItem('authToken') || this._settings.authGuestToken;
    return token.indexOf('"') > -1 ? token.replace('\"\g', '') : token;
  }

  /**
   * Send GET request.
   *
   * @param {string} url
   * @param {Object} [options]
   * @returns {Observable<any>}
   * @memberof HttpService
   */
  public get(url: string, options?: Object): Observable<any> {
    if (options === undefined) {
      options = {};
    }

    options['headers'] = new HttpHeaders({
      'php-auth-digest': !!localStorage.getItem('authToken') ?
        this.parseToken() : this._appService.getAuthToken()
    });

    return this._http.get(this._settings.apiUrl + '/' + url, options)
      .pipe(
        map((res: any) => {
          return this.mapListResponse(res.data, options);
        })
      );
  }

  /**
   * Send GET request.
   *
   * @param {string} url
   * @param {Object} [options]
   * @returns {Observable<any>}
   * @memberof HttpService
   */
  public postLogin(url: string, body: any, token?, options?: Object): Observable<any> {
    return this._http.post(this._settings.apiUrl + '/' + url, body, options)
      .pipe(
        map((res: any) => {
          return res.data;
        })
      );
  }

  /**
   * Send GET request.
   *
   * @param {string} url
   * @param {Object} [options]
   * @returns {Observable<any>}
   * @memberof HttpService
   */
  public getBlob(url: string, options?: Object): Observable<any> {
    if (options === undefined) {
      options = {};
    }

    options['headers'] = new HttpHeaders({
      'php-auth-digest': !!localStorage.getItem('authToken') ?
        this.parseToken() : this._appService.getAuthToken()
    });

    return this._http.get(this._settings.apiUrl + '/' + url, options)
    .pipe(
      map(res => {
        return {
          filename: `export_${new Date().toISOString()}.csv`,
          blob: res
        };
      })
    );
  }
}
