import { TokenService } from './../../components/auth/token.service';
import { Component, OnInit, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AuthService } from '../../components/auth/auth.service';
import { Router } from '@angular/router';
import { AppSettings } from '../../app.settings';
import { AppService } from '../..//app.service';
import { CustomHttpService } from '../../shared/customHttp.service';
import { SharedService } from 'app/shared/shared.service';
import { MatSnackBar } from '@angular/material';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public recoveryForm: FormGroup = new FormGroup({
    email: new FormControl(),
    captcha: new FormControl()
  });
  public requireCaptcha = false;
  public validLogin = true;
  public rememberUser = false;
  public isRecoveryFormVisible = false;
  public isRecoveryFormSent = false;
  public passwordVisible = false;
  @ViewChild('reCaptcha') reCaptcha;
  public captchaInvalid = false;

  constructor(
    public authService: AuthService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    public configuration: AppSettings,
    private appService: AppService,
    private _customHttp: CustomHttpService,
    private _tokenService: TokenService,
    private _sharedService: SharedService,
    private _snackBar: MatSnackBar
  ) {
    this.authService.isCaptchaRequired().subscribe(isRequired => {
      if (isRequired) {
        this.addCaptcha();
      }
    });
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      login: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });
    this._sharedService.clearLoaders();
  }

  /**
   * Dodanie captchy
   *
   * @returns void
   */
  addCaptcha(): void {
    if (!this.loginForm.get('captcha')) {
      this.loginForm.addControl('captcha', new FormControl(null, Validators.required));
      this.requireCaptcha = true;
      this.cdRef.markForCheck();
    } else {
      this.loginForm.get('captcha').markAsPristine();
      this.reCaptcha.reset();
      this.cdRef.markForCheck();
    }
  }

  /**
   * Wywołanie change detectora po zwróceniu tokena przez reCaptcha
   *
   * @param  {string} $event
   * @returns void
   */
  onCaptchaResponse($event: string): void {
    this.cdRef.markForCheck();
  }

  /**
   * Obsługa formularza do logowania
   *
   * @returns void
   */
  login(): void {
    const form = this.loginForm.value;
    const login = form.login ? form.login.trim() : form.login;
    const password = form.password ? form.password.trim() : form.password;

    if (!login || !password) {
      return;
    }

    if (this.requireCaptcha && !form.captcha) {
      this.validLogin = true;
      this.captchaInvalid = true;
      return;
    }

    const credentials = {
      identifier: login,
      password: password,
      ...(this.requireCaptcha && { captcha: form.captcha })
    };
    this.authService.login(credentials).subscribe(
      success => {
        this.validLogin = true;
        this.captchaInvalid = false;
        this.router.navigate(['/dashboard']);
        if (this.rememberUser) {
          localStorage.setItem('authToken', success.token);
        }
        this.authService.refreshTokenPeriodically();
        this.appService.saveUserToLocalStorage(success);
        this.appService.passChartStatesToSharedService();
        this.appService.customThemeInit();
        this.appService.setAuthToken(success.token);
      },
      error => {
        this.validLogin = false;
        this.captchaInvalid = false;
        localStorage.clear();
        this.addCaptcha();
      },
      () => {
        this._sharedService.setInitialFilters();
      }
    );
  }

  /**
   * Wywołanie akcji do wysłania linku z resetem hasła
   *
   * @returns void
   */
  recoverPassword(): void {
    const form = this.recoveryForm.value;
    const email = form.email;
    const captcha = form.captcha;

    const credentials = {
      email: email,
      captcha: captcha
    };

    this.authService.sendPasswordRecoveryRequest(credentials).subscribe(
      success => {
        this.isRecoveryFormSent = true;
        this._snackBar.open('Email has been sent', 'Ok', {duration: 1000});
      },
      error => {
        console.error(error);
        this._snackBar.open(`Error occured: ${error}`, 'Ok', {duration: 1000});
      },
      () => {
        console.log('email sent');
      }
    );
  }

  /**
   * Pokazanie/ukrycie formularza resetu hasła
   *
   * @returns void
   */
  toggleRecoveryForm(): void {
    this.isRecoveryFormVisible = this.isRecoveryFormVisible === true ? false : true;
  }

  /**
   * Pokazanie/ukrycie hasła w inpucie
   *
   * @param  {HTMLInputElement} input
   * @returns void
   */
  togglePasswordVisibility(input: HTMLInputElement): void {
    // input.type = input.type === 'password' ? 'text' : 'password';
    this.passwordVisible = this.passwordVisible === false ? true : false;
  }
}
