import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'dialog-component',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  public dialogKind: string = '';

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: string
  ) {}

  ngOnInit() {
    this.dialogRef.afterOpen().subscribe((res) => {
      if (this.data) {
        this.dialogKind = this.data;
      }
    });
  }

  /**
   * Przesłanie akcji po kliknięciu na btn-a
   *
   * @param {string} action
   * @return void
   */
  btnDialogAction(action: string): void {
    if (action === 'userLoggedIn') {
      localStorage.clear();
    }
    this.dialogRef.close(action);
    this.dialog.closeAll();
  }

  /**
   * Reloads page
   *
   * @returns void
   */
  reloadPage(): void {
    window.location.reload();
  }

  /**
   * Zamkniecie dialoga
   *
   * @returns void
   */
  closeDialog(): void {
    this.dialogRef.close();
    this.dialog.closeAll();
  }
}
