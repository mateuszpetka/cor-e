import { AuthService } from '../../components/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { SharedService } from './../../shared/shared.service';

@Component({
  selector: 'app-logout',
  template: ''
})
export class LogoutComponent implements OnInit {
  constructor(private authService: AuthService, private _sharedService: SharedService) {}

  ngOnInit() {
    this._sharedService.clearFilters();
    this.authService.removeAuthData();
    this.authService.logout();
  }
}
