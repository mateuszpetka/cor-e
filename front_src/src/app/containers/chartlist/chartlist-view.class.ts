import { ElementRef, ViewChild } from '@angular/core';
import { BoxplotChartCustomOption } from 'app/classes/boxplot';
import { ChartCustomOption } from 'app/classes/chart';
import { Country } from 'app/classes/classes';
import { ChartAvailabilityComponent } from 'app/components/chart-availability/chart-availability.component';
import { ChartBoxplotComponent } from 'app/components/chart-boxplot/chart-boxplot.component';
import { ChartDemandComponent } from 'app/components/chart-demand/chart-demand.component';
import { ChartHydroComponent } from 'app/components/chart-hydro/chart-hydro.component';
import { ChartPricesComponent } from 'app/components/chart-prices/chart-prices.component';
import { ChartWeatherComponent } from 'app/components/chart-weather/chart-weather.component';
import { ChartComponent } from 'app/components/chart/chart.component';
import { DataTableComponent } from 'app/components/data-table/data-table.component';
import { BrowserStateService } from 'app/shared/browser-state.service';
import { SharedService } from 'app/shared/shared.service';
import { fromEvent as observableFromEvent, Observable, Subject, Subscription } from 'rxjs';
import { combineLatest, merge, takeUntil, delay } from 'rxjs/operators';

import { ChartListService } from './chartlist.service';

export class ChartListView {
  @ViewChild('renewablesChart') renewablesChart: ChartComponent; // referencja wykresu chart-renewables
  @ViewChild('pricesChart') pricesChart: ChartPricesComponent; // referencja wykresu chart-prices
  @ViewChild('availabilityChart') availabilityChart: ChartAvailabilityComponent; // referencja wykresu chart-availability
  @ViewChild('demandChart') demandChart: ChartDemandComponent; // referencja wykresu chart-demand
  @ViewChild('weatherChart') weatherChart: ChartWeatherComponent; // referencja wykresu chart-weather
  @ViewChild('riskManagementChart') riskManagementChart: ChartBoxplotComponent; // referencja wykresu chart-boxplot
  @ViewChild('hydroChart') hydroChart: ChartHydroComponent; // referencja wykresu chart-hydro
  @ViewChild('dataTable') dataTable: DataTableComponent; // referencja  data-table
  @ViewChild('chartList') chartList: ElementRef; // referencja komponentu chartlist-component jako element DOM

  // ZMIENNE PRZECHOWUJACE STAN WIDOKU DASHBOARDA
  public currentBrowser: string;
  public majorBrowserVersion: number;
  public showDashboard$: Observable<boolean>; // zmienna przełączają widowczność dashboarda i konta usera
  public filterMenuState$: Observable<boolean>; // zmienna przechowująca stan bocznego menu

  // ZMIENNE LOADERÓW
  public renewablesLoader$; // renewables chart loader, true = show animation, false = show chart
  public weatherLoader$; // weather chart loader, true = show animation, false = show chart
  public availabilityLoader$; // availability chart loader, true = show animation, false = show chart
  public pricesLoader$; // prices chart loader, show loading animation, false = show chart
  public demandLoader$; // demand chart loader, show loading animation, false = show chart
  public riskLoader$; // boxplot chart loader, show loading animation, false = show chart
  public hydroLoader$; // hydro chart loader, show loading animation false = show chart
  public tableDataLoader$; // data table  loader, true = show loading animation, false = show chart

  // Zmienne przechowujące opcje wykresu
  public renewablesOptions: ChartCustomOption; // zmienna przechowująca dane wykresu renewables
  public demandOptions: ChartCustomOption; // zmienna przechowująca dane wykresu demand
  public riskOptions: BoxplotChartCustomOption; // zmienna przechowująca dane wykresu risk managment
  public availabilityOptions: ChartCustomOption; // zmienna przechowująca dane wykresu availability
  public pricesOptions: ChartCustomOption; // zmienna przechowująca dane wykresu prices
  public hydroOptions: ChartCustomOption; // zmienna przechowująca dane wykresu hydro
  public weatherOptions: ChartCustomOption; // zmienna przechowująca dane wykresu weather

  public chartsList: string[] = ['Weather', 'Demand', 'Prices', 'Availability', 'Hydro',
  'Risk Management', 'Renewables']; // lista nazw wykresow

  // POZOSTAŁE ZMIENNE
  public _fullscreenChart: Subscription; // subskrypcja na włączenie fullscreena
  public _fullscreenOffChart: Subscription; // subskrypcja na wyłączenie fullscreena
  public _updateInterval: any; // interwał używa do odliczania czasu updatu
  public selectedCountries$: Observable<Country[]>; // obserwator obecnie wybranych krajow
  public guard$ = new Subject(); // subject zamykają wszystkie subskrypcje po pushu

  constructor(private _browserStateService: BrowserStateService, private _sharedService: SharedService,
    private _chartListService: ChartListService) {}

  /**
   * Sprawdzanie rodzaju i wersji przeglądarki
   *
   * @returns void
   */
  checkBrowser(): void {
    const browserData = this._browserStateService.checkBrowser();
    this.currentBrowser = browserData.currentBrowser;
    this.majorBrowserVersion = browserData.majorBrowserVersion;
   }

   /**
    * Inicjalizuje subsckrypcj dla rozwijanego menu
    *
    * @returns void
    */
   startMenuStateSubscription(): void {
     this.filterMenuState$ = this._sharedService.colapseFilters$;
   }

  /**
   * Inicjalizuje subsckrypcje dla eventow - widows rize oraz callapse filtrow
   *
   * @returns void
   */
  startWindowsResizeAndCollapseFilters(): void {
    const windowResizeEv = observableFromEvent(window, 'resize')
      .pipe(
        takeUntil(this.guard$),
        combineLatest(this._sharedService.chartListState$)
      );

    const onCollapsedFilters = this._sharedService.colapseFilters$;
    windowResizeEv.pipe(
      merge(onCollapsedFilters),
      delay(200)
    ).subscribe(() => {
      this.resizeAllChartsCanvases();
    });
  }

  /**
   * Dla każdego wykresu inicjalizuje metode resizowania
   *
   * @returns void
   */
  resizeAllChartsCanvases(): void {
    this.chartsList.forEach((chartName: string) => {
      this.resizeCanvasInChart(chartName);
    });
  }

  /**
   * Wywołuje zmiene wymiarow wykresow
   *
   * @param  {string} chartType
   * @returns void
   */
  resizeCanvasInChart(chartType: string): void {
    switch (chartType) {
      case 'Renewables':
        this.renewablesChart.resizeChart();
        break;
      case 'Weather':
        this.weatherChart.resizeChart();
        break;
      case 'Prices':
        this.pricesChart.resizeChart();
        break;
      case 'Availability':
        this.availabilityChart.resizeChart();
        break;
      case 'Demand':
        this.demandChart.resizeChart();
        break;
      case 'Risk Management':
        this.riskManagementChart.resizeChart();
        break;
      case 'Hydro':
        this.hydroChart.resizeChart();
        break;
    }
  }

  /**
   * Przypisuje obserwatory loaderow z serwisow do zmiennych
   *
   * @returns void
   */
  assignLoadersObservablesFromServiceToVariables(): void {
    this.renewablesLoader$ = this._sharedService.loadingRenewables$;
    this.weatherLoader$ = this._sharedService.loadingWeather$;
    this.availabilityLoader$ = this._sharedService.loadingAvailability$;
    this.pricesLoader$ = this._sharedService.loadingPrices$;
    this.demandLoader$ = this._sharedService.loadingDemand$;
    this.riskLoader$ = this._sharedService.loadingRiskManagment$;
    this.hydroLoader$ = this._sharedService.loadingHydro$;
    this.showDashboard$ = this._sharedService.chartListState$;
    this.tableDataLoader$ = this._sharedService.tableDataLoader$;
    this.selectedCountries$ = this._sharedService.selectedCountries$;
  }

  /**
   * Inicjalizuje subskrypcje fullscreena dla wykresow
   *
   * @returns void
   */
  startFullscreenSubscription(): void {
    this._fullscreenChart = this._sharedService.fullscreenChart$
    .pipe(
      takeUntil(this.guard$)
    ).subscribe((chartType: string) => {
      this.checkChartToResize(chartType);
    });

    this._fullscreenOffChart = this._sharedService.fullscreenOffChart$
    .pipe(
      takeUntil(this.guard$)
    ).subscribe((chartType: string) => {
      this.turnFullscreenOff(chartType);
    });
  }

  /**
   * Turn fullscreen on
   *
   * @param  {string} chartType
   * @returns void
   */
  checkChartToResize(chartType: string): void {
    const state = 'on';
    this.checkAndResize(chartType, state);
  }

  /**
   * Turns fullscreen off
   *
   * @param  {string} chartType
   * @returns void
   */
  turnFullscreenOff(chartType: string): void {
    const state = 'off';
    this.checkAndResize(chartType, state);
  }

    /**
   * Checks and resize chart
   *
   * @param  {string} chartType
   * @param  {string} state
   * @returns void
   */
  checkAndResize(chartType: string, state: string): void {
    let chart: HTMLElement;

    switch (chartType) {
      case 'Renewables':
        chart = this.renewablesChart.elementRef.nativeElement;
        this.renewablesChart.changeLoaderState(true);
        this.resizeChart(chart, chartType, state, true);
        break;
      case 'Weather':
        chart = this.weatherChart.elementRef.nativeElement;
        this.weatherChart.changeLoaderState(true);
        this.resizeChart(chart, chartType, state, true);
        break;
      case 'Prices':
        chart = this.pricesChart.elementRef.nativeElement;
        this.pricesChart.changeLoaderState(true);
        this.resizeChart(chart, chartType, state, true);
        break;
      case 'Risk Management':
        chart = this.riskManagementChart.elementRef.nativeElement;
        this.riskManagementChart.changeLoaderState(true);
        this.resizeChart(chart, chartType, state, false);
        break;
        case 'Hydro':
          chart = this.hydroChart.elementRef.nativeElement;
          this.hydroChart.changeLoaderState(true);
          this.resizeChart(chart, chartType, state, true);
          break;
      case 'Availability':
        chart = this.availabilityChart.elementRef.nativeElement;
        this.availabilityChart.changeLoaderState(true);
        this.resizeChart(chart, chartType, state, true);
        break;
      case 'Demand':
        chart = this.demandChart.elementRef.nativeElement;
        this.demandChart.changeLoaderState(true);
        this.resizeChart(chart, chartType, state, true);
        break;
      case 'Table':
        chart = this.dataTable.elementRef.nativeElement;
        this.resizeChart(chart, chartType, state, true);
        break;
    }
  }

  /**
   * Wywołuje zmiany wymiarow wykresu przy fullscreenie
   *
   * @param  {HTMLElement} element
   * @param  {string} chartType
   * @param  {string} state
   * @param  {boolean} animate
   * @returns void
   */
  resizeChart(element: HTMLElement, chartType: string, state: string, animate?: boolean): void {
    this._chartListService.setChartListBoundingClientProperties(element, this.chartList);
    if (state === 'on' && animate === true) {
      this._chartListService.addCssPropertiesForFullScreenMode();
      this._chartListService.animateFullscreenOn();
    } else if (state === 'off') {
      this._chartListService.animateFullscreenOff();
      this._chartListService.removeCssPropertiesForFullScreenMode();
    }
    // trzeba odczekać na zakończenie animacji, dopiero poźniej można resizować wykres.
    setTimeout(() => {
      this.resizeAllChartsCanvases();
    }, 1000);
  }

  /**
   * Resets internal charts filters
   *
   * @returns void
   */
  resetChartFilters(): void {
    this.resertTopRowChartsFitlers();
    this.pricesChart.clearFilters();
  }

  /**
   * Resetuje wewnętrzne filtry wykresow Renewabled/Hydro/Demand
   *
   * @returns void
   */
  resertTopRowChartsFitlers(): void {
    this.renewablesChart.resetFilters('wind');
    this.hydroChart.resetFilters('run of river');
    this.demandChart.resetFilters('demand');
  }

  /**
   * Przekazuje do serwisu stan widocznosci tabeli danych
   *
   * @returns void
   */
  hideDataTable(): void {
    this._sharedService.changeTableVisibility(false);
  }

  /**
   * Cleans charts data and changes loaders state
   *
   * @returns void
   */
  clearChartsExceptHydroAndAvailability(models?: string[], countries?: Country[]): void {
    this.renewablesOptions = new ChartCustomOption([], 'Renewables');
    this.renewablesChart.changeLoaderState(true);

    if (!countries.length) {
      this.pricesOptions = new ChartCustomOption([], 'Prices');
      this.pricesChart.changeLoaderState(true);
      this.weatherOptions = new ChartCustomOption([], 'Weather');
      this.weatherChart.changeLoaderState(true);
    } else {
      this.clearWeatherModelDependentChart();
      this.clearPricesModelDependentChart();
    }

    this.demandOptions = new ChartCustomOption([], 'Demand');
    this.demandChart.changeLoaderState(true);

    this.riskOptions = new BoxplotChartCustomOption([], 'Risk Managment');
    this.riskManagementChart.changeLoaderState(true);
  }

  /**
   * Czyści weather chart oprocz static curve
   *
   * @returns void
   */
  clearWeatherModelDependentChart(): void {
    this.weatherChart.changeLoaderState(true);

    this.weatherOptions.data.datasets = [
      ...this.weatherOptions.data.datasets.filter(elem => elem.model === ''),
    ];
    this.weatherChart.changeLoaderState(false);
    this.weatherChart.myChart.update();
  }

  clearPricesModelDependentChart(): void {
    this.pricesChart.changeLoaderState(true);

    this.pricesOptions.data.datasets = [
      ...this.pricesOptions.data.datasets.filter(elem => elem.model === '')
    ];

    this.pricesChart.changeLoaderState(false);
    this.pricesChart.myChart.update();
  }

  /**
   * Czyści wykresy hydro i availability, włącza ich wewnętrzne loadery
   *
   * @returns void
   */
  clearHydroAndAvailabilityCharts(): void {
    this.availabilityOptions = new ChartCustomOption([], 'Availability');
    this.availabilityChart.changeLoaderState(true);
    this.hydroOptions = new ChartCustomOption([], 'Hydro');
    this.hydroChart.changeLoaderState(true);
  }
}
