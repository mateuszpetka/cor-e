import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { ChartData } from '../../classes/chart';
import { ChartDataBox } from '../../classes/boxplot';
import { AvailabilityTableData, ChartQueryParams, TableData } from '../../classes/classes';
import { SharedService } from 'app/shared/shared.service';
import { CustomHttpService } from '../../shared/customHttp.service';

import * as moment from 'moment';

@Injectable()
export class ChartListHttpService {

  constructor(private _http: CustomHttpService, private _sharedService: SharedService) {}

  /**
   * Pobiera z bazy dane do wykresu renewables
   *
   * @param  {string} kind
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getRenewablesData(kind: string, queryParams: ChartQueryParams): Observable<ChartData> {
    const url = `ChartData/renewables?country=${queryParams.iso}&kind=${kind}&offset=${queryParams.offset}&model=${queryParams.model}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z bazy run dates dla wykresu renewables
   *
   * @param  {string} kind
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getRenewablesRunDates(kind: string, queryParams: ChartQueryParams): Observable<string[]> {
    const url = `ChartData/renewablesRunDates?country=${queryParams.iso}&kind=${kind}&model=${queryParams.model}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z bazy dane dla wykresu availability
   *
   * @param  {Array<string>} kind
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getAvailabilityData(kind: Array<string>, queryParams: ChartQueryParams): Observable<ChartData> {
    const url = `ChartData/availability?country=${queryParams.iso}&` +
      `kind=${kind}&dateFrom=${queryParams.dateFrom}&dateTo=${queryParams.dateTo}&model=${queryParams.model}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z bazy dane do tabeli - ongoing changes
   *
   * @param  {string} iso
   * @returns Observable
   */
  public getAvailabilityChartData(iso: string): Observable<AvailabilityTableData[]> {
    const url = `ChartData/ongoingOutages?country=${iso}`;
    return this._http.get(url).pipe(map(res => res.series));
  }

  /**
   * Pobiera run dates dla wykresu availability
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getAvailabilityRunDates(queryParams: ChartQueryParams): Observable<string[]> {
    return this._http.get(`ChartData/availabilityRunDates?country=${queryParams.iso}`).pipe(map((runDates: string[]) => {
       const indexesToReplace: number[] = [0, 1, 7];
       indexesToReplace.forEach(index => {
        runDates[index] = runDates[index].replace(runDates[index], `d${index}`);
       });
       return runDates;
    }));
  }

  /**
   * Pobiera z bazy dane dla wykresu prices
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getPricesData(queryParams: ChartQueryParams): Observable<ChartData> {
    const url = `ChartData/prices?country=${queryParams.iso}` +
      `&model=${queryParams.model}&offset=${queryParams.offset}&subModel=${queryParams.subModel}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z bazy run dates dla wykresu prices
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getPricesRunDates(queryParams: ChartQueryParams): Observable<string[]> {
    const url = `ChartData/pricesRunDates?country=${queryParams.iso}` +
      `&model=${queryParams.model}&offset=${queryParams.offset}&subModel=${queryParams.subModel}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z bazy realised prices
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getPricesRealisedData(queryParams: ChartQueryParams): Observable<ChartData> {
    const url = `ChartData/pricesRealised?country=${queryParams.iso}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z bazy dane dla wykresu demand
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getDemandData(queryParams: ChartQueryParams): Observable<ChartData> {
    return this._http
      .get(`ChartData/demand?country=${queryParams.iso}&model=${queryParams.model}&offset=${queryParams.offset}`);
  }

  /**
   * Pobiera z bazy dane dla wykresu demand - seria danych Residual Demand
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getResidualDemandData(queryParams: ChartQueryParams): Observable<ChartData> {
    const url = `ChartData/residualDemand?country=${queryParams.iso}&model=${queryParams.model}&offset=${queryParams.offset}`;
    return this._http.get(url);
  }

  /**
  *  Pobiera z bazy run dates dla wykresu demand - seria danych Residual Demand
  *
  * @param  {ChartQueryParams} queryParams
  * @returns Observable
  */
  public getResidualDemandRunDates(queryParams: ChartQueryParams): Observable<string[]> {
    const url = `ChartData/residualDemandRunDates?country=${queryParams.iso}&model=${queryParams.model}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z bazy run dates dla wykresu demand - seria danych Demand
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getDemandRunDates(queryParams: ChartQueryParams): Observable<string[]> {
    const url = `ChartData/demandRunDates?country=${queryParams.iso}&model=${queryParams.model}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z bazy dane dla wykresu weather - seria danych Isotherm
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getNewIsothermWeatherData(queryParams: ChartQueryParams): Observable<ChartData> {
    const url = `ChartData/getAllIsothermData?country=${queryParams.iso}&model=${queryParams.model}&offset=${queryParams.offset}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z bazy run dates dla wykresu weather - seria danych Isotherm
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getIsothermRunDates(queryParams: ChartQueryParams): Observable<string[]> {
    const url = `ChartData/IsothermRunDates?country=${queryParams.iso}&model=${queryParams.model}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z bazy dane dla wykresu weather - seria danych Temperature
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getTemperatureWeatherData(queryParams: ChartQueryParams): Observable<ChartData> {
    return this._http
      .get(`ChartData/temperature?country=${queryParams.iso}&model=${queryParams.model}&offset=${queryParams.offset}`);
  }

   /**
   * Pobiera z bazy dane dla wykresu weather - seria danych Temperature
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getTemperatureRunDates(queryParams: ChartQueryParams): Observable<string[]> {
    const url = `ChartData/temperatureRunDates?country=${queryParams.iso}&model=${queryParams.model}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z bazy dane dla wykresu weather - seria statycznych danych dla Temperatury
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getTemperatureStaticCurve(queryParams: ChartQueryParams): Observable<ChartData> {
    const url = `ChartData/temperatureStaticCurveData?country=${queryParams.iso}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z bazy dane dla wykresu weather - różnicy temperatur dla każdego dnia
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getTemperatureDifference(queryParams: ChartQueryParams): Observable<ChartData> {
    const url = `ChartData/temperatureCalculatedDifference?country=${queryParams.iso}` +
    `&model=${queryParams.model}&offset=${queryParams.offset}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z bazy dane dla wykresu weather - seria danych Temperature (difference)
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getTemperatureDifferenceRunDates(queryParams: ChartQueryParams): Observable<string[]> {
    const url = `ChartData/temperatureCalculatedDifferenceRunDates?country=${queryParams.iso}&model=${queryParams.model}`;
    return this._http.get(url);
  }

  /**
  * Pobiera z bazy dane dla wykresu risk managment
  *
  * @param  {ChartQueryParams} queryParams
   *@returns Observable
   */
  public getCalculatedBoxPlotData(queryParams: ChartQueryParams): Observable<ChartDataBox> {
    const url = `ChartData/boxPlot?country=${queryParams.iso}` +
      `&model=${queryParams.model}&offset=${queryParams.offset}&runDate=${queryParams.runDate}`;
    return this._http.get(url);
  }


  /**
   * Pobiera z bazy realised boxplot
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getBoxplotRealisedData(queryParams: ChartQueryParams): Observable<ChartData> {
    const url = `ChartData/boxplotRealised?country=${queryParams.iso}&offset=${queryParams.offset}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z bazy dane dla wykresu hydro
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getHyroRiverData(queryParams: ChartQueryParams): Observable<ChartData> {
    const url = `ChartData/hydroRiver?country=${queryParams.iso}&offset=${queryParams.offset}`;
    return this._http.get(url);
  }

  /**
   * Pobiera z run dates dane dla wykresu hydro
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getHyroRunDates(queryParams: ChartQueryParams): Observable<string[]> {
    const url = `ChartData/hydroRunDates?country=${queryParams.iso}&model=${queryParams.model}`;
    return this._http.get(url);
  }

  /**
   * Pobiera dane dla tabeli danych - dane dzienne
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getTableData(model: string): Observable<TableData[]> {
    const url = `ChartData/bigTable?model=${model}`;
    return this._http.get(url)
    .pipe(
      map(data => {
        Object.keys(data.data).forEach(countryKey => {
          Object.keys(data.data[countryKey]).forEach(dataTypeKey => {
            const orderedObj = {};
            const unorderedObj = data.data[countryKey][dataTypeKey];

            Object.keys(data.data[countryKey][dataTypeKey]).sort((element1, element2) => {
              return data.data[countryKey][dataTypeKey][element1].date > data.data[countryKey][dataTypeKey][element2].date ? 1 : -1;
            }).forEach(elem => {
              orderedObj[elem] = unorderedObj[elem];
            });

            data.data[countryKey][dataTypeKey] = orderedObj;
          });
        });

        return data;
      })
    );
  }

  /**
   * Pobiera dane dla tabeli danych - dane godzinowe
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getTableDataHourly(model: string, date?: string): Observable<TableData[]> {
    let url = `ChartData/bigTableHourly?model=${model}`;
    if (date) {
      url += `&date=${date}`;
    }
    return this._http.get(url);
  }

  /**
   * Sprawdza godzinę o ktorej musi zostać wywołany update danych
   *
   * @returns void
   */
  public checkDashbordUpdateTime() {
    const timeArray: Array<string> = [];
    this._http.get('UpdateTimes').subscribe(times => {
      for (const time of times) {
        timeArray.push(time['time'].toString());
      }
    });

    return setInterval(() => {
      const UTCtime = moment.utc(moment()).format('HH:mm').toString();
      for (const time of timeArray) {
        if (time.includes(UTCtime)) {
          this._sharedService.setUpdateState(true);
        }
      }
    }, 60000);
  }
}

