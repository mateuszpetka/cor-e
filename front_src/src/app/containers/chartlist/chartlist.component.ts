import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BrowserStateService } from './../../shared/browser-state.service';
import { isEmpty } from 'lodash';
import { forkJoin, Observable, of as observableOf, Subject, Subscription, BehaviorSubject } from 'rxjs';
import {
  combineLatest,
  debounceTime,
  delay,
  distinctUntilChanged,
  filter,
  map,
  switchMap,
  take,
  takeUntil,
  tap
} from 'rxjs/operators';

import { ChartsStateService } from '../../shared/chars-state.service';
import { BoxplotChartCustomOption, DataSetsBubble } from './../../classes/boxplot';
import { ChartCustomOption, ChartData, DataSets, ModelRunDates } from './../../classes/chart';
import {
  AvailabilityTableData,
  ChartQueryParams,
  ChartState,
  Country,
  CountryWithTitle,
  RunDatesResponse,
  TableData,
} from './../../classes/classes';
import { SharedService } from './../../shared/shared.service';
import { ChartListHttpService } from './chartlist-http.service';
import { ChartListView } from './chartlist-view.class';
import { ChartListService } from './chartlist.service';
import { AppService } from 'app/app.service';

@Component({
  selector: 'chart-list',
  templateUrl: './chartlist.component.html',
  styleUrls: ['./chartlist.component.scss'],
})
export class ChartlistComponent extends ChartListView implements OnInit, OnDestroy {
  public queryParams: ChartQueryParams = new ChartQueryParams(); // parametry wykresu

  private tableDataSrc: BehaviorSubject<TableData[]>
  = new BehaviorSubject<TableData[]>([]); // subject danych przekazywanych do komponentu data-table
  public tableData$: Observable<TableData[]> = this.tableDataSrc.asObservable(); // obserwator stworzony z subjecta,
  // przekazywany do komponentu data-table

  // CHART RENEWABLES ZMIENNE PRZEKAZYWANE W INPUTACH
  public renewablesWindRunDates: Subject<ModelRunDates> =
  new Subject<ModelRunDates>(); // subject run dates dla seri danych Wind
  // Obserwator stworzony z subjecta, przekazywany do komponentu
  public renewablesWindRunDates$: Observable<ModelRunDates> = this.renewablesWindRunDates.asObservable();
  // subject run dates dla seri danych Solar
  public renewablesSolarRunDates: Subject<ModelRunDates> = new Subject<ModelRunDates>();
  // Obserwator stworzony z subjecta, przekazywany do komponentu
  public renewablesSolarRunDates$: Observable<ModelRunDates> = this.renewablesSolarRunDates.asObservable();
  // subject run dates dla seri danych Combined
  public renewablesCombinedRunDates: Subject<ModelRunDates> = new Subject<ModelRunDates>();
  // Obserwator stworzony z subjecta, przekazywany do komponentu
  public renewablesCombinedRunDates$: Observable<ModelRunDates> = this.renewablesCombinedRunDates.asObservable();
  public renewablesQueryParams: ChartQueryParams; // opcje query paramas wykresu renewables

  // CHART DEMAND  ZMIENNE PRZEKAZYWANE W INPUTACH
  // subject run dates dla seri danych Demand
  public demandRunDates: Subject<ModelRunDates> = new Subject<ModelRunDates>();
  // Obserwator stworzony z subjecta, przekazywany do komponentu
  public demandRunDates$: Observable<ModelRunDates> = this.demandRunDates.asObservable();
  // subject run dates dla seri danych Residual Demand
  public residualDemandRunDates: Subject<ModelRunDates> = new Subject<ModelRunDates>();
  // Obserwator stworzony z subjecta, przekazywany do komponentu
  public residualDemandRunDates$: Observable<ModelRunDates> = this.residualDemandRunDates.asObservable();
  public demandQueryParams: ChartQueryParams; // opcje query paramas wykresu demand

  // CHART RISK MANAGMENT(BOXPLOT) ZMIENNE PRZEKAZYWANE W INPUTACH
  // public riskOptions: BoxplotChartCustomOption; // zmienna przechowująca dane wykresu risk managment
  public riskQueryParams: ChartQueryParams; // opcje query paramas wykresu risk managment

  // CHART AVAILABILITY  ZMIENNE PRZEKAZYWANE W INPUTACH
  public availabilityTableData: AvailabilityTableData[] = []; // zmienna przechowująca dane tabeli - Ongoing outages
  // subject run dates wykresu availability
  public availabilityRunDates: Subject<string[]> = new Subject<string[]>();
  // Obserwator stworzony z subjecta, przekazywany do komponentu
  public availabilityRunDates$: Observable<string[]> = this.availabilityRunDates.asObservable();
  public availabilityQueryParams: ChartQueryParams; // opcje query paramas wykresu availability

  // CHART PRICES ZMIENNE PRZEKAZYWANE W INPUTACH
  // subject run dates wykresu prices
  public pricesRunDates: Subject<ModelRunDates> = new Subject<ModelRunDates>();
  // Obserwator stworzony z subjecta, przekazywany do komponentu
  public pricesRunDates$: Observable<ModelRunDates> = this.pricesRunDates.asObservable();
  public pricesQueryParams: ChartQueryParams; // opcje query paramas wykresu prices

  // CHART HYDRO ZMIENNE PRZEKAZYWANE W INPUTACH
  public hydroRunDates: Subject<string[]> = new Subject<string[]>();  // subject run dates wykresu prices
  // Obserwator stworzony z subjecta, przekazywany do komponentu
  public hydroRunDates$: Observable<string[]> = this.hydroRunDates.asObservable();
  public hydroQueryParams: ChartQueryParams; // opcje query paramas wykresu hydro

  // CHART WEATHER  ZMIENNE PRZEKAZYWANE W INPUTACH
  // source of run dates for isotherm kind, gets value after request to db
  public isothermRunDates: Subject<ModelRunDates> = new Subject<ModelRunDates>();
  // Obserwator stworzony z subjecta, przekazywany do komponentu
  public isothermRunDates$: Observable<ModelRunDates> = this.isothermRunDates.asObservable();
  // source of run dates for temeprature kind, gets value after request to db
  public temperatureRunDates: Subject<ModelRunDates> = new Subject<ModelRunDates>();
  // Obserwator stworzony z subjecta, przekazywany do komponentu
  public temperatureRunDates$: Observable<ModelRunDates> = this.temperatureRunDates.asObservable();
  // source of run dates for temeprature kind, gets value after request to db
  public temperatureDifferenceRunDates: Subject<ModelRunDates> = new Subject<ModelRunDates>();
  // Obserwator stworzony z subjecta, przekazywany do komponentu
  public temperatureDifferenceRunDates$: Observable<ModelRunDates> = this.temperatureDifferenceRunDates.asObservable();
  public weatherQueryParams: ChartQueryParams; // opcje query paramas wykresu weather

  private _updateStateSub: Subscription; // subsktypcja na "automatyyczny" update o określonej porze
  private _boxPlotOffsetSub: Subscription; // subksktypcja wywoływana przy zmianach parametrow w wykresie prices
  private _currentModels: string[]; // stores selected models from filters
  private _currentCountries: Country[]; // stores current countries from filters

  private _firstCountryIso: string = ''; // stores first country (from global filters list) iso code. Used for making initial requests to db
  public bigChartFullscreen: boolean;
  public bigTableDay: string;
  public bigTableDaysAvailable: [boolean, boolean] = [true, true];

  public subscriptions: Subscription = new Subscription();

  constructor(
    private titleService: Title,
    private _chartListHttpService: ChartListHttpService,
    private sharedService: SharedService,
    private _chartsStateService: ChartsStateService,
    private chartListService: ChartListService,
    private browserStateService: BrowserStateService,
    private _appService: AppService
    ) {
    super(browserStateService, sharedService, chartListService);
    this.titleService.setTitle('Dashboard');
  }

  ngOnInit() {
    this.checkBrowser();
    this.startUpdateInterval();
    this.startWindowsResizeAndCollapseFilters();
    this.startUpdateDashbordSubscription();
    this.assignLoadersObservablesFromServiceToVariables();
    this.startFullscreenSubscription();
    this.startBoxPlotChartSubscription();
    this.startMenuStateSubscription();
    this.startCombinedSubstriptionForModelsAndCountriesFilters();
    this.startSubscriptionForSelectedCountriesOnly();
  }

  /**
   * Inicjalizuje subsktypcje updatu wykresu - update ma zachodzi o odpowiednich godzinach
   *
   * @returns void
   */
  startUpdateDashbordSubscription(): void {
    this._updateStateSub = this.sharedService.updateDashboard$.pipe(
      takeUntil(this.guard$),
      filter(Boolean),
      delay(200),
      filter((updateStatus: boolean) => !isEmpty(this._currentCountries) && !isEmpty(this._currentModels) && updateStatus)
    ).subscribe(() => {
      this.getInitialData();
      this.getInitialAvailabilityAndHydroData();
      this.sharedService.setUpdateState(false);
    });
  }

  /**
   * Inicjaluzuje subsktypcje, wymuszjącą zmianę danych w wykresie Risk Managment - przy zmianiach w wykresie Prices
   *
   * @returns void
   */
  startBoxPlotChartSubscription(): void {
    this._boxPlotOffsetSub = this.sharedService.newBoxPlotOffset$
      .pipe(
        takeUntil(this.guard$),
        filter(Boolean)
      ).subscribe((boxplotData) => {
        const params = new ChartQueryParams(boxplotData.offset, boxplotData.runDate, boxplotData.iso);
        this.getRiskManagmentData('', this._currentModels, params);
      });
  }

  /**
   * Inicjalizuje połączoną subskrypcje wybranych modeli oraz krajow
   *
   * @returns void
   */
  startCombinedSubstriptionForModelsAndCountriesFilters(): void {
    const sub = this.sharedService.currentModels$.pipe(
      takeUntil(this.guard$),
      combineLatest(this.sharedService.selectedCountries$),
      debounceTime(200),
      distinctUntilChanged(([prevModel, prevCountries], [nextModel, nextCountries]) => {
        return this.chartListService.arePrevNextCountriesAndModelsEqual(prevModel, prevCountries, nextModel, nextCountries);
      }),
      tap(([models, countries]: [string[], Country[]]) => {
        this._currentCountries = countries;
        this._currentModels = models;

        if (!this._currentCountries.length || !this._currentModels.length) {
          this.setEmptyDataReminder();
        }
      })
    )
    .subscribe(([models, countries]: [string[], Country[]]) => {
      this.checkIfChartsDependentOnModelsAndCountriesShouldBeCleared(models, countries);
    });

    this.subscriptions.add(sub);
  }

  /**
   * Sprawdza długość wybrach modeli oraz krajow - na ich podstawie czyści wykresy lub pobiera dla wykresow zależnych od modeli i krajow
   *
   * @param  {string[]} models
   * @param  {Country[]} countries
   * @returns void
   */
  checkIfChartsDependentOnModelsAndCountriesShouldBeCleared(models: string[], countries: Country[]): void {
    if (!models.length || !countries.length) {
      this.clearChartsExceptHydroAndAvailability(models, countries);
      this.hideDataTable();
    } else if (models.length && countries.length) {
      this.setInitalCountry(countries[0]);
      this.getInitialData();
    }
  }

  /**
   * Sprawdza długość wybranych krajow - na jej podstawie czyście wykresy niezależne od wybranych modeli
   *
   * @param  {Country[]} countries
   * @returns void
   */
  checkIfDependentOnlyOnCountriesShouldBeCleared(countries: Country[]): void {
    if (!countries.length) {
      this.clearHydroAndAvailabilityCharts();
      this.hideDataTable();
    }
  }

  /**
   * Sprawdza pierwszy kraj zostal zmieniony
   *
   * @returns void
   */
  startSubscriptionForSelectedCountriesOnly(): void {
    this.sharedService.selectedCountries$.pipe(
      takeUntil(this.guard$),
      debounceTime(200),
      distinctUntilChanged((prevCountries, nextCountries) => {
        if (prevCountries[0] !== nextCountries[0]) {
          return false;
        } else {
          return true;
        }
      })
    )
      .pipe(
        tap((countries: Country[]) => {
          this.checkIfDependentOnlyOnCountriesShouldBeCleared(countries);
        }),
        filter(countries => countries.length > 0)
      )
      .subscribe((countries: Country[]) => {
        this.setInitalCountry(countries[0]);
        this.getInitialAvailabilityAndHydroData();
      });
  }

  /**
   * Ustawia początkowy kraj - pierwszy element wybranej listy krajow
   *
   * @param  {Country} initialCountry
   * @returns void
   */
  setInitalCountry(initialCountry: Country): void {
    this.sharedService.setActiveCountry(initialCountry);
    this._firstCountryIso = initialCountry.iso;
  }

  /**
   * Pobiera dane z serwisu dla wykresow
   *
   * @returns void
   */
  getInitialData(): void {
    let iso = this.getInitialCountry(`renewablesState`);
    this.getRenewablesData(iso, this._currentModels);

    if (this._currentModels.length) {
      iso = this.getInitialCountry(`weatherState`);
      this.getWeatherData(iso, this._currentModels);
      iso = this.getInitialCountry(`pricesState`);
      this.getPricesData(iso, this._currentModels);
    }

    this.getRiskManagmentData(iso, this._currentModels);

    iso = this.getInitialCountry(`demandState`);
    this.getDemandData(iso, this._currentModels);

    iso = this.getInitialCountry(`tableState`);
    this.bigTableDay
      ? this.getTableData(this._currentModels, this.bigTableDay)
      : this.getTableData(this._currentModels);
  }

  /**
   * Gets initial data for availability and hydro charts
   *
   * @returns void
   */
  getInitialAvailabilityAndHydroData(): void {
    let iso = this.getInitialCountry(`availabilityState`);
    this.getAvailabilityData(iso);

    iso = this.getInitialCountry(`hydroState`);
    this.getHydroRiverData(iso);

    if (this._currentModels && !this._currentModels.length) {
      iso = this.getInitialCountry(`weatherState`);
      this.getWeatherData(iso);
      iso = this.getInitialCountry(`pricesState`);
      this.getPricesData(iso);
    }
  }

  /**
   * Sprawdza godzinę o ktorej musi zajść update
   *
   * @returns void
   */
  startUpdateInterval(): void {
    this._updateInterval = this._chartListHttpService.checkDashbordUpdateTime();
  }

  /**
   * Get data for renewables chart, based od country, model and days
   *
   * @param  {ChartQueryParams} queryParams
   * @returns void
   */
  getRenewablesData(iso: string, currentModels?: string[], changeRange?: boolean): void {
    const queryParams = new ChartQueryParams();
    queryParams.iso = iso;

    this.sharedService.changeLoaderState('renewables', true); // hide chart

    // W przypadku pobierana danych dla kilku modeli jednocześnie
    const currentModels$ = observableOf(currentModels);

    currentModels$
      .pipe(
        takeUntil(this.guard$),
        switchMap((models) => {
          return this.chartListService.getRenewablesDataBasedOnModels(models, queryParams);
        }),
        map(([data, rundates]: [ChartData[], RunDatesResponse[]]) => {
          this.assignRenewablesRunDatesResponse(rundates);
          return data;
        })
      ).subscribe((data: ChartData[]) => {
        this.renewablesOptions = new ChartCustomOption(data, 'Renewables');
        this.renewablesQueryParams = queryParams;
      },
        (error) => {
          this.sharedService.checkIfUsersIsAlreadyLoggedIn(error);
        },
        () => {
          this.sharedService.changeLoaderState('renewables', false); // show chart
          setTimeout(() => {
            this.resizeCanvasInChart('Renewables');
          }, 500);
        });
  }

  /**
   * Filtruje i pushuje wartości runDates
   *
   * @param  {RunDatesResponse[]} rundates
   * @returns void
   */
  assignRenewablesRunDatesResponse(rundates: RunDatesResponse[]): void {
    const windRunDates = this.chartListService.filterRunDatesByKind(rundates, 'wind');
    this.renewablesWindRunDates.next(windRunDates);
    const solarRunDates = this.chartListService.filterRunDatesByKind(rundates, 'solar');
    this.renewablesSolarRunDates.next(solarRunDates);
    const combinedRunDates = this.chartListService.filterRunDatesByKind(rundates, 'combined');
    this.renewablesCombinedRunDates.next(combinedRunDates);
  }

  /**
   * Get data for availability chart, based od country, model and days
   *
   * @param  {ChartQueryParams} queryParams
   * @returns void
   */
  getAvailabilityData(iso: string): void {
    this.sharedService.changeLoaderState('availability', true); // hide chart
    const queryParams = new ChartQueryParams();
    queryParams.iso = iso;

    this.sharedService.getAvailabilityKinds().pipe(
      switchMap(kinds => {
        const requests = [];
        requests.push(this._chartListHttpService.getAvailabilityData(kinds, queryParams));
        return forkJoin(requests);
      }),
      switchMap((resData: ChartData[]) => {
        return this._chartListHttpService.getAvailabilityChartData(queryParams.iso).pipe(map(res => [resData, res]));
      }),
      switchMap(([resData, tableDataRes]: [ChartData[], AvailabilityTableData[]]) => {
        return this._chartListHttpService.getAvailabilityRunDates(queryParams)
          .pipe(
            map((resRunDates: string[]) => [resData, tableDataRes, resRunDates])
          );
      }),
    ).subscribe(([resData, tableDataRes, resRunDates]: [ChartData[], AvailabilityTableData[], string[]]) => {
      this.availabilityOptions = new ChartCustomOption(resData[0], 'Availability');
      this.availabilityRunDates.next(resRunDates);
      this.availabilityTableData = tableDataRes;
      this.availabilityQueryParams = queryParams;
    },
      (error) => {
        this.sharedService.checkIfUsersIsAlreadyLoggedIn(error);
      },
      () => {
        this.sharedService.changeLoaderState('availability', false); // show chart
        setTimeout(() => {
          this.resizeCanvasInChart('Availability');
        }, 500);
      });
  }

  /**
   * Get data for prices chart, based od country, model and days
   *
   * @param  {ChartQueryParams} queryParams
   * @returns void
   */
  getPricesData(iso: string, currentModels?: string[]): void {
    const queryParams = new ChartQueryParams;
    queryParams.iso = iso;

    this.sharedService.changeLoaderState('prices', true); // hide chart
    const currentModels$ = observableOf(currentModels);

    const currModelPipe = currentModels$
      .pipe(
        switchMap((models: string[]) => {
          models = models ? models : [];

          return this.chartListService.getPricesDataBasedOnModelsAndSubModels(models, queryParams);
        }),
        map(([data, runDatesRes]: [ChartData[], RunDatesResponse[]]) => {
          this.assignPricesRunDates(runDatesRes);
          return data;
        })
      );

    this.chartListService.getPricesRealisedData(queryParams).pipe(
      switchMap(chartData => {
        chartData.series = chartData.series
        .map((val) => {
          return {
            name: val.flow_date,
            value: val.value
          };
        });
        this.pricesOptions = new ChartCustomOption([], 'Prices');
        this.pricesOptions.data.datasets.forEach(dataset => {
          dataset.remove();
        });

        // kolor powinien byc ustalany na podstawie ustawionego motywu, jak motyw czarny - to wykres bialy
        const color = this.getRealisedColorBasedOnTheme(this.getCurrentTheme());
        const dataSetStatic = new DataSets(chartData.name, chartData.series, '', chartData.kind, null, null, color);

        dataSetStatic.setStyles({
          pointStyle: 'rectRounded',
          pointRadius: 4,
          pointBorderColor: color,
          borderWidth: 1,
          pointBorderWidth: 2,
          pointHoverRadius: 5
        });

        this.pricesOptions.data.datasets.push(dataSetStatic);

        return currModelPipe;
      })
    ).subscribe(resData => {
      resData.forEach((data, index) => {
        const removalIndex = this.pricesOptions.data.datasets.findIndex(elem => {
          return elem.kind === data.kind && elem.model === data.model && elem.name === data.name && elem.runDate === data.runDate;
        });

        if (removalIndex > -1) {
          this.pricesOptions.data.datasets = this.pricesOptions.data.datasets.splice(removalIndex, 1);
        }

        this.pricesOptions.data.datasets.push(new DataSets(data.name, data.series, data.model, data.kind, data.runDate, 0));
      });

      this.pricesQueryParams = queryParams;
    },
      (error) => {
        this.sharedService.checkIfUsersIsAlreadyLoggedIn(error);
      },
      () => {
        this.sharedService.changeLoaderState('prices', false); // show chart
        setTimeout(() => {
          this.resizeCanvasInChart('Prices');
        }, 500);
      }
    );
  }

  /**
   * Filtruje i pushuje run datesdla wykresu Prices
   *
   * @param  {RunDatesResponse[]} runDatesRes
   * @returns void
   */
  assignPricesRunDates(runDatesRes: RunDatesResponse[]): void {
    const runDates = this.chartListService.filterRunDatesByKind(runDatesRes, 'prices');
    this.pricesRunDates.next(runDates);
  }

  /**
   * Get data for prices chart, based od country, model and days
   *
   * @param  {ChartQueryParams} queryParams
   * @returns void
   */
  getDemandData(iso: string, currentModels?: string[]): void {
    this.sharedService.changeLoaderState('demand', true); // hide chart
    const queryParams = new ChartQueryParams();
    queryParams.iso = iso;

    const currentModels$ = observableOf(currentModels);
    currentModels$
      .pipe(
        takeUntil(this.guard$),
        switchMap((models) => {
          return this.chartListService.getDemandDataBasedOnModel(models, queryParams);
        }),
        map(([chartData, runDatesRes]: [ChartData[], RunDatesResponse[]]) => {
          this.assignDemandRunDates(runDatesRes);
          return chartData;
        })
      ).subscribe((chartData: ChartData[]) => {
        this.demandOptions = new ChartCustomOption(chartData, 'Demand');
        this.demandQueryParams = queryParams;
      },
        (error) => {
          this.sharedService.checkIfUsersIsAlreadyLoggedIn(error);
        },
        () => {
          this.sharedService.changeLoaderState('demand', false);
          setTimeout(() => {
            this.resizeCanvasInChart('Demand');
          }, 500);
        });
  }

  /**
   * Przypisuje dane z subskrypcji do zmiennych
   *
   * @param  {RunDatesResponse[]} runDatesRes
   * @returns void
   */
  assignDemandRunDates(runDatesRes: RunDatesResponse[]): void {
    const demandRunDates = this.chartListService.filterRunDatesByKind(runDatesRes, 'demand');
    const residualDemandRunDates = this.chartListService.filterRunDatesByKind(runDatesRes, 'residual demand');

    this.demandRunDates.next(demandRunDates);
    this.residualDemandRunDates.next(residualDemandRunDates);
  }

  /**
  * Get data for availability chart, based od country, model and days
  *
  * @param  {ChartQueryParams} queryParams
  * @returns void
  */
  getWeatherData(iso: string, currentModels?: string[]): void {
    const queryParams = new ChartQueryParams;
    queryParams.iso = iso;

    const currentModels$ = observableOf(currentModels);
    this.sharedService.changeLoaderState('weather', true);

    const currModelPipe = currentModels$.pipe(
      switchMap(models => {
        models = models ? models : [];

        return this.chartListService.getWeatherDataBasedOnModels(models, queryParams);
      }),
      map(([chartData, runDates]: [ChartData[], RunDatesResponse[]]) => {
        this.assignWeatherRunDates(runDates);
        return chartData;
      })
    );

    this.weatherQueryParams = queryParams;

    this.chartListService.getStaticWeatherData(queryParams).pipe(
      switchMap(chartData => {
        chartData.series = chartData.series.map((val) => {
          return {
            name: val.flow_date,
            value: val.value
          };
        });

        this.weatherOptions = new ChartCustomOption([], 'Weather');
        this.weatherOptions.data.datasets.forEach(dataset => {
          dataset.remove();
        });

        const dataSetStatic = new DataSets(chartData.name, chartData.series, '', chartData.kind, '', null, '#909090');
        dataSetStatic.pointRadius = 0;
        this.weatherOptions.data.datasets.push(dataSetStatic);

        return currModelPipe;
      })
    ).subscribe(chartData => {
      chartData.forEach(data => {
        this.weatherOptions.data.datasets.push(new DataSets(data.name, data.series, data.model, data.kind, data.runDate, 0));
      });

      this.weatherQueryParams = queryParams;
    },
      (error) => {
        this.sharedService.checkIfUsersIsAlreadyLoggedIn(error);
      },
      () => {
        this.sharedService.changeLoaderState('weather', false);
        setTimeout(() => {
          this.resizeCanvasInChart('Weather');
        }, 500);
      });
  }

  /**
   * Przypisuje dane z subskrypcji do zmiennych
   *
   * @param  {RunDatesResponse[]} runDates
   * @returns void
   */
  assignWeatherRunDates(runDates: RunDatesResponse[]): void {
    const isothermRunDates = this.chartListService.filterRunDatesByKind(runDates, 'isotherm');
    const temperatureRunDates = this.chartListService.filterRunDatesByKind(runDates, 'temperature');
    const temperatureDifferenceRunDates = this.chartListService.filterRunDatesByKind(runDates, 'calculated difference');

    this.isothermRunDates.next(isothermRunDates);
    this.temperatureRunDates.next(temperatureRunDates);
    this.temperatureDifferenceRunDates.next(temperatureDifferenceRunDates);
  }

  /**
   * Wywołuje pobieranie danych dla wykresu Risk Managment (Boxplot)
   *
   * @param  {string} iso
   * @param  {string[]} currentModels?
   * @param  {ChartQueryParams} params?
   * @returns void
   */
  getRiskManagmentData(iso: string, currentModels?: string[], params?: ChartQueryParams): void {
    const queryParams: ChartQueryParams = this.chartListService.checkResidualDemandParams(iso, params);
    const currentModels$ = observableOf(currentModels);
    this.sharedService.changeLoaderState('risk', true);

    currentModels$.pipe(
      switchMap((models: string[]) => {
        return forkJoin([
          this.chartListService.getResidualDemandDataBasedOnModels(models, queryParams),
          this._chartListHttpService.getBoxplotRealisedData(queryParams)
        ]);
      }),
      take(1)
    )
      .subscribe(([resData, realisedData]: [any, any]) => {
        this.riskOptions = new BoxplotChartCustomOption(resData, 'Risk Management');

        const color = this.getRealisedColorBasedOnTheme(this.getCurrentTheme());

        const bubbleData = new DataSetsBubble(realisedData.series);
        bubbleData.borderColor = color;

        const boxplotData = this.riskOptions.data.datasets;

        this.riskOptions.data.datasets = [];
        this.riskOptions.data.datasets.push(bubbleData);
        this.riskOptions.data.datasets.push.apply(this.riskOptions.data.datasets, boxplotData);
      },
        (error) => {
          this.sharedService.checkIfUsersIsAlreadyLoggedIn(error);
        },
        () => {
          this.sharedService.changeLoaderState('risk', false);
          setTimeout(() => {
            this.resizeCanvasInChart('Risk Management');
          }, 500);
        });
  }

  /**
   * Wywołuje pobieranie danych dla wykresu Hydro
   *
   * @param  {string} iso
   * @returns void
   */
  getHydroRiverData(iso: string): void {
    const queryParams = new ChartQueryParams();
    queryParams.iso = iso;

    this.sharedService.changeLoaderState('hydro', true);

    const runDates$ = this._chartListHttpService.getHyroRunDates(queryParams);
    const chartData$ = this._chartListHttpService.getHyroRiverData(queryParams);

    forkJoin(chartData$, runDates$)
      .subscribe(([chartData, runDates]: [ChartData, string[]]) => {
        this.hydroOptions = new ChartCustomOption(chartData, 'Hydro', '#259ca7');
        this.hydroRunDates.next(runDates);
        this.hydroQueryParams = queryParams;
      },
        (error) => {
          this.sharedService.checkIfUsersIsAlreadyLoggedIn(error);
        },
        () => {
          this.sharedService.changeLoaderState('hydro', false);
          setTimeout(() => {
            this.resizeCanvasInChart('Hydro');
          }, 500);
        });
  }

  /**
   * Wywołuje pobieranie danych tabeli danych
   *
   * @param  {string[]} currentModels
   * @returns void
   */
  getTableData(currentModels: string[], day?: string): void {
    this.sharedService.changeLoaderState('table', true);
    this.sharedService.changeTableVisibility(false);

    const currentModels$ = observableOf(currentModels);
    currentModels$.pipe(
      switchMap((models: string[]) => {
        return day
          ? this.chartListService.getTableDataBasedOnModels(models, day)
          : this.chartListService.getTableDataBasedOnModels(models);
      })).subscribe(tableDataResponse => {
        this.tableDataSrc.next(tableDataResponse);
      },
        (error) => {
          this.sharedService.checkIfUsersIsAlreadyLoggedIn(error);
        },
        () => {
          setTimeout(() => {
            this.sharedService.changeLoaderState('table', false);
            this.sharedService.changeTableVisibility(true);
          }, 500);
        });
  }

  /**
   * Zmiana aktualnie wyświetlanego dnia w bigTableHourly
   *
   * @param  {string} day
   * @returns void
   */
  changeBigTableDay(day: string): void {
    this.bigTableDay = day;

    if (!this.chartListService.isHourlyDataPresent(day)) {
      this.sharedService.changeLoaderState('table', true);
      this.sharedService.changeTableVisibility(false);
    }

    this.chartListService.getTableDataHourly(this._currentModels, day).subscribe(value => {
      const tableData = this.tableDataSrc.getValue()[0];

      let isPrevDataAvailable = false;
      let isNextDataAvailable = false;

      value.forEach(elem => {
        if (elem.isPrevData) {
          isPrevDataAvailable = true;
        }

        if (elem.isNextData) {
          isNextDataAvailable = true;
        }
      });

      this.bigTableDaysAvailable = [isPrevDataAvailable, isNextDataAvailable];

      this.tableDataSrc.next([tableData, value]);
    }, (error) => {
      this.sharedService.checkIfUsersIsAlreadyLoggedIn(error);
    }, () => {
      setTimeout(() => {
        this.sharedService.changeLoaderState('table', false);
        this.sharedService.changeTableVisibility(true);
      }, 500);
    });
  }

  /**
   * Pobiera dane dla wszystkich wykresow przy zmianie kraju wewnątrz wykresu
   *
   * @param  {CountryWithTitle} data
   * @param  {number} ?offset
   * @returns void
   */
  changeData(data: CountryWithTitle, offset?: number): void {
    const chartTitle = data.title;
    const country = data.country;
    const iso = country.iso;

    switch (chartTitle) {
      case 'Renewables':
        {
          this.getRenewablesData(iso, this._currentModels);
          break;
        }
      case 'Availability':
        {
          this.getAvailabilityData(iso);
          break;
        }
      case 'Prices':
        {
          this.getPricesData(iso, this._currentModels);
          this.getRiskManagmentData(iso, this._currentModels);
          break;
        }
      case 'Weather':
        {
          this.getWeatherData(iso, this._currentModels);
          break;
        }
      case 'Demand':
        {
          this.getDemandData(iso, this._currentModels);
          break;
        }
      case 'Hydro':
        {
          this.getHydroRiverData(iso);
          break;
        }
    }
  }

  /**
   * Pobiera i zwraca z serwisu początkowy kod iso kraju
   *
   * @param  {string} chartState
   * @returns string
   */
  getInitialCountry(chartState: string): string {
    const tempChartState: ChartState = this._chartsStateService.getChartState(chartState) as ChartState;
    if (!!tempChartState && !!tempChartState.country) {
      return tempChartState.country;
    } else {
      return this._firstCountryIso;
    }
  }

  /**
   * Przypomnienie użytkownikowi po 3 sekundach, że brakuje mu niektórych ustawień
   * niezbędnych do wyświetlenia danych
   *
   * @returns void
   */
  setEmptyDataReminder(): void {
    setTimeout(() => {
      if (!this._currentModels.length || !this._currentCountries.length) {
        this.sharedService.openDialog('noParamsSelected');
      }
    }, 5000);
  }

  /**
   * Pobiera aktualny motyw aplikacji
   *
   * @returns string
   */
  getCurrentTheme(): string {
    const themeFromSubject = this._appService.getThemeClass() || this._appService.getUserFromLocalStorage().customFields.themeDisplay;
    const themeName = !!themeFromSubject
    ? themeFromSubject
    : 'default_theme';

    return themeName;
  }

  /**
   * Pobiera kolor dla wykresow zrealizowanych na podstawie motywu aplikacji
   *
   * @param  {string} theme
   * @returns string
   */
  getRealisedColorBasedOnTheme(theme: string): string {
    switch (theme) {
      case 'dark_theme':
        return '#FEFEFE';
      default:
        return '#909090';
    }
  }

  ngOnDestroy() {
    this.guard$.next();
    clearInterval(this._updateInterval);
    this.subscriptions.unsubscribe();
  }
}
