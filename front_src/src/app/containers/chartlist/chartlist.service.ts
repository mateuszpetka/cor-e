import { ElementRef, Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { Observable, forkJoin, of } from 'rxjs';

import { ChartData, ModelRunDates } from 'app/classes/chart';
import { ChartQueryParams, Country, RunDatesResponse } from 'app/classes/classes';
import isEmpty from 'lodash/isEmpty';

import {
  ChartListHttpService
} from './chartlist-http.service';
import { ChartDataBox } from 'app/classes/boxplot';
import { map, tap } from 'rxjs/operators';
import { isEqual, sortBy } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ChartListService {
  private _renderer: Renderer2;

  public chartHeight: number;
  public chartWidth: number;
  public chartTop: number;
  public chartLeft: number;

  private chartElement: HTMLElement;
  private chartList: HTMLElement;
  private chartContainerPlacement: ClientRect;
  private chartListPlacement: ClientRect;
  private deltaX: number;
  private deltaY: number;

  private bigTableBackupHourlyData: {
    date: string;
    models: string[],
    data: {
      data: {
        load: {
          value: number;
          date: string;
          hour: string;
        },
        wind: {
          value: number;
          date: string;
          hour: string;
        }
      },
      model: string;
    }[]
  }[] = [];

  constructor(
    rendererFactory: RendererFactory2,
    private _chartListHttpService: ChartListHttpService
  ) {
    // Stworzenie nowej instancji renederera
    this._renderer = rendererFactory.createRenderer(null, null);
  }

  /**
   * Porownuje poprzednią i nastepną wartość listy krajow (wybrane z filtrow głownych), nastepnie porownuje te wartości czy się zmieniły
   * oraz czy nie są puste
   *
   * @param  {Country[]} prevCountries
   * @param  {Country[]} nextCountries
   * @param  {string[]} prevModel
   * @param  {string[]} nextModel
   * @returns boolean
   */
  public arePrevNextCountriesAndModelsEqual(prevModel: string[], prevCountries: Country[],
    nextModel: string[], nextCountries: Country[]): boolean {
    if (prevCountries[0] !== nextCountries[0] && nextModel.length) {
      return false;
    } else if ((prevCountries.length === 0 && nextCountries.length) && nextModel.length &&
      prevCountries[0] !== nextCountries[0]) {
      return false;
    } else if (prevModel.length !== nextModel.length && nextCountries.length) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * Porownuje poprzednią i nastepną wartość listy krajow, bez uwzględnienia modelu
   *
   * @param  {Country[]} prevCountries
   * @param  {Country[]} nextCountries
   * @returns boolean
   */
  public arePrevNextCountriesEqual(prevCountries: Country[], nextCountries: Country[]): boolean {
    return prevCountries[0] === nextCountries[0];
  }

  /**
   * Ustawia pozycje listy wykresow oraz powiekszanego elementu, oblicza rożnice pozycji
   *
   * @param  {HTMLElement} element
   * @param  {ElementRef} chartListRef
   * @returns void
   */
  public setChartListBoundingClientProperties(element: HTMLElement, chartListRef: ElementRef): void {
    this.chartList = chartListRef.nativeElement;
    this.chartElement = element;
    this.chartContainerPlacement = element.parentElement.getBoundingClientRect();
    this.chartListPlacement = this.chartList.getBoundingClientRect();
    this.deltaX = this.chartListPlacement.left - this.chartContainerPlacement.left;
    this.deltaY = this.chartListPlacement.top - this.chartContainerPlacement.top;
  }

  /**
   * Dodaje style css odpowiedzialne za wygląd wykresu w fullscreenie
   *
   * @returns void
   */
  public addCssPropertiesForFullScreenMode(): void {
    $(this.chartElement).parent().addClass('chartFullScreenContainer');
    $('.chart-list').addClass('chartListFullScreen');
    $('.params__colapse').addClass('disableToggle');
  }

  /**
   * Usuwa style css odpowiedzialne za wygląd wykresu po wyłaczeniu wykresu
   *
   * @returns void
   */
  public removeCssPropertiesForFullScreenMode(): void {
    $(this.chartElement).parent().removeClass('chartFullScreenContainer');
    $('.chart-list').removeClass('chartListFullScreen');
    $('.params__colapse').removeClass('disableToggle');
  }

  /**
   * Resetuje zmienne używane do obliczeń animacji
   *
   * @returns void
   */
  private resetChartPosition(): void {
    this.chartHeight = this.chartContainerPlacement.height;
    this.chartWidth = this.chartContainerPlacement.width;
    this.chartTop = 0;
    this.chartLeft = 0;
  }

  /**
   * Uruchamia animacje rozszerzenia wykresu do wielkości listy wykresow
   *
   * @returns void
   */
  public animateFullscreenOn(): void {
    this.resetChartPosition();
    // Animacja w jQUery bo jest wspierana przez wszystkie przegladarki
    $('.chartListFullScreen').animate({ scrollTop: 0 }, 100);

    $(this.chartElement).parent()
      .css({
        'position': 'absolute'
      })
      .animate({
        width: (0.55 * this.chartListPlacement.width) + 'px',
        height: 'auto',
        zIndex: '9999',
        left: -`${this.deltaX * 0.5}` + 'px',
        right: -`${this.deltaX * 0.5}` + 'px',
        top: '0',
        bottom: '10px'
      }, 50)
      .animate({
        width: (0.99 * this.chartListPlacement.width) + 'px',
        height: 'auto',
        left: '0',
        right: '0',
        bottom: '10px'
      }, 50);
  }

  /**
   * Animacja wyłączenia fullscreena
   *
   * @returns void
   */
  public animateFullscreenOff(): void {
    $('.chartFullScreenContainer')
      .animate({
        width: (0.55 * this.chartListPlacement.width) + 'px',
        height: (0.55 * this.chartListPlacement.height) + 'px',
        left: -`${this.deltaX * 0.5}` + 'px',
        right: -`${this.deltaX * 0.5}` + 'px'
      }, 300)
      .animate({
        width: (this.chartWidth) + 'px',
        height: '100%',
        zIndex: '0',
        left: '0'
      }, 250, function () {
        $('.chart-container').removeAttr('style');
      });
  }

  /**
   * Pobiera dane dla wykresu renewables w zależności od wybranych modeli
   *
   * @param  {string[]} models
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getRenewablesDataBasedOnModels(models: string[], queryParams: ChartQueryParams): Observable<[ChartData[], RunDatesResponse[]]> {
    const requests: Observable<ChartData>[] = [];
    const asOfRequests = [];
    const defaultKinds = ['wind', 'solar', 'combined'];
    models.forEach(model => {
      defaultKinds.forEach(kind => {
        const params = queryParams;
        params.model = model;
        asOfRequests.push(
          this._chartListHttpService.getRenewablesRunDates(kind, queryParams)
            .pipe(
              map(data => {
                return {
                  kind,
                  model,
                  runDates: data
                };
              })
            )
        );
        requests.push(this._chartListHttpService.getRenewablesData(kind, params));
      });
    });

    const joinedResData = forkJoin(requests);
    const joinedRunDates = forkJoin(asOfRequests);

    return forkJoin(joinedResData, joinedRunDates);
  }

  /**
   * Pobiera dane dla wykresu Demand w zależności od wybranych modeli
   *
   * @param  {string[]} models
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getDemandDataBasedOnModel(models: string[], queryParams: ChartQueryParams): Observable<[ChartData[], RunDatesResponse[]]> {
    const requests: Observable<ChartData>[] = [];
    const asOfRequests = [];
    models.forEach(model => {
      const params = queryParams;
      params.model = model;

      asOfRequests.push(
        this._chartListHttpService.getDemandRunDates(params)
          .pipe(
            map(data => {
              return {
                kind: 'demand',
                model,
                runDates: data
              };
            })
          )
      );
      asOfRequests.push(
        this._chartListHttpService.getResidualDemandRunDates(params)
          .pipe(
            map(data => {
              return {
                kind: 'residual demand',
                model,
                runDates: data
              };
            })
          )
      );
      requests.push(this._chartListHttpService.getDemandData(queryParams));
      requests.push(this._chartListHttpService.getResidualDemandData(queryParams));
    });

    const joinedResData = forkJoin(requests);
    const joinedRunDates = forkJoin(asOfRequests);
    return forkJoin(joinedResData, joinedRunDates);
  }

  /**
   * Pobiera dane dla wykresu Prices w zależności od wybranych modeli
   *
   * @param  {string[]} models
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getPricesDataBasedOnModelsAndSubModels(models: string[],
    queryParams: ChartQueryParams): Observable<[ChartData[], RunDatesResponse[]]> {
    const requests: Observable<ChartData>[] = [];
    const asOfRequests = [];

    models.forEach(model => {
      queryParams.model = model;
      queryParams.subModel = (model === 'gfs') ? 14 : 17; // 14 - gfs model, 17 - hres model
      asOfRequests.push(this._chartListHttpService.getPricesRunDates(queryParams)
        .pipe(
          map(data => {
            return {
              kind: 'prices',
              model,
              runDates: data
            };
          })
        ));
      requests.push(this._chartListHttpService.getPricesData(queryParams));
    });

    const joinedResData = forkJoin(requests);
    const runDates$ = forkJoin(asOfRequests);

    return forkJoin(joinedResData, runDates$);
  }

  /**
   * Pobiera dane realised prices
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getPricesRealisedData(queryParams: ChartQueryParams): Observable<ChartData> {
    return this._chartListHttpService.getPricesRealisedData(queryParams);
  }

  /**
   * Pobiera dane realised risk management (boxplot)
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getBoxplotRealisedData(queryParams: ChartQueryParams): Observable<ChartData> {
    return this._chartListHttpService.getBoxplotRealisedData(queryParams);
  }

  /**
   * Pobiera dane statycznych danych na wykresie temperatury
   *
   * @param  {ChartQueryParams} params
   * @returns Observable
   */
  public getStaticWeatherData(params: ChartQueryParams): Observable<ChartData> {
    return this._chartListHttpService.getTemperatureStaticCurve(params);
  }

  /**
   * Pobiera dane dla wykresu Weather na podstawie wykranych modeli
   *
   * @param  {string[]} models
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getWeatherDataBasedOnModels(models: string[], queryParams: ChartQueryParams): Observable<[ChartData[], RunDatesResponse[]]> {
    const requests: Observable<ChartData>[] = [];
    const asOfRequests = [];

    models.forEach(model => {
      const params = queryParams;
      params.model = model;

      asOfRequests.push(
        this._chartListHttpService.getIsothermRunDates(params)
          .pipe(
            map(data => {
              return {
                kind: 'isotherm',
                model,
                runDates: data
              };
            })
          )
      );
      asOfRequests.push(
        this._chartListHttpService.getTemperatureRunDates(params)
          .pipe(
            map(data => {
              return {
                kind: 'temperature',
                model,
                runDates: data
              };
            })
          )
      );
      asOfRequests.push(
        this._chartListHttpService.getTemperatureDifferenceRunDates(params)
          .pipe(
            map(data => {
              return {
                kind: 'calculated difference',
                model,
                runDates: data
              };
            })
          )
      );

      requests.push(this._chartListHttpService.getTemperatureWeatherData(params));
      requests.push(this._chartListHttpService.getNewIsothermWeatherData(params));
      requests.push(this._chartListHttpService.getTemperatureDifference(params));
    });

    const joinedResData = forkJoin(requests).pipe(map(res => {
      const data = [];
      res.forEach(dataResponse => {
        if (!isEmpty(dataResponse)) {
          data.push(dataResponse);
        }
      });
      const filteredData = [];
      for (const key in data) {
        if (data[key].length) {
          data[key].forEach(element => {
            filteredData.push(element);
          });
        } else {
          filteredData.push(data[key]);
        }
      }
      return filteredData;
    }));

    const joinedRunDates = forkJoin(asOfRequests);
    return forkJoin(joinedResData, joinedRunDates);
  }

  /**
   * Pobiera dane dla komponentu data-table
   *
   * @param  {string[]} models
   * @returns Observable
   */
  public getTableDataBasedOnModels(models: string[], date?: string): Observable<any> {
    const requests = [];
    const requestsHourly = [];

    models.forEach((model) => {
      requests.push(this._chartListHttpService.getTableData(model));
      requestsHourly.push(date ?
        this._chartListHttpService.getTableDataHourly(model, date) :
        this._chartListHttpService.getTableDataHourly(model)
        );
    });

    const fullViewData$ = forkJoin(requests);
    const hourlyData$ = forkJoin(requestsHourly).pipe(
      tap(hourlyData => {
        this.bigTableBackupHourlyData.push({
          date: date,
          models: models,
          data: hourlyData
        });
      })
    );

    return forkJoin(fullViewData$, hourlyData$);
  }

  /**
   * Pobiera dane dla data table hourly
   *
   * @param  {string[]} models
   * @param  {string} date
   * @returns Observable
   */
  getTableDataHourly(models: string[], date: string): Observable<any> {
    const dateTableData = this.bigTableBackupHourlyData.find(el => {
      return el.date === date && isEqual(sortBy(models), sortBy(el.models));
    });

    if (dateTableData) {
      return of(dateTableData.data);
    }

    const requests = [];
    models.forEach(model => {
      requests.push(this._chartListHttpService.getTableDataHourly(model, date));
    });

    return forkJoin(requests).pipe(
      tap(dataHourly => {
        this.bigTableBackupHourlyData.push({
          date: date,
          models: models,
          data: dataHourly
        });
      })
    );
  }

  /**
   * Sprawdza, czy dane z taką datą były już pobierane
   *
   * @param  {string} date
   * @returns boolean
   */
  isHourlyDataPresent(date: string): boolean {
    return !!this.bigTableBackupHourlyData.find(el => {
      return el.date === date;
    });
  }

  /**
   * Pobiera dane dla wykresu Residual Demand (Boxplot)
   *
   * @param  {string[]} models
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  public getResidualDemandDataBasedOnModels(models: string[], queryParams: ChartQueryParams): Observable<ChartDataBox[]> {
    const requests = [];
    models.forEach(model => {
      const paramsTemp = queryParams;
      paramsTemp.model = model;
      requests.push(this._chartListHttpService.getCalculatedBoxPlotData(queryParams));
    });
    const chartDataRequests$ = forkJoin(requests);
    return chartDataRequests$;
  }

  /**
   * Filtruje run dates na podstawie określonego rodzaju
   * W przypadku dwoch i wiecej modeli przypisuje run dates dla modelu GFS!!!
   *
   * @param  {RunDatesResponse[]} rundates
   * @param  {string} kind
   * @returns string
   */
  public filterRunDatesByKind(rundates: RunDatesResponse[], kind: string): ModelRunDates {
    let runDates = {};
    const filteredData = rundates.filter(data => {
      return data.kind === kind;
    });
    if (filteredData.length > 1) {
      runDates = {
        gfs: this.filterRunDatesByModel(filteredData, 'gfs'),
        hres: this.filterRunDatesByModel(filteredData, 'hres')
      };
    } else {
      filteredData.forEach(data => {
        if (data.model === 'gfs') {
          runDates = {
            gfs: data.runDates
          };
        } else {
          runDates = {
            hres: data.runDates
          };
        }
      });
    }
    return runDates;
  }

  /**
   * Filtruje run dates na podstawie modelu
   *
   * @param  {RunDatesResponse[]} rundates
   * @returns string
   */
  public filterRunDatesByModel(rundates: RunDatesResponse[], model: string): string[] {
    let runDates = [];
    const fitleredByModel = rundates.filter(data => {
      return data.model === model;
    });

    fitleredByModel.forEach(data => {
      runDates = data.runDates;
    });
    return runDates;
  }

  /**
   * Sprawdza, czy paramtery zapytani dla wykresu residual demand zostały przekazane
   *
   * @param  {string} iso
   * @param  {ChartQueryParams} params
   * @returns ChartQueryParams
   */
  public checkResidualDemandParams(iso: string, params: ChartQueryParams): ChartQueryParams {
    let queryParams: ChartQueryParams;
    if (!(!!params)) {
      queryParams = new ChartQueryParams();
      queryParams.iso = iso;
    } else {
      queryParams = params;
    }
    return queryParams;
  }
}
