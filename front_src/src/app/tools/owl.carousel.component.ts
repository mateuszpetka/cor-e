import {
  Component, Input, Output, EventEmitter, NgZone, forwardRef,
  OnDestroy, Directive, ElementRef, Host, HostBinding, OnInit, AfterViewInit
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import 'owl.carousel';

/**
 * UWAGA! Zainstaluj najpierw karuzelę oraz jej typy!
 *
 * npm install --save --save-exact owl.carousel@2.3.4
 * npm install --save-dev --save-exact @types/owl.carousel@2.2.1
 *
 * Aby skorzystać z karuzeli, konieczna jest edycja pliku .angular-cli.json
 * Należy we właściwościach scripts oraz styles zdefiniować pliki z którego pobierane są karuzele np:
 *    ...
 *    "styles": [
 *       "styles.scss",
 *       "../node_modules/owl.carousel/dist/assets/owl.carousel.min.css"
 *     ],
 *     "scripts": [
 *       "../node_modules/jquery/dist/jquery.min.js",
 *       "../node_modules/owl.carousel/dist/owl.carousel.min.js"
 *     ],
 *    ...
 *
 *  Przykład użycia:
 *
 * <owl-carousel (event)="test($event)" class="">
 *   <div *ngFor="let item of arr" owlCarouselItem>{{ item }}</div>
 * </owl-carousel>
 *
 * UWAGA! <owl-carousel> MUSI posiadać atrybut class
 */

const OWL_EVENTS = [
  'initialize.owl.carousel',
  'initialized.owl.carousel',
  'resize.owl.carousel',
  'resized.owl.carousel',
  'refresh.owl.carousel',
  'refreshed.owl.carousel',
  'drag.owl.carousel',
  'dragged.owl.carousel',
  'translate.owl.carousel',
  'translated.owl.carousel',
  'change.owl.carousel',
  'changed.owl.carousel',
  'load.owl.lazy',
  'loaded.owl.lazy',
  'stop.owl.video',
  'play.owl.video'
];

@Component({
  selector: 'owl-carousel',
  template: '<ng-content></ng-content>',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => OwlCarouselComponent),
      multi: true
    }
  ],
})
export class OwlCarouselComponent implements OnInit, OnDestroy {
  @HostBinding('class.owl-carousel') owlClass = true;
  @Input() options: OwlCarousel.Options = {};
  @Output() event: EventEmitter<any> = new EventEmitter();

  public slides: any[] = [];
  public $instance: any;
  public currentSlideIndex: number;
  private initialized: Boolean = false;
  private $carouselElement: JQuery;
  private init = false;

  constructor(private elementRef: ElementRef, private zone: NgZone) {
  }

  ngOnInit() {
    if (typeof jQuery.fn.owlCarousel !== 'function') {
      throw new Error('Nie znaleziono Owl\'a!');
    }
    this.$carouselElement = jQuery(this.elementRef.nativeElement);
  }

  /**
   * Inincalizuje karuzelę
   *
   * @returns void
   */
  initOwl(): void {
    this.zone.runOutsideAngular(() => {
      if (this.$carouselElement) {
        const options = {...this.options};
        if (this.currentSlideIndex) {
          options.startPosition = this.currentSlideIndex;
        }

        this.$instance = this.$carouselElement.owlCarousel(options);
        this.$carouselElement.on('changed.owl.carousel', event => {
          this.currentSlideIndex = event.item.index;
        });

        this.registerCallbacks();
        this.initialized = true;
      }
    });
  }

  /**
   * Dodaje slajdy do karuzeli,
   * jeżeli karuzela nie została zainicjalizowana to prowadzi inicalizację
   *
   * @param  {OwlCarouselItem} owlItem
   * @param  {number} position?
   * @returns void
   */
  addSlide(owlItem: OwlCarouselItem, position?: number): void {
    if (!this.initialized) {
      this.initOwl();
    }

    const newPosition = this.slides.push(owlItem) - 1;
    this.$instance.trigger('add.owl.carousel', [owlItem.el.nativeElement, newPosition]);
    this.reInitOwl();
  }

  /**
   * Usuwa konkretny slajd z karuzeli
   *
   * @param  {OwlCarouselItem} owlItem
   * @returns void
   */
  removeSlide(owlItem: OwlCarouselItem): void {
    const slideIndex = this.slides.indexOf(owlItem);
    this.$instance.trigger('remove.owl.carousel', slideIndex);
    this.slides = this.slides.filter(slide => slide !== owlItem);
  }

  /**
   * Pozwala wywołać event na karuzeli
   *
   * [Docs](https://owlcarousel2.github.io/OwlCarousel2/docs/api-events.html)
   *
   * [Docs]{@link https://owlcarousel2.github.io/OwlCarousel2/docs/api-events.html}
   * @param  {OwlCarousel.TriggerEvent} eventName
   * @param  {any[]} params?
   * @returns void
   */
  trigger(eventName: OwlCarousel.TriggerEvent, params?: any[]): void {
    this.$carouselElement.trigger(eventName, params);
  }

  /**
   * Restartuje karuzelę z zachowaniem indeksu aktualnie wyświetlanego slajdu
   *
   * @returns void
   */
  reInitOwl(): void {
    if (this.$instance) {
      this.$instance.css('display', 'none');
    }
    setTimeout(() => {
      this.destroyOwl();
      if (this.$instance) {
          const slidesCount = this.slides && this.slides.length;
          if (slidesCount && slidesCount < this.currentSlideIndex) {
              this.currentSlideIndex = slidesCount;
          }
          this.$instance.css('display', 'block');
      }
      this.initOwl();
    }, 0);
  }

  /**
   * Nisszczy owla, jednocześnie odbindowując callbacki i usuwając zbędne elementy
   *
   * @returns void
   */
  destroyOwl(): void {
    if (this.$instance) {
      this.clearCallbacks();
      this.$instance.trigger('destroy.owl.carousel')
        .removeClass('owl-loaded owl-hidden')
        .find('.owl-stage:empty, .owl-item:empty')
        .remove();
      this.initialized = false;
    }
  }

  /**
   * Rejestruje callbacki Owla
   *
   * @returns void
   */
  registerCallbacks(): void {
    OWL_EVENTS.forEach(eventName => {
      this.$carouselElement.on(eventName, eventData => {
        this.zone.run(() => {
          this.event.emit(eventData);
        });
      });
    });
  }

  /**
   * Czyści callbacki Owla
   *
   * @returns void
   */
  clearCallbacks(): void {
    if (this.$instance) {
      OWL_EVENTS.forEach(eventName => {
        this.$carouselElement.unbind(eventName);
      });
    }
  }

  ngOnDestroy() {
    this.destroyOwl();
  }
}

@Directive({
  selector: '[owlCarouselItem]',
})
export class OwlCarouselItem implements AfterViewInit, OnDestroy {
  constructor(
    public el: ElementRef,
    @Host() private carousel: OwlCarouselComponent
  ) { }

  ngAfterViewInit() {
    this.carousel.addSlide(this);
  }
  ngOnDestroy() {
    this.carousel.removeSlide(this);
  }
}
