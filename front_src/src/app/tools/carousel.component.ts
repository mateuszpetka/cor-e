import {
  Component, Input, Output, EventEmitter, NgZone, forwardRef,
  AfterViewInit, OnDestroy, Directive, ElementRef, Host
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import 'slick-carousel';

@Component({
  selector: 'slick-carousel',
  template: '<ng-content></ng-content>',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CarouselComponent),
      multi: true
    }
  ],
})
export class CarouselComponent implements AfterViewInit, OnDestroy {

  @Input() options: JQuerySlickOptions = {};
  @Output() afterChange: EventEmitter<any> = new EventEmitter();
  @Output() beforeChange: EventEmitter<any> = new EventEmitter();
  @Output() breakpoint: EventEmitter<any> = new EventEmitter();
  @Output() destroy: EventEmitter<any> = new EventEmitter();

  public slides: any = [];
  public $instance: any;
  private initialized: Boolean = false;
  private carouselElement: JQuery;
  private init: boolean = false;

  constructor(private elementRef: ElementRef, private zone: NgZone) { }

  ngOnDestroy() {
    this.unslick();
  }

  ngAfterViewInit() {
  }


  initSlick() {
    const self = this;

    this.zone.runOutsideAngular(() => {
      this.$instance = jQuery(this.elementRef.nativeElement).slick(this.options);
      this.initialized = true;

      this.$instance.on('afterChange', (event, slick, currentSlide) => {
        self.zone.run(() => {
          self.afterChange.emit({ event, slick, currentSlide });
        });
      });

      this.$instance.on('beforeChange', (event, slick, currentSlide, nextSlide) => {
        self.zone.run(() => {
          self.beforeChange.emit({ event, slick, currentSlide, nextSlide });
        });
      });

      this.$instance.on('breakpoint', (event, slick, breakpoint) => {
        self.zone.run(() => {
          self.breakpoint.emit({ event, slick, breakpoint });
        });
      });

      this.$instance.on('destroy', (event, slick) => {
        self.zone.run(() => {
          self.destroy.emit({ event, slick });
        });
      });
    });
  }

  addSlide(slickItem: SlickCarouselItem) {
    if (!this.initialized) {
      this.initSlick();
    }

    this.slides.push(slickItem);
    this.$instance.slick('slickAdd', slickItem.el.nativeElement);
  }

  removeSlide(slickItem: SlickCarouselItem) {
    const idx = this.slides.indexOf(slickItem);
    this.$instance.slick('slickRemove', idx);
    this.slides = this.slides.filter(s => s !== slickItem);
  }

  /**
   * Slick Method
   */
  public slickGoTo(index: number) {
    this.zone.run(() => {
      this.$instance.slick('slickGoTo', index);
    });
  }

  public slickNext() {
    this.zone.run(() => {
      this.$instance.slick('slickNext');
    });
  }


  public slickPrev() {
    this.zone.run(() => {
      this.$instance.slick('slickPrev');
    });
  }

  public slickPause() {
    this.zone.run(() => {
      this.$instance.slick('slickPause');
    });
  }

  public slickPlay() {
    this.zone.run(() => {
      this.$instance.slick('slickPlay');
    });
  }

  public unslick() {
    this.zone.run(() => {
      this.$instance.slick('unslick');
    });
  }
}

@Directive({
  selector: '[slickCarouselItem]',
})
export class SlickCarouselItem implements AfterViewInit, OnDestroy {
  constructor(
    public el: ElementRef,
    @Host() private carousel: CarouselComponent
  ) { }

  ngAfterViewInit() {
    this.carousel.addSlide(this);
  }
  ngOnDestroy() {
    this.carousel.removeSlide(this);
  }
}
