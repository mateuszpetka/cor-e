import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilePathPipe } from './pipes/file-path.pipe';
import { SafeHtmlPipe, SafeStylePipe } from './pipes/bypass.pipe';
import { LocalizeRouterPipe } from './pipes/localize-router.pipe';
import { SliceRunDatePipe } from './pipes/sliceRunDate.pipe';

// USER-CODE-O
// USER-CODE-C

@NgModule({
  imports: [CommonModule],
  declarations: [
    FilePathPipe,
    SafeHtmlPipe,
    SafeStylePipe,
    LocalizeRouterPipe,
    // USER-CODE-O
    SliceRunDatePipe
    // USER-CODE-C
  ],
  providers: [FilePathPipe],
  exports: [
    FilePathPipe,
    SafeHtmlPipe,
    SafeStylePipe,
    LocalizeRouterPipe,
    // USER-CODE-O
    SliceRunDatePipe
    // USER-CODE-C
  ]
})
export class PipesModule { }
