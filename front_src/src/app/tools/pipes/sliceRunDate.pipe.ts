import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'sliceRunDate'})
export class SliceRunDatePipe implements PipeTransform  {

  /**
   * Usuwa tyle pierwszych elementów tablicy, ile jest w zmiennej
   * elements, jeśli zmienna nie jest podana, to usuwane są 2 elementy,
   * pipe zrobiony po to, żeby nie wywoływać milion razy slice'a w templatce
   *
   * @param  {T[]} value
   * @param  {number=2} elements
   * @returns T
   */
  transform<T>(value: T[], elements: number = 2): T[] {
    return value.slice(elements);
  }
}
