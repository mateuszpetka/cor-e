import { Pipe, PipeTransform } from '@angular/core';
import { AppSettings } from '../../app.settings';

@Pipe({
  name: 'filePath'
})
export class FilePathPipe implements PipeTransform {
  private apiIp: string;
  constructor(private configuration: AppSettings) {
    this.apiIp = configuration.apiUrl;
  }
  /**
   * Zwraca ścieżkę do oryginalnego pliku lub miniaturki
   *
   * @param  {any} value
   * @param  {boolean} thumbnail? (true, jeśli miniaturka)
   * @param  {number} height?
   * @param  {number} width?
   * @param  {string} modificator? (c - proporcjonalne przycięcie od środka)
   * @returns string
   */
  transform(value: any, thumbnail?: boolean, height?: number, width?: number, modificator?: string): string {
    const apiIp = this.apiIp;
    let filePath: string;
    value = value.replace(new RegExp(' ', 'g'), '%20');
    if (!thumbnail) {
      filePath = apiIp.slice(0, -4) + `/public/upload/${value}`;
    } else {
      filePath = apiIp +  `/public/thumbs/${height}_${width}_${modificator}/${value}`;
    }
    return filePath;
  }
}
