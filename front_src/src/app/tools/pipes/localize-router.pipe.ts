import { Injectable, Pipe, PipeTransform, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, RouterLinkActive } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

// USER-CODE-O
// USER-CODE-C

const VIEW_DESTROYED_STATE = 128;

@Injectable()
@Pipe({
  name: 'localizeRouterLink',
  pure: false
})
export class LocalizeRouterPipe implements PipeTransform {
  private value: string;
  private lastKey: string;
  private lastQuery: string;
  private subscription: Subscription;
  private rla: RouterLinkActive;
  private params;
  private lastLanguage: string;

  constructor(private _ref: ChangeDetectorRef, private router: Router, private translateSrv: TranslateService) {
    this.subscription = this.router.events.subscribe(ev => {
      this.transform(this.lastKey);
    });
  }

  /**
   * Transform current url to localized one
   *
   * @param query
   * @returns {string | any[]}
   */
  transform(query: string, rla?, params?): string {
    if (params) {
      this.params = params;
    }

    if (rla) {
      this.rla = rla;
    }

    if (query) {
      this.lastQuery = query;
    }

    if (!query && !this.lastQuery) {
      return this.value;
    }

    if (!this.translateSrv.currentLang) {
      return this.value;
    }

    if (!this.lastQuery) {
      return this.value;
    }

    if (this.lastLanguage === this.translateSrv.currentLang) {
      return this.value;
    }

    this.lastKey = query || this.lastQuery;
    this.lastLanguage = this.translateSrv.currentLang;

    this.value = this.translateSrv.instant(query || this.lastQuery, this.params);

    if ((<any>this._ref)._view.state === VIEW_DESTROYED_STATE) {
      return this.value;
    }

    // bez timeouta nie odpala się wykrywanie zmian
    setTimeout(() => {
      const rlaUpdate = this.rla && (this.rla as any).update();
      this._ref.detectChanges();
    }, 0);

    return this.value;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  // USER-CODE-O
  // USER-CODE-C
}
