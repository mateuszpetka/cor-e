import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from '../../http.service';
import { QueryParams } from '../../classes/QueryParams';
import { LimitedResults } from '../../classes/LimitedResults';

@Injectable()
export class Service<T> {

  constructor(
    @Inject('') public entityUrl: string,
    public httpService: HttpService,
  ) {
  }

  /**
   * Wysłanie zapytania o pobranie danych
   *
   * @param {string} lang
   * @param {QueryParams} [params]
   * @returns {Observable<T[] & LimitedResults<T>>}
   */
  public get(lang: string = 'pl', params?: QueryParams): Observable<T[] & LimitedResults<T>> {
    const queryOptions = {
      params: params ? params.getParams() : {}
    };
    return this.httpService.get(lang + '/' + this.entityUrl, queryOptions);
  }

  /**
   * Wysłanie zapytania o pobranie danych dla jednego rekordu po ID
   *
   * @param {number} id
   * @param {string} lang
   * @param {boolean} join
   * @param {boolean} withPrevNext
   * @param {Array<string>} prevNextFields
   * @returns {Observable<T>}
   */
  public getOne(
    id: number,
    lang: string = 'pl',
    join: boolean = false,
    withPrevNext: boolean = false,
    prevNextFields: Array<string> = []
  ): Observable<T> {
    const params = new QueryParams();

    params.join(join);
    params.withPrevNext(withPrevNext);
    params.prevNextFields(prevNextFields);

    const queryOptions = {
      params: params.getParams()
    };

    return this.httpService.get(lang + '/' + this.entityUrl + '/' + id, queryOptions);
  }
  /**
   * Wysłanie zapytania o pobranie danych
   *
   * @param {string} slug
   * @param {string} lang
   * @param {boolean} join
   * @param {boolean} prevNext
   * @param {boolean} withAllLangSlugs
   * @param {Array<string>} prevNextFields
   * @returns {Observable<T>}
   */
  public getOneBySlug(
    slug: string,
    lang: string = 'pl',
    join: boolean = false,
    prevNext: boolean = false,
    withAllLangSlugs: boolean = false,
    prevNextFields: Array<string> = []
  ): Observable<T> {
    const params = new QueryParams();

    params.join(join);
    params.withPrevNext(prevNext);
    params.withAllLangSlugs(withAllLangSlugs);
    params.prevNextFields(prevNextFields);

    const queryOptions = {
      params: params.getParams()
    };

    return this.httpService.get(lang + '/' + this.entityUrl + '/slug/' + slug, queryOptions);
  }
}
