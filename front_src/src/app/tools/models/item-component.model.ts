import { Input, OnChanges, OnDestroy, OnInit } from '@angular/core';

import { TranslationService } from '../../translation.service';
import { QueryParams } from '../../classes/QueryParams';
import { LimitedResults } from '../../classes/LimitedResults';

import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { switchMap, tap, shareReplay, takeUntil } from 'rxjs/operators';

/**
 * Service interface
 */
interface Service<T> {
  readonly get: (
    lang: string,
    params?: QueryParams
  ) => Observable<T[] & LimitedResults<T>>;

  readonly getOne: (
    id: number,
    lang: string,
    join: boolean,
    withPrevNext: boolean
  ) => Observable<T>;

  readonly getOneBySlug?: (
    slug: string,
    lang: string,
    join: boolean,
    prevNext: boolean,
    withAllLangSlugs: boolean,
    prevNextFields: Array<string>
  ) => Observable<T>;
}

/**
 * Model list dla generowanych komponentów
 */
export class ItemComponentModel<T> implements OnInit, OnChanges, OnDestroy {

  /**
   * Behavior subject do nasłuchiwania OnChanges
   *
   * @type {BehaviorSubject<any>}
   */
  private modelBS: BehaviorSubject<string> = new BehaviorSubject(null);

  /**
   * Subject do nasluchiwania destroy'a componentu
   *
   * @type {Subject<boolean>} Subject<boolean>()
   */
  private destroy$: Subject<boolean> = new Subject<boolean>();

  private urlSlugs: Object = {};

  public activeSlug: string;

  /**
   * Observable do wywolywania pobieania danych przy uruchomieniu OnChanges
   *
   * @type {Observable<string>}
   */
  public modelObs: Observable<string> = this.modelBS.asObservable();

  /**
   * Observable dla rekordów
   *
   * @type {Observable<any>
   */
  public modelData$: Observable<T>;

  /**
   * Id rekordu
   */
  @Input() urlId: number = 1;

  /**
   * Slug rekordu
   */
  @Input() urlSlug: string;

  /**
   * Template komponentu
   *
   * @type {string}
   */
  @Input() tpl: string = 'default';

  /**
   * Ustawienia czy mają być zwracane dane z encji joinowanej
   * np.: [join]="true"
   *
   * @type {boolean}
   */
  @Input() join: boolean = false;

  /**
   * Czy ma zwracać poprzedni i następny rekord.
   * Jeżeli rekord jest pobierany po slugu, w `prevRecord` i `nextRecord` zwracany jest slug
   *
   * @type {boolean}
   */
  @Input() prevNext: boolean = false;

  /**
   * Jakie pola mają być zwracane kiedy `prevNext === true`, w tablicy należy wykorzystać strukture odpowiedniej encji.
   * Pamiętaj że nie musisz przekazywać czy pole slug/id musi być zwracane.
   *
   * np.:
   * [prevNextFields]=[NewsStructure.title, NewsStructure.desc]
   *
   * response:
   * "prevRecord": {
   *   "title": "PL Do you see any Teletubbies in here?",
   *   "position": 2,
   *   "slug": "pl-do-you-see-any-teletubbies-in-here"
   * },
   *
   * @type {boolean}
   */
  @Input() prevNextFields: string[] = [];

  /**
   * Czy ma zwracać slugi z wszystkich języków dla danego rekordu
   * np.:
   * "allLangSlugs": {
   *   "pl": "pl-slug-trololo-lolo",
   *   "en": "en-slug-trololo-lolo"
   * },
   *
   * @type {boolean}
   */
  @Input() withAllLangSlugs: boolean = false;

  constructor (
    public service: Service<T>,
    public translationService: TranslationService
  ) {

  }

  ngOnInit() {

    this.modelData$ = combineLatest(this.translationService.currentLangObservator, this.modelObs)
      .pipe(
        takeUntil(this.destroy$),
        switchMap(([lang]) => {
          if (!!this.urlSlug) {
            return this.handleSlug(lang);
          } else {
            return this.service.getOne(this.urlId, lang, this.join, this.prevNext)
          }
        }),
        shareReplay(1)
      );

  }

  protected handleSlug(lang): Observable<T> {

    if (this.urlSlugs.hasOwnProperty(lang) && this.activeSlug === this.urlSlug) {
      this.urlSlug = this.urlSlugs[lang];
    }

    this.urlSlugs = {};

    return this.service.getOneBySlug(this.urlSlug, lang, this.join, this.prevNext, this.withAllLangSlugs, this.prevNextFields).pipe(
      tap( res => {
        if (this.withAllLangSlugs) {
          this.urlSlugs = res['allLangSlugs'];
          this.activeSlug = res['slug'];
        }
      })
    )
  }

  /***
   * Event gdy zmieni sie jakikolwiek input
   */
  ngOnChanges() {
    /**
     * Wysyła do observable informacje, że któryś z @Input ma nową wartość
     */
    this.modelBS.next(null);
  }

  /**
   * Event na zniszczenie componentu
   */
  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
