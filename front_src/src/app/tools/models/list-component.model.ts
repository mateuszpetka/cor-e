import { Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { TranslationService } from '../../translation.service';
import { QueryParams } from '../../classes/QueryParams';
import { LimitedResults } from '../../classes/LimitedResults';

import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { switchMap, map, shareReplay, takeUntil } from 'rxjs/operators';

/**
 * Service interface
 */
interface Service<T> {
  readonly get: (lang: string, params?: QueryParams) => Observable<T[] & LimitedResults<T>>;
  readonly getOne: (id: number, lang: string, join: boolean, withPrevNext: boolean) => Observable<T>;
}

/**
 * Model list dla generowanych komponentów
 */
export class ListComponentModel<T> implements OnInit, OnDestroy, OnChanges {

  /**
   * Behavior subject do nasłuchiwania OnChanges
   *
   * @type {BehaviorSubject<string>} BehaviorSubject(null)
   */
  private modelBS: BehaviorSubject<string> = new BehaviorSubject(null);

  /**
   * Subject do nasluchiwania destroy'a componentu
   *
   * @type {Subject<boolean>} Subject<boolean>()
   */
  private destroy$: Subject<boolean> = new Subject<boolean>();

  /**
   * Observable do wywolywania pobieania danych przy uruchomieniu OnChanges
   *
   * @type {Observable<string>} Observable
   */
  public modelObs: Observable<string> = this.modelBS.asObservable();

  /**
   * Observable dla rekordów
   *
   * @type {Observable<T[]>)
   */
  public modelItem$: Observable<T[]>;

  /**
   * Zwraca liczbę rekordów z modelItem$
   *
   * @type {number}
   */
  public modelItemsCount: number;

  /**
   * Nazwa szablonu do wyświetlania danych, domyślnie default
   *
   * @type {string}
   */
  @Input() tpl: string = 'default';

  /**
   * Warunki do pobrania danych, bindujemy jako obiekt
   * np.: [where]="{'id': 2}"
   *
   * @type {Object}
   */
  @Input() where: object = {};

  /**
   * Określenie kolumny po której sortować dane i kierunku sortowania DESC(malejąco) lub ASC(rosnąco)
   * np.: [order]="[[entitystructure.id, 'DESC']]"
   * np.: [order]="[[entitystructure.id, 'DESC'], [entitystructure.position, 'DESC']]"
   *
   * @type {Array<[string, string]>}
   */
  @Input() order: Array<[string, string]> = [];

  /**
   * Określenie przesunięcia wyników
   *
   * @type {number}
   */
  @Input() offset: number;

  /**
   * Określenie liczby wyników do pobrania
   */
  @Input() limit: number;

  /**
   * Ustawienia czy mają być zwracane dane z encji joinowanej
   * np.: [join]="true"
   *
   * @type {boolean}
   */
  @Input() join: boolean = false;

  /**
   * Określa czy mają być zwracane losowe rekordy,
   * najlepiej korzystać z tego gdy ma się ustawiony
   * this.limit
   */
  @Input() randomOrder: boolean = false;

  constructor (
    public service: Service<T>,
    public translationService: TranslationService
  ) {}

  ngOnInit() {
    this.modelItem$ = combineLatest(this.translationService.currentLangObservator, this.modelObs)
      .pipe(
        takeUntil(this.destroy$),
        switchMap(([lang]) => this.getData(lang).pipe(
          map( (res: T[] & LimitedResults<T>) => {
            this.modelItemsCount = (!!this.limit) ? res.totalCount : res.length;
            return (!!this.limit) ? res.items : res
          })
        ))
      )
      .pipe(
        shareReplay(1)
      );
  }

  /**
   * Pobiera dane z serwisu
   *
   * @param {string} lang
   * @returns {Observable<T[] & LimitedResults<T>>}
   */
  getData(lang: string): Observable<T[] & LimitedResults<T>> {
    const queryParams = new QueryParams();
    queryParams.paramsForListComponent({}, this.where, this.order, this.offset, this.limit, this.join, this.randomOrder);

    return this.service.get(lang, queryParams);
  }

  /***
   * Event gdy zmieni sie jaki kolwiek input
   */
  ngOnChanges() {
    /**
     * Wysyła do observable informacje, że któryś z @Input ma nową wartość
     */
    this.modelBS.next('');
  }

  /**
   * Event na zniszczenie componentu
   */
  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}