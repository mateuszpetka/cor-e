import { Injectable } from '@angular/core';
import { Router, Route, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Observable, forkJoin, BehaviorSubject, Subject, of } from 'rxjs';
import { distinctUntilChanged, map, shareReplay, skip, switchMap, filter, take, tap } from 'rxjs/operators';

import { TranslateService } from '@ngx-translate/core';

import { HttpService } from './http.service';
import { BlingLanguage } from './classes/bling/BlingLanguage/BlingLanguage';
import { BlingRoute } from './classes/bling/BlingRoute/BlingRoute';

import { AppSettings } from './app.settings';

interface BlingRouteTranslationWithJoin {
  id: number;
  languageId: number;
  translationKey: string;
  translation: string;
  routeId: number;
  title: string;
  params: Dictionary<any>;
  language: BlingLanguage;
  route: BlingRoute;
}

interface BlingTranslationWithJoin {
  id: number;
  languageId: number;
  translationKey: string;
  translation: string;
  description: string;
  language: BlingLanguage;
}

interface Dictionary<T> {
  [key: string]: T;
}

@Injectable()
export class TranslationService {
  private availableLanguagesCodes: string[];
  private availableLanguages: BlingLanguage[];
  private availableLanguagesToEmit: Subject<string[]> = new Subject();
  public availableLanguagesBS = this.availableLanguagesToEmit.asObservable().pipe(shareReplay());

  private currentLanguage: string;
  private currentLanguageBS: Subject<string> = new Subject();

  public currentLangObservator: Observable<string> = this.currentLanguageBS.asObservable().pipe(
    distinctUntilChanged(),
    shareReplay()
  );

  public ready: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public defaultLanguageCode: string;
  public previousUrl: string;

  constructor(
    private http: HttpService,
    private keywordsService: TranslateService,
    private appSettings: AppSettings,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.manageTranslations();
    this.observeLanguageChange();

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((event: NavigationEnd) => this.previousUrl = event.url)
  }

  /**
   * Manage translations
   *
   * @returns void
   */
  manageTranslations(): void {
    const language$ = this.http.get('BlingLanguage?where[active]=1&where[appId]=2');
    const routeTranslations$ = this.http.get('BlingRouteTranslation?withJoin=1');
    const keywordTranslations$ = this.http
      .get('BlingTranslation?withJoin=1')
      .pipe(map((translations: BlingTranslationWithJoin[]) => this.parseTranslations(translations)));

    forkJoin(language$, routeTranslations$, keywordTranslations$)
      .pipe(
        map(([languages, translations, keywords]: [BlingLanguage[], BlingRouteTranslationWithJoin[], any]) => {
          if (languages.length === 0) {
            throw Error('Empty results for /BlingLanguage?where[active]=1&where[appId]=2 request. Add any languages in admin panel.');
          }
    
          this.availableLanguages = languages;
          this.availableLanguagesCodes = languages.map(language => language.code);
          this.availableLanguagesToEmit.next(this.availableLanguagesCodes)
          this.defaultLanguageCode = languages.find(language => language.isDefault === 1).code;

          const oldConfig = [...this.router.config];
          const newConfig: Route[] = this.createRouterConfig(oldConfig, translations);
          this.setRoutesTranslations(translations);
          this.setTranslations(keywords);
          this.router.resetConfig(newConfig);
        }),
        switchMap(() => {
          this.ready.next(true);
          return this.router.events.pipe(
            filter(event => event instanceof NavigationEnd),
            take(1),
            tap((event: NavigationEnd) => {
              const checkRoute = this.findCurrentRouteInRouterConfig();
              if (checkRoute && checkRoute.data && checkRoute.data.language) {
                this.setStartingLanguage(checkRoute.data.language);
              } else {
                this.setStartingLanguage(this.availableLanguages);
              }
            })
          );
        })
      )
      .subscribe();
  }

  /**
   * Navigate to proper route after language change
   *
   * @returns void
   */
  observeLanguageChange(): void {
    this.currentLangObservator.pipe(skip(1)).subscribe(lang => {
      const startingRoute = this.findCurrentRouteInRouterConfig();
      const translatedRoute = this.router.config.find(
        route => (startingRoute.data || {}).routeId === (route.data || {}).routeId && route.data.language === lang
      );
      
      const pathToNavigate = startingRoute.path.includes(':') ? this.getTranslatedPathWithParams(translatedRoute) : translatedRoute.path;
      this.router.navigate([pathToNavigate]);
    });
  }

  /**
   * Find current route in router config
   *
   * @returns Route
   */
  private findCurrentRouteInRouterConfig(): Route {
    let startRoute = this.router.config.find(route => '/' + route.path === this.router.url);
    if (!startRoute) {
      startRoute = this.router.config.find(route => route.path === this.activatedRoute.children[0].snapshot.routeConfig.path)
    }

    return startRoute;
  }

  /**
   * Put query params into translated route
   *
   * @param  {Route} translatedRoute
   * @returns string
   */
  private getTranslatedPathWithParams(translatedRoute: Route): string {
    const isDefaultLanguage = translatedRoute.data.language === this.defaultLanguageCode;
    const translatedUrlParts = translatedRoute.path.split('/');
    const previousUrlParts = this.previousUrl.split('/').slice(1);
    return translatedUrlParts.map((urlPart, index) => {
      if (urlPart.startsWith(':')) {
        return isDefaultLanguage ? previousUrlParts[index + 1] : previousUrlParts[index - 1];
      } else {
        return urlPart;
      }
    }).join('/');
  }

  /**
   * Parse translations
   *
   * @param  {BlingTranslationWithJoin[]} translations
   * @returns any
   */
  parseTranslations(translations: BlingTranslationWithJoin[]): any[] {
    const parsedTranslations = [];
    translations.map(translation => {
      const keyValue = { [translation.translationKey]: translation.translation };
      if (parsedTranslations.hasOwnProperty(translation.language.code)) {
        parsedTranslations[translation.language.code] = { ...parsedTranslations[translation.language.code], ...keyValue };
      } else {
        parsedTranslations[translation.language.code] = keyValue;
      }
    });

    return parsedTranslations;
  }

  /**
   * Create route config with translated routes
   *
   * @param  {Route[]} oldConfig
   * @param  {BlingRouteTranslationWithJoin[]} translations
   * @returns Route
   */
  createRouterConfig(oldConfig: Route[], translations: BlingRouteTranslationWithJoin[]): Route[] {
    const newConfig: Route[] = [];
    oldConfig.forEach((entry: Route) => {
      const foundTranslations = translations.filter(translation => translation.route.path === entry.path).map(translation => {
        return {
          path: this.determineRoutePath(translation),
          component: entry.component,
          data: {
            ...entry.data,
            title: translation.title,
            params: translation.params,
            routeId: translation.routeId,
            language: translation.language.code
          }
        };
      });
      if (foundTranslations.length) {
        newConfig.push(...foundTranslations);
      } else {
        newConfig.push(entry);
      }
    });

    return newConfig;
  }

  /**
   * Zwraca ścieżkę z doklejonym prefiksem językowym
   *
   * @param  {BlingRouteTranslationWithJoin} translation
   * @returns string
   */
  determineRoutePath(translation: BlingRouteTranslationWithJoin): string {
    let path = '';
    if (!translation.language.isDefault) {
      path += translation.language.code;
    }

    if (translation.translation) {
      if (path.length !== 0) {
        path += '/';
      }

      path += translation.translation;
    }

    return path;
  }

  /**
   * Set starting language of the app
   *
   * @param  {BlingLanguage[]} languages
   * @returns void
   */
  private setStartingLanguage(languages: BlingLanguage[] | string): void {
    if (typeof languages === 'string') {
      this.setLang(languages);
      return;
    }

    const userAgentLanguage = (navigator.languages && navigator.languages[0]) || navigator.language || navigator['userLanguage'];
    const userAgentLanguageCode = userAgentLanguage.substring(0, 2);
    if (this.availableLanguagesCodes.includes(userAgentLanguageCode)) {
      localStorage.setItem('lang', userAgentLanguageCode);
      this.setLang(userAgentLanguageCode);
    } else if (localStorage.getItem('lang')) {
      const lang = localStorage.getItem('lang');
      this.setLang(lang);
    } else {
      location.href = this.appSettings.apiIp + '/' + this.defaultLanguageCode;
      this.currentLanguage = this.defaultLanguageCode;
    }
  }

  /**
   * Set given language as current.
   *
   * @param {string} lang
   * @memberof TranslationService
   */
  public setLang(lang: string): void {
    if (this.availableLanguagesCodes.indexOf(lang) < 0) {
      throw Error(`Language ${lang} is not supported.`);
    }

    this.currentLanguage = lang;
    this.currentLanguageBS.next(this.currentLanguage);
    this.keywordsService.use(this.currentLanguage);
  }

  /**
   * Get current language.
   *
   * @returns {string}
   * @memberof TranslationService
   */
  public getLang(): string {
    return this.currentLanguage;
  }

  /**
   * Get available languages array
   *
   * @returns {Array<string>}
   * @memberof TranslationService
   */
  public getLangs() {
    return of(this.availableLanguagesCodes);
  }

  /**
   * Set translation for @ngx-translate
   *
   * @param  {any[]} keywords
   * @returns void
   */
  private setTranslations(keywords: any[]): void {
    for (const lang in keywords) {
      if (keywords.hasOwnProperty(lang)) {
        for (const key in keywords[lang]) {
          if (keywords[lang].hasOwnProperty(key)) {
            const element = keywords[lang][key];
            this.keywordsService.setTranslation(lang, { [key]: element }, true);
          }
        }
      }
    }
  }

  /**
   * Set routes translations for @ngx-translate
   *
   * @param  {BlingRouteTranslationWithJoin[]} translations
   * @returns void
   */
  private setRoutesTranslations(translations: BlingRouteTranslationWithJoin[]): void {
    translations.forEach(translation => {
      let value = translation.language.isDefault ? translation.translation : `${translation.language.code}/${translation.translation}`;
      if (value.includes(':')) {
        const pathParts = value.split('/');
        value = pathParts.map(pathPart => {
          return pathPart.startsWith(':') ? '{{' + pathPart.replace(':', '') + '}}' : pathPart;
        }).join('/')
      }
      this.keywordsService.setTranslation(
        translation.language.code,
        {
          [translation.translationKey]: value,
          [`title.${translation.translationKey}`]: translation.title
        },
        true
      );
    });
  }
}
