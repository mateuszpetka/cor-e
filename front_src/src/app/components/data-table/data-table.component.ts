import { Component, ElementRef, Input, OnChanges, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import * as _ from 'lodash';
import { BehaviorSubject, Observable, Subject, Subscription, fromEvent } from 'rxjs';

import { Country, CsvData, DataSeries, MergedTableData, TableData, TableState } from '../../classes/classes';
import { SharedService } from '../../shared/shared.service';
import { ChartsStateService } from './../../shared/chars-state.service';
import * as moment from 'moment';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit, OnChanges {
  @Input() dataSrc; // table data
  @Input() selectedCountries: Country[];
  @Input() prevNextDayAvailable: [boolean, boolean];
  @Output() dayEmitter: EventEmitter<string> = new EventEmitter();
  public selectCtrlSub: Subscription; // sub na wybrane rodzaje
  public savedToCsv: boolean = false; // zmienna przechowują stan loadera przy pobieraniu danych

  public dataSources: MergedTableData[] = []; // dane dla głownej tabeli
  public fixedSources: MergedTableData[] = []; // dane dla tabel next week
  public fixedSourcesDisplay: MergedTableData[] = [];
  public backupData: MergedTableData[] = []; // backup danych dla głownej tablei

  // źrodło dla danych drugiej tabeli - przekazywane do komponentu hourly-table
  private dataHourlySourcesSrc: BehaviorSubject<MergedTableData[]> = new BehaviorSubject<MergedTableData[]>([]);
  // obserwator danych tabeli hourly
  public dataHourlySources$: Observable<MergedTableData[]> = this.dataHourlySourcesSrc.asObservable();
  public backupHourlyData: MergedTableData[] = []; // backup danych dla tabeli hourly

  // źrodło danych przechowujące informacje o kolumnach tabeli hourly
  private hourlyColumnsToDisplaySrc: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
  // obserwator tych danych
  public hourlyColumnsToDisplay$: Observable<string[]> = this.hourlyColumnsToDisplaySrc.asObservable();

  public countries: Country[] = []; // lista krajow - pobierane z bazy, uzupełniane w cms
  public columnsToDisplay: string[] = []; // aktualna tabelica kolumn d+x dla głownej tabeli
  public staticColumnsToDisplay: string[] = [];
  public hourlyColumnsToDisplay: string[] = []; // aktualna tabelica kolumn d+x dla tabeli hourly
  public fullscreenActive: boolean = false; // stan fullscreena
  public viewHourly: boolean = false; // stan widoczności tabeli hourly
  public selectCtrl = new FormControl(); // form control dla multiselcta
  public selectOptions: string[] = ['Demand', 'Price', 'Solar', 'Wind']; // opcje multiselecta
  public selectedKinds: string[] = []; // aktualnie wybrane opcje przez usera

  private selectedModels: string[] = []; // wybrane modele - z listy globalnych filtrow
  private _guard$ = new Subject(); // subject wykorzystywany do zabijania subow
  public visibilityStatus$: Observable<boolean>; // stan widocznosci komponentu
  private tableState: TableState; // stan tabeli na podstawie zapisanych filtrow

  private hourlyDateFilter: string = moment().format('YYYY-MM-DD');
  private todayFormatted: string = moment().format('YYYY-MM-DD');
  private currentDate: string = moment().format('MMM DD');

  constructor(
    private _sharedService: SharedService,
    private _chartsStateService: ChartsStateService,
    public elementRef?: ElementRef
  ) { }

  ngOnInit() {
    this.formatData();
    this.tableState = this._chartsStateService.getChartState('tableState') as TableState;
    this.checkInitialState();

    this._sharedService.currentModels$.pipe(
      takeUntil(this._guard$)
    ).subscribe(selectedModels => {
      this.selectedModels = selectedModels;
    });

    this.visibilityStatus$ = this._sharedService.dataTableStatus$;

    fromEvent(window, 'resize').pipe(
      debounceTime(300),
      takeUntil(this._guard$)
    ).subscribe(event => {
      if (this.fullscreenActive) {
        this._sharedService.setFullscreenChart('Table');
      }
    });
  }

  ngOnChanges(changes) {
    if (!!this.dataSrc && this.dataSrc.length && this.selectedCountries && this.selectedCountries.length) {
      const mainData = this.dataSrc[0];
      const hourlyData = this.dataSrc[1];
      this.createMainTableData(mainData);
      this.createHourlyTableData(hourlyData);
      const initialValue: string[] = this.selectedKinds.length ? this.selectedKinds : ['Price'];
      this.selectCtrl.setValue(initialValue);
    }
  }

  /**
   * Sprawdza, czy tableState istnieje oraz przypisuje początkowy stan filtrow
   *
   * @returns void
   */
  checkInitialState(): void {
    if (!!this.tableState) {
      this.viewHourly = !!this.tableState.hourly ? this.tableState.hourly : this.viewHourly;
      this.selectedKinds = !!this.tableState.kinds ? [...this.tableState.kinds] : this.selectedKinds;
    }
  }

  /**
   * Formats data, start subscription
   *
   * @returns void
   */
  formatData(): void {
    this._sharedService.getCountries().pipe(
      takeUntil(this._guard$)
    ).subscribe((resCountries: Country[]) => {
      this.countries = resCountries;
    });


    this.selectCtrlSub = this.selectCtrl.valueChanges.pipe(
      takeUntil(this._guard$)
    ).subscribe((kinds: string[]) => {
      if (kinds.length) {
        this.selectedKinds = [...kinds];
        this.filterTablesData(kinds);
        this.saveCurrentLocalState();
      } else {
        this.dataSources = [];
        this.dataHourlySourcesSrc.next([]);
      }
    });
  }

  /**
   * Filters data based on choice from multiselect
   *
   * @param  {string[]} kinds
   * @returns void
   */
  filterTablesData(kinds: string[]): void {
    const countries = _.cloneDeep(this.selectedCountries);
    const clonedData = _.cloneDeep(this.backupData);
    const fixedData = _.cloneDeep(this.fixedSources);

    let newData = clonedData.map(countryData => {
      const newSeries = countryData.series.filter(serie => kinds.some(kind => serie.name.toLowerCase().includes(kind.toLowerCase())));

      return {
        ...countryData,
        series: newSeries,
      };
    });
    const clonedHourlyData = _.cloneDeep(this.backupHourlyData);
    const newHourlyData = clonedHourlyData.map(countryData => {
      const newSeries = countryData.series.filter(serie => kinds.some(kind => serie.name.toLowerCase().includes(kind.toLowerCase())));

      return {
        ...countryData,
        series: newSeries
      };
    });

    let groupedHourlyData = [];

    newHourlyData.forEach(elem => {
      const foundObjectWithCountry = groupedHourlyData.find(hourlyDataItem => hourlyDataItem.country === elem.country);

      if (foundObjectWithCountry) {
        foundObjectWithCountry.series.push.apply(foundObjectWithCountry.series, elem.series);
      } else {
        groupedHourlyData.push({
          country: elem.country,
          series: elem.series
        });
      }
    });

    let newFixedData = fixedData.map(countryData => {
      const newSeries = countryData.series.filter(serie => kinds.some(kind => serie.name.toLowerCase().includes(kind.toLowerCase())));

      return {
        ...countryData,
        series: newSeries,
      };
    });

    newFixedData = newFixedData.filter(data => countries.some(country => data.country.toLowerCase().includes(country.name.toLowerCase())));

    newData = newData.filter(data => countries.some(country => data.country.toLowerCase().includes(country.name.toLowerCase())));
    groupedHourlyData = groupedHourlyData.filter(data =>
      countries.some(country => data.country.toLowerCase().includes(country.name.toLowerCase())));

    // ZOSTAWIĆ!
    newData.forEach(dataItem => {
      dataItem.series.map(item => {

        if (item.name.toLowerCase().includes('price')) {
          const tempData = {
            'base': '-',
            'peak': '-',
            'flow_date': this.currentDate
          };

          item.data.unshift(tempData);
          item.data.splice(-1);
        }
      });

      return newData;
    });

    this.dataSources = newData;
    this.fixedSourcesDisplay = _.groupBy(newFixedData, 'country');

    const newFixed = Object.keys(this.fixedSourcesDisplay).map(country => {
      return {
        country: country,
        model: this.fixedSourcesDisplay[country].model,
        series: _.flatten(this.fixedSourcesDisplay[country].map(item => [...item.series])).sort((el1, el2) => {
          return el1.name < el2.name ? -1 : 1;
        })
      };
    }).sort((country1, country2) => {
      return country1.country < country2.country ? -1 : 1;
    });

    this.fixedSourcesDisplay = newFixed;
    this.dataHourlySourcesSrc.next(groupedHourlyData);
  }

  /**
   * Initial data format
   *
   * @param  {TableData[]} data
   * @returns MergedTableData
   */
  formatDataToFrontObj(data: TableData[]): MergedTableData[] {
    const newData: MergedTableData[] = [];

    data.forEach((tableDataRes: TableData) => {
      for (const key in tableDataRes.data) {
        if (tableDataRes.data.hasOwnProperty(key)) {
          const tempObj = {
            model: tableDataRes.model,
            country: this.changeCountryName(key),
            series: this.renameData(tableDataRes.model, tableDataRes.data[key])
          };

          if (tempObj.country !== '') {
            newData.push(tempObj);
          }
        }
      }
    });
    return newData;
  }

  /**
   * Zmiana dnia w bigTableHourly
   *
   * @param  {'add'|'sub'} type
   * @returns void
   */
  changeDayFilter(type: 'add' | 'sub'): void {
    const date = this.hourlyDateFilter ? moment(this.hourlyDateFilter) : moment();
    if (type === 'add') {
      date.add(1, 'day');
    } else if (type === 'sub') {
      date.subtract(1, 'day');
    }

    this.hourlyDateFilter = date.format('YYYY-MM-DD');
    this.dayEmitter.emit(this.hourlyDateFilter);
  }

  /**
   * Creates backupdata from main table
   *
   * @param  {TableData[]} mainData
   * @returns void
   */
  createMainTableData(mainData: TableData[]): void {
    const newData: MergedTableData[] = this.formatDataToFrontObj(mainData);
    this.divideData(mainData);
    const fixedData = this.formatDataToFrontObj(this.fixedSources as any);

    this.staticColumnsToDisplay = ['Weekend', 'Next week'];

    this.formatObjToArr(fixedData);
    this.formatObjToArr(newData);

    this.fixedSources = fixedData;

    if (this.selectedModels.length > 1) {
      this.backupData = _.sortBy(this.mergeDataSeries(newData), data => data.country);
      this.checkColumnsSize(this.backupData, 'main');
      this.fillTableDataWithEmptyObj(this.backupData, 'main');
    } else {
      this.backupData = _.sortBy(newData, data => data.country);
      this.checkColumnsSize(this.backupData, 'main');
      this.fillTableDataWithEmptyObj(this.backupData, 'main');
    }
  }

  /**
   * Dzielenie danych do tabeli na codzienne i tygodniowe
   *
   * @param  {TableData[]} backupData
   * @returns void
   */
  divideData(backupData: TableData[]): void {
    const dataSources = [];
    const fixedSources = [];

    backupData.forEach(modelData => {
      const modelObj = {
        model: { ...modelData }.model,
        data: {}
      };

      const modelObjStatic = {
        model: { ...modelData }.model,
        data: {}
      };

      Object.keys(modelData.data).forEach(countryData => {
        modelObj.data[countryData] = {};
        modelObjStatic.data[countryData] = {};

        Object.keys(modelData.data[countryData]).forEach(typeData => {
          modelObj.data[countryData][typeData] = {};
          modelObjStatic.data[countryData][typeData] = {};

          Object.keys(modelData.data[countryData][typeData]).forEach((dayElem, index) => {
            if (dayElem === 'nextWeek' || dayElem === 'weekend') {
              modelObjStatic.data[countryData][typeData][dayElem] = modelData.data[countryData][typeData][dayElem];
            } else {
              modelObj.data[countryData][typeData][dayElem] = modelData.data[countryData][typeData][dayElem];
            }
          });
        });
      });

      dataSources.push(modelObj);
      fixedSources.push(modelObjStatic);
    });

    this.fixedSources = fixedSources;
    this.dataSources = dataSources;
  }

  /**
   * Creates backupdata from hourly table
   *
   * @param  {TableData[]} hourlyData
   * @returns void
   */
  createHourlyTableData(hourlyData: TableData[]): void {
    const newData: MergedTableData[] = this.formatDataToFrontObj(hourlyData);
    const foundMerged = !!this.mergeDataSeries(newData).find(el => el.model === 'gfs')
      && !!this.mergeDataSeries(newData).find(el => el.model === 'hres');

    if (this.selectedModels.length > 1 && foundMerged) {
      this.backupHourlyData = _.sortBy(this.mergeDataSeries(newData), data => data.country);
      this.checkColumnsSize(this.backupHourlyData, 'hourly');
      this.fillTableDataWithEmptyObj(this.backupHourlyData, 'hourly');
    } else {
      this.backupHourlyData = _.sortBy(newData, data => data.country);
      this.checkColumnsSize(this.backupHourlyData, 'hourly');
      this.fillTableDataWithEmptyObj(this.backupHourlyData, 'hourly');
    }
  }

  /**
   * Fills datasources with empty objects
   *
   * @param  {any[]} tableData
   * @returns void
   */
  fillTableDataWithEmptyObj(tableData: any[], type: string): void {
    const columnsLength: number = type === 'main' ? this.columnsToDisplay.length : this.hourlyColumnsToDisplay.length;

    const filler = {
      base: '-',
      peak: '-'
    };

    tableData.forEach(countryData => {
      countryData.series.forEach(series => {
        const arrTemp = new Array(columnsLength).fill(null).map((_, i) => {
          return series.data[i] || filler;
        });
        series.data = arrTemp;
      });
    });
  }


  /**
   * Format backend data to simpler objects
   *
   * @param  {} newData
   * @returns void
   */
  formatObjToArr(newData): void {
    newData.forEach(data => {
      data.series.forEach(element => {
        const datObj = element.data;
        const newDataArr = [];
        for (const key in datObj) {
          if (datObj.hasOwnProperty(key)) {
            const tempElement = datObj[key];
            const tempObj = {
              base: !!tempElement.base ? parseFloat(tempElement.base) : '-',
              peak: !!tempElement.peak ? parseFloat(tempElement.peak) : '-',
              flow_date: key
            };
            newDataArr.push(tempObj);
          }
        }
        element.data = [];
        element.data = _.cloneDeep(newDataArr);
      });
    });
  }

  /**
   * Checks length of dataseries and invokes method createColumns
   *
   * @param  {} newData
   */
  checkColumnsSize(newData: MergedTableData[], kind: string) {
    const maxArr = [];
    const mainHeader = [];
    const columnsToDisplay = [];
    newData.forEach((data) => {
      const sizes = [];
      data.series.forEach(series => {
        sizes.push(_.size(series.data));
      });
      maxArr.push(_.max(sizes));

      data.series.filter(series => {
        if (_.max(maxArr) === series.data.length) {
          mainHeader.push(series);
        }
      });
    });

    if (mainHeader.length) {
      for (const item of mainHeader[0].data) {
        columnsToDisplay.push(item.flow_date);
      }
    }

    const numbersOfColumns = _.max(maxArr);
    this.createColumns(numbersOfColumns, kind, columnsToDisplay);
  }

  /**
   * Creates first row with d+ columns.
   *
   * @param  {number} count
   * @param  {string} kind
   * @returns void
   */
  createColumns(count: number, kind: string, items?: any): void {
    if (kind === 'main') {
      this.columnsToDisplay = items.filter(elem => elem !== 'weekend' && elem !== 'nextWeek');
    } else if (kind === 'hourly') {
      this.hourlyColumnsToDisplaySrc.next([]);
      const columns: string[] = [];
      for (let index = 1; index <= 24; index++) {
        columns.push(`Hour ${index}`);
      }
      this.hourlyColumnsToDisplay = columns;
      this.hourlyColumnsToDisplaySrc.next(columns);
    }
  }

  /**
   * Merges data from diffrent models
   *
   * @param  {} dataSeries
   * @returns TableData
   */
  mergeDataSeries(dataSeries): MergedTableData[] {
    const tempDataSeries = _.cloneDeep(dataSeries);
    const mergedDataSeries = [];
    dataSeries.forEach((data) => {
      tempDataSeries.forEach((element, elementIndex) => {
        if (data.country === element.country && data.model !== element.model) {
          const tempSeries = [...data.series, ...element.series];
          const sortedSeries = _.sortBy(tempSeries, series => series.name);
          const tempObj = {
            country: data.country,
            series: sortedSeries
          };
          mergedDataSeries.push(tempObj);
          tempDataSeries.splice(elementIndex, 1);
        }
      });
    });
    return _.uniqBy(mergedDataSeries, 'country');
  }


  /**
   * Changes data structure
   *
   * @param  {} model
   * @param  {} data
   * @returns dataSeries
   */
  renameData(model, data): DataSeries[] {
    const tempSeries: DataSeries[] = [];
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        const tempObj = {
          name: this.renameKind(key, model),
          data: data[key]
        };
        tempSeries.push(tempObj);
      }
    }
    return tempSeries;
  }

  /**
   * Renames kinds to differnt format - kind + model
   *
   * @param  {string} kind
   * @param  {string} model
   * @returns string
   */
  renameKind(kind: string, model: string): string {
    const renamedModel = this.checkModel(model);
    switch (kind) {
      case 'load':
        return `demand ${renamedModel}`;
      default:
        return `${kind} ${renamedModel}`;
    }
  }

  /**
   * Renames model
   *
   * @param  {string} model
   * @returns string
   */
  checkModel(model: string): string {
    switch (model) {
      case 'gfs':
        return 'GFS OP';
      case 'hres':
        return 'EC OP';
    }
  }

  /**
   * Returns country name by iso
   *
   * @param  {string} iso
   * @returns string
   */
  changeCountryName(iso: string): string {
    const tempCountry = this.countries.find(country => country.iso.toLowerCase() === iso.toLowerCase());
    return !!tempCountry ? tempCountry.name.toUpperCase() : '';
  }


  /**
   * Rizes table
   *
   * @param  {string} charType
   * @param  {boolean} chartFullScreen
   * @returns string
   */
  setFullscreen(chartType: string, chartFullScreen: boolean): void {
    if (!!chartType) {
      if (chartFullScreen === true) {
        this._sharedService.setFullscreenChart(chartType);
        this.fullscreenActive = true;
      } else {
        this._sharedService.setFullscreenOffChart(chartType);
        this.fullscreenActive = false;
      }
    }
  }

  /**
   * Changes state of visibility between main and hourly table
   *
   * @returns void
   */
  changeView(): void {
    this.viewHourly = !this.viewHourly;
    this.saveCurrentLocalState();
  }

  /**
   * Saves table data to csv
   *
   * @returns void
   */
  saveDataToCsv(): void {
    const tables = _.cloneDeep(this.selectCtrl.value.map(value => {
      if (value === 'Demand') {
        value = 'load';
      }
      return value.toLowerCase();
    }));

    const countries = this.selectedCountries.map(country => {
      return country.iso.toUpperCase();
    });

    const data: CsvData = {
      models: this.selectedModels,
      tables: tables,
      countries: countries
    };

    if (!!data) {
      this.savedToCsv = true;
      this._sharedService.saveToCsv(data).subscribe((blob) => {
        const link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob.blob);
        link.download = blob.filename;
        link.click();
      },
        (err) => {
          console.error(err);
         },
        () => {
          this.savedToCsv = false;
        },
      );
    }
  }

  /**
   * Saves hourly table data to csv
   *
   * @returns void
   */
  saveHourlyDataToCsv(): void {
    const tables = _.cloneDeep(this.selectCtrl.value.map(value => {
      if (value === 'Demand') {
        value = 'load';
      }
      return value.toLowerCase();
    }));
    const countries = this.selectedCountries.map(country => {
      return country.iso.toUpperCase();
    });

    const data: CsvData = {
      models: this.selectedModels,
      tables: tables,
      countries: countries
    };

    if (!!data) {
      this.savedToCsv = true;
      this._sharedService.saveHourlyDataToCsv(data).subscribe((blob) => {
        const link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob.blob);
        link.download = blob.filename;
        link.click();
      },
        () => { },
        () => {
          this.savedToCsv = false;
        },
      );
    }
  }

  /**
   * Przypisuje aktualny stan filtrow do  lokalnej zmiennej
   *
   * @returns void
   */
  saveCurrentLocalState(): void {
    const selectedKinds = this.selectedKinds;
    const hourlyState = this.viewHourly;

    const state = new TableState(hourlyState, selectedKinds);
    this.tableState = state;
    this._chartsStateService.saveChartState('table', state);
  }

  ngDestroy() {
    this._guard$.next();
  }
}
