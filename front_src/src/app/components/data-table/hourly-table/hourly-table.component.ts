import { Component, OnInit, Input } from '@angular/core';
import { MergedTableData } from '../../../classes/classes';
import { Observable } from 'rxjs';

@Component({
  selector: 'hourly-table',
  templateUrl: './hourly-table.component.html',
  styleUrls: ['../data-table.component.scss']
})
export class HourlyTableComponent {
  @Input() public dataSources$: Observable<MergedTableData[]>;
  @Input() public columnsToDisplay$: Observable<string[]>;
  @Input() public fullscreenActive: boolean;

  constructor() { }

}
