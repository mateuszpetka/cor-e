import { DataSeries, ChartDbData, ChartData } from './../../classes/chart';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ChartQueryParams, Params } from '../../classes/classes';
import { SharedService } from '../../shared/shared.service';
import { CustomHttpService } from '../../shared/customHttp.service';

@Injectable()
export class ChartService {

  constructor(private _http: CustomHttpService, private _sharedService: SharedService) {}

   /**
   * Gets renewables chart data
   *
   * @param  {string} kind
   * @param  {string} country
   * @param  {number=7} days
   * @returns Observable
   */
  public getRenewablesData(kind: string, queryParams: ChartQueryParams): Observable<ChartData> {
    const params = new Params(queryParams.iso, queryParams.model, queryParams.offset, queryParams.runDate);
    return this._http
    .get(`ChartData/renewables?` + this._sharedService.toQueryString(params) + `&kind=${kind}`);
  }

  /**
   * Gets data for Wind chart type
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  getWindData(kind: string, queryParams: ChartQueryParams): Observable<ChartData> {
    const params = new Params(queryParams.iso, queryParams.model, queryParams.offset, queryParams.runDate);
    return this._http
      .get(`ChartData/renewables?` + this._sharedService.toQueryString(params) + `&kind=${kind}`);
  }

  /**
   * Gets data for Solar chart type
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  getSolarData(kind: string, queryParams: ChartQueryParams): Observable<ChartData> {
    const params = new Params(queryParams.iso, queryParams.model, queryParams.offset, queryParams.runDate);
    return this._http
      .get(`ChartData/renewables?` + this._sharedService.toQueryString(params) + `&kind=${kind}`);
  }

  /**
   * Gets data for Combined chart type
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  getCombinedData(kind: string, queryParams: ChartQueryParams): Observable<ChartData> {
    const params = new Params(queryParams.iso, queryParams.model, queryParams.offset, queryParams.runDate);
    return this._http
      .get(`ChartData/renewables?` + this._sharedService.toQueryString(params) + `&kind=${kind}`);
  }
}
