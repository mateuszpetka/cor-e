import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { Country, Model, BtnLabel } from './../../../classes/classes';
import { SharedService } from './../../../shared/shared.service';
import { find } from 'lodash';
import { startWith, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'chart-button',
  templateUrl: './chart-button.component.html',
  styleUrls: ['./chart-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartButtonComponent implements OnInit, OnDestroy {
  @Input() selectedAvailabilityBtns?: BtnLabel[];
  @Input() selectedIsothermBtns?: string[];
  @Input() selectedIsothermBtns$?: Observable<string[]>;
  @Input() btnAvailabilityLabel?: BtnLabel;
  @Input() name: string;
  @Input() kind: string;
  @Input() chartType: string;
  @Input() model: Model;
  @Input() country: Country;
  @Input() isothermIndex?: number;
  @Input() currentCountry$: Observable<Country>;
  @Input() currentKind$: Observable<string>;
  @Input() currentModel$: Observable<Country>;
  @Output() emitCountry: EventEmitter<string> = new EventEmitter<string>();
  @Output() emitKind: EventEmitter<string> = new EventEmitter<string>();
  @Output() emitModel: EventEmitter<string> = new EventEmitter<string>();
  public active: boolean = true;
  public activeModel: boolean = false;
  public activeAvailability: boolean = true;
  public activeIsotherm: boolean = true;
  public activeCountry$: Observable<Country>;
  private _guard$ = new Subject();
  
  constructor(private _sharedService: SharedService, private cdRef: ChangeDetectorRef) {}

  ngOnInit() {
    this.activeCountry$ = this._sharedService.activeCountry$;

    if (this.kind === 'availability' && !!this.selectedAvailabilityBtns && this.selectedAvailabilityBtns.length > 0) {
      this.checkIfActive();
    }
    if (this.kind === 'isotherm' && !!this.selectedIsothermBtns && this.selectedIsothermBtns.length > 0) {
      this.checkIfIsothermActive();
    }

    if (this.selectedIsothermBtns$) {
      this.selectedIsothermBtns$
        .pipe(
          startWith([]),
          takeUntil(this._guard$)
         )
        .subscribe(res => {
          const searchedBtn = find (res, selectedBtn => selectedBtn.toLowerCase() === this.name.toLowerCase());
          if (!!searchedBtn) {
            this.activeIsotherm = false;
          } else {
            this.activeIsotherm = true;
          }
          this.cdRef.detectChanges();
      });
    }
  }

  /**
   * Sprawdza czy dany rodzaj został odznaczony
   *
   * @param  {string} name
   * @returns boolean
   */
  checkIfActive() {
    const searchedBtn = find (this.selectedAvailabilityBtns,
      selectedBtn => selectedBtn.name.toLowerCase() === this.btnAvailabilityLabel.name.toLowerCase());
    if (!!searchedBtn) {
      this.activeAvailability = false;
    }
  }

    /**
   * Sprawdza czy dany rodzaj isothermu został odznaczony
   *
   * @param  {string} name
   * @returns boolean
   */
  checkIfIsothermActive() {
    const searchedBtn = find (this.selectedIsothermBtns,
      selectedBtn => selectedBtn.toLowerCase() === this.name.toLowerCase());
    if (!!searchedBtn) {
      this.activeIsotherm = false;
    }
  }

  /**
   * Finds active model in models  table
   *
   * @param  {string[]} models
   * @param  {string} model
   * @returns boolean
   */
  findModel(models: string[], model: string): boolean {
    return !!models.find(mod => {
      return mod.toLowerCase() === model.toLowerCase();
    })
  }

  /**
   * Przełącza stan przycisku
   *
   * @returns void
   */
  toggleBtn(country: string): void {
    this.active = !this.active;
    this.emitCountry.emit(country);
  }

  /**
   * Toggles kind
   *
   * @returns void
   */
  toggleBtnKind(kind: string): void {
    this.active = !this.active;
    this.emitKind.emit(kind);
  }

  /**
   * Przełącza stan przycisku
   *
   * @returns void
   */
  toggleBtnModel(model: Model): void {
    this.activeModel = !this.activeModel;
    this.emitModel.emit(model.modelName);
  }

  /**
   * Przełącza stan przycisku
   *
   * @returns void
   */
  toggleAvailability(): void {
    this.activeAvailability = !this.activeAvailability;
  }

  /**
   * Przełącza stan przycisku
   *
   * @returns void
   */
  toggleIsotherm(): void {
    this.activeIsotherm = !this.activeIsotherm;
  }

  ngOnDestroy() {
    this._guard$.next();
  }
}
