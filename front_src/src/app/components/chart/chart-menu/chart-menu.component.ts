import { Component, OnInit, ElementRef, ViewChild, ViewChildren, QueryList, Renderer2, Input, EventEmitter, Output } from '@angular/core';
import { SharedService } from '../../../shared/shared.service';
import { Chart } from 'chart.js';
@Component({
  selector: 'chart-menu',
  templateUrl: './chart-menu.component.html',
  styleUrls: ['./chart-menu.component.scss']
})
export class ChartMenuComponent implements OnInit {
  @Input() chartType: string;
  @ViewChildren('iconElement') private iconQuery: QueryList<ElementRef>;
  public icons: string[] = ["icon-3 ico-filter","icon-2 ico-cog", "icon-1 ico-fullscreen"];
  public isMenuOpen: boolean = false;
  public fullscreenActive: boolean = false;

  constructor(private _renderer: Renderer2,
    private _sharedService: SharedService) { }

  ngOnInit() {
  }

  toggleMenu() {
    const start_angle = 190;
    const offset_angle = 40;
    this.isMenuOpen = !this.isMenuOpen;

    this.iconQuery.forEach((iconItem, index) => {
      const rotateAngle = start_angle + (offset_angle * index || 0);
      const transitionDelay = 200 - ((index+1)*50);

      // TODO ogarnąc czas animacji, DRY!!!
      if (this.isMenuOpen) {
        this._renderer.setStyle(iconItem.nativeElement, 'opacity', `1`);
        this._renderer.setStyle(iconItem.nativeElement, 'transition-delay', `${transitionDelay}ms`);
        this._renderer.setStyle(iconItem.nativeElement, 'transform',
        `rotate(${rotateAngle}deg) translate(30px) rotate(-${rotateAngle}deg)`);
      } else {
        this._renderer.setStyle(iconItem.nativeElement, 'transform',
        `rotate(${-rotateAngle}deg) translate(0) rotate(${rotateAngle}deg)`  );
        this._renderer.setStyle(iconItem.nativeElement, 'transition-delay', `${transitionDelay}ms`);
        this._renderer.setStyle(iconItem.nativeElement, 'opacity', `0`);
      }
    });
  }

  /**
   * Switches between actions - based on clicked menu icon
   *
   * @param  {number} index
   */
  btnAction(index: number) {
    switch (index) {
      case 0: {
        this._sharedService.setFullscreenParameters(true);
        break;
      }
      case 1: {
        break;
      }
      case 2: {
        this.fullScreenOn();
        this.fullscreenActive = true;
        break;
      }
      case 3: {
        this.fullScreenOff();
        this.fullscreenActive = false;
        break;
      }
    }
  }

  /**
   * Pasees chartType to shared service
   *
   * @param {string} chartType
   * @returns void
   */
  fullScreenOn(): void {
    if (!!this.chartType) {
      this._sharedService.setFullscreenChart(this.chartType);
      this.toggleMenu();
    }
  }

  /**
   * Pasees chartType to shared service
   *
   * @param {string} chartType
   * @returns void
   */
  fullScreenOff(): void {
    if (!!this.chartType) {
      this._sharedService.setFullscreenOffChart(this.chartType);
      this.toggleMenu();
    }
  }

  saveToCsv() {
    // TODO save to csv
  }
}
