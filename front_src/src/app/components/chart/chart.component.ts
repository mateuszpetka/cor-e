import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { BehaviorSubject ,  Observable ,  of ,  Subject ,  Subscription, forkJoin, fromEvent } from 'rxjs';
import { combineLatest ,  debounceTime ,  map ,  takeUntil ,  delay, shareReplay, debounce } from 'rxjs/operators';
import { MatCheckbox, MatSelect } from '@angular/material';

import { ChartsStateService } from '../../shared/chars-state.service';
import { ChartCustomOption, DataSets, ModelRunDates } from './../../classes/chart';
import { SharedService } from './../../shared/shared.service';
import { ChartService } from './chart.service';
import { AppService } from 'app/app.service';

import { Chart } from 'chart.js';
import { ChartQueryParams, ChartState, Country, CountryWithTitle } from './../../classes/classes';

import { cloneDeep, filter, find, groupBy, isArray, isEmpty, max, maxBy, min, minBy, uniq } from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() chartOptions: ChartCustomOption; // opcje wykresu, przekazywane z chart-list compoenent
  @Input() renewablesWindRunDates: ModelRunDates; // oberwator z Wind- run dates, przekazywane z chart-list compoenent
  @Input() renewablesSolarRunDates: ModelRunDates;
  // oberwator z Solar - run dates, przekazywane z chart-list compoenent
  @Input() renewablesCombinedRunDates: ModelRunDates;
  // oberwator z Combined - run dates, przekazywane z chart-list compoenent
  @Input() queryParams: ChartQueryParams; // aktualne query params wykresu
  @Input() chartTitle: string; // tytuł wykresu - Renewables, Weather, itp.
  @Output() changeDataEvent: EventEmitter<CountryWithTitle> = new EventEmitter; // emituje do rodzica informacje o zmianie kraju
  @ViewChild('chart') chart: ElementRef; // referencja elementu DOM canvasu
  @ViewChild('runDatesSelect') public runDatesSelect: MatSelect; // referencja elementu DOM selecta RunDates
  @ViewChild('lastCheckBox') public lastCheckBox: MatCheckbox; // referencja elementu DOM checkboxa last
  @ViewChild('prevCheckBox') public prevCheckBox: MatCheckbox; // referencja elementu DOM checkboxa previous
  public themeName: string;

  public backupData: any[]; // backup datasetow
  public meanData: DataSets[] = []; // obliczona średnia datasetow - do przełaczania widokow full range/day
  public myChart: Chart; // głowny obiekt wykresu
  public ctx; // canvas contex
  public labels: string[] = ['Wind', 'Solar', 'Combined']; // tablica przyciskow reprezentujących datasety
  public chartStatus: string = 'hide'; // toggles chart visibility
  public currentDate = moment().format('YYYY-MM-DD'); // aktualny dzień, zmienna filtrująca datasety po flow date
  public loaderSub: Subscription; // sub głownego lodaera
  public fullscreenActive: boolean = false; // stan fullscreena
  public countriesSub: Subscription; // sub dla wybranych krajow z filtrow globalnych
  public countries: Country[]; // lista dostepnych krajow
  public countries$: Observable<Country[]>; // obserwator wybranych krajow
  public runDates: Array<string[]>; // lista run dates
  public lastAndPrevRunDate: string[] = []; // pierwszy i drugi element z tablicy run dates
  public currentRunDates: string[] = []; // aktualnie wybrana lista run date
  public selectedRunDate: string = null; // aktualnie wybrany run date
  public currentModels: string[]; // current, selected models
  public legendLabels: string[];

  public currentCountry$: Observable<Country>; // przyjmuje wartość currentChartCountry$ lub country$
  public country$: Observable<Country>; // obwerwator pierwszego elementu z listy aktualnie wybranych krajow
  protected selectedCountry: Country = null; // zmienna przechowująca aktualnie wybrany kraj
  public currentChartCountry$: BehaviorSubject<Country> = new BehaviorSubject<Country>(null); // obserwator kraju wybranego z wewnętrznych
  // filtrow wykresu

  public currentKind$: Observable<string> = of(null); // przyjmuje wartość currentChartKind$ lub initialKind$
  private initialKindSrc: Subject<string> = new Subject<string>();
  public initialKind$: Observable<string> = this.initialKindSrc.asObservable(); // początkowy rodzaj serii danych
  public currentChartKind$: BehaviorSubject<string> = new BehaviorSubject<string>(null); // obserwaator aktualnego wybranego rodzaju
  // serii danych z filtrow wewnetrznych
  public selectedKind: string = ''; // przechowuje wybrany rodzaj
  private resizeLoaderSrc: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false); // source of resize loader sub
  public resizeLoader$: Observable<boolean> = this.resizeLoaderSrc.asObservable(); // toggles internal loader state

  public lastToogleState: boolean = true; // last run date checkbox state
  public prevToogleState: boolean = false; // last run date checkbox state

  public selectedOffset: number; // selected offset based selected run date
  public viewFullRange: boolean = false; // toggles hourly to days view

  public _guard$ = new Subject(); // global subject used in most subscribtions. Destroyed in ngDestroy life cycle

  private firstDateSrc: Subject<string> = new Subject<string>();
  public firstDate$: Observable<string> = this.firstDateSrc.asObservable();

  private lastDateSrc: Subject<string> = new Subject<string>();
  public lastDate$: Observable<string> = this.lastDateSrc.asObservable();

  public visibleArrows: boolean = false; // zmienna przechowuje stan wyświetlania nawigacji slidera
  private windRunDate: string = ''; // run date wybrany z selecta dla rodzaju danych Wind
  private solarRunDate: string = ''; // run date wybrany z selecta dla rodzaju danych Solar
  private combinedRunDate: string = ''; // run date wybrany z selecta dla rodzaju danych Combined
  public chartState: ChartState; // stan wewnętrznych filtrow wykresow
  public chartUpdateTime: number = 1500;

  constructor(
    private _chartService: ChartService,
    private _sharedService: SharedService,
    private _chartsStateService: ChartsStateService,
    private cd: ChangeDetectorRef,
    private _zone: NgZone,
    public elementRef?: ElementRef,
    public _appService?: AppService
  ) {}

  ngOnInit() {
    this.chartState = this.getChartState();
    this.subscribeThemeChange();
    // Global chart settings
    Chart.defaults.global.defaultFontFamily = 'Overpass';
    Chart.defaults.global.defaultFontSize = 10;
    this.country$ = this._sharedService.activeCountry$;

    this.currentCountry$ = this.country$.pipe(
      debounceTime(200),
      takeUntil(this._guard$),
      combineLatest(this.currentChartCountry$),
      map(([countryFromService, selectedCountry]) => {

        const currentCountry = selectedCountry ? selectedCountry : countryFromService;
        if (!!this.queryParams && !(!!this.queryParams.iso)) {
          this.queryParams.iso = currentCountry.iso;
        }

        this.selectedCountry = currentCountry;
        return currentCountry;
      }),
      shareReplay()
    );

    this.countries$ = this._sharedService.selectedCountries$
      .pipe(
        debounceTime(200),
        takeUntil(this._guard$),
        map((countries: Country[]) => {
          this.countries = countries;
          const searchedCountry = find(countries, country => country === this.selectedCountry);

          // Jezeli aktualnie zaznaczony kraj zostanie usuniety z filtrow globalnych pobiera pierwszy kraj z listy
          if (!!this.selectedCountry && countries.length > 1 && !(!!searchedCountry) && this.selectedCountry !== countries[0]) {
            this.changeDataByCountry(countries[0]);
            this.currentChartCountry$.next(countries[0]);
          }
          if (countries.length === 1 && !!this.selectedCountry && this.selectedCountry !== countries[0]) {
            this.changeDataByCountry(countries[0]);
            this.currentChartCountry$.next(countries[0]);
          }
          return countries;
        }),
        shareReplay()
      );

    // przypisuje wybrane modele do zmiennej
    this._sharedService.currentModels$.subscribe((currentModels: string[]) => {
      this.currentModels = currentModels;
    });

    fromEvent(window, 'resize').pipe(
      debounceTime(300),
      takeUntil(this._guard$)
    ).subscribe(event => {
      if (this.fullscreenActive) {
        this._sharedService.setFullscreenChart(this.chartTitle);
      }
    });
  }

  ngAfterViewInit() {
    // sprawdza stan głownego loadera
    this.loaderSub =  this._sharedService.loadingRenewables$
      .pipe(
        takeUntil(this._guard$),
        debounceTime(200)
      ).subscribe((loadingState: boolean) => {
        const initialKind = !!this.selectedKind ? this.selectedKind : 'Wind';
        this.initialKindSrc.next('Wind');
        this.chartSetup(loadingState, initialKind, this.labels);

        if (!loadingState && this.chartOptions && this.chartOptions.data.datasets.length) {
          // legenda
          const newLabels = [];
          this.legendLabels = this.chartOptions.data.datasets.map(set => {
            const labelValue = set.label.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
            newLabels.push({
              value: labelValue, name: set.label
            });
            return set.label.trim();
          });
          this.legendLabels = newLabels;
        }
      });

    // obserwuje obecnie wybrany rodzaj, przypisuje do zmiennej
    this.currentKind$ = this.initialKind$.pipe(
      takeUntil(this._guard$),
      combineLatest(this.currentChartKind$),
      map(([initialKind, selectedKind]) => {
        this.selectedKind = selectedKind;
        this.currentRunDates = selectedKind ? this.checkCurrentRunDates(selectedKind)
          : this.checkCurrentRunDates(initialKind);
        this.setLastPrevDates();
        return selectedKind ? selectedKind : initialKind;
      })
    );
  }

  /**
   * Nasłuchuje zmian motywu dashboarda
   *
   * @returns void
   */
  subscribeThemeChange(): void {
    const themeFromStorage = this._appService.getUserFromLocalStorage().customFields.themeDisplay;
    this.themeName = !!themeFromStorage ? themeFromStorage : 'default_theme';
    this._appService.newTheme$.pipe(
      takeUntil(this._guard$)
    ).subscribe(themeName => {
      if (themeName) {
        this.themeName = themeName;
        this.changeChartLegendColorByTheme();
      }
    });
  }

  /**
   * Sets last and previous run dates
   *
   * @returns void
   */
  setLastPrevDates(): void {
    this.lastAndPrevRunDate = [];
    const runDatesTemp = cloneDeep(this.currentRunDates);
    for (let i = 0; i < 2; i++) {
      this.lastAndPrevRunDate.push(runDatesTemp.shift());
    }
    this.currentRunDates = cloneDeep(runDatesTemp);
  }

  /**
   * Checks and returns current run dates table
   *
   * @param  {string} currentKind
   * @returns RunDates
   */
  checkCurrentRunDates(currentKind: string): string[] {
    switch (currentKind) {
      case 'Wind':
        return !!(this.runDates && this.runDates.length) ? this.runDates[0] : [];
      case 'Solar':
        return !!(this.runDates && this.runDates.length)  ? this.runDates[1] : [];
      case 'Combined':
        return !!(this.runDates && this.runDates.length)  ? this.runDates[2] : [];
      default:
        return !!(this.runDates && this.runDates.length) ? this.runDates[0] : [];
    }
  }

  /**
   * Chart setup - draw, update, resize
   *
   * @param  {boolean} loadingState
   * @returns void
   */
  chartSetup(loadingState: boolean, initialSeries: string, labels?: string[], chart?): void {
    this.changeLoaderState(true);
    if (!loadingState && this.chartOptions && this.chartOptions.data.datasets.length) {
      this.drawChart();
      initialSeries = !!this.chartState && !!this.chartState.kind ? this.chartState.kind : initialSeries;
      this.viewFullRange = !!this.chartState && !!this.chartState.viewFullRange ? this.chartState.viewFullRange : this.viewFullRange;
      this.changeChartLegendColorByTheme();
      this.setInitialData(initialSeries);
      this.backupData = cloneDeep(this.myChart.config.data.datasets);
      this.calculateAverage();
      this.getOneDay(this.currentDate.toString());
      // Wyswietlanie rodzajow isothermow

      // pobranie danych na podstawie zaznaczonych checkboxow
      // sprawdzanie checkboxow w menu i jeżeli widok jest
      this.checkMenuCheckboxValue();
      this.checkCheckBoxesValue();

      this.toggleChartVisibility(loadingState);
    } else if (loadingState && this.chartOptions && !this.chartOptions.data.datasets.length) {
      // if models oraz countries filters are empty arrays
      if (this.myChart) {
        this.myChart.update(); // destroys chart data
        this.toggleChartVisibility(loadingState);
        this.resizeChart();
      }
    }
  }

  /**
   * Calculates average data for datasets
   *
   * @returns void
   */
  calculateAverage(): void {
    this.meanData = [];

    this.backupData.forEach(dataset => {
      const groupedByDay = groupBy(dataset.data, el => el.x.split(' ')[0]);
      const tempDataset = cloneDeep(dataset);
      tempDataset.data = [];
      for (const key in groupedByDay) {
        if (groupedByDay.hasOwnProperty(key)) {
          const element: any[] = groupedByDay[key];
          const sum = element.reduce((prev: number, next: any) => {
            return prev + parseFloat(next.y);
          }, 0);

          const mean = sum / element.length;
          const tempObj = {
            x: key,
            y: Number(mean.toFixed(2))
          };
          tempDataset.data.push(tempObj);
        }
      }
      this.meanData.push(tempDataset);
    });
  }
  /**
   * Toggles chart visibility
   *
   * @param  {boolean} loadingState
   * @returns void
   */
  toggleChartVisibility(loadingState: boolean): void {
    this.chartStatus = !loadingState ? 'show' : 'hide';
    // this.cd.detectChanges();
  }

  /**
   * Sets initial data - wind datasets visible
   *
   * @returns void
   */
  setInitialData(key: string): void {
    const data = [...this.chartOptions.data.datasets];
    data.forEach(dataset => {
      dataset.hidden = !dataset.kind.includes(key);
      this.setInitialColors(dataset);
    });
    this.chartOptions.data.datasets = data;
    this.currentChartKind$.next(key);
    this.updateChart();
    this.cd.detectChanges();
  }

  /**
   * Sets initial dataset color for Renewables chart
   *
   * @param  {DataSets} dataset
   * @returns void
   */
  setInitialColors(dataset: DataSets): void {
    if (dataset.label.toLowerCase().includes('hres')) {
      dataset.borderColor = this.checkColor('hres').main;
    } else if (dataset.label.toLowerCase().includes('gfs')) {
      dataset.borderColor = this.checkColor('gfs').main;
    }
  }

  /**
   * Resets checkboxes and filters values
   *
   * @param  {string} initialKind
   * @returns void
   */
  resetFilters(initialKind: string): void {
    if (this.prevCheckBox && this.lastCheckBox) {
      if (this.runDatesSelect) {
        this.runDatesSelect.value = null;
      }
      this.initialKind$ = of(initialKind);
      this.selectedKind = '';
    }
  }

  /**
   * Wycina jeden dzien dla serii danych
   *
   * @param  {string} currentDay
   * @param {boolean} hoursView
   * @returns void
   */
  getOneDay(currentDay: string, hoursView: boolean = true): void {
    const tempData = [];
    const dataSetsMax = [];
    const dataSetsMin = [];

    this.backupData.forEach(dataset => {
      const tempDataset = cloneDeep(dataset);
      const tempDates = cloneDeep(dataset);
      if (hoursView) {
        const filteredData = filter(dataset.data, object => object.x.toString().includes(currentDay));
        tempDataset.data = cloneDeep(filteredData);
      }
      tempData.push(tempDataset);

      if (!!tempDates && tempDates.data.length) {
        const maxDate = maxBy(tempDates.data, object => object.x);
        dataSetsMax.push(maxDate.x);

        const minDate = minBy(tempDates.data, object => object.x);
        dataSetsMin.push(minDate.x);
      }
    });

    let maxData: string = '';
    let minData: string = '';

    if (dataSetsMax.length && dataSetsMin.length) {
      maxData = max(uniq(dataSetsMax));
      minData = min(uniq(dataSetsMin));

      maxData = moment(maxData).format('YYYY-MM-DD');
      !!this.selectedRunDate
        ? minData = moment(this.selectedRunDate).format('YYYY-MM-DD')
        : minData = moment().format('YYYY-MM-DD');
    }

    if (this.myChart.config.data  && this.myChart.config.data.datasets.length > 0) {
      this.myChart.config.data.datasets.forEach((dataset, index) => {
        dataset.data = cloneDeep(tempData[index].data);
      });
      this.myChart.update();
      this.cd.detectChanges();
    }

    if (!!maxData && !!minData) {
      this.lastDateSrc.next(maxData);
      this.firstDateSrc.next(minData);
    }
  }

  /**
   * Toggles one day view to whole day view
   *
   * @returns void
   */
  showAllDays(): void {
    this.updateChart();
    this.cd.detectChanges();
  }

  /**
   * Load next/prev data based on day
   *
   * @param  {string} step
   * @returns void-
   */
  loadDay(step: number | string): void {
    this.currentDate = (step === 1) ?
      moment(this.currentDate).add(1, 'days').format('YYYY-MM-DD')
      : moment(this.currentDate).add(-1, 'days').format('YYYY-MM-DD');
    this.getOneDay(this.currentDate.toString());
  }

  /**
   * Load days from current day(+1)+ step data
   *
   * @param  {number} step
   * @returns void
   */
  loadDays(step: number | string): void {
    this.changeLoaderState(true);
    let currentDate = moment().add(1, 'days').format('YYYY-MM-DD');
    currentDate = moment(currentDate).add(step, 'days').format('YYYY-MM-DD');
    const tempData = [];
    this.backupData.forEach(dataSets => {
      const tempDataset = cloneDeep(dataSets);
      const filteredData = filter(dataSets.data, object => moment(object.x.toString()).isBefore(currentDate) ||
       moment(object.x.toString()).isSame(currentDate));

      tempDataset.data = cloneDeep(filteredData);
      tempData.push(tempDataset);
    });

    this.myChart.config.data.datasets.forEach((dataset, index) => {
      if (tempData[index]) {
        dataset.data = cloneDeep(tempData[index].data);
      }
    });

    this.myChart.config.options.scales.xAxes.forEach(data => {
      if (step == 1) {
        data.time.unit = 'hour';
        data.time.displayFormats.day = 'YYYY-MM-DD HH';
        data.time.stepSize = step;
      } else {
        data.time.unit = 'day';
        data.time.displayFormats.day = 'MMM-DD';
        data.time.stepSize = 1;
      }
    });
    this.updateChart();
  }


  /**
   * Draws chart
   *
   * @param  {ChartData} dataSeries
   * @returns void
   */
  drawChart(): void {
    const canvas = this.chart.nativeElement;
    const ctx = canvas.getContext('2d');
    this.ctx = ctx;

    // Działająca wersja charts.js
    this._zone.runOutsideAngular(() => {
      if (this.myChart) {
        this.myChart.destroy(); // destroys chart data
      }
      this.myChart = new Chart(ctx, this.chartOptions);
      this.resizeChart();
      this.cd.detectChanges();
    });
  }

  /**
   * Odświeża dane wykresu
   *
   * @returns void
   */
  updateChart(): void {
    this.myChart.update();
    this.changeLoaderState(false);
  }

  /**
   * Toggles data
   *
   * @param  {string} key
   * @returns void
   */
  toggleData(key: string): void {
    this.changeLoaderState(true);
    const config = this.myChart.config as ChartCustomOption;
    config.toggle(key);
    this.myChart.config.data.datasets.forEach(dataset => {
      if (dataset.kind !== this.selectedKind) {
        dataset.hidden = true;
      }
      if (!!this.selectedOffset && (dataset.offset !== this.selectedOffset) && dataset.asOf) {
        dataset.hidden = true;
      }
      if (dataset.asOf) {
        dataset.hidden = true;
      }
    });
    this.checkCheckBoxesValue();
    this.saveCurrentLocalState();
  }

  /**
   * After toggle checks checkboxes states and hide/show datasets
   *
   * @returns void
   */
  checkCheckBoxesValue(): void {
    if (this.chartTitle === 'Renewables') {
      this.checkAsOfValueBasedOnKind();
    }

    if (!this.lastToogleState) {
      // jeżeli checkbox LAST jest nieaktywny, schowaj dataset last dla aktualnego rodzaju
      this.myChart.config.data.datasets.forEach((dataset, index) => {
        if ((dataset.kind === this.selectedKind)
          && dataset.offset === 0 && !dataset.asOf) {
          this.myChart.config.data.datasets[index].hidden = true;
        }
      });
    }

    if (!this.prevToogleState) {
      // jeżeli checkbox PREV jest nieaktywny, schowaj dataset prev dla aktualnego rodzaju
      this.myChart.config.data.datasets.forEach(dataset => {
        if ((dataset.kind === this.selectedKind)
          && dataset.offset === 1) {
          dataset.hidden = true;
        }
      });
    }

    // jezeli masyf legendy jest ukryty, to ukrywa jego dataseta
    this._sharedService.chartLegendHiddenFilters$.subscribe(btns => {
      this.myChart.config.data.datasets.forEach(dataset => {
        if (btns.includes(dataset.label.toLowerCase())) {
          dataset.hidden = true;
        }
      });
    });

    if (this.viewFullRange) {
      this.changeView(true);
    }
    this.updateChart();
  }

  /**
   * Sprawdza i austawia aktualną wartość selecta as of
   *
   */
  checkAsOfValueBasedOnKind(): void {
    const kind = this.selectedKind.toLowerCase();

    switch (kind) {
      case 'wind': {
        this.runDatesSelect.value = this.windRunDate;
        this.updateChart();
        break;
      }
      case 'solar': {
        this.runDatesSelect.value = this.solarRunDate;
        this.updateChart();
        break;
      }
      case 'combined': {
        this.runDatesSelect.value = this.combinedRunDate;
        this.updateChart();
        break;
      }
    }

    this.myChart.config.data.datasets.forEach(dataset => {
      if (dataset.asOf && dataset.kind.toLowerCase() === kind && dataset.runDate === this.runDatesSelect.value) {
        dataset.hidden = false;
      }
    });
  }

  /**
   * Emits selected country and chart type to parent component - chart-list
   *
   * @param  {Country} country
   * @returns void
   */
  changeDataByCountry(country: Country): void {
    this.toggleChartVisibility(true);
    const data: CountryWithTitle = {
      country: country,
      title: this.chartTitle,
    };
    this.changeDataEvent.emit(data);
    this.saveCurrentLocalState();
  }

  /**
   * Sets selected button
   *
   * @param  {Country} name
   * @returns void
   */
  setChartSelectedBtn(selectedCountry: Country): void {
    const tempCountry = this.countries.find(country => country.iso === selectedCountry.iso);
    this.currentChartCountry$.next(tempCountry || null);
  }


  /**
   * Sets
   *
   * @param  {string} name
   * @returns void
   */
  setCurrentKind(name: string): void {
    this.currentChartKind$.next(name || null);
  }

  /**
   * Resize and redraw chart
   *
   * @returns void
   */
  resizeChart(): void {
    if (this.myChart) {
      this.myChart.resize();
      this.changeLoaderState(false); // nie komentować
    }
  }

  /**
   * Changes canvas loader - true - show loader, false - hide loader
   *
   * @param  {boolean} state
   * @returns void
   */
  changeLoaderState(state: boolean): void {
    this.resizeLoaderSrc.next(state);
  }

  /**
   * Loads more data based on SELECTED offset
   *
   * @param  {string} runDate
   * @returns void
   */
  loadMoreData(runDate: string, type?: string): void {
    this.changeLoaderState(true);
    let currentDate: string = '';
    if (runDate === null) {
      this.selectedRunDate = null;
      currentDate = moment().format('YYYY-MM-DD');
      this.currentDate = currentDate;
      this.myChart.config.data.datasets.forEach((dataset, index) => {
        if (dataset.asOf) {
          this.myChart.config.data.datasets[index].hidden = true;
        }
      });
      this.updateChart();
    } else {
      // const offset = this.currentRunDates.indexOf(runDate) + 2;
      currentDate = moment(runDate).format('YYYY-MM-DD');
      let offset: number;
      switch (type) {
        case 'wind':
          this.windRunDate = runDate;
          offset = this.renewablesWindRunDates.gfs
            ? this.renewablesWindRunDates.gfs.indexOf(runDate)
            : this.renewablesWindRunDates.hres.indexOf(runDate);
          break;
        case 'solar':
          this.solarRunDate = runDate;
          offset = this.renewablesSolarRunDates.gfs
            ? this.renewablesSolarRunDates.gfs.indexOf(runDate)
            : this.renewablesSolarRunDates.hres.indexOf(runDate);
          break;
        case 'combined':

          this.combinedRunDate = runDate;
          offset = this.renewablesCombinedRunDates
            ? this.renewablesCombinedRunDates.gfs.indexOf(runDate)
            : this.renewablesCombinedRunDates.hres.indexOf(runDate);
          break;
      }

      this.selectedRunDate = runDate;
      this.selectedOffset = offset;
      this.queryParams.offset = offset;
      this.checkSelectedRunDate(offset, runDate, type);
    }

    this.firstDateSrc.next(currentDate);
    this.getOneDay(currentDate);
  }

  /**
   * Checks if datasets for selected run date was downloaded
   *
   * @param  {string} runDate
   * @returns void
   */
  checkSelectedRunDate(offset: number, runDate: string, type?: string): void {
    const searchedIndexes: string[] = [];
    this.myChart.config.data.datasets.forEach(dataset => {
      if (dataset.asOf && (dataset.offset === offset)
        && dataset.kind === this.selectedKind) {
        dataset.hidden = true;
        // wyszukaj indeksy wszystkich datasetow spełniających warunki
        searchedIndexes.push(this.myChart.config.data.datasets.indexOf(dataset));
      }
    });

    if (!(searchedIndexes.length)) {
      this.getDataByOffset(offset, runDate, true, type);
    } else {
      searchedIndexes.forEach(searchedIndex => {
        this.myChart.config.data.datasets.forEach((dataset, index) => {
          if (searchedIndex === index) {
            this.myChart.config.data.datasets[index].hidden = false;
          } else if (searchedIndex !== index
            && dataset.asOf && dataset.offset !== offset) {
              this.myChart.config.data.datasets[index].hidden = true;
            }
          });
        });
      }
    this.updateChart();
    this.cd.detectChanges();
  }

  /**
   * Add new dataset to chart and
   *
   * @param  {DataSets} newDataset
   * @returns void
   */
  addNewDataSet(newDataset: DataSets): void {
    this.myChart.config.data.datasets.push({...newDataset});
    this.backupData.push({...newDataset});
    this.calculateAverage();
    if (this.viewFullRange) {
      this.changeView(true);
    } else {
      this.getOneDay(this.currentDate);
    }
  }

  /**
   * Checks last/prev checkbox
   *
   * @param  {boolean} checked
   * @param  {string} kind
   * @returns void
   */
  checkCheckBoxKind(checked: boolean, kind: string): void {
    this.changeLoaderState(true);
    const tempDates = cloneDeep((this.lastAndPrevRunDate));
    if (kind === 'last') {
      this.lastToogleState = !this.lastToogleState;
    }
    if (kind === 'prev') {
      this.prevToogleState = !this.prevToogleState;
    }
    switch (kind) {
      case 'last':
        this.loadLastOrPrevRunDate(checked, tempDates[0], 0);
        break;
      case 'prev':
        this.loadLastOrPrevRunDate(checked, tempDates[1], 1);
        break;
    }
    this.saveCurrentLocalState();
  }

  /**
   * Load last or
   *
   * @param  {boolean} checkBoxValue
   * @param  {string} runDate
   * @param  {number} offset
   * @returns void
   */
  loadLastOrPrevRunDate(checkBoxValue: boolean, runDate: string, offset: number): void {
    const searchedIndex: number[] = [];

    // jeżeli run date = LAST
    if (offset === 0) {
      this.myChart.config.data.datasets.forEach((dataset) => {
        if (dataset.offset === offset && dataset.kind === this.selectedKind) {
          dataset.hidden = !dataset.hidden;
        }
        this.updateChart();
      });
    } else {
      // jeżeli run date = PREV, sprawdź jego wartość
      if (checkBoxValue) {
        // jeżeli checkbox jest zaznaczony sprawdź, czy jakiś dataset zawiera obecny rundate oraz czy zawiera aktualny rodzaj
        // jeżeli dataset istnieje ustawia flagę na true, pushuje indeksy datasetow do tablicy
        this.myChart.config.data.datasets.forEach((dataset, index) => {
          if ((dataset.offset === offset)
            && dataset.kind === this.selectedKind) {
            searchedIndex.push(index);
          }
        });
        if (!!searchedIndex.length) {
          searchedIndex.forEach(index => {
            this.myChart.config.data.datasets[index].hidden = !this.myChart.config.data.datasets[index].hidden;
          });
          this.updateChart();
        } else {
          this.queryParams.offset = offset;
          this.getDataByOffset(offset, runDate, false, this.selectedKind);
        }
      } else {
        this.myChart.config.data.datasets.forEach(dataset => {
          if ((dataset.offset === offset)
            && dataset.kind === this.selectedKind) {
              dataset.hidden = !dataset.hidden;
          }
        });
      }
    }

    // jezeli przyciski sa odznaczone ukrywa wszystkie dane
    if (!this.prevToogleState && !this.lastToogleState) {
      this.myChart.config.data.datasets.forEach(dataset => {
        if (!this.prevToogleState && !this.lastToogleState) {
          dataset.hidden = true;
        }

        if (!this.prevToogleState && !this.lastToogleState
          &&  dataset.runDate === this.runDatesSelect.value ) {
            dataset.hidden = false;
          }
      });
    }

    // jezeli dane nie wystepuja w danym rodzaju
    if (checkBoxValue === true) {
      this.myChart.config.data.datasets.forEach(dataset => {
        if (dataset.kind !== this.selectedKind) {
          dataset.hidden = true;
        }
      });
    }

    this.updateChart();
  }


  // // ZOSTAWIC! POTRZEBNE DO LEGENDY PRZY FULLSCREENIE WYKRESU WEATHER
  // customActionToHideDataSetBasedOnLegendOnlyForWeather(dataset: DataSets) {
  //   //TO DO
  // }
  // // ZOSTAWIC! POTRZEBNE DO LEGENDY PRZY FULLSCREENIE WYKRESU WEATHER
  // checkIfDataSetNeedToBeHiddenBasedOnFullscreenFiltersAndOffset(dataset: DataSets, offset: number): boolean {
  //   //TO DO
  //   return false;
  // }


  /**
   * Gets data from service by offset and current queryParams
   *
   * @param  {string} runDate
   * @param  {ChartQueryParams} queryParams
   * @returns void
   */
  getDataByOffset(offset: number, runDate?: string, asOf?: boolean, type?: string): void {
    let dataRequest$: Observable<any> = of('');
    this.currentModels.forEach((model: string) => {
      const params = this.queryParams;
      params.model = model;
      const color = this.checkColor(model);

      if (asOf) {
        params.runDate = runDate;
        params.offset = null;
      } else {
        params.runDate = null;
      }

      if (asOf && type === 'wind') {
        dataRequest$ = this._chartService.getWindData(type, params);
      } else if (asOf && type === 'solar') {
        dataRequest$ = this._chartService.getSolarData(type, params);
      } else if (asOf && type === 'combined') {
        dataRequest$ = this._chartService.getCombinedData(type, params);
      } else if (!asOf) {
        const windData$ = this._chartService.getWindData('wind', params);
        const solarData$ = this._chartService.getSolarData('solar', params);
        const combinedData$ = this._chartService.getCombinedData('combined', params);
        dataRequest$ = forkJoin(windData$, solarData$, combinedData$);
      }

      dataRequest$.pipe(map(res => {
        const data = [];
        if (isArray(res)) {
          res.forEach(resData => {
            if (!isEmpty(resData)) {
              data.push(resData);
            }
          });
        } else {
          if (!isEmpty(res)) {
            data.push(res);
          }
        }

        const filteredData = [];
        for (const key in data) {
          if (data[key].length) {
            for (const element in data[key]) {
              if (data[key].hasOwnProperty(key)) {
              filteredData.push(element);
              }
            }
          } else {
            filteredData.push(data[key]);
          }
        }
        return filteredData;
      })).pipe(
        debounceTime(200)
      )
        .subscribe((resData: any) => {
            resData.forEach(data => {
              const newDataSet = new DataSets(data.name, data.series, model, data.kind, data.runDate, offset, color.main, asOf);
              if (newDataSet.asOf) {
                newDataSet['borderDash'] = [5, 5];
              } else {
                newDataSet['borderColor'] = color.opac;
                newDataSet['borderDash'] = [0.5, 0.5];
              }

              // jeżeli nowy set nie należy do obecnego rodzaju ukryj go
              if (!(newDataSet.kind === this.selectedKind)) {
                newDataSet.hidden = true;
              }
              this.myChart.config.data.datasets.forEach(dataset => {
                if (dataset.asOf && dataset.offset !== this.selectedOffset) {
                  dataset.hidden = true;
                }
                if (dataset.kind !== this.selectedKind) {
                  dataset.hidden = true;
                }
              });
              this.addNewDataSet(newDataSet);
            });

          },
          (error) => {
            this._sharedService.checkIfUsersIsAlreadyLoggedIn(error);
          }
        );
    });
  }


  /**
   * Checks dataseries color based on model
   *
   * @param  {string} model
   * @returns string
   */
  checkColor(model: string): any {
    switch (model) {
      case 'gfs':
        return {
          main: '#c1342c',
          opac: 'rgba(193, 52, 44, 0.75)'
        };
      case 'hres':
        return {
          main: '#195f9a',
          opac: 'rgba(25, 95, 154, 0.75)'
        };
      default:
        break;
    }
  }

  /**
   * Resizuje wykres na buttonie u góry
   *
   * @param  {string} charType
   * @param  {boolean} chartFullScreen
   * @returns string
   */
  setFullscreen(chartType: string, chartFullScreen: boolean): void {
    if (!!chartType) {
      if (chartFullScreen === true) {
        this._sharedService.setFullscreenChart(chartType);
        this.fullscreenActive = true;
      } else {
        this._sharedService.setFullscreenOffChart(chartType);
        this.fullscreenActive = false;
      }
    }
  }

  /**
   * Changes View from hourly to
   *
   * @param  {boolean} changeState?
   * @returns void
   */
  changeView(changeState?: boolean): void {
    this.changeLoaderState(true);

    if (!changeState) {
      this.viewFullRange = !this.viewFullRange;
    }

    if (this.viewFullRange && this.myChart.config.data  && this.myChart.config.data.datasets.length > 0) {
      this.myChart.config.data.datasets.forEach((dataset, index) => {
        if (this.meanData[index]) {
          dataset.data = cloneDeep(this.meanData[index].data);
        }
      });
      this.myChart.config.options.scales.xAxes.forEach(data => {{
        data.time.unit = 'day';
        data.time.displayFormats.day = 'MMM DD';
        data.time.stepSize = 1;
      }
      });
      this.updateChart();
    } else {
      this.myChart.config.options.scales.xAxes.forEach(data => {
        data.time.unit = 'hour';
        data.time.displayFormats.day = 'MMM DD HH';
        data.time.stepSize = 1;
      });
      this.getOneDay(this.currentDate.toString());
    }
    this.saveCurrentLocalState();
    this.changeLoaderState(false);
  }

  /**
   * Zapamietywanie danych w checboxach last i prev i wyswietlanie danych
   *
   * @returns void
   */
  checkMenuCheckboxValue(): void {
    this.changeLoaderState(true);
    const tempDates = cloneDeep((this.lastAndPrevRunDate));

    if (!!this.chartState && !!this.chartState.country) {
      if (this.countries.length > 0) {
        const tempCountry = this.countries.find(country => country.iso === this.chartState.country);
        const initialCountry = !tempCountry && !!this.selectedCountry ? this.selectedCountry : tempCountry;
        this.setChartSelectedBtn(initialCountry);
      }
    }

    if (!!this.chartState && this.chartState.prev === true) {
      this.prevToogleState = this.chartState.prev;
      this.loadLastOrPrevRunDate(true, tempDates[0], 1);
    }


    if (!!this.chartState && this.chartState.runDate) {
      this.runDatesSelect.value = this.chartState.runDate;
      this.loadMoreData(this.runDatesSelect.value, this.selectedKind.toLowerCase());
    }
  }

  /**
   * Pobiera z serwisu i przypisuej wybrany stan wykresu do lokalnej zmiennej
   *
   * @returns void
   */
  getChartState(): ChartState {
    return this._chartsStateService.getChartState(`${this.chartTitle.toLowerCase()}State`) as ChartState;
  }

  /**
   * Przypisuje aktualny stan filtrow do  lokalnej zmiennej
   *
   * @returns void
   */
  saveCurrentLocalState(): void {
    const iso = !!this.selectedCountry ? this.selectedCountry.iso : '';
    const selectValue = !!this.runDatesSelect ? this.runDatesSelect.value : '';

    const state = new ChartState(this.lastToogleState, this.prevToogleState, selectValue,
      this.viewFullRange, iso, this.selectedKind);
    this.chartState = state;
    this._chartsStateService.saveChartState(this.chartTitle.toLowerCase(), state);
  }

  /**
  * Zmiana kolorow dla legendy canvasa i siatki
  *
  * @return void
  */
  changeChartLegendColorByTheme(): void {

    const gridColorDark = {
      color : 'rgba(255,255,255, 0.4)',
      zeroLineColor : 'rgba(255,255,255, 0.4)'
    };

    const xAxesDark = 'rgba(255,255,255, 0.9)';
    const yAxesDark = 'rgba(255,255,255, 0.9)';

    const gridColorDefault = {
      color : 'rgba(41,41,41, 0.4)',
      zeroLineColor : 'rgba(41,41,41, 0.4)'
    };
    const xAxesDefault = 'rgba(0,0,0, 0.8)';
    const yAxesDefault = 'rgba(0,0,0, 0.8)';

    if (this.chartOptions) {
      if (this.themeName === 'dark_theme') {
        this.chartOptions.options.scales.xAxes[0].ticks.minor.fontColor = xAxesDark;
        this.chartOptions.options.scales.xAxes[0].gridLines.color = gridColorDark.color;
        this.chartOptions.options.scales.yAxes[0].ticks.minor.fontColor = yAxesDark;
        this.chartOptions.options.scales.yAxes[0].gridLines = gridColorDark;
      } else {
        this.chartOptions.options.scales.xAxes[0].ticks.minor.fontColor = xAxesDefault;
        this.chartOptions.options.scales.xAxes[0].gridLines.color = gridColorDefault.color;
        this.chartOptions.options.scales.yAxes[0].ticks.minor.fontColor = yAxesDefault;
        this.chartOptions.options.scales.yAxes[0].gridLines = gridColorDefault;
      }
    }

    if (this.myChart) {
      this.myChart.update();
    }
  }

  ngOnDestroy() {
    this.loaderSub.unsubscribe();
    this._guard$.next();
    this.chartState = null;
    this.myChart = null;
  }
}
