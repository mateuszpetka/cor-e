import {Component, EventEmitter, OnInit, Output, Renderer2} from '@angular/core';
import { UserFormService } from '../userform.service';
import { AppService } from 'app/app.service';

@Component({
  selector: 'user-form-settings',
  templateUrl: './user-form-settings.component.html',
  styleUrls: ['./user-form-settings.component.scss']
})
export class UserFormSettingsComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  public openTab: boolean = false;
  public defaultThemeSelect: string;
  constructor(private _userFormService: UserFormService, private _appService: AppService, private _renderer?: Renderer2) { }

  ngOnInit() {
    const localStorage = this._appService.getUserFromLocalStorage();
    this.defaultThemeSelect = localStorage.customFields.themeDisplay;
  }

  /**
   * Przełaczanie szablonu
   *
   * @param themeName: string
   * @return void
   */
  themeSwitcher(themeName: string): void {
    this.defaultThemeSelect = themeName;
    this.valueChange.emit(themeName);
  }

  /**
   * Show/Hide Tabs
   *
   * @param tab: HTMLElement
   * @param content: HTMLElement
   * @returns void
   */
  toggleTab(tab: HTMLElement, content: HTMLElement): void {
    if(tab.classList.contains('openTab')) {
      this._renderer.removeClass(tab, 'openTab');
      this._renderer.removeClass(content, 'openTabContent');
    } else {
      this._renderer.addClass(tab, 'openTab');
      this._renderer.addClass(content, 'openTabContent');
    }
  }
}
