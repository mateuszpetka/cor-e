import {Component, OnDestroy, OnInit, Renderer2} from '@angular/core';
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { SharedService } from "../../../shared/shared.service";
import { takeUntil } from "rxjs/operators";
import { ChartDisplay } from "../../../classes/classes";

@Component({
  selector: 'user-saved-settings',
  templateUrl: './user-saved-settings.component.html',
  styleUrls: ['./user-saved-settings.component.scss']
})
export class UserSavedSettingsComponent implements OnInit, OnDestroy {
  public chartsSrc: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  public charts$: Observable<any> = this.chartsSrc.asObservable();
  public modelsSrc: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  public models$: Observable<any> = this.modelsSrc.asObservable();
  public countriesSrc: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  public countries$: Observable<any> = this.countriesSrc.asObservable();
  private guard$ = new Subject();

  constructor(private _sharedService: SharedService, private _renderer?: Renderer2) {}

  ngOnInit() {
    this._sharedService.userData$
      .pipe(
        takeUntil(this.guard$)
      ).subscribe(localStorage => {
      if (localStorage) {
        const tempCharts = [];
        const tempCountries = [];
        const tempModels = [];
        if (localStorage.customFields) {
          Object.keys(localStorage.customFields).forEach((key) => {
            if (key.includes('State')) {
              const newName = key.replace('State', '').toLocaleUpperCase();
              if (localStorage.customFields[key]) {
                const tempObj = new ChartDisplay(newName, JSON.parse(localStorage.customFields[key]));
                tempCharts.push(tempObj);
              }
            }

            if (key.includes('filterCountries')) {
              JSON.parse(localStorage.customFields[key]).forEach(country => {
                tempCountries.push(country);
              });
            }
            if (key.includes('filterModels')) {
              JSON.parse(localStorage.customFields[key]).forEach(model => {
                tempModels.push(model);
              });
            }
          });
        }
        this.chartsSrc.next(tempCharts);
        this.modelsSrc.next(tempModels);
        this.countriesSrc.next(tempCountries);
      }
    });
  }

  /**
   * Show/Hide Tabs
   *
   * @param tab: HTMLElement
   * @param content: HTMLElement
   * @returns void
   */
  toggleTab(tab: HTMLElement, content: HTMLElement): void {
    if(tab.classList.contains('openTab')) {
      this._renderer.removeClass(tab, 'openTab');
      this._renderer.removeClass(content, 'openTabContent');
    } else {
      this._renderer.addClass(tab, 'openTab');
      this._renderer.addClass(content, 'openTabContent');
    }
  }

  ngOnDestroy() {
    this.guard$.next();
  }
}
