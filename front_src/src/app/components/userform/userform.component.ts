

import {Component, OnChanges, OnInit} from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Observable , BehaviorSubject, Subject} from 'rxjs';
import { debounceTime, startWith } from 'rxjs/operators';

import { CustomHttpService } from '../../shared/customHttp.service';
import { SharedService } from '../../shared/shared.service';
import { AppService } from './../../app.service';
import { Country, DashboardLayouts, LoggedUserInitialData, PatchedData, PhoneNumber } from './../../classes/classes';
import { ChartsStateService } from './../../shared/chars-state.service';
import { IsoCountries, UserFormService } from './userform.service';

@Component({
  selector: 'userform',
  templateUrl: './userform.component.html',
  styleUrls: ['./userform.component.scss']
})
export class UserformComponent implements OnInit {
  public form: FormGroup;
  public countryCtrl: FormControl;
  public filteredCountries: Observable<Country[]>;
  public countries: Country[];
  public phonePrefix: Country[];
  public formBuilded: boolean = false;
  public controls: string[];
  public DashboardLayouts: DashboardLayouts[];
  private _originalDataBackup: Country[];
  private _searchedDataBackup: Country[];
  public _userData: LoggedUserInitialData;
  private _themeName: string;
  public userData: any;

  constructor(private _formBuilder: FormBuilder,
    private _userFormService: UserFormService,
    private customHttpService: CustomHttpService,
    private chartsStateService: ChartsStateService,
    private _sharedService: SharedService,
    private _snackBar: MatSnackBar,
    private _appService: AppService) {
    this.createForm();
  }

  ngOnInit() {
    this._originalDataBackup = this.countries = this.phonePrefix = IsoCountries;
    this.form.controls.country.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
      ).subscribe((searchedValue: string) => {
        this.countries = !!searchedValue ? this.filterCountries(searchedValue) : this._originalDataBackup;
      });
    this._userData = this._appService.getUserFromLocalStorage();
    this._sharedService.userDataSrc.next(this._userData);

    this.patchesCtrlsValue();
    this.getDashboardLayouts();
  }

  /**
   * Filters countries by name
   *
   * @param  {string} name
   * @returns Country
   */
  filterCountries(name: string): Country[] {
    const data = !!this._searchedDataBackup ? this._searchedDataBackup : this._originalDataBackup;
    return data.filter(country => {
      return country.name.toLowerCase().startsWith(name.toLowerCase());
    });
  }

  /**
   * Builds form controls
   *
   * @returns void
   */
  createForm(): void {
    this.form = this._formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['',  Validators.compose([Validators.required, Validators.email])],
      zipCode: [''],
      street: [''],
      city: [''],
      country: [''],
      phonePrefix: [''],
      phoneNumber: [''],
      filterModels: [''],
      filterCountries: [''],
      themeDisplay : ['']
    });
    this.controls = Object.keys(this.form.controls);
    this.formBuilded = true;
   }

   /**
    * Patches form controls value
    *
    * @returns void
    */
   patchesCtrlsValue(): void {
    // patches custom fields values
    if (!!this._userData.customFields) {
      Object.keys(this._userData.customFields).forEach((key) => {
        this.patchFieldsControlValues(key, this._userData.customFields[key]);
      });
    }

    // patch email adress ctrl
    const emailCtrl = this.form.get('email');
    emailCtrl.patchValue(this._userData.email);
   }

  /**
   * Patches control value
   *
   * @param  {number} index
   * @returns void
   */
  patchFieldsControlValues(key: any, value: any): void {
    switch (key) {
      case 'countryIso':
        const searchedCountry: Country = this._userFormService.searchCountryByIso(value);
        const countryCtrl: AbstractControl = this.form.get('country');
        if (searchedCountry) {
          countryCtrl.patchValue(searchedCountry.name);
        }
        break;
      default:
        const fieldCtrl: AbstractControl = this.form.get(key);
        if (!!fieldCtrl) {
          fieldCtrl.patchValue(value);
        }
        break;
    }
  }

  /**
   * Saves user data to service
   *
   * @returns void
   */
  saveUserData(form: FormGroup): void {
    if (form.invalid) {
      this._snackBar.open('Please fill all required fields', 'Ok', {duration: 1000});
      return;
    }
    const ctrlsTempObj: object[] = [];
    this._userData = this._appService.getUserFromLocalStorage();
    const tempUser = this._appService.getUserFromLocalStorage();
    const filterCountries = tempUser.customFields.filterCountries;
    const filterModels = tempUser.customFields.filterModels;

    Object.keys(form.controls).forEach(control => {
      const fieldCtrl: AbstractControl = form.get(control);
      ctrlsTempObj.push({
        [control]: fieldCtrl.value
      });
    });

    let patchedData: PatchedData = new PatchedData(ctrlsTempObj, this._userFormService);
    if (patchedData.email === this._userData.email) {
      delete patchedData['email'];
    }
    patchedData['themeDisplay'] = this._themeName;
    patchedData['filterCountries'] = filterCountries;
    patchedData['filterModels'] = filterModels;
    patchedData = this.createChartsStatesFields(patchedData);
    delete patchedData['_service'];

    this._userFormService.updateUser(patchedData, this._userData.token).subscribe(dataResponse => {
      const newUserData = this._userData;
      newUserData.customFields = patchedData;
      this._appService.saveUserToLocalStorage(newUserData);
      if (this._themeName) {
        this._appService.changeThemeClass(this._themeName);
        this._sharedService.updateDashTemplateSrc.next(this._themeName);
      }
      this._snackBar.open('Save successful', 'Ok', {duration: 1000});
    },
    () => this._snackBar.open('Server error', 'Ok', {duration: 1000}));
  }

  /**
   * Tworzy customowe pola dla stanow wykresow
   *
   * @param  {PatchedData} patchedData
   * @returns PatchedData
   */
  createChartsStatesFields(patchedData: PatchedData): PatchedData {
    const statesList = this.chartsStateService.chartStatesList;
    statesList.forEach((stateName: string) => {
      patchedData[`${stateName}`] = this._userData.customFields[`${stateName}`];
    });
    return patchedData;
  }

  /**
   * Get all dashboard layouts
   *
   * @returns void
   */
  getDashboardLayouts(): void {
    this.customHttpService.get('DashboardLayout').subscribe(res => {
      const newItems = [];
      while (res.length) {
        const splitedItems = res.splice(0, 4);
        newItems.push(splitedItems);
      }
      this.DashboardLayouts = newItems;
    });
  }

  /**
   * Po zmianie wartosci szablonu nadpisuje aktualny szablon
   *
   * @returns void
   */
  displayContent($event): void {
    this._themeName = $event;
    this._appService.changeThemeClass($event);
    this._sharedService.updateDashTemplateSrc.next($event);
  }

  /**
   * Delete single layout
   *
   * @returns void
   */
  deleteLayout(): void {
    console.log('deleteLayout');
  }

  /**
   * Edit single layout
   *
   * @returns void
   */
  editLayout(): void {
    console.log('editLayout');
  }
}
