import { Component, OnInit, ViewChild } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

import {tap } from 'rxjs/operators';
import {of as observableOf, Observable } from 'rxjs';
import { SharedService } from '../../shared/shared.service';
import {ChartComponent} from '../chart/chart.component';
import {CustomHttpService} from '../../shared/customHttp.service';

import { CountryDb, Country, Model } from './../../classes/classes';

@Component({
  selector: 'globalfilters',
  templateUrl: './globalfilters.component.html',
  styleUrls: ['./globalfilters.component.scss'],
  animations: [
    trigger('dropMenu', [
      state('inactive', style({
        height: '0',
      })),
      state('active',  style({
        height: '*'
      })),
      transition('inactive => active', animate('400ms ease-in')),
      transition('active => inactive', animate('400ms ease-out'))
    ])
  ]
})
export class GlobalFiltersComponent implements OnInit {
  @ViewChild('renewablesChart') renewablesChart: ChartComponent;
  public colapseFilters: boolean = false;
  public countries$: Observable<Country[]>;
  public activeModel$: Observable<string[]>  =  observableOf(['hres']);
  public currentModel$: Observable<any>;
  public selectedCountries: Country[] = [];
  public selectedCountries$: Observable<Country[]>;
  public dropMenuState: string = 'active';
  public dropModelMenuState: string = 'active';
  public countriesLoaded: boolean = false;
  public models: string[] = [];
  public filterMenuState$: Observable<boolean>; // Observable of filters menu state
  public models$: Observable<Model[]>;
  public forecastPublicationTimes: string;

  constructor(private _sharedService: SharedService, private customHttpService: CustomHttpService) {}

  ngOnInit() {
    this.filterMenuState$ = this._sharedService.colapseFilters$;
    this.currentModel$ = this._sharedService.currentModels$;
    this.getMenuForecastPublicationTimes();

    this.models$ = this._sharedService.getModels();

    this.countries$ = this._sharedService.getCountries(); // all available countries

    this.selectedCountries$ = this._sharedService.selectedCountries$
    .pipe(
      tap((selectedCoutriesRes: Country[]) => {
        this.selectedCountries = selectedCoutriesRes;
      })
    );
  }

  /**
   * Converts country type from CountryDb to Country
   *
   * @param  {CountryDb} country
   * @returns Country
   */
  convertCountryType(country: CountryDb): Country {
    return new Country(country.countryName, country.countryIsoCode);
  }

  /**
   * Changes countries menu state
   *
   * @returns void
   */
  changeState(): void {
    this.dropMenuState = (this.dropMenuState === 'active') ? 'inactive' : 'active';
  }

  /**
   * Changes models menu state
   *
   * @returns void
   */
  changeModelState() {
    this.dropModelMenuState  = (this.dropModelMenuState === 'active') ? 'inactive' : 'active';
  }

  /**
   * Changes selected model filter
   *
   * @param  {} model
   * @returns void
   */
  changeModel(model: string): void {
    this._sharedService.setModels(model);
  }

  /**
   * Passes filters menu state to service
   *
   * @returns void
   */
  colapseFiltersMenu(): void {
    this.colapseFilters = !this.colapseFilters;
    this._sharedService.colapseFiltersMenu( this.colapseFilters)
  }

  /**
   * Textarea do edycji czasu w menu
   *
   * @returns void
   */
  getMenuForecastPublicationTimes(): void {
    this.customHttpService.get('MenuPublicationTime').subscribe(res => {
      this.forecastPublicationTimes = res[0].publicationTimeTextarea;
    })
  }
}
