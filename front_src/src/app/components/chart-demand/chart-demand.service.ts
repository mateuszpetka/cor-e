import { DataSeries, ChartDbData, ChartData } from './../../classes/chart';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import {ChartQueryParams, Params} from '../../classes/classes';
import { SharedService } from '../../shared/shared.service';
import { CustomHttpService } from "../../shared/customHttp.service";

@Injectable()
export class ChartDemandService {

  constructor(private _http: CustomHttpService, private _sharedService: SharedService) {}

  /**
   * Gets data for prices chart
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  getDemandData(queryParams: ChartQueryParams): Observable<ChartData> {
    const params = new Params(queryParams.iso, queryParams.model, queryParams.offset, queryParams.runDate);
    return this._http
      .get(`ChartData/demand?` + this._sharedService.toQueryString(params));
  }
  
  /**
   * Gets data for prices chart
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  getResidualDemandData(queryParams: ChartQueryParams): Observable<ChartData> {
    const params = new Params(queryParams.iso, queryParams.model, queryParams.offset, queryParams.runDate);
    return this._http
      .get(`ChartData/residualDemand?`+ this._sharedService.toQueryString(params));
  }
}
