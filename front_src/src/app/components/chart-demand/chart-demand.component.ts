
import {
  Component, ElementRef,
  NgZone, ChangeDetectorRef, AfterViewInit, Input
} from '@angular/core';
import {of as observableOf,  Observable ,  forkJoin, BehaviorSubject } from 'rxjs';
import { combineLatest ,  map ,  takeUntil ,  debounceTime ,  delay } from 'rxjs/operators';

import { ChartService } from './../chart/chart.service';
import { SharedService } from './../../shared/shared.service';
import { DataSets, ChartData, ModelRunDates } from '../../classes/chart';
import { ChartDemandService } from './chart-demand.service';
import { ChartsStateService } from '../../shared/chars-state.service';
import { AppService } from '../../app.service';

import { ChartComponent } from './../chart/chart.component';
import { isEmpty, isArray } from 'lodash';
import { Chart } from 'chart.js';
import * as moment from 'moment';

@Component({
  selector: 'chart-demand',
  templateUrl: './chart-demand.component.html',
  styleUrls: ['./chart-demand.component.scss']
})
export class ChartDemandComponent extends ChartComponent implements AfterViewInit {
  private kind: string = '';
  @Input() demandRunDates: ModelRunDates;
  @Input() residualDemandRunDates: ModelRunDates;
  private selectedKindSrc: BehaviorSubject<string> = new BehaviorSubject<string>('Demand');
  public currentSelectedKind$: Observable<string> = this.selectedKindSrc.asObservable();
  public legendLabels: string[];

  private demandRunDate: string = '';
  private residualDemandRunDate: string = '';

  constructor(public sharedService: SharedService, public chartsStateService: ChartsStateService,
    public chartService: ChartService, public zone: NgZone, public cdRef: ChangeDetectorRef,
    private _chartDemandSerive: ChartDemandService, public elementRef?: ElementRef, public _appService?: AppService) {
    super(chartService, sharedService, chartsStateService, cdRef, zone)
  }

  ngAfterViewInit() {
    this.initialKind$ = observableOf('Demand');

    this.loaderSub = this.sharedService.loadingDemand$
      .pipe(
        takeUntil(this._guard$),
        debounceTime(200)
      ).subscribe((loadingState: boolean) => {
        this.labels = ['Demand', 'Residual Demand'];
        const initialKind = !!this.kind ? this.kind : 'Demand';
        this.chartSetup(loadingState, initialKind, this.labels);

        if (!loadingState && this.chartOptions && this.chartOptions.data.datasets.length) {
          // legenda
          const newLabels = [];
          this.legendLabels = this.chartOptions.data.datasets.map(set => {
            const labelValue = set.label.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
            newLabels.push({
              value: labelValue, name: set.label
            });
            return set.label.trim();
          });
          this.legendLabels = newLabels;
        }
    });

    // obserwuje obecnie wybrany rodzaj, przypisuje do zmiennej
    this.currentKind$ = this.initialKind$.pipe(
      takeUntil(this._guard$),
      combineLatest(this.currentChartKind$),
      map(([initialKind, selectedKind]) => {
        const currentKind = selectedKind ? selectedKind : initialKind;
        this.selectedKindSrc.next(currentKind);
        this.selectedKind = selectedKind;
        this.kind = selectedKind;
        return selectedKind ? selectedKind : initialKind;
      })
    );
  }

    /**
   * Sets initial data - wind datasets visible
   *
   * @returns void
   */
  setInitialData(key: string): void {
    const data = [...this.chartOptions.data.datasets];
    data.forEach(dataset => {
      dataset.hidden = (dataset.label.includes(key)) ? false : true;
      if (this.chartTitle === 'Demand') {
        this.setInitialColors(dataset);
      }
      dataset.hidden = (dataset.kind === key) ? false : true;
    });

    this.chartOptions.data.datasets = data;
    this.currentChartKind$.next(key);
    this.updateChart();
    this.cdRef.detectChanges();
  }

  /**
   * Draws chart, modifies chart options
   *
   * @returns void
   */
  drawChart(): void {
    // Global chart settings
    Chart.defaults.global.defaultFontFamily = ' Overpass';
    Chart.defaults.global.defaultFontSize = 10;

    const canvas = this.chart.nativeElement;
    const ctx = canvas.getContext('2d');

    // draw chart outside angular zone
    this.zone.runOutsideAngular(() => {
      if (this.myChart) {
        this.myChart.destroy();
      }
      this.myChart = new Chart(ctx, this.chartOptions);
      this.resizeChart();
    })
  }

  /**
   * Loads more data based on SELECTED offset
   *
   * @param  {string} runDate
   * @returns void
   */
  loadMoreData(runDate: string, type?: string): void {
    this.changeLoaderState(true);
    let currentDate: string = '';
    if (runDate === null) {
      this.selectedRunDate = null;
      currentDate = moment().format('YYYY-MM-DD');
      this.currentDate = currentDate;

      this.myChart.config.data.datasets.forEach((dataset, index) => {
        if (dataset.asOf) {
          this.myChart.config.data.datasets[index].hidden = true;
        }
      });

      this.updateChart();
    } else {
      currentDate = moment(runDate).format('YYYY-MM-DD');
      let offset: number;
      switch (type) {
        case 'demand':
          this.demandRunDate = runDate;
          offset = this.demandRunDates.gfs
            ? this.demandRunDates.gfs.indexOf(runDate)
            : this.demandRunDates.hres.indexOf(runDate);
          break;
        case 'residual-demand':
          this.residualDemandRunDate = runDate;
          offset = this.residualDemandRunDates.gfs
            ? this.residualDemandRunDates.gfs.indexOf(runDate)
            : this.residualDemandRunDates.hres.indexOf(runDate);
          break;
      }

      this.selectedOffset = offset;
      this.selectedRunDate = runDate;
      this.queryParams.offset = offset;
      this.checkSelectedRunDate(offset, runDate, type);
    }

    this.getOneDay(currentDate);
  }

  /**
   * Sprawdza i austawia aktualną wartość selecta as of
   *
   */
  checkAsOfValueBasedOnKind(): void {
    const kind = this.selectedKind.toLowerCase();

    switch (kind) {
      case 'demand': {
        this.runDatesSelect.value = this.demandRunDate;
        this.updateChart();
        break;
      }
      case 'residual demand': {
        this.runDatesSelect.value = this.residualDemandRunDate;
        this.updateChart();
        break;
      }
    }

    this.myChart.config.data.datasets.forEach(dataset => {
      if (dataset.asOf && dataset.kind.toLowerCase() === kind && dataset.runDate === this.runDatesSelect.value) {
        dataset.hidden = false;
      }
    });
  }

  /**
   * Gets data from service by offset and current queryParams
   *
   * @param  {string} runDate
   * @param  {ChartQueryParams} queryParams
   * @returns void
   */
  getDataByOffset(offset: number, runDate: string, asOf?: boolean, type?: string): void {
    if (!!type) {
      type = type.toLowerCase();
    }
    let dataRequest$: Observable<any> = observableOf('');
    this.currentModels.forEach((model: string) => {
      const params = this.queryParams;
      params.model = model;
      params.offset = offset;
      const color = this.checkColor(model);

      if (asOf) {
        params.runDate = runDate;
        params.offset = null;
      } else {
        params.runDate = null;
      }

      if (asOf && type === 'demand') {
        dataRequest$ = this._chartDemandSerive.getDemandData(params);
      } else if (asOf && type === 'residual-demand') {
        dataRequest$ = this._chartDemandSerive.getResidualDemandData(params);
      } else if (!asOf) {
        const demand$ = this._chartDemandSerive.getDemandData(params);
        const residualDemand$ = this._chartDemandSerive.getResidualDemandData(params);
        dataRequest$ = forkJoin(demand$, residualDemand$);
      }

      dataRequest$.pipe(map(res => {
        const data = [];
        if (isArray(res)) {
          res.forEach(resData => {
            if (!isEmpty(resData)) {
              data.push(resData)
            }
          })
        } else {
          if (!isEmpty(res)) {
            data.push(res)
          }
        }

        const filteredData = [];
        for (const key in data) {
          if (data[key].length) {
            data[key].forEach(element => {
              filteredData.push(element);
            });
          } else {
            filteredData.push(data[key]);
          }
        }
        return filteredData;
      })).pipe(
          debounceTime(200)
        ).subscribe((resData: ChartData[]) => {
          resData.forEach(data => {
            const newDataSet = new DataSets(data.name, data.series, model, data.kind, data.runDate, offset, color.main, asOf);
              if (newDataSet.asOf) {
                newDataSet['borderDash'] = [5, 5];
              } else {
                newDataSet['borderColor'] = color.opac;
                newDataSet['borderDash'] = [0.5, 0.5];
              }

              // jeżeli nowy set nie należy do obecnego rodzaju ukryj go
              if (!(newDataSet.kind === this.selectedKind)) {
                newDataSet.hidden = true;
              }
              this.myChart.config.data.datasets.forEach(dataset => {
                if (dataset.asOf && dataset.offset !== this.selectedOffset) {
                  dataset.hidden = true;
                }
                if (dataset.kind !== this.selectedKind) {
                  dataset.hidden = true;
                }
              });
              this.addNewDataSet(newDataSet);
          });
        },
        (error) => {
          this.sharedService.checkIfUsersIsAlreadyLoggedIn(error);
        });
      });
    }
}
