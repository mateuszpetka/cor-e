import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRouteSnapshot, ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { AuthService } from './../auth/auth.service';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {
  public recoveryForm: FormGroup;
  private token: string = '';

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute, private _snackBar: MatSnackBar, 
    private _authService: AuthService) {
   }

  ngOnInit() {
    this.token = this._activatedRoute.snapshot.params.token;
    this.recoveryForm = new FormGroup({
      newPassword: new FormControl(null, Validators.required),
      retypePassword: new FormControl(null, Validators.required)
    });
  }

  /**
   * Ustawia nowe hasło
   *
   * @returns void
   */
  changePassword(): void {
    const credencials = {
      password: this.recoveryForm.value.newPassword, 
      passwordConfirm: this.recoveryForm.value.retypePassword,  
      token: this.token
    };

    this._authService.resetPassword(credencials).subscribe(
      () => {
        this._snackBar.open('Save successful', 'Ok', {duration: 1000});
        this._router.navigate(['/auth/login']);
      },
      (err) => {
        console.log(err);
        this._snackBar.open(`Save error ${err}`, 'Ok', {duration: 1000});
      });
  }
}
