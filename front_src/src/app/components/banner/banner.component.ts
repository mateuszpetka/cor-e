import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Subject } from 'rxjs';

import { SharedService } from '../../shared/shared.service';
import { CustomHttpService } from '../../shared/customHttp.service';
import { AppService } from './../../app.service';

import { delay, takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

import { trigger, state, style, transition, animate } from '@angular/animations';
import { CustomFields, Country, LoggedUserInitialData } from './../../classes/classes';
import { UserFormService } from './../userform/userform.service';
import { ChartsStateService } from '../../shared/chars-state.service';
import { AppSettings } from '../../app.settings';

@Component({
  selector: 'banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
  animations: [
    trigger('dropMenu', [
      state('inactive', style({
        height: '0',
      })),
      state('active',  style({
        height: '*'
      })),
      transition('inactive => active', animate('300ms ease-in')),
      transition('active => inactive', animate('300ms ease-out'))
    ])
  ]
})
export class BannerComponent implements OnInit {
  public userData: CustomFields;
  public avatar: string;
  public state: string = 'inactive';
  public menuStatus: boolean = false;
  public menuItems: any;
  public pageTitle: boolean = true;
  public updateState: any;
  public globalFilters: boolean = false;
  public testPage: boolean = false;

  private _selectedCountries: Country[] = [];
  private _selectedModels: string[] = [];
  private guard$: Subject<any> = new Subject();
  private chartsList: string[];

  public currentTheme: any;

  constructor(private _appService: AppService,
    private _sharedService: SharedService,
    private _userFormService: UserFormService,
    private _chartsStateService: ChartsStateService,
    private _appSettings: AppSettings,
    public router: Router, private customHttpService: CustomHttpService, private snackBar: MatSnackBar) {
    this.getUserData();
  }

  ngOnInit() {
    this.getMenuItems();

    this.updateState = this._sharedService.parametersUpdate$.pipe(
      delay(200)
    );

    this._sharedService.updateDashTemplate$
    .pipe(
      takeUntil(this.guard$)
    ).subscribe(theme => {
        if (!theme) {
          this._appService.newTheme$.subscribe(res => {
            theme = res;
          });
        }
        const themeDisplay: string = this._appService.getUserFromLocalStorage().customFields.themeDisplay;
        this.currentTheme = !theme ? themeDisplay ? themeDisplay : 'default_theme' : theme;
    });

    this._sharedService.selectedCountries$
      .pipe(
        takeUntil(this.guard$)
      ).subscribe((countriesIso: Country[]) => {
        this._selectedCountries = countriesIso;
      });

    this._sharedService.currentModels$
      .pipe(
        takeUntil(this.guard$)
      ).subscribe((selectedModels: string[]) => {
      this._selectedModels = selectedModels;
    });

    if (this._appSettings['apiIp'] && this._appSettings['apiIp'].includes('bling')) {
      this.testPage = true;
    }
  }

  /**
   * Gets user data from local storage
   *
   * @returns void
   */
  getUserData(): void {
   const customFields = this._appService.getUserFromLocalStorage().customFields;
   this.chartsList = this._chartsStateService.chartStatesList;
   this.avatar = !!this._appService.getUserFromLocalStorage().avatar ? this._appSettings['apiIp']  + '/public/upload/' + this._appService.getUserFromLocalStorage().avatar : '../../../assets/img/default_avatar.png';
   this.userData = !!customFields ? customFields : null;
  }

  /**
   * Changes menu state value
   *
   * @returns void
   */
  changeState(): void {
    this.state = (this.state === 'active') ? 'inactive' : 'active';
  }

  /**
   * Closes menu
   *
   * @returns void
   */
  closeMenu(close: boolean): void {
    this.state = 'inactive';
  }

  /**
   * Menu Button Collapse
   *
   * @returns void
   */
  menuToggle(): void {
    this.menuStatus = !this.menuStatus;
  }

  /**
   * Get menu items from MenuLink
   *
   * @returns void
   */
  getMenuItems(): void {
    this.customHttpService.get('MenuLink').subscribe(res => {
      this.menuItems = res;
    })
  }

  /**
   * Changes chart-list visibility
   *
   * @returns void
   */
  changeDashboardVisibility(dashState: boolean): void {
    this._sharedService.setChartListVisibility(dashState);
    this.pageTitle = dashState;
  }

  /**
   * Change update state
   *
   * @param  {boolean} state
   * @returns void
   */
  updateDashbord(dashState: boolean): void {
    this._sharedService.setUpdateState(dashState);
    this._sharedService.updateDashState(true);
  }

  /**
   * Save global filters - countries & models
   *
   * @returns void
   */
  saveDashboard(): void {
    let tempUser = this._appService.getUserFromLocalStorage();
    if (!tempUser.customFields.firstName || !tempUser.customFields.lastName || !tempUser.email) {
      this._sharedService.openDialog('userEmpty');
    } else {
      tempUser.customFields.filterCountries = JSON.stringify(this._selectedCountries);
      tempUser.customFields.filterModels = JSON.stringify(this._selectedModels);
      tempUser = this.createChartsStatesFields(tempUser);
      this._userFormService.updateUser(tempUser.customFields, tempUser.token)
        .subscribe(() => {
          this._appService.saveUserToLocalStorage(tempUser);
          this._sharedService.userDataSrc.next(tempUser);
          this.snackBar.open('Dashboard Save Complete', 'X', {
            duration: 10000
          });
        });
    }
  }

  /**
   * Uzupełnienie customowych pol usera aktualnymi stanami wykresow
   *
   * @param  {} tempUser
   * @returns any
   */
  createChartsStatesFields(tempUser: LoggedUserInitialData): LoggedUserInitialData {
    this.chartsList.forEach((chartStateName: string) => {
      tempUser.customFields[chartStateName] = JSON.stringify(this._chartsStateService.getChartState(chartStateName));
    });
    return tempUser;
  }

  /**
   * Wymusza pojawienie się komunikatu o updacie
   *
   * @returns void
   */
  forceUpdate(): void {
    this._sharedService.setUpdateState(true);
  }

  /**
   *
   */
  globalFiltersState(): void {
    this.globalFilters = !this.globalFilters;
  }

  ngDestroy() {
    this.guard$.next();
    this.updateState.unsubscribe();
  }
}
