import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from "../../translation.service";
import { Observable } from 'rxjs';

interface Path {
  url: string;
  name: string;
}

@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {

  public paths: Path[] = [];
  public activeLang: string = this.translationService.getLang();
  public availableLangs: Array<string> | Observable<string[]> = this.translationService.getLangs();

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translationService: TranslationService
  ) {
    this.getPathFromConfig().forEach(path => this.paths.push(path));

    this.translationService.currentLangObservator.subscribe(lang => {
      this.activeLang = lang;
    });
  }

  /**
   * Zmiana języka
   *
   * @param {string} lang
   */
  public setLang(lang: string): void {
    localStorage.setItem('lang', lang);
    this.translationService.setLang(lang);
  }

  /**
   * Pobranie z konfiguracji routingu wszystkich ścieżek po wcześniejszym sparsowaniu ich.
   *
   * @private
   * @returns {Path[]}
   */
  private getPathFromConfig(): Path[] {
    const paths: Path[] = [];

    for (let i = 0; i < this.router.config.length; i++) {
      const routePath = this.router.config[i];

      if (routePath.path !== '***') {
        this.paths.push({
          url: '/' + routePath.path,
          name: routePath.component.name.replace('Component', '')
        });
      }
    }

    return paths;
  }
}