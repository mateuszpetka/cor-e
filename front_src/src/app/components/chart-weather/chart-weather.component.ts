import { HttpErrorResponse } from '@angular/common/http';
import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, NgZone, ViewChild} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Chart } from 'chart.js';
import { cloneDeep, find, isArray, isEmpty, remove, uniq } from 'lodash';
import { BehaviorSubject, forkJoin, Observable, of as observableOf } from 'rxjs';
import { combineLatest, debounceTime, delay, map, takeUntil } from 'rxjs/operators';

import { AppService } from '../../app.service';
import { AppSettings } from '../../app.settings';
import { ChartData, DataSets, ModelRunDates } from '../../classes/chart';
import { ChartsStateService } from '../../shared/chars-state.service';
import { CustomHttpService } from '../../shared/customHttp.service';
import { SharedService } from '../../shared/shared.service';
import { ChartComponent } from '../chart/chart.component';
import { ChartService } from '../chart/chart.service';
import { ChartWeatherService } from './chart-weather.service';

export interface IsothermColor {
  main: string;
  opac: string;
}
@Component({
  selector: 'chart-weather',
  templateUrl: './chart-weather.component.html',
  styleUrls: ['./chart-weather.component.scss']
})
export class ChartWeatherComponent extends ChartComponent implements AfterViewInit {
  @Input() isothermRunDates: ModelRunDates;
  @Input() temperatureRunDates: ModelRunDates;
  @Input() temperatureDifferenceRunDates: ModelRunDates;
  @ViewChild ('differentLastCheckbox') differentLastCheckbox : ElementRef<HTMLInputElement>;
  @ViewChild ('differentPrevCheckbox') differentPrevCheckbox : ElementRef<HTMLInputElement>;
  public hideMap: boolean = true;
  public checked: boolean = true;
  public parametersChart$;
  public isothermKind: Array<string>;
  public filterIsothermData: Array<any>;
  public checkedAll: boolean = true;
  public currentLabel: string;
  public currentModels$: Observable<string[]> = observableOf([]);
  public visibleHistogram: boolean = false;
  public subKind: string = '';
  // subject przyciskow legendy przy fullscreenie - przekazywany do chart-button
  public selectedLegendBtnsSrc: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  // obserwator z powyzszego subjecta
  public selectedLegendBtns$: Observable<boolean> = this.selectedLegendBtnsSrc.asObservable();

  private selectedKindSrc: BehaviorSubject<string> = new BehaviorSubject<string>('Temperature');
  public currentSelectedKind$: Observable<string> = this.selectedKindSrc.asObservable();

  public maps = [];
  public legendLabels: string[]; // CO YO? sprawdzic
  public filteredBtns: string[];

  public temperatureRunDate: string = '';
  public isothermRunDate: string = '';
  public temperatureDifferenceRunDate: string = '';
  private selectedBtns: string[] = [];

  private isothermColorMap: Map<string, IsothermColor> = new Map();
  private isothermInitialColors: IsothermColor[] = [
    { main: '#8be14d',
      opac: 'rgba(139, 225, 77, .5)'},
    { main: '#2da7dd',
      opac: 'rgba(45, 167, 221, .5)'},
    { main: '#a86355',
      opac: 'rgba(168, 99, 85, .5)'},
    { main: '#dcae4d',
      opac: 'rgba(220, 174, 77, .5)'},
    { main: '#4fb99f',
      opac: 'rgba(79, 185, 159, .5)'},
    { main: '#dcd3b2',
      opac: 'rgba(220, 211, 178, .5)'}
  ];
  private staticCurveColor: IsothermColor =  {
    main: '#909090',
    opac: 'rgba(144, 144, 144, 0.5)'
  };

  constructor(public sharedService: SharedService,
    public chartsStateService: ChartsStateService, public chartService: ChartService, public zone: NgZone,
    public cdRef: ChangeDetectorRef, private _chartWeatherService: ChartWeatherService, private customHttpService: CustomHttpService,
    private appSettings: AppSettings, private sanitizer: DomSanitizer, public elementRef?: ElementRef, public _appService?: AppService) {
    super(chartService, sharedService, chartsStateService, cdRef, zone);
  }

    ngAfterViewInit() {

      this.initialKind$ = observableOf('Temperature');

      this.loaderSub = this.sharedService.loadingWeather$
        .pipe(
          takeUntil(this._guard$),
          debounceTime(200)
        ).subscribe((loadingState: boolean) => {
          this.labels = ['Temperature', 'Isotherm'];
          this.toggleChartVisibility(loadingState);
          const initialKind = !!this.selectedKind ? this.selectedKind : 'Temperature';
          this.chartSetup(loadingState, initialKind);

          if (!loadingState && this.backupData) {
            // Wyswietlanie rodzajow isothermow
            this.filterIsothermData = this.backupData;

            const allIsothermKind = [];
            this.filterIsothermData.forEach(item => {
              if (item.kind.toLowerCase() !== 'temperature' && item.kind.toLowerCase() !== 'calculated difference') {
                allIsothermKind.push(item['label']);
              }
            });
            this.isothermKind = uniq(allIsothermKind);

            // legenda
            const newLabels = [];
            this.legendLabels = this.chartOptions.data.datasets.map(set => {
              const labelValue = set.label.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
              newLabels.push({
                value: labelValue, name: set.label
              });
              return set.label.trim();
            });
            this.legendLabels = newLabels;
          }
        });

      // obserwuje obecnie wybrany rodzaj, przypisuje do zmiennej
      this.currentKind$ = this.initialKind$.pipe(
        takeUntil(this._guard$),
        combineLatest(this.currentChartKind$),
        map(([initialKind, selectedKind]) => {
          const currentKind = selectedKind ? selectedKind : initialKind;
          this.selectedKindSrc.next(currentKind);
          this.selectedKind = currentKind;
          return currentKind;
        })
      );

      // przypisanie statusu dla wyswietlania Parametrow
      this.parametersChart$ = this.sharedService.parametersChart$.pipe(delay(200));
      this.sharedService.chartLegendHiddenFilters$.pipe(takeUntil(this._guard$)).subscribe(btns => {
        this.filteredBtns = btns;
      });
  }

  /**
   * Chart setup - draw, update, resize
   *
   * @param  {boolean} loadingState
   * @param {string} initialSeries
   * @returns void
   */
  chartSetup(loadingState: boolean, initialSeries: string): void {
    this.changeLoaderState(true);
    if (!loadingState && this.chartOptions && this.chartOptions.data.datasets.length) {
      this.setIsothermColor(this.chartOptions.data.datasets);
      this.changeChartLegendColorByTheme();
      this.drawChart();
      initialSeries = !!this.chartState && !!this.chartState.kind ? this.chartState.kind : initialSeries;
      this.viewFullRange = true;

      this.setInitialData(initialSeries);
      this.scalesLabelsConfig(initialSeries);

      this.backupData = cloneDeep(this.myChart.config.data.datasets);
      this.calculateAverage();
      this.getOneDay(this.currentDate.toString(), false);
      // Wyswietlanie rodzajow isothermow

      // zapamietywanie danych w checboxach last i prev i wyswietlanie danych
      this.checkMenuCheckboxValue();
      this.checkCheckBoxesValue();

      this.toggleChartVisibility(loadingState);
      this.changeLoaderState(false);

    } else if (loadingState && this.chartOptions && !this.chartOptions.data.datasets.length) {
      // if models oraz countries filters are empty arrays
      if (this.myChart) {
        this.myChart.update(); // destroys chart data
        this.toggleChartVisibility(loadingState);
        this.resizeChart();
      }
    }
  }

  /**
   * Show/hide map
   *
   * @param  {string} key
   * @returns void
   */
  toggleData(key: string): void {
    this.currentLabel = key;
    this.scalesLabelsConfig(key);

    this.myChart.config.data.datasets.forEach(dataset => {

      dataset.kind !== this.selectedKind || this.filteredBtns.includes(dataset.label)
        ? dataset.hidden = true
        : dataset.hidden = false;

      let runDate: string;

      switch (this.currentLabel.toLowerCase()) {
        case 'isotherm':
          runDate = this.isothermRunDate;
          break;
        case 'temperature':
          runDate = this.temperatureRunDate;
          break;
        case 'calculated difference':
          runDate = this.temperatureDifferenceRunDate;
      }

      if (!runDate) {
        if (dataset.kind === this.selectedKind && dataset.asOf || this.filteredBtns.includes(dataset.label)) {
          dataset.hidden = true;
        }
      } else {
        if (dataset.kind === this.selectedKind
          && dataset.asOf
          && dataset.runDate === runDate
          && !this.filteredBtns.includes(dataset.label)) {
          dataset.hidden = false;
        } else if (dataset.runDate !== runDate && dataset.asOf) {
          dataset.hidden = true;
        }
      }
    });

    // this.toggleTempHistogramDatasets();
    this.checkCheckBoxesValue();
    this.saveCurrentLocalState();
  }

  /**
   * Sets initial scales setup
   *
   * @param  {string} key
   * @returns void
   */
  scalesLabelsConfig(key: string): void {
    if (key.includes('Wind map')) {
      // this.hideMap = false;
      this.hideMap = true;
    } else if (key.includes('Calculated Difference')) {
      this.hideMap = true;
      this.chartOptions.type = 'bar';
      this.chartOptions.options.scales.xAxes[0].offset = true;
      this.chartOptions.options.scales.yAxes[0].offset = true;
      this.myChart.config.data.datasets.forEach(dataset => {
        if (dataset.model.toLowerCase().includes('gfs')) {
          if (dataset.asOf === undefined) {
            dataset.backgroundColor = '#c1342c';
          }

        } else if (dataset.model.toLowerCase().includes('ec')) {
          if (dataset.asOf === undefined) {
            dataset.backgroundColor = '#195f9a';
          }
        }
      });

      this.myChart.config.options.scales.yAxes.forEach(data => {
        this.myChart.config.chartSubType = key;
        data.ticks.callback();
      });
      
    } else {
      this.hideMap = true;
      this.chartOptions.type = 'line';
      this.chartOptions.options.scales.xAxes[0].offset = false;
      this.chartOptions.options.scales.yAxes[0].offset = false;
      this.myChart.config.options.scales.xAxes.forEach(data => {
        data.time.unit = 'day';
        data.time.displayFormats.day = 'MMM DD';
        data.time.stepSize = 1;
      });
      this.myChart.config.options.scales.yAxes.forEach(data => {
        this.myChart.config.chartSubType = key;
        data.ticks.callback();
      });
    }
  }

   /**
   * Draws chart, modifies chart options
   *
   * @returns void
   */
  drawChart(): void {
    // Global chart settings
    Chart.defaults.global.defaultFontFamily = ' Overpass';
    Chart.defaults.global.defaultFontSize = 10;

    const canvas = this.chart.nativeElement;
    const ctx = canvas.getContext('2d');

    // draw chart outside angular zone
    this.zone.runOutsideAngular(() => {
      if (this.myChart) {
        this.myChart.destroy();
      }
      // this.modifyOptions(ctx, this.chartOptions.data.datasets)
      this.myChart = new Chart(ctx, this.chartOptions);
      this.resizeChart();
    });
  }

  /**
   * Zaznaczanie buttona dla wszystkich parametrów
   *
   * @param {boolean} checkedAll
   * @returns void
   */
  selectAllParameters(checkedAll: boolean): void {
    this.checkedAll = checkedAll;
    this.checked = !checkedAll;

    this.myChart.config.data.datasets.forEach(dataset => {
      if (dataset.kind === this.selectedKind) {
        dataset.hidden = checkedAll;
      }
    });

    // sprawdzenie, czy przyciski legendy przy fullscreenie powinny być aktywne
    if (checkedAll) {
      this.selectedLegendBtnsSrc.next(this.isothermKind);
      this.sharedService.singleChartLegendFilters(this.isothermKind);
    } else {
      this.selectedLegendBtnsSrc.next([]);
      this.sharedService.singleChartLegendFilters([]);
    }
    this.updateChart();
    this.cdRef.detectChanges();
  }

  /**
   * Dodaje/usuwa do tablicy filtrowane parametry
   *
   * @param {boolean} ckbState
   * @param {string} massif
   * @returns void
   */
  selectedParameters(massif: string, ckbState?: boolean): void {
    this.setActiveBtns(massif);

    this.myChart.config.data.datasets.forEach(dataset => {
       if (this.prevToogleState === false && dataset.offset === 1) {
         dataset.hidden = true;
       } else if (this.lastToogleState === false && dataset.offset === 0) {
         dataset.hidden = true;
       } else if (dataset.label.toLowerCase() === massif.toLowerCase()) {
         let runDate: string;
         switch (this.selectedKind.toLowerCase()) {
           case 'isotherm':
             runDate = this.isothermRunDate;
             break;
           case 'temperature':
             runDate = this.temperatureRunDate;
             break;
           case 'calculated difference':
             runDate = this.temperatureDifferenceRunDate;
         }
         if (!!runDate) {
          if (dataset.asOf === true && dataset.runDate === runDate
            || !dataset.asOf && dataset.label.toLowerCase() === massif.toLowerCase()) {
            dataset.hidden = !ckbState;
          } else {
            dataset.hidden = true;
          }
         } else {
           dataset.hidden = !ckbState;
           if (dataset.asOf === true) {
             dataset.hidden = true;
           }
         }
       }

    });

    this.updateChart();
    this.cdRef.detectChanges();
  }

  /**
   * Pushuje state = true do obserwatora popupu
   *
   * @returns void
   */
  openFilterModal(): void {
    this.sharedService.setFullscreenParameters(true);
  }

  /**
   * Zmienia status wyswietlonego Popupu parametrow i zamyka go
   *
   * @param {boolean} state
   * @returns void
   */
  closeParametersModal(state: boolean): void {
    this.sharedService.setFullscreenParameters(state);
  }

   /**
   * Checks if datasets for selected run date was downloaded
   *
   * @param  {number} offset
   * @param  {string} runDate
   * @param  {string} type
   * @returns void
   */
  checkSelectedRunDate(offset: number, runDate: string, type?: string): void {
    const searchedIndexes: string[] = [];
    this.myChart.config.data.datasets.forEach(dataset => {
      if (dataset.asOf && (dataset.offset === offset)
        && dataset.kind === this.selectedKind) {
        dataset.hidden = true;
        // wyszukaj indeksy wszystkich datasetow spełniających warunki
        searchedIndexes.push(this.myChart.config.data.datasets.indexOf(dataset));
      }
    });

    if (!(searchedIndexes.length)) {
      this.getDataByOffset(offset, runDate, true, type);
    } else {
      searchedIndexes.forEach(searchedIndex => {
        this.myChart.config.data.datasets.forEach((dataset, index) => {

          if (searchedIndex === index) {
            this.myChart.config.data.datasets[index].hidden = false;
          } else if (searchedIndex !== index
            && dataset.asOf && dataset.offset !== offset) {
              this.myChart.config.data.datasets[index].hidden = true;
          }

          if (this.filteredBtns.includes(dataset.label.toLowerCase())) {
            dataset.hidden = true;
          }
          });
        });
      }

    this.updateChart();
    this.cdRef.detectChanges();
  }

  /**
   * Loads more data based on SELECTED offset
   *
   * @param  {string} runDate
   * @param  {string} type
   * @returns void
   */
  loadMoreData(runDate: string, type?: string): void {
    this.changeLoaderState(true);
    if (runDate === null) {
      this.myChart.config.data.datasets.forEach((dataset, index) => {
       if (dataset.asOf) {
          this.myChart.config.data.datasets[index].hidden = true;
        }
      });
      this.updateChart();
    } else {
      let offset: number;
      switch (type) {
        case 'temperature':
          this.temperatureRunDate = runDate;
          offset = this.temperatureRunDates.gfs
            ? this.temperatureRunDates.gfs.indexOf(runDate)
            : this.temperatureRunDates.hres.indexOf(runDate);
          break;
        case 'isotherm':
          this.isothermRunDate = runDate;
          offset = this.isothermRunDates.gfs
            ? this.isothermRunDates.gfs.indexOf(runDate)
            : this.isothermRunDates.hres.indexOf(runDate);
          break;
        case 'calculated difference':
          this.temperatureDifferenceRunDate = runDate;
          offset = this.temperatureDifferenceRunDates.gfs
            ? this.temperatureDifferenceRunDates.gfs.indexOf(runDate)
            : this.temperatureDifferenceRunDates.hres.indexOf(runDate);
      }
      if (this.queryParams) {
        this.queryParams.offset = offset;
      }
      this.selectedOffset = offset;
      this.checkSelectedRunDate(offset, runDate, type);
    }
  }

  /**
   * Sprawdza i ustawia aktualną wartość selecta as of
   *
   */
  checkAsOfValueBasedOnKind(): void {
    const kind = this.selectedKind.toLowerCase();
    switch (kind) {
      case 'temperature': {
        this.runDatesSelect.value = this.temperatureRunDate;
        this.updateChart();
        break;
      }
      case 'isotherm' : {
        this.runDatesSelect.value = this.isothermRunDate;
        this.updateChart();
        break;
      }
      case 'calculated difference' : {
        this.runDatesSelect.value = this.temperatureDifferenceRunDate;
        this.updateChart();
      }
    }

    this.myChart.config.data.datasets.forEach(dataset => {
      if (dataset.asOf && dataset.kind.toLowerCase() === kind && dataset.runDate === this.runDatesSelect.value) {
        dataset.hidden = false;
      }
    });
  }

  /**
   * Gets data from service by offset and current queryParams
   *
   * @param  {number} offset
   * @param  {string} runDate
   * @param  {boolean} asOf
   * @param  {string} type
   * @returns void
   */
  getDataByOffset(offset: number, runDate?: string, asOf?: boolean, type?: string): void {
    this.changeLoaderState(true);
    let dataRequest$: Observable<any> = observableOf('');

    this.currentModels.forEach((model: string) => {
      const params = this.queryParams;

      if (params) {
        params.model = model;
        params.offset = offset;
      }
      const color = this.checkColor(model);

      if (asOf) {
        params.runDate = runDate;
        params.offset = 0;
      } else {
        params.runDate = null;
      }

      // type jest przekazywane z selecta
      if (asOf && type === 'isotherm') {
        dataRequest$ = this._chartWeatherService.getIsothermData(params);
      } else if (asOf && type === 'temperature') {
        dataRequest$ = this._chartWeatherService.getTemperatureData(params);
      } else if (asOf && type === 'calculated difference') {
        dataRequest$ = this._chartWeatherService.getTemperatureDifferenceData(params);
      } else if (!asOf) {
        const isotherm$ = this._chartWeatherService.getIsothermData(params);
        const temperature$ = this._chartWeatherService.getTemperatureData(params);
        const temperatureDifference$ = this._chartWeatherService.getTemperatureDifferenceData(params);
        dataRequest$ = forkJoin(isotherm$, temperature$, temperatureDifference$);
      }

      dataRequest$.pipe(map(res => {
        const data = [];
        if (isArray(res)) {
          res.forEach(resData => {
            if (!isEmpty(resData)) {
              data.push(resData);
            }
          });
        } else {
          if (!isEmpty(res)) {
            data.push(res);
          }
        }

        const filteredData = [];
        for (const key in data) {
          if (data[key].length) {
            data[key].forEach(element => {
              filteredData.push(element);
            });
          } else {
            filteredData.push(data[key]);
          }
        }
        return filteredData;
      })).pipe(
          debounceTime(200),
        ).subscribe((resData: ChartData[]) => {
          // sortowanie danych
          resData.sort((a, b) => {
            const nameA = a.name.toLowerCase();
            const nameB = b.name.toLowerCase();
            return nameA < nameB ? -1 : nameA > nameB ? 1 : 0;
          });

          resData.forEach((data, index) => {
            // this.setIsothermColor
            const newDataSet = new DataSets(data.name, data.series, model, data.kind, data.runDate, offset, '', asOf);
            // jeżeli nowy set nie należy do obecnego rodzaju ukryj go
            if (!(newDataSet.kind.toLowerCase() === this.selectedKind.toLowerCase())) {
              newDataSet.hidden = true;
            }

            // kolorowanie datasetow dla wykresu temperatury -- different
            if (newDataSet.kind.toLowerCase() === 'calculated difference') {
              if (newDataSet.model.toLowerCase().includes('gfs')) {
                !newDataSet.asOf
                  ? newDataSet.backgroundColor = 'rgba(193,52,44, .3)'
                  : newDataSet.backgroundColor = 'transparent';

                newDataSet.borderColor = '#c1342c';
              } else if (newDataSet.model.toLowerCase().includes('ec')) {
                !newDataSet.asOf
                  ? newDataSet.backgroundColor = 'rgba(25,95,154, .3)'
                  : newDataSet.backgroundColor = 'transparent';

                newDataSet.borderColor = '#195f9a';
              }
            }

            this.addNewDataSet(newDataSet);
          });
        },
        (error: HttpErrorResponse) => {
          console.log('ERROR', error.error);
        },
        () => this.changeLoaderState(false));
    });
  }

  /**
   * Przypisuje odpowiedni kolor datasetow dla serii Isotherm
   *
   * @param  {DataSets[]} datasets
   * @returns void
   */
  setIsothermColor(datasets: DataSets[]): void {
    let index = 0;
    datasets.forEach((element) => {
      if (!this.isothermColorMap.has(element.label)) {
        if (element.kind === 'Isotherm') {
          const color = this.isothermInitialColors[index];
          this.isothermColorMap.set(element.label, color);
          element.borderColor = color.main;
          index++;
        } else {
          const color = this._chartWeatherService.checkColorBasedOnModel(element.model) ?
            this._chartWeatherService.checkColorBasedOnModel(element.model) : this.staticCurveColor;

          this.isothermColorMap.set(element.label, color);
          element.borderColor = color.main;
        }
      } else {
        const color = this.isothermColorMap.get(element.label);
        this._chartWeatherService.setSingleDataSetColor(element, color);
      }
    });
  }

 /**
   * Add new dataset to chart and
   *
   * @param  {DataSets} newDataset
   * @returns void
   */
  addNewDataSet(newDataset: DataSets): void {
    this.myChart.config.data.datasets.push({...newDataset});
    this.backupData.push({...newDataset});
    this.setIsothermColor(this.chartOptions.data.datasets);
    this.hideDateSetBasedOnSelectedKindOffsetAndFilteredMassifs();
    this.calculateAverage();
    if (this.viewFullRange) {
      this.changeView(true);
    } else {
      this.getOneDay(this.currentDate);
    }
  }

  /**
   * Sprawdza stan filtrow przy fulscreenie, chowa odpowiedni dataset
   *
   * @param  {DataSets} dataset
   * @returns void
   */
  hideDataSetBasedOnFullscreenFilters(dataset: DataSets): void {
    if (this.filteredBtns.includes(dataset.label.toLowerCase())) {
      dataset.hidden = true;
    }
  }

  /**
   * Sprawdza stan filtrow przy fulscreenie oraz chowa dataset w zależności od zadanego offsetu
   *
   * @param  {DataSets} dataset
   * @param  {number} offset
   * @returns boolean
   */
  checkIfDataSetNeedToBeHiddenBasedOnFullscreenFiltersAndOffset(dataset: DataSets, offset: number): boolean {
    if (this.filteredBtns.includes(dataset.label.toLowerCase()) && dataset.offset === offset) {
      return true;
    }
  }

  /**
   * Chowa datasety w zaleznosci od aktywnego offsetu oraz stanowi filtrow
   *
   * @returns void
   */
  hideDateSetBasedOnSelectedKindOffsetAndFilteredMassifs(): void {
    this.myChart.config.data.datasets.forEach(dataset => {
      if (dataset.asOf && dataset.offset !== this.selectedOffset) {
        dataset.hidden = true;
      }
      if (dataset.kind.toLowerCase() !== this.selectedKind.toLowerCase()) {
        dataset.hidden = true;
      }

      this.hideDataSetBasedOnFullscreenFilters(dataset);
    });
  }

  /**
   * Load last or
   *
   * @param  {boolean} checkBoxValue
   * @param  {string} runDate
   * @param  {number} offset
   * @returns void
   */
  loadLastOrPrevRunDate(checkBoxValue: boolean, runDate: string, offset: number): void {
    const searchedIndex: number[] = [];
    // jeżeli run date = LAST
    if (offset === 0 ) {
      this.myChart.config.data.datasets.forEach((dataset) => {
        if (dataset.offset === offset && dataset.kind === this.selectedKind) {
          dataset.hidden = !dataset.hidden;
        }
        this.hideDataSetBasedOnFullscreenFilters(dataset);
        this.updateChart();
      });
    } else {
      // jeżeli run date = PREV, sprawdź jego wartość
      if (checkBoxValue) {
        // jeżeli checkbox jest zaznaczony sprawdź, czy jakiś dataset zawiera obecny rundate oraz czy zawiera aktualny rodzaj
        // jeżeli dataset istnieje ustawia flagę na true, pushuje indeksy datasetow do tablicy
        this.myChart.config.data.datasets.forEach((dataset, index) => {
          if ((dataset.offset === offset)
            && dataset.kind === this.selectedKind) {
            searchedIndex.push(index);
          }
        });
        if (!!searchedIndex.length) {
          searchedIndex.forEach(index => {
            if (this.checkIfDataSetNeedToBeHiddenBasedOnFullscreenFiltersAndOffset(this.myChart.config.data.datasets[index], 1)) {
              this.myChart.config.data.datasets[index].hidden = true;
            } else {
              this.myChart.config.data.datasets[index].hidden = !this.myChart.config.data.datasets[index].hidden;
            }
          });
          this.updateChart();
        } else {
          if (this.queryParams) {
            this.queryParams.offset = offset;
          }
          this.getDataByOffset(offset, runDate, false, this.selectedKind);
        }
      } else {
        this.myChart.config.data.datasets.forEach(dataset => {
          if ((dataset.offset === offset)
            && dataset.kind === this.selectedKind) {
            if (this.checkIfDataSetNeedToBeHiddenBasedOnFullscreenFiltersAndOffset(dataset, 1)) {
              dataset.hidden = true;
            } else {
              dataset.hidden = !dataset.hidden;
            }
          }
        });
      }
    }

    // jezeli przyciski sa odznaczone ukrywa wszystkie dane
    if (!this.prevToogleState && !this.lastToogleState) {
      this.myChart.config.data.datasets.forEach(dataset => {
        dataset.hidden = true;

        if (!this.prevToogleState && !this.lastToogleState
          && dataset.runDate === this.runDatesSelect.value && dataset.kind.toLowerCase() === this.selectedKind.toLowerCase()) {
            dataset.hidden = false;
        }

        if (dataset.kind === 'Temperature' && dataset.model === '' && this.selectedKind === 'Temperature') {
          dataset.hidden = false;
        }

        this.hideDataSetBasedOnFullscreenFilters(dataset);
      });
    }

    this.updateChart();
  }

  /**
   * Dodaje lub usuwa aktywowane przyciski
   *
   * @param  {string} name
   * @returns void
   */
  setActiveBtns(name: string): void {
    const exists = find(this.selectedBtns, selectedBtn => selectedBtn.toLowerCase() === name.toLowerCase());
    if (!exists) {
      this.selectedBtns.push(name.toLowerCase());
    } else {
      remove(this.selectedBtns, selectedBtn => selectedBtn.toLowerCase() === name.toLowerCase());
    }
    this.selectedLegendBtnsSrc.next(this.selectedBtns);
    this.sharedService.singleChartLegendFilters(this.selectedBtns);
  }

  /**
   * Przelacza na Calculated Difference w wykresie temperatury
   * @param {string} name
   * return void
   */
  switchKind(name: string): void {
    if (name === 'Calculated Difference') {
      this.subKind = 'Calculated Difference';
      remove(this.labels, label => label.includes('Temperature'));
    } else if (name === 'Temperature') {
      this.subKind = 'Temperatue';
      this.labels.unshift(name);
    }

    this.setCurrentKind(name);
    this.toggleData(name);
  }

}
