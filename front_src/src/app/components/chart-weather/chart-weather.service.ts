import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ChartData, DataSets } from './../../classes/chart';
import { ChartQueryParams, Params, MapPath } from '../../classes/classes';
import { SharedService } from '../../shared/shared.service';
import { CustomHttpService } from "../../shared/customHttp.service";
import { IsothermColor } from './chart-weather.component';

@Injectable()
export class ChartWeatherService {

  constructor(private _http: CustomHttpService, private _sharedService: SharedService) {}

  /**
   * Pobiera dane dla wykresu Weather - isothermy
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  getIsothermData(queryParams: ChartQueryParams): Observable<ChartData> {
    const params = new Params(queryParams.iso, queryParams.model, queryParams.offset, queryParams.runDate);
    return this._http
      .get(`ChartData/getAllIsothermData?` + this._sharedService.toQueryString(params));
  }

  /**
   * Pobiera dane dla wykresu Weather - temperatura
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  getTemperatureData(queryParams: ChartQueryParams): Observable<ChartData> {
    const params = new Params(queryParams.iso, queryParams.model, queryParams.offset, queryParams.runDate);
    return this._http
      .get(`ChartData/temperature?` + this._sharedService.toQueryString(params));
  }

  /**
   * Pobiera dane dla wykresu Weather - isothermy
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  getTemperatureDifferenceData(queryParams: ChartQueryParams): Observable<ChartData> {
    const params = new Params(queryParams.iso, queryParams.model, queryParams.offset, queryParams.runDate);
    return this._http
      .get(`ChartData/temperatureCalculatedDifference?` + this._sharedService.toQueryString(params));
  }

  /**
   * Pobiera mape w zależności od modelu
   *
   * @param  {string} model
   * @returns Observable
   */
  getWeatherMapPath(model: string): Observable<MapPath> {
    return this._http.get(`ChartData/weatherMap?model=` + model);
  }

  /**
   * Zwraca odpowiedni kolor w zależności od modelu
   *
   * @param  {string} model
   * @returns IsothermColor
   */
  checkColorBasedOnModel(model: string): IsothermColor {
    switch (true) {
      case model.toLowerCase().includes('gfs'):
        return {
          main: '#c1342c',
          opac: 'rgba(193, 52, 44, 0.5)'
        };
      case model.toLowerCase().includes('hres') || model.toLowerCase().includes('ec'):
        return {
          main: '#195f9a',
          opac: 'rgba(25, 95, 154, 0.5)'
        };
      default:
        break;
    }
  }

  /**
   * Ustawia style datasetu
   *
   * @param  {DataSets} element
   * @param  {IsothermColor} color
   * @returns void
   */
  setSingleDataSetColor(element: DataSets, color: IsothermColor): void {
    if (element.offset === 1) {
      element.borderColor = color.opac;
    } else if (element.asOf) {
      element['borderDash'] = [5, 5];
      element.borderColor = color.main;
    } else {
      element.borderColor = color.main;
    }
  }

}
