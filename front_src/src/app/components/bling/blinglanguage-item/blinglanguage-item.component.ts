import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BlingLanguage } from '../../../classes/bling/BlingLanguage/BlingLanguage';
import { BlingLanguageService } from '../../../classes/bling/BlingLanguage/BlingLanguage.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'blinglanguage-item',
  templateUrl: './blinglanguage-item.component.html',
  styleUrls: ['./blinglanguage-item.component.scss'],
  providers: [BlingLanguageService]
})
export class BlingLanguageItemComponent extends ItemComponentModel<BlingLanguage> implements OnInit {
  public data$: Observable<BlingLanguage>;

  constructor(
    private blingLanguageService: BlingLanguageService<BlingLanguage>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(blingLanguageService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
