import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BlingTemplateFile } from '../../../classes/bling/BlingTemplateFile/BlingTemplateFile';
import { BlingTemplateFileService } from '../../../classes/bling/BlingTemplateFile/BlingTemplateFile.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'blingtemplatefile-item',
  templateUrl: './blingtemplatefile-item.component.html',
  styleUrls: ['./blingtemplatefile-item.component.scss'],
  providers: [BlingTemplateFileService]
})
export class BlingTemplateFileItemComponent extends ItemComponentModel<BlingTemplateFile> implements OnInit {
  public data$: Observable<BlingTemplateFile>;

  constructor(
    private blingTemplateFileService: BlingTemplateFileService<BlingTemplateFile>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(blingTemplateFileService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
