import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BlingUser } from '../../../classes/bling/BlingUser/BlingUser';
import { BlingUserService } from '../../../classes/bling/BlingUser/BlingUser.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'blinguser-item',
  templateUrl: './blinguser-item.component.html',
  styleUrls: ['./blinguser-item.component.scss'],
  providers: [BlingUserService]
})
export class BlingUserItemComponent extends ItemComponentModel<BlingUser> implements OnInit {
  public data$: Observable<BlingUser>;

  constructor(
    private blingUserService: BlingUserService<BlingUser>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(blingUserService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
