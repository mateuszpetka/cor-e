import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { BlingTranslation } from '../../../classes/bling/BlingTranslation/BlingTranslation';
import { BlingTranslationService } from '../../../classes/bling/BlingTranslation/BlingTranslation.service';

@Component({
  selector: 'blingtranslation-list',
  templateUrl: './blingtranslation-list.component.html',
  styleUrls: ['./blingtranslation-list.component.scss'],
  providers: [BlingTranslationService]
})
export class BlingTranslationListComponent extends ListComponentModel<BlingTranslation> implements OnInit {
  public item$: Observable<BlingTranslation[]>;

  constructor(
    private blingTranslationService: BlingTranslationService<BlingTranslation>,
    public translationService: TranslationService
  ) {
    super(blingTranslationService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
