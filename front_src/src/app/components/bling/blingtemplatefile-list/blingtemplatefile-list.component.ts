import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { BlingTemplateFile } from '../../../classes/bling/BlingTemplateFile/BlingTemplateFile';
import { BlingTemplateFileService } from '../../../classes/bling/BlingTemplateFile/BlingTemplateFile.service';

@Component({
  selector: 'blingtemplatefile-list',
  templateUrl: './blingtemplatefile-list.component.html',
  styleUrls: ['./blingtemplatefile-list.component.scss'],
  providers: [BlingTemplateFileService]
})
export class BlingTemplateFileListComponent extends ListComponentModel<BlingTemplateFile> implements OnInit {
  public item$: Observable<BlingTemplateFile[]>;

  constructor(
    private blingTemplateFileService: BlingTemplateFileService<BlingTemplateFile>,
    public translationService: TranslationService
  ) {
    super(blingTemplateFileService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
