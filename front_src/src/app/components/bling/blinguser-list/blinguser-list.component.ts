import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { BlingUser } from '../../../classes/bling/BlingUser/BlingUser';
import { BlingUserService } from '../../../classes/bling/BlingUser/BlingUser.service';

@Component({
  selector: 'blinguser-list',
  templateUrl: './blinguser-list.component.html',
  styleUrls: ['./blinguser-list.component.scss'],
  providers: [BlingUserService]
})
export class BlingUserListComponent extends ListComponentModel<BlingUser> implements OnInit {
  public item$: Observable<BlingUser[]>;

  constructor(
    private blingUserService: BlingUserService<BlingUser>,
    public translationService: TranslationService
  ) {
    super(blingUserService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
