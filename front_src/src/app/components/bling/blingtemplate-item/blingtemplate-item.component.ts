import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BlingTemplate } from '../../../classes/bling/BlingTemplate/BlingTemplate';
import { BlingTemplateService } from '../../../classes/bling/BlingTemplate/BlingTemplate.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'blingtemplate-item',
  templateUrl: './blingtemplate-item.component.html',
  styleUrls: ['./blingtemplate-item.component.scss'],
  providers: [BlingTemplateService]
})
export class BlingTemplateItemComponent extends ItemComponentModel<BlingTemplate> implements OnInit {
  public data$: Observable<BlingTemplate>;

  constructor(
    private blingTemplateService: BlingTemplateService<BlingTemplate>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(blingTemplateService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
