import { Component, OnInit, Input, ViewChild, ElementRef, ChangeDetectorRef, NgZone, AfterViewInit, OnDestroy } from '@angular/core';

import { BoxplotChartCustomOption, DataSets } from './../../classes/boxplot';
import { SharedService } from '../../shared/shared.service';
import { ChartService } from './../chart/chart.service';
import { ChartsStateService } from '../../shared/chars-state.service';
import { ChartComponent } from './../chart/chart.component';
import { AppService } from '../../app.service';

import { Chart } from 'chart.js';
import 'chartjs-chart-box-and-violin-plot';
import { combineLatest } from 'rxjs';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { PricesDates } from 'app/classes/classes';
import * as moment from 'moment';
import { cloneDeep, filter } from 'lodash';

@Component({
  selector: 'chart-boxplot',
  templateUrl: './chart-boxplot.component.html',
  styleUrls: ['./chart-boxplot.component.scss']
})
export class ChartBoxplotComponent extends ChartComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() chartBoxOptions: BoxplotChartCustomOption;
  @Input() chartTitle: string;
  @Input() models: string[];
  @ViewChild('chartBoxplot') public chartBoxplot: ElementRef;

  public myChart: any;

  constructor(
    public sharedService: SharedService,
    public chartsStateService: ChartsStateService,
    public chartService: ChartService,
    public zone: NgZone,
    public cdRef: ChangeDetectorRef,
    public elementRef?: ElementRef,
    public _appService?: AppService
  ) {
    super(chartService, sharedService, chartsStateService, cdRef, zone);
  }

  ngAfterViewInit() {
    this.loaderSub = this.sharedService.loadingRiskManagment$
      .pipe(
        takeUntil(this._guard$),
        debounceTime(200)
      ).subscribe((loadingState: boolean) => {
        this.toggleChartVisibility(loadingState);
        this.chartSetup(loadingState, '');
      });

    combineLatest([this.sharedService.loadingRiskManagment$, this.sharedService.pricesDates$]).pipe(
      takeUntil(this._guard$),
      debounceTime(200)
    ).subscribe(([loadingState, pricesDate]) => {
      if (!loadingState) {
        this.filterChartByDate(pricesDate);
        this.drawChart();
        this.changeChartLegendColorByTheme();
        this.toggleChartVisibility(loadingState);
        this.changeLoaderState(false);
      }
    });

    this._appService.newTheme$.pipe(
      takeUntil(this._guard$)
    ).subscribe(themeName => {
      this.changeBubblesColor(themeName);
    });
  }


  /**
   * Chart setup - draw, update, resize
   *
   * @param  {boolean} loadingState
   * @returns void
   */
  chartSetup(loadingState: boolean, initialSeries: string, labels?: string[]): void {
    this.changeLoaderState(true);

    if (!loadingState && this.chartBoxOptions && this.chartBoxOptions.data.datasets.length) {
      this.backupData = cloneDeep(this.chartBoxOptions.data.datasets);
    } else if (loadingState && this.chartOptions && !this.chartOptions.data.datasets.length) {
      // if models oraz countries filters are empty arrays
      if (this.myChart) {
        this.myChart.update(); // destroys chart data
        this.toggleChartVisibility(loadingState);
        this.resizeChart();
      }
    }
  }

  /**
   * Filtruje wykres po dacie z dateFilter
   *
   * @returns void
   */
  filterChartByDate(pricesDate: string): void {
    const startDate: string = !pricesDate ? moment().format('YYYY-MM-DD') : pricesDate;
    const tempData = [];

    this.backupData.forEach(dataSets => {
      const tempDataset = cloneDeep(dataSets);
      const filteredData = filter(dataSets.data, (object) => {
        const dataIsSameStartDate: boolean = moment(object.x.toString()).isSame(moment(startDate).add(1, 'day'), 'day'); // dane ktore sa tej samej daty co startDate
        const dataIsAfterStartDate: boolean = moment(object.x.toString()).isAfter(startDate, 'day'); // dane ktore sa po dacie startDate
        return (dataIsSameStartDate || dataIsAfterStartDate);
      });

      tempDataset.data = cloneDeep(filteredData);
      tempData.push(tempDataset);
    });

    this.chartBoxOptions.data.datasets.forEach((dataset, index) => {
      if (tempData[index]) {
        dataset.data = cloneDeep(tempData[index].data);
      }
    });

    this.resizeChart();
    this.cdRef.detectChanges();
  }

  /**
   * @returns void
   */
  drawChart(): void {
    Chart.defaults.global.defaultFontFamily = ' Overpass';
    Chart.defaults.global.defaultFontSize = 10;

    const canvas = this.chartBoxplot.nativeElement.getContext('2d');

    this.zone.runOutsideAngular(() => {
      if (this.myChart) {
        this.myChart.destroy();
      }

      this.myChart = new Chart(canvas, this.chartBoxOptions);
      this.resizeChart();
    });
  }

  /**
  * Add new dataset to chart and get
  *
  * @param  {DataSets} newDataset
  * @returns void
  */
  addDataSet(newDataset: DataSets): void {
    this.myChart.config.data.datasets.push({ ...newDataset });
  }

  /**
   * Zmiana kolorow dla legendy canvasa i siatki
   *
   * @return void
   */
  changeChartLegendColorByTheme(): void {

    const gridColorDark = {
      color: 'rgba(255,255,255, 0.4)',
      offsetGridLines: true
    };
    const xAxesDark = 'rgba(255,255,255, 0.9)';
    const yAxesDark = 'rgba(255,255,255, 0.9)';

    const gridColorDefault = {
      color: 'rgba(41,41,41, 0.4)',
      offsetGridLines: true
    };
    const xAxesDefault = 'rgba(0,0,0, 0.8)';
    const yAxesDefault = 'rgba(0,0,0, 0.8)';

    if (this.chartBoxOptions) {
      if (this.themeName === 'dark_theme') {
        this.chartBoxOptions.options.scales.xAxes[0].ticks.minor.fontColor = xAxesDark;
        this.chartBoxOptions.options.scales.xAxes[0].gridLines.color = gridColorDark.color;
        this.chartBoxOptions.options.scales.xAxes[0].gridLines.offsetGridLines = gridColorDark.offsetGridLines;
        this.chartBoxOptions.options.scales.yAxes[0].ticks.minor.fontColor = yAxesDark;
        this.chartBoxOptions.options.scales.yAxes[0].gridLines.color = gridColorDark.color;
      } else {
        this.chartBoxOptions.options.scales.xAxes[0].ticks.minor.fontColor = xAxesDefault;
        this.chartBoxOptions.options.scales.xAxes[0].gridLines.color = gridColorDefault.color;
        this.chartBoxOptions.options.scales.xAxes[0].gridLines.offsetGridLines = gridColorDefault.offsetGridLines;

        this.chartBoxOptions.options.scales.yAxes[0].ticks.minor.fontColor = yAxesDefault;
        this.chartBoxOptions.options.scales.yAxes[0].gridLines.color = gridColorDefault.color;
      }
    }

    if (this.myChart) {
      this.myChart.update();
    }
  }

  /**
   * Zmienia kolor punktów zaleznie od koloru motywu
   *
   * @returns void
   */
  changeBubblesColor(theme): void {
    if (this.myChart) {
      this.myChart.data.datasets.forEach(dataset => {
        if (dataset.type === 'bubble') {
          dataset.borderColor = theme === 'default_theme' ? '#909090' : '#FEFEFE';
        }
      });

      this.myChart.update();
    }
  }

  ngOnDestroy() {
  }
}
