import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ChartData } from './../../classes/chart';
import { ChartQueryParams, Params } from '../../classes/classes';
import { SharedService } from '../../shared/shared.service';
import { CustomHttpService } from "../../shared/customHttp.service";

@Injectable()
export class ChartPricesService {

  constructor(private _http: CustomHttpService, private _sharedService: SharedService) {}

  /**
   * Gets data for prices chart
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  getPricesData(queryParams: ChartQueryParams): Observable<ChartData> {
    const params = new Params(queryParams.iso, queryParams.model, queryParams.offset, queryParams.runDate);
    return this._http
      .get(`ChartData/prices?` + this._sharedService.toQueryString(params) + `&subModel=14`);
  }
}
