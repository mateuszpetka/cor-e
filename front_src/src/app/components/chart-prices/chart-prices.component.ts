
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  NgZone,
  ViewChild,
  OnInit,
  OnChanges,
} from '@angular/core';
import { of as observableOf, BehaviorSubject, Observable, fromEvent } from 'rxjs';
import { shareReplay, delay, combineLatest, debounceTime, map, takeUntil } from 'rxjs/operators';
import { MatSelect } from '@angular/material';

import { ChartListHttpService } from '../../containers/chartlist/chartlist-http.service';
import { ChartsStateService } from '../../shared/chars-state.service';
import { SharedService } from './../../shared/shared.service';
import { ChartService } from './../chart/chart.service';
import { ChartPricesService } from './chart-prices.service';
import { AppService } from '../../app.service';

import { ChartAvailabilityOrPricesState, Country, CountryWithTitle, ChartQueryParams } from '../../classes/classes';
import { ChartData, DataSets, ModelRunDates } from '../../classes/chart';

import { ChartComponent } from './../chart/chart.component';
import { cloneDeep, filter, find, isArray, isEmpty, uniqBy } from 'lodash';
import * as moment from 'moment';
import { Chart } from 'chart.js';

@Component({
  selector: 'chart-prices',
  templateUrl: './chart-prices.component.html',
  styleUrls: ['./chart-prices.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartPricesComponent extends ChartComponent implements OnInit, AfterViewInit {
  @Input() pricesRunDates: ModelRunDates;
  @ViewChild('runDatesSelect') private runDateSelect: MatSelect;
  public currentOffset: number = 16;
  private tempDeCountry: Country = {
    name: 'Germany',
    iso: 'de'
  };

  private selectedKindSrc: BehaviorSubject<string> = new BehaviorSubject<string>('Prices');
  public currentSelectedKind$: Observable<string> = this.selectedKindSrc.asObservable();
  private chartPricesState: ChartAvailabilityOrPricesState;
  public dateFilter: {
    start: string | Date | moment.Moment,
    end: string | Date | moment.Moment,
    firstDay: string | Date | moment.Moment,
    lastDay: string | Date | moment.Moment,
    startFormatted: string,
    endFormatted: string,
    showBeforeArrow: boolean,
    showNextArrow: boolean
  } = {
      start: '',
      end: '',
      firstDay: '',
      lastDay: '',
      startFormatted: '',
      endFormatted: '',
      showBeforeArrow: false,
      showNextArrow: false
    };

  constructor(
    public sharedService: SharedService,
    public chartsStateService: ChartsStateService,
    public chartService: ChartService,
    public zone: NgZone,
    public cdRef: ChangeDetectorRef,
    private _chartListHttpService: ChartListHttpService,
    private _chartPricesService: ChartPricesService,
    public elementRef?: ElementRef,
    public _appService?: AppService
  ) {
    super(chartService, sharedService, chartsStateService, cdRef, zone);
  }

  ngOnInit() {
    Chart.defaults.global.defaultFontFamily = 'Overpass';
    Chart.defaults.global.defaultFontSize = 10;
    this.chartPricesState = this.getChartState() as ChartAvailabilityOrPricesState;
    this.subscribeThemeChange();

    if (!!this.chartPricesState) {
      this.currentOffset = this.chartPricesState.currentDay ? Number(this.chartPricesState.currentDay) : this.currentOffset;

      // rozwiazanie tymczasowe, zeby nikomu nie wywalilo bledu
      // bo na razie chcieli, zeby 16 dni bylo ukryte
      this.currentOffset = this.currentOffset === 16 ? 10 : this.currentOffset;
    }

    this.country$ = this.sharedService.activeCountry$;
    // obserwuje obecnie wybrany kraj
    this.currentCountry$ = this.country$.pipe(
      takeUntil(this._guard$),
      debounceTime(200),
      combineLatest(this.currentChartCountry$),
      map(([countryFromService, selectedCountry]) => {
        const currentCountry = !!selectedCountry ? selectedCountry : countryFromService;
        if (!!this.queryParams && !(!!this.queryParams.iso)) {
          this.queryParams.iso = currentCountry.iso;
        }

        this.selectedCountry = currentCountry;
        return currentCountry;
      }),
      shareReplay()
    );

    this.countriesSub = this.sharedService.selectedCountries$
      .pipe(
        takeUntil(this._guard$)
      ).subscribe(countries => {
        this.countries = countries;
      });

    this.countries$ = this.sharedService.selectedCountries$
      .pipe(
        debounceTime(200),
        takeUntil(this._guard$),
        map((countries: Country[]) => {
          const uniqCountries = uniqBy(countries, 'iso');
          this.countries = cloneDeep(uniqCountries);

          // Jezeli aktualnie zaznaczony kraj zostanie usuniety z filtrow globalnych pobiera pierwszy kraj z listy
          if (!!this.selectedCountry && this.selectedCountry !== uniqCountries[0] && !!uniqCountries.length) {
            this.changeDataByCountry(uniqCountries[0]);
            this.currentChartCountry$.next(uniqCountries[0]);
          }

          return uniqCountries;
        })
      );

    // przypisuje wybrane modele do zmiennej
    this.sharedService.currentModels$.subscribe((currentModels: string[]) => {
      this.currentModels = currentModels;
    });

    this._appService.newTheme$.pipe(
      takeUntil(this._guard$)
    ).subscribe(themeName => {
      this.changeRealisedColor();
    });

    fromEvent(window, 'resize').pipe(
      debounceTime(300),
      takeUntil(this._guard$)
    ).subscribe(event => {
      if (this.fullscreenActive) {
        this.sharedService.setFullscreenChart(this.chartTitle);
      }
    });
  }

  ngAfterViewInit() {
    this.loaderSub = this.sharedService.loadingPrices$
      .pipe(
        takeUntil(this._guard$),
        debounceTime(200)
      ).subscribe((loadingState: boolean) => {
        this.labels = [''];
        this.toggleChartVisibility(loadingState);
        this.chartSetup(loadingState);
        this.setDateFilters();
      });
  }

  /**
   * Sets selected button
   *
   * @param  {Country} name
   * @returns void
   */
  setChartSelectedBtn(selectedCountry: Country): void {
    const tempCountry = this.countries.find(country => country.iso === selectedCountry.iso);
    this.currentChartCountry$.next(tempCountry);
  }

  /**
   * Chart setup - draw, update, resize
   *
   *
   * @param  {boolean} loadingState
   * @returns void
   */
  chartSetup(loadingState: boolean): void {
    this.changeLoaderState(true);
    if (!loadingState) {
      if (!!this.chartOptions && this.chartOptions.data.datasets.length) {
        this.filterChartData();
        this.setInitialsColors();
        this.chartOptions.options.scales.xAxes.forEach(data => {
          data.time.unit = 'day';
          // data.time.displayFormats['hour'] = 'MMM DD hA';
          data.time.displayFormats.day = 'MMM DD';
          data.time.stepSize = 1;
        });
        this.changeChartLegendColorByTheme();
        this.drawChart();

        // sprawdzanie checkboxow w menu i jeżeli widok jest
        this.checkSelectedKindAndLabels();

        this.filtersByDays(this.currentOffset);
        this.toggleChartVisibility(loadingState);
      }
    } else if (loadingState && this.chartOptions && !this.chartOptions.data.datasets.length) {
      // if models oraz countries filters are empty arrays
      if (this.myChart) {
        this.myChart.update(); // destroys chart data
        this.toggleChartVisibility(loadingState);
        this.resizeChart();
      }
    }
  }

  /**
   * Sets initial colros based on model
   *
   * @returns void
   */
  setInitialsColors(): void {
    const data = [...this.chartOptions.data.datasets];
    data.forEach(dataset => {
      this.setInitialColors(dataset);
    });
    this.chartOptions.data.datasets = data;
  }

  /**
   * Filters by days
   *
   * @returns void
   */
  filtersByDays(day: number): void {
    this.dateFilter.end = this.dateFilter.end === '' ?
      this.dateFilter.end :
      moment(this.dateFilter.start).add(day - 1, 'days').format('YYYY-MM-DD');
    this.setDateFilters();
    this.currentOffset = day;
    this.loadDays(day);
    this.updateChart();
    this.cdRef.detectChanges();
    this.filterChartByDate();
  }

  changeDayFilter(operation: string): void {
    const days = this.currentOffset;

    this.changeLoaderState(true);

    let startDate = this.dateFilter.start === '' ? moment().add(1, 'days').format('YYYY-MM-DD') : moment(this.dateFilter.start);
    let endDate = this.dateFilter.end === '' ? moment().add(days, 'days').format('YYYY-MM-DD') : moment(this.dateFilter.end);

    startDate = operation === 'add' ?
      moment(startDate).add(1, 'days').format('YYYY-MM-DD') :
      moment(startDate).subtract(1, 'days').format('YYYY-MM-DD');

    endDate = operation === 'add' ?
      moment(endDate).add(1, 'days').format('YYYY-MM-DD') :
      moment(endDate).subtract(1, 'days').format('YYYY-MM-DD');

    this.dateFilter.start = startDate;
    this.dateFilter.end = endDate;

    this.dateFilter.showBeforeArrow = this.checkSameOrBefore(this.dateFilter.firstDay, this.dateFilter.start);
    this.dateFilter.showNextArrow = this.checkSameOrBefore(this.dateFilter.end, this.dateFilter.lastDay);

    this.dateFilter.startFormatted = moment(startDate).format('YYYY-MM-DD');
    this.dateFilter.endFormatted = moment(endDate).format('YYYY-MM-DD');

    this.filterChartByDate();
  }

  /**
   * Filtruje wykres po dacie z dateFilter
   *
   * @returns void
   */
  filterChartByDate(): void {
    if (this.dateFilter.start === '') {
      return;
    }

    const tempData = [];
    this.backupData.forEach(dataSets => {
      const tempDataset = cloneDeep(dataSets);
      const filteredData = filter(dataSets.data, (object) => {
        return (moment(object.x.toString()).isSame(this.dateFilter.start, 'day') ||
          moment(object.x.toString()).isAfter(this.dateFilter.start, 'day')) &&
          (moment(object.x.toString()).isSame(this.dateFilter.end, 'day') ||
          moment(object.x.toString()).isBefore(this.dateFilter.end, 'day'));
      });

      tempDataset.data = cloneDeep(filteredData);
      tempData.push(tempDataset);
    });

    this.myChart.config.data.datasets.forEach((dataset, index) => {
      if (tempData[index]) {
        dataset.data = cloneDeep(tempData[index].data);
      }
    });

    this.updateChart();
    this.cdRef.detectChanges();
  }

  /**
   * Filter current chart data -> from current day + 1
   *
   * @returns void
   */
  filterChartData(): void {
    this.backupData = this.chartOptions.data.datasets;
    const tempData = [];

    this.backupData.forEach(dataSets => {
      const tempDataset = cloneDeep(dataSets);
      const filteredData = dataSets.data;
      tempDataset.data = cloneDeep(filteredData);
      tempData.push(tempDataset);
    });
    this.backupData = tempData;
  }

  /**
   * Loads more data based on SELECTED offset
   *
   * @param  {string} runDate
   * @returns void
   */
  loadMoreData(runDate: string, type?: string): void {
    this.changeLoaderState(true);
    this.lastCheckBox.checked = false;
    this.lastToogleState = false;
    this.prevCheckBox.checked = false;
    this.prevToogleState = false;

    if (runDate === null) {
      this.myChart.config.data.datasets.forEach((dataset, index) => {
        if (dataset.asOf) {
          this.myChart.config.data.datasets[index].hidden = true;
        }
      });
      this.updateChart();
    } else {
      const offset = this.pricesRunDates.gfs
        ? this.pricesRunDates.gfs.indexOf(runDate)
        : this.pricesRunDates.hres.indexOf(runDate);
      this.selectedOffset = offset;
      this.queryParams.offset = offset;
      this.selectedRunDate = runDate;

      const boxplotData = {
        offset: offset,
        runDate: runDate,
        iso: this.queryParams.iso
      };
      this.sharedService.setBoxPlotOffset(boxplotData);
      this.saveCurrentLocalState();
      this.checkSelectedRunDate(offset, runDate, type);
    }

    this.sharedService.pricesDates.next(runDate);
  }

  /**
   * Load last or prev dataset
   *
   * @param  {number} index
   * @param  {boolean} asOf
   * @returns void
   */
  loadDataBasedOnRadioBtnValue(index: number, asOf?: boolean): void {
    this.changeLoaderState(true);
    if (index === 0) {
      this.prevToogleState = false;
      this.lastToogleState = true;
      this.prevCheckBox.checked = false;
    } else if (index === 1) {
      this.lastCheckBox.checked = false;
      this.lastToogleState = false;
      this.prevToogleState = true;
    }

    const runDate: string = '';
    const offset: number = index;
    this.selectedOffset = offset;

    if (!asOf) {
      if (this.runDateSelect) {
        this.runDateSelect.value = '';
      }
      this.selectedRunDate = null;
    }

    const searchedIndex: number[] = [];
    this.myChart.config.data.datasets.forEach((dataset, datasetIndex) => {
      if (dataset.offset === offset && dataset.kind.toLowerCase() === this.selectedCountry.iso) {
        this.myChart.config.data.datasets[datasetIndex].hidden = false;
        searchedIndex.push(index);
      } else {
        this.myChart.config.data.datasets[datasetIndex].hidden = true;
      }

      if (dataset.label === 'Prices Realised') {
        this.myChart.config.data.datasets[datasetIndex].hidden = false;
      }
    });

    if (!searchedIndex.length) {
      this.getDataByOffset(offset, runDate);
    } else {
      this.setDateFilters();
    }

    const boxplotData = {
      offset: offset,
      runDate: runDate,
      iso: this.queryParams ? this.queryParams.iso : this.selectedCountry.iso
    };

    this.sharedService.pricesDates.next(this.selectedRunDate);
    this.sharedService.setBoxPlotOffset(boxplotData);
    this.saveCurrentLocalState();
    this.updateChart();
    this.cdRef.detectChanges();
  }

  /**
   * Gets data from service by offset and current queryParams
   *
   * @param  {number} offset
   * @param  {string} runDate
   * @param  {boolean} asOf
   * @param  {string} type
   * @returns void
   */
  getDataByOffset(offset: number, runDate?: string, asOf?: boolean, type?: string): void {
    let dataRequest$: Observable<any> = observableOf('');
    this.currentModels.forEach((model: string) => {
      const params = this.queryParams ? this.queryParams : {} as ChartQueryParams;
      params.iso = this.queryParams && this.queryParams.iso ? this.queryParams.iso : this.selectedCountry.iso;

      if (params) {
        params.model = model;
        params.offset = offset;
      }
      const color = this.checkColor(model);

      if (asOf) {
        params.runDate = runDate;
        params.offset = null;
      } else {
        params.runDate = null;
      }
      dataRequest$ = this._chartPricesService.getPricesData(params);

      this.updateChart();

      dataRequest$.pipe(map(res => {
        const data = [];
        if (isArray(res)) {
          res.forEach(resData => {
            if (!isEmpty(resData)) {
              data.push(resData);
            }
          });
        } else {
          if (!isEmpty(res)) {
            data.push(res);
          }
        }

        const filteredData = [];
        for (const key in data) {
          if (data[key].length) {
            data[key].forEach(element => {
              filteredData.push(element);
            });
          } else {
            filteredData.push(data[key]);
          }
        }
        return filteredData;
      }))
        .pipe(
          debounceTime(200)
        ).subscribe((resData: ChartData[]) => {
          resData.forEach(data => {
            const newDataSet = new DataSets(data.name, data.series, model, data.kind, data.runDate, offset, color.main);
            if (offset > 1) {
              newDataSet['borderDash'] = [5, 5];
            } else {
              newDataSet['borderColor'] = color.opac;
              newDataSet['borderDash'] = [0.5, 0.5];
            }
            this.addNewDataSet(newDataSet);
          });
          this.myChart.config.data.datasets.forEach((dataset) => {
            if (dataset.offset !== offset && dataset.kind.toLowerCase() === this.selectedCountry.iso) {
              dataset.hidden = true;
            }

            if (dataset.label === 'Prices Realised') {
              dataset.hidden = false;
            }
          });

          this.filtersByDays(this.currentOffset);
          this.updateChart();
          this.cdRef.detectChanges();
        },
          (error) => {
            this.sharedService.checkIfUsersIsAlreadyLoggedIn(error);
          });
    });
  }

  /**
  * Add new dataset to chart and
  *
  * @param  {DataSets} newDataset
  * @returns void
  */
  addNewDataSet(newDataset: DataSets): void {
    this.myChart.config.data.datasets.push({ ...newDataset });
    this.backupData.push({ ...newDataset });
  }

  /**
   * Clears filters
   *
   * @returns void
   */
  clearFilters(): void {
    this.lastCheckBox.checked = true;
    this.prevCheckBox.checked = false;
    if (this.runDateSelect) {
      this.runDateSelect.value = '';
    }
    this.cdRef.detectChanges();
  }

  /**
  * Zapamietywanie danych w checboxach last i prev i wyswietlanie danych
  *
  * @returns void
  */
  checkSelectedKindAndLabels(): void {
    this.changeLoaderState(true);
    // sprawdzenie czy state posiada kraj
    if (!!this.chartPricesState) {
      if (this.countries.length > 0 && !!this.chartPricesState.country) {
        const tempCountry = this.countries.find(country => country.iso === this.chartPricesState.country);
        const initialCountry = !tempCountry && !!this.selectedCountry ? this.selectedCountry : tempCountry;
        this.setChartSelectedBtn(initialCountry);
      }

      if (this.chartPricesState.selectedOffset === 0) {
        this.lastCheckBox.checked = true;
        this.prevCheckBox.checked = false;
        this.loadDataBasedOnRadioBtnValue(0, false);
      } else if (this.chartPricesState.selectedOffset === 1) {
        this.lastCheckBox.checked = false;
        this.prevCheckBox.checked = true;
        this.loadDataBasedOnRadioBtnValue(1, false);
      } else if (this.pricesRunDates) {
        if (
          this.pricesRunDates.gfs
            ? this.pricesRunDates.gfs.includes(this.chartPricesState.runDate)
            : this.pricesRunDates.hres.includes(this.chartPricesState.runDate)
        ) {
          this.selectedRunDate = this.chartPricesState.runDate;
          this.lastCheckBox.checked = false;
          this.prevCheckBox.checked = false;
          this.runDateSelect.value = this.selectedRunDate;
          this.loadMoreData(this.selectedRunDate, 'prices');
        }
      }
    }
  }

  /**
   * Emits selected country and chart type to parent component - chart-list
   *
   * @param  {Country} country
   * @returns void
   */
  changeDataByCountry(country: Country): void {
    this.toggleChartVisibility(true);
    const data: CountryWithTitle = {
      country: country,
      title: this.chartTitle,
    };
    this.currentChartCountry$.next(country);
    this.changeDataEvent.emit(data);
    this.saveCurrentLocalState();
  }

  /**
  * Przypisuje aktualny stan filtrow do  lokalnej zmiennej
  *
  * @returns void
  */
  saveCurrentLocalState(): void {
    const iso = !!this.selectedCountry ? this.selectedCountry.iso : '';
    const selectValue = !!this.runDatesSelect ? this.runDatesSelect.value : '';
    const currentDataOffset = this.selectedOffset;
    const currentDayOffset = !!this.currentOffset ? this.currentOffset.toString() : null;

    const state = new ChartAvailabilityOrPricesState(iso, currentDayOffset,
      currentDataOffset, null, selectValue);
    this.chartPricesState = state;
    this.chartsStateService.saveChartState(this.chartTitle.toLowerCase(), state);
  }

  /**
   * Parsuje daty do filtrów
   *
   * @param  {string|Date|moment.Moment} date
   * @param  {string} type
   */
  parseDate(date: string | Date | moment.Moment, type: string): void {
    if (type === 'start') {
      date = date === '' ? moment().add(1, 'days') : date;
      date = moment(date);
      this.dateFilter.start = date;
      this.dateFilter.startFormatted = moment(date).format('YYYY-MM-DD');
    } else if (type === 'end') {
      date = date === '' ? moment().add(this.currentOffset, 'days') : moment(date);
      this.dateFilter.end = date;
      this.dateFilter.endFormatted = moment(date).format('YYYY-MM-DD');
    }
  }

  /**
   * Sprawdzenie najstarszych i najnowszych danych z wykresu i ustawienie ograniczen
   *
   * @returns void
   */
  setMinAndMaxDates(): void {
    let minDate;
    let maxDate;
    if (this.myChart) {
      this.backupData.forEach(dataset => {
        if (dataset.data.length) {
          const datasetMin = dataset.data[0].x || dataset.data[0].name;
          const datasetMax = dataset.data[dataset.data.length - 1].x || dataset.data[dataset.data.length - 1].name;

          if (minDate) {
            minDate = moment(minDate).isAfter(moment(datasetMin), 'days') ? datasetMin : minDate;
          } else {
            minDate = datasetMin;
          }

          if (maxDate) {
            maxDate = moment(maxDate).isBefore(moment(datasetMax), 'days') ? datasetMax : maxDate;
          } else {
            maxDate = datasetMax;
          }
        }
      });
    }

    this.dateFilter.firstDay = minDate;
    this.dateFilter.lastDay = maxDate;

    this.dateFilter.showBeforeArrow = this.checkSameOrBefore(this.dateFilter.firstDay, this.dateFilter.start);
    this.dateFilter.showNextArrow = this.checkSameOrBefore(this.dateFilter.end, this.dateFilter.lastDay);
  }

  /**
   * Sprawdza, czy data1 jest przed dniem lub taka sama jak date2
   *
   * @param  {string|moment.Moment|Date} date1
   * @param  {string|moment.Moment|Date} date2
   * @returns boolean
   */
  checkSameOrBefore(date1: string | moment.Moment | Date, date2: string | moment.Moment | Date): boolean {
    return moment(date1).isSameOrBefore(moment(date2), 'days');
  }

  /**
   * Zmienia kolor linii realised prices zaleznie od koloru motywu
   *
   * @returns void
   */
  changeRealisedColor(): void {
    if (this.myChart) {
      this.myChart.data.datasets.forEach(dataset => {
        if (dataset.label === 'Prices Realised') {
          dataset.borderColor = this.themeName === 'default_theme' ? '#909090' : '#FEFEFE';
          dataset.pointBorderColor = dataset.borderColor;
        }
      });

      this.myChart.update();
    }
  }

  setDateFilters() {
    this.parseDate(this.dateFilter.start, 'start');
    this.parseDate(this.dateFilter.end, 'end');

    this.setMinAndMaxDates();
  }

}
