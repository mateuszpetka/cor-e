
import {interval as observableInterval,  Observable ,  Subscription ,  timer } from 'rxjs';

import {share, switchMap, map, filter} from 'rxjs/operators';
import { CustomHttpService } from './../../shared/customHttp.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { LoggedUserInitialData } from './../../classes/classes';
import { TokenService } from './token.service';
import { HttpService } from '../../http.service';
import { SharedService } from 'app/shared/shared.service';
import { AppService } from 'app/app.service';
import { ChartsStateService } from 'app/shared/chars-state.service';


@Injectable()
export class AuthService {
  public regenerateTokenSubscription = new Subscription();
  private readonly intervalMultiplier = 0.15;
  private readonly minExpirationTime = 5000;

  constructor(
    private http: CustomHttpService,
    private defaultHttpService: HttpService,
    private router: Router,
    private tokenService: TokenService,
    private sharedService: SharedService,
    private appService: AppService,
    private chartStateService: ChartsStateService
  ) {}

  /**
   * Logowanie
   *
   * @param  {Object} credentials {identifier: string, password: string}
   * @returns Observable
   */
  login(credentials): Observable<LoggedUserInitialData> {
    return this.http
      .postLogin('Auth/login', credentials).pipe(
      map(res => {
        return res;
      }),
      share(),);
  }

  /**
   * Wylogowanie
   *
   * @returns void
   */
  logout(): void {
    this.chartStateService.clearChartStates();
    localStorage.clear();
    this.router.navigateByUrl('auth/login');
  }

  /**
   * Sprawdzenie, czy captcha jest wymagana podczas logowania
   *
   * @returns Observable
   */
  isCaptchaRequired(): Observable<boolean> {
    return this.defaultHttpService.get('Auth/captchaRequired').pipe(map(res => res.captchaRequired));
  }

  /**
   * Ustawienie automatycznego odświeżania tokena
   *
   * @returns void
   */
  refreshTokenPeriodically(): void {
    const token = localStorage.getItem('authToken');

    if (token) {
      const expirationTime = Math.floor(this.tokenService.getTimeLeftToTokenExpiration(token) / 10);
      const interval = Math.floor(expirationTime * this.intervalMultiplier);
      const delayTime = expirationTime - interval < this.minExpirationTime ? 0 : expirationTime - interval;
      const initialRefreshTimer = timer(delayTime);

      const regenerateToken$ = initialRefreshTimer.pipe(switchMap(() =>
        this.http.get('Auth/regenerateToken').pipe(switchMap(({ newToken }) => {
          if (!!newToken) {
            this.setAuthToken(newToken);
            const newTokenLifeSpan = this.tokenService.getTokenLifeSpan(newToken);
            const newInterval = Math.floor(newTokenLifeSpan * this.intervalMultiplier);
            return observableInterval(newTokenLifeSpan - newInterval)
            .pipe(
              switchMap(() => this.http.get('Auth/regenerateToken')),
              map(res => res as any & { newToken: string })
            );
           } else {
             console.error('newToken undefined', newToken);
           }
        }))
      ));

      this.regenerateTokenSubscription = regenerateToken$.pipe(
        filter(Boolean)
      ).subscribe(({newToken}) => {
        this.setAuthToken(newToken);
      }, error => {
        console.error('Regeneration went wrong', error);
        this.sharedService.openDialog('userEmpty');
      });
    }
  }

  /**
   * Zapisanie tokena w localStorage
   *
   * @param  {string} token
   */
  setAuthToken(token: string) {
    // localStorage.setItem('frontAuthToken', token);
    if (localStorage.getItem('authToken')) {
      localStorage.setItem('authToken', token);
    } else {
      this.appService.setAuthToken(token);
    }
    return true;
  }

  /**
   * Wyczyszczenie tokena i usera z localstorage
   */
  removeAuthData() {
    this.regenerateTokenSubscription.unsubscribe();
    this.removeAuthToken();
    this.removeRoutesInfo();
    localStorage.clear();
  }

  /**
   * Usunięcie informacji o dostępie do ścieżek z localstorage
   *
   * @returns void
   */
  removeRoutesInfo(): void {
    localStorage.removeItem('routes');
    localStorage.removeItem('routesKey');
  }

  /**
   * Usunięcie tokena z localstorage
   *
   * @returns void
   */
  removeAuthToken(): void {
    localStorage.removeItem('authToken');
  }

  /**
   * Wysłanie linku do resetu hasła
   *
   * @param  {email:string, captcha:string} credentials
   * @returns Observable
   */
  public sendPasswordRecoveryRequest(credentials: { email: string; captcha: string }): Observable<any> {
    return this.http.postLogin('Auth/sendResetCode', credentials);
  }

  /**
   * Ustawia nowe hasło
   *
   * @param  {email:string, captcha:string} credentials
   * @returns Observable
   */
  public resetPassword(credentials: { password: string; passwordConfirm: string, token: string }): Observable<any> {
    return this.http.postLogin('Auth/resetPassword', credentials);
  }
}
