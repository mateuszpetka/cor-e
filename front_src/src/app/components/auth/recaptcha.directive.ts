import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  forwardRef,
  Injectable,
  Injector,
  Input,
  NgZone,
  OnInit,
  Output
} from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR, NgControl, Validators } from '@angular/forms';
import { AuthService } from './auth.service';

declare const grecaptcha: any;

declare global {
  interface Window {
    grecaptcha: any;
    reCaptchaLoad: () => void;
  }
}

@Injectable()
export class ReCaptchaAsyncValidator {
  constructor(private authService: AuthService) {}
}

export interface ReCaptchaConfig {
  theme?: 'dark' | 'light';
  type?: 'audio' | 'image';
  size?: 'compact' | 'normal';
  tabindex?: number;
}

@Directive({
  selector: '[recaptcha]',
  exportAs: 'recaptcha',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RecaptchaDirective),
      multi: true
    },
    ReCaptchaAsyncValidator
  ]
})
export class RecaptchaDirective implements OnInit, AfterViewInit, ControlValueAccessor {
  @Input() key: string;
  @Input() config: ReCaptchaConfig = {};
  @Input() lang: string;

  @Output() captchaResponse = new EventEmitter<string>();
  @Output() captchaExpired = new EventEmitter<null>();

  private control: FormControl;
  private widgetId: number;

  private onChange: (value: string) => void;
  private onTouched: (value: string) => void;

  constructor(private element: ElementRef, private ngZone: NgZone, private injector: Injector) {}

  ngOnInit() {
    this.registerReCaptchaCallback();
    this.addScript();
  }

  /**
   * Dodanie callbacka dla recaptchy
   *
   * @returns void
   */
  registerReCaptchaCallback(): void {
    window.reCaptchaLoad = () => {
      const config = {
        ...this.config,
        sitekey: this.key,
        callback: this.onSuccess.bind(this),
        'expired-callback': this.onExpired.bind(this)
      };
      this.widgetId = this.render(this.element.nativeElement, config);
    };
  }

  ngAfterViewInit() {
    this.control = this.injector.get(NgControl).control;
    this.setValidators();
  }

  /**
   * Obsługa kilku captchy (na wszelki wypadek)
   * @returns {number}
   */
  getId() {
    return this.widgetId;
  }

  /*tslint:disable-next-line:no-empty*/
  writeValue(obj: any): void {}

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  /**
   * Obsługa captchy, która wygasła
   *
   * @returns void
   */
  onExpired(): void {
    this.ngZone.run(() => {
      this.captchaExpired.emit();
      this.onChange(null);
      this.onTouched(null);
    });
  }

  /**
   * Obsługa odpowiedzi z backendu
   *
   * @param  {string} token
   * @returns void
   */
  onSuccess(token: string): void {
    this.ngZone.run(() => {
      this.verifyToken();
      this.captchaResponse.next(token);
      this.onChange(token);
      this.onTouched(token);
    });
  }

  /**
   * Weryfikacja tokena
   *
   * @param  {string} token
   * @returns void
   */
  verifyToken(): void {
    this.control.updateValueAndValidity();
  }

  /**
   * Reset captchy
   *
   * @returns void
   */
  reset(): void {
    grecaptcha.reset();
    this.onChange(null);
  }

  /**
   * Pobranie odpowiedzi dla captchy
   *
   * @returns {string}
   */
  getResponse(): string {
    if (!this.widgetId) {
      return grecaptcha.getResponse(this.widgetId);
    }
  }

  /**
   * Dodanie skryptu obsługującego captcha
   *
   * @returns void
   */
  addScript(): void {
    const script = document.createElement('script');
    const lang = this.lang ? '&hl=' + this.lang : '';
    script.src = `https://www.google.com/recaptcha/api.js?onload=reCaptchaLoad&render=explicit${lang}`;
    script.async = true;
    script.defer = true;
    document.body.appendChild(script);
  }

  /**
   * Wyrenderowanie captchy
   *
   * @param {HTMLElement} element
   * @param {ReCaptchaConfig} config
   * @returns {number}
   */
  private render(element: HTMLElement, config): number {
    return grecaptcha.render(element, config);
  }

  /**
   * Zdefiniowanie walidatora
   *
   * @returns void
   */
  private setValidators(): void {
    this.control.setValidators(Validators.required);
    this.control.updateValueAndValidity();
  }
}
