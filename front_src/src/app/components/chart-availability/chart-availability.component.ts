
import {switchMap, delay, shareReplay, combineLatest, debounceTime, map, takeUntil } from 'rxjs/operators';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  NgZone,
  OnChanges,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Chart } from 'chart.js';
import { cloneDeep, find, remove, sortBy } from 'lodash';
import { Observable ,  forkJoin, fromEvent } from 'rxjs';

import { AppService } from '../../app.service';
import { ChartCustomOption, ChartData } from '../../classes/chart';
import { ChartsStateService } from '../../shared/chars-state.service';
import { DataSets } from './../../classes/chart';
import { AvailabilityTableData, BtnLabel, ChartAvailabilityOrPricesState, Country } from './../../classes/classes';
import { SharedService } from './../../shared/shared.service';
import { ChartComponent } from './../chart/chart.component';
import { ChartService } from './../chart/chart.service';
import { ChartAvailabilityService } from './chart-availability.service';

class SelectItem {
  model: string = null;
  labels: string[] = [];
}

@Component({
  selector: 'chart-availability',
  templateUrl: './chart-availability.component.html',
  styleUrls: ['./chart-availability.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartAvailabilityComponent extends ChartComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {
  @Input() tableData: AvailabilityTableData[];
  @Input() availabilityRunDates: string[];
  public displayedColumns = ['typeOfOutage', 'startDate', 'endDate', 'nameUnit',
  'nameCapacity', 'availableCapacity', 'update']; // kolumny tabeli ongoing outages
  public newTableData: any[] = [];  // dane tabeli ongoing outgaes
  public labels: string[] = []; // zmienna przechowująca nazwy przyciskow legendy
  public showChart: boolean = true; // toggles between chart and table
  public currentOffset: number = 30; // state of data offset - selected by user ()
  public asOfData$: Observable<string[]>; // Observable of availability run dates
  public defaultCheckboxState: boolean = true; // d0 checkbox state
  public d1CheckboxState: boolean = false; // d-1 checkbox state
  public d2CheckboxState: boolean = false; // d-7 checkbox state
  public currentRunDateOffset: number = null;
  public selectedBtns: BtnLabel[] = []; // selected legend buttons
  private chartAvailabilityState: ChartAvailabilityOrPricesState;

  constructor(
    public sharedService: SharedService,
    public chartService: ChartService,
    public chartsStateService: ChartsStateService,
    public zone: NgZone,
    public cdRef: ChangeDetectorRef,
    private _chartAvailabilityService: ChartAvailabilityService,
    public elementRef?: ElementRef,
    public _appService?: AppService
  ) {
    super(chartService, sharedService, chartsStateService, cdRef, zone);
  }

  ngOnInit() {
    this.chartAvailabilityState = this.getChartState() as ChartAvailabilityOrPricesState;
    this.setInitialKinds();
    this.subscribeThemeChange();

    // Global chart settings
    Chart.defaults.global.defaultFontFamily = 'Overpass';
    Chart.defaults.global.defaultFontSize = 10;
    this.country$ = this.sharedService.activeCountry$;
       // obserwuje obecnie wybrany kraj
       this.currentCountry$ = this.country$.pipe(
        takeUntil(this._guard$),
        combineLatest(this.currentChartCountry$),
        map(([countryFromService, selectedCountry]) => {
          const currentCountry = selectedCountry ? selectedCountry : countryFromService;
          if (!!this.queryParams && !(!!this.queryParams.iso)) {
            this.queryParams.iso = currentCountry.iso;
          }
          this.selectedCountry = currentCountry;
          return currentCountry;
        }),
        shareReplay()
      );

      this.countries$ = this.sharedService.selectedCountries$
      .pipe(
        debounceTime(200),
        takeUntil(this._guard$),
        map((countries: Country[]) => {
          this.countries = countries;
          const searchedCountry = find(countries, country => country === this.selectedCountry);

          // Jezeli aktualnie zaznaczony kraj zostanie usuniety z filtrow globalnych pobiera pierwszy kraj z listy
          if (!!this.selectedCountry && countries.length > 1 && !(!!searchedCountry) && this.selectedCountry !== countries[0]) {
            this.changeDataByCountry(countries[0]);
            this.currentChartCountry$.next(countries[0]);
          }
          if (countries.length === 1 && !!this.selectedCountry && this.selectedCountry !== countries[0]) {
            this.changeDataByCountry(countries[0]);
            this.currentChartCountry$.next(countries[0]);
          }
          return countries;
        }),
    );

    this.countriesSub = this.sharedService.selectedCountries$
      .pipe(
        takeUntil(this._guard$)
      )
      .subscribe(countries => {
      this.countries = countries;
    });

    // przypisuje wybrane modele do zmiennej
    this.sharedService.currentModels$.pipe(
        takeUntil(this._guard$)
      ).subscribe((currentModels: string[]) => {
      this.currentModels = currentModels;
    });

    fromEvent(window, 'resize').pipe(
      debounceTime(300),
      takeUntil(this._guard$)
    ).subscribe(event => {
      if (this.fullscreenActive) {
        this.sharedService.setFullscreenChart(this.chartTitle);
      }
    });
  }

  ngAfterViewInit() {
    this.loaderSub = this.sharedService.loadingAvailability$
      .pipe(
        takeUntil(this._guard$),
        debounceTime(200)
      ).subscribe((loadingState: boolean) => {
        this.toggleChartVisibility(loadingState);
        this.chartSetup(loadingState, '');
    });
  }

  ngOnChanges() {
    if (!!this.queryParams && !!this.queryParams.iso) {
      this.ongoingOutages();
    }
    this.chartAvailabilityState = this.getChartState() as ChartAvailabilityOrPricesState;
  }

  /**
   * Sprawdzenie stanu początkowego zaznaczonych rodzajow
   *
   * @returns void
   */
  setInitialKinds(): void {
    // sprawdzenie czy state posiada aktualne rodzaje
    if (!!this.chartAvailabilityState && !!this.chartAvailabilityState.kind) {
      const tempArr = JSON.parse(this.chartAvailabilityState.kind);
      tempArr.forEach((kind: string) => {
        const tempBtn = new BtnLabel(kind);
        this.selectedBtns.push(tempBtn);
      });
    }
  }

  /**
   * Gets real time data based on checkboxes selection
   *
   * @param  {number} offset
   * @returns void
   */
  getRealTimeDataByOffset(offset: number): void {
    this.defaultCheckboxState = false;
    this.d1CheckboxState = offset !== 1 ? false : true;
    this.d2CheckboxState = offset !== 7 ? false : true;
    this.runDatesSelect.value = (offset === 1 || offset === 2 || offset === 7) ? 'As of' : offset;
    this.currentRunDateOffset = offset;
    this.saveCurrentLocalState();
    const params = this.queryParams;
    params.offset = offset;

    this.sharedService.getAvailabilityKinds().pipe(
      switchMap(kinds => {
        const requests = [];
        requests.push(this._chartAvailabilityService.getDataByRunDate(params, kinds));
        return forkJoin(requests);
      }))
      .subscribe((resData: Array<any>) => {
        this.changeLoaderState(true);
        const newDataSets: DataSets[] = [];
          resData.forEach(datasets => {
            datasets.forEach(dataset => {
              const label = dataset.name.replace('Fossil ', '');
              const canvas = this.chart.nativeElement;
              const ctx = canvas.getContext('2d');

              const color = this.checkDatasetColor(label, ctx);
              const newDataSet: DataSets = new DataSets(label, dataset.series, '', dataset.kind, dataset.runDate, offset, color);
              newDataSet.backgroundColor = color;
              newDataSets.push(newDataSet);
            });
          });

          const data = [...newDataSets];
          data.sort(dataset => {
            if (dataset.data.length) {
              const maximum = Math.max(...dataset.data.map(datasetData => datasetData.y));
              return maximum;
            }
          });
          data.forEach((dataset, index) => {
              dataset.fill = index === 0 ? 'origin' : (index - 1).toString();
          });

          this.myChart.config.data.datasets = [];
          this.myChart.config.data.datasets = data.slice();
          this.backupData = cloneDeep(this.myChart.config.data.datasets);
          this.myChart.update();

          this.filtersByDays(this.currentOffset);
          this.hideDataSetByLabel();

          this.cdRef.detectChanges();
        },
      (error) => {
        this.sharedService.checkIfUsersIsAlreadyLoggedIn(error);
      },
      () => {
        setTimeout(() => {
          this.changeLoaderState(false);
        }, this.chartUpdateTime)
      }
    );
  }

  /**
   * Hides datasets based on selected legend btns
   *
   * @returns void
   */
  hideDataSetByLabel(): void {
    const searchedDatasets: any = [];
    this.selectedBtns.forEach((btn: BtnLabel) => {
      this.myChart.config.data.datasets.forEach((dataset, index) => {
       if (dataset.label.toLowerCase() === btn.name.toLowerCase()) {
          searchedDatasets.push(index);
        }
      });
    });
    searchedDatasets.forEach(index => {
      this.myChart.config.data.datasets[index].hidden = true;
     });
    this.hideDataSetsWithoutData();
    this.updateChart();
    this.cdRef.detectChanges();
  }


  hideDataSetsWithoutData() {
    this.myChart.config.data.datasets.forEach((dataset, index) => {
      if (dataset.data.length === 0) {
        this.myChart.config.data.datasets[index].hidden = true;
      }
    })
  }

  /**
   * Resets data
   *
   * @returns void
   */
  resetData(): void {
    this.changeLoaderState(true);
    if (this.defaultCheckboxState) {
      return;
    } else {
      this.defaultCheckboxState = true;
      this.runDatesSelect.value = 'As of';
      this.currentRunDateOffset = 0;
      this.myChart.destroy();
      this.saveCurrentLocalState();
      this.changeDataByCountry(this.selectedCountry);
    }
  }

  /**
   * Add new dataset to chart and
   *
   * @param  {DataSets} newDataset
   * @returns void
   */
  addNewDataSet(newDataset: DataSets): void {
    this.myChart.config.data.datasets.push({...newDataset});
    this.backupData.push({...newDataset});
  }


   /**
   * Odświeża dane wykresu
   *
   * @returns void
   */
  updateChart(): void {
   setTimeout(() => {
      if (!!this.myChart && !!this.myChart.config.data.datasets) {
        this.myChart.update();
        this.cdRef.detectChanges();
        this.changeLoaderState(false);
      }
    }, this.chartUpdateTime);
  }

  /**
   * OngoingOutages table slice date and hour
   *
   *
   * @returns void
   */
  ongoingOutages(): void {
    this.newTableData = [];
    const table = this.tableData;
    for (const row of table) {
      const startDate = row['start_date'].split(' ');
      let startDateAddress = startDate[0];
      startDateAddress = startDateAddress.replace(/\-/g, '/');
      const startDateHour = startDate[1];

      const endDate = row['end_date'].split(' ');
      let endDateAddress = endDate[0];
      endDateAddress = endDateAddress.replace(/\-/g, '/');
      const endDateHour = endDate[1];

      const runDate = row['run_date'].split(' ');
      let runDateAddress = runDate[0];
      runDateAddress = runDateAddress.replace(/\-/g, '/');
      const runDateHour = runDate[1];

      this.newTableData.push({
        installed: row['installed'],
        remaining: row['remaining'],
        start_date: startDateAddress,
        start_hour: startDateHour,
        end_date: endDateAddress,
        end_hour: endDateHour,
        run_date: runDateAddress,
        run_hour: runDateHour,
        type_outage_planned: row['type_outage_planned'],
        unit_name: row['unit_name']
      });
    }
  }

  /**
   * Chart setup - draw, update, resize
   *
   *
   * @param  {boolean} loadingState
   * @returns void
   */
  chartSetup(loadingState: boolean, initialSeries: string, labels?: string[]): void {
    this.changeLoaderState(true);
    if (!loadingState && this.chartOptions && this.chartOptions.data.datasets.length) {
      const newLabels = [];
      this.labels = this.chartOptions.data.datasets.map(set => {
        if (set.data.length > 0) {
          if (set.label.includes('Fossil')) {
            set.label = set.label.replace('Fossil ', '');
          }
          const labelValue = set.label.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
          newLabels.push({
            value: labelValue, name: set.label
          });
          return set.label.trim();
        }
      });
      this.labels = newLabels;

      this.drawChart();
      this.backupData = cloneDeep(this.myChart.config.data.datasets);
      this.changeChartLegendColorByTheme();
      this.checkSelectedKindAndLabels();

      this.toggleChartVisibility(loadingState);
    } else if (loadingState && this.chartOptions && !this.chartOptions.data.datasets.length) {
      // if models oraz countries filters are empty arrays
      if (this.myChart) {
        this.myChart.update(); // destroys chart data
        this.toggleChartVisibility(loadingState);
        this.resizeChart();
      }
    }
  }

  /**
   * Sprawdza stan wykresu i na jego podstawie wywołuje odpowiednie metody
   *
   * @returns void
   */
  checkSelectedKindAndLabels(): void {
    this.changeLoaderState(true);
    // sprawdzenie czy state posiada kraj
    if (!!this.chartAvailabilityState && !!this.chartAvailabilityState.country) {
      if (this.countries.length > 0) {
        const tempCountry = this.countries.find(country => country.iso === this.chartAvailabilityState.country);
        const initialCountry = !tempCountry && !!this.selectedCountry ? this.selectedCountry : tempCountry;
        this.setChartSelectedBtn(initialCountry);
      }
    }

    // sprawdzenia czy state posiada aktualny offset danych - D-2/D-7/D-X
    if (!!this.chartAvailabilityState && !!this.chartAvailabilityState.selectedOffset) {
      this.currentRunDateOffset = this.chartAvailabilityState.selectedOffset;
    }
    if (!!this.chartAvailabilityState && !!this.chartAvailabilityState.currentDay) {
      this.currentOffset = parseInt(this.chartAvailabilityState.currentDay, 10);
    }

    if (!!this.currentRunDateOffset && this.currentRunDateOffset !== 0) {
      this.checkInitialRadioBtns();
    } else {
      this.filtersByDays(this.currentOffset);
      this.hideDataSetByLabel();
    }
  }

  /**
   * Pobiera dane na podstawie wartości selectow
   *
   * @returns void
   */
  checkInitialRadioBtns(): void {
    this.getRealTimeDataByOffset(this.currentRunDateOffset);
  }

  /**
   * Filtruje datasety na podstawie zaznaczonych rodzajow
   *
   * @returns void
   */
  filterDataBasedOnSelectedKinds(): void {
    this.selectedBtns.forEach(btn => {
      this.chartOptions.data.datasets.forEach(dataset => {
        if (dataset.label === btn.name) {
          dataset.hidden = true;
        }
      });
    });
    this.saveCurrentLocalState();
  }

  /**
   * Draws chart, modifies chart options
   *
   * @returns void
   */
  drawChart(): void {
    // Global chart settings
    Chart.defaults.global.defaultFontFamily = ' Overpass';
    Chart.defaults.global.defaultFontSize = 10;

    const canvas = this.chart.nativeElement;
    const ctx = canvas.getContext('2d');

    // draw chart outside angular zone
    this.zone.runOutsideAngular(() => {
      if (this.myChart) {
        this.myChart.destroy();
      }
      if (this.chartOptions) {
        this.modifyOptions(ctx);
        this.myChart = new Chart(ctx, this.chartOptions);
        this.resizeChart();
      }
    });
  }

  /**
   * Modifies data
   *
   * @param  {} ctx
   * @param  {DataSets[]} dataSets
   * @returns void
   */
  modifyOptions(ctx): void {
    // sortowanie datasetow po najwiekszej wartosci punktu pomiarowego, zmiana kolorow, wypelnienia
    this.sortDataSetsByValue();
    this.chartOptions.data.datasets.forEach((dataset, index) => {
      const gradientColor = this.checkDatasetColor(dataset.label, ctx);
        dataset.fill = index === 0 ? 'origin' : (index - 1).toString();
        dataset.borderWidth = 1;
        dataset.borderColor = gradientColor;
        dataset.backgroundColor = gradientColor;
      });

    // formatowanie danych osi X do formatu YYYY-MM-DD
    // this.chartOptions.options.elements.line.tension = 1; // zakrzywia linie datasetow
    // this.chartOptions.options.elements.point.radius = 0.8;
    // this.chartOptions.options['maintainAspectRatio'] = false

     this.chartOptions.options.scales.xAxes.forEach(data => {
       data.time.unit = 'day';
       data.time.displayFormats.day = 'YYYY-MM-DD';
       data.time.stepSize = 1;
       data.stacked = true;
      });
    // this.chartOptions.options.legend = true;
     this.chartOptions.options.scales.yAxes.forEach(data => {
       data.stacked = true;
     });
  }

  /**
   * Sorts datasets by max value
   *
   * @returns void
   */
  sortDataSetsByValue(): void {
    const dataSets = cloneDeep(this.chartOptions.data.datasets);
    const sortedDataSets = sortBy(dataSets, dataset => {
      if (dataset.data.length) {
        const maximum = Math.max(...dataset.data.map(data => data.y));
        return maximum;
      }
    });

    this.chartOptions.data.datasets = cloneDeep(sortedDataSets);
  }
  /**
   * Changes data series color based on label type
   *
   * @param  {any} ctx
   * @param  {string} label
   * @returns string
   */
  checkDatasetColor(label: string, ctx?: any): string {
      const fossilBrown = 'rgba(141, 94, 77, 0.8)';
      const fossilGas = 'rgba(221, 83, 51, 0.8)';
      const fossilOil = 'rgba(112, 112, 112, 0.8)';
      const fossilHard = 'rgba(61, 57, 51, 0.8)';
      const nuclear = 'rgba(254, 210, 91, 0.8)';
      label = label.trim();

      switch (label) {
        case 'Brown coal/Lignite': {
        return fossilBrown;
      }
      case 'Gas': {
        return fossilGas;
      }
      case 'Oil': {
        return fossilOil;
      }
      case 'Hard coal': {
        // const gradientColor = ctx.createLinearGradient(315.52, 0, 0, 0);
        // gradientColor.addColorStop(0, fossilHard.start);
        // gradientColor.addColorStop(1, fossilHard.stop);
        return fossilHard;
      }
      case 'Nuclear': {
        return nuclear;
      }
      default:
      break;
    }
  }

  /**
   * Hide table
   *
   * @returns void
   */
  hideTable(): void {
    this.showChart = true;
    this.resizeChart();
  }

  /**
   * Show table
   *
   * @returns void
   */
  showTable(): void {
    this.showChart = false;
  }

  /**
   * Filters by daysCodegenComponentFactoryResolver
   *
   * @returns void
   */
  filtersByDays(day: number): void {
    this.currentOffset = day;
    this.loadDays(day);
    this.updateChart();
    this.cdRef.detectChanges();
    this.saveCurrentLocalState();
  }

   /**
   * Toggles data based on clicked legend btn
   *
   * @param  {BtnLabel} key
   * @returns void
   */
  toggleAvailabilityData(btn: BtnLabel): void {
    this.setActiveBtns(btn);
    this.changeLoaderState(true);
    const config = this.myChart.config as ChartCustomOption;
    config.toggleAvaibilityData(btn.name);
    this.updateChart();
    this.cdRef.detectChanges();

    setTimeout(() => {
      this.changeLoaderState(false);
    },  1000);
    this.saveCurrentLocalState();
  }

  /**
   * Zapisuje aktualny stan wybrane
   *
   * @returns void
   */
  saveCurrentLocalState(): void {
    const iso = !!this.selectedCountry ? this.selectedCountry.iso : '';
    const currentDayOffset = !!this.currentOffset ? this.currentOffset.toString() : null;
    const currentDataOffset = !!this.currentRunDateOffset ? this.currentRunDateOffset : null;
    const kinds = !!this.selectedBtns ? this.selectedBtns.map((selectedBtn: BtnLabel) => {
      return selectedBtn.name;
    }) : '';
    const kindsStr = JSON.stringify(kinds);

    const state = new ChartAvailabilityOrPricesState(iso, currentDayOffset, currentDataOffset, kindsStr);
    this.chartAvailabilityState = state;
    this.chartsStateService.saveChartState(this.chartTitle.toLowerCase(), state);
  }

  /**
   * Checks if btn was already selected
   *
   * @param  {BtnLabel} btn
   * @returns void
   */
  setActiveBtns(btn: BtnLabel): void {
    const exists = find(this.selectedBtns, selectedBtn => selectedBtn.name.toLowerCase() === btn.name.toLowerCase());
    if (!exists) {
      this.selectedBtns.push(btn);
    } else {
      remove(this.selectedBtns, selectedBtn => selectedBtn.name.toLowerCase() === btn.name.toLowerCase());
    }
  }

    /**
   * Resize and redraw chart
   *
   * @returns void
   */
  resizeChart(): void {
    if (this.myChart) {
      setTimeout(() => {
        this.myChart.resize();
        this.changeLoaderState(false); // nie komentować
      }, 300);
    }
  }
}
