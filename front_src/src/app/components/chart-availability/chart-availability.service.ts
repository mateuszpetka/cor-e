
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import {ChartQueryParams, Params} from '../../classes/classes';
import { Observable } from 'rxjs';
import { CustomHttpService } from '../../shared/customHttp.service';
import { SharedService } from '../../shared/shared.service';
import { ChartData } from '../../classes/chart';

@Injectable()
export class ChartAvailabilityService {

  constructor(private _customnHttp: CustomHttpService, private _sharedService: SharedService) { }

  /**
   * Gets data from db based on run_date
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  getDataByRunDate(queryParams: ChartQueryParams, kinds: Array<string>): Observable<any> {
    const params = new Params(queryParams.iso, queryParams.model, queryParams.offset, queryParams.runDate);
    return this._customnHttp.get(`ChartData/availabilityByRunDate?` + this._sharedService.toQueryString(params) + `&kind=${kinds}`)
  }

  // /**
  //  * Gets avalability chart data
  //  *
  //  * @param  {string} kind
  //  * @param  {string} country
  //  * @param  {number=7} days
  //  * @returns Observable
  //  */
  // public getRealTimeData(kind: string, queryParams: ChartQueryParams): Observable<ChartData> {
  //   return this._customnHttp.get(`ChartData/availability?country=${queryParams.iso}&kind=${kind}&dateFrom=${queryParams.dateFrom}&dateTo=${queryParams.dateTo}&model=${queryParams.model}`);
  // }


  /**
   * Gets data from db based on run_date
   *
   * @param  {ChartQueryParams} queryParams
   * @returns Observable
   */
  getRunDates(queryParams: ChartQueryParams): Observable<any> {
    return this._customnHttp.get(`ChartData/availabilityRunDates?country=${queryParams.iso}`).pipe(map((runDates: string[]) => {
       const indexesToReplace: number[] = [0, 1, 7];
       indexesToReplace.forEach(index => {
        runDates[index] = runDates[index].replace(runDates[index],`d${index}`);
       });
       return runDates;
    }));
  }
}
