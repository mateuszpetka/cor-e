import { FrontendSamplePage } from './app.po';

describe('frontend-sample App', () => {
  let page: FrontendSamplePage;

  beforeEach(() => {
    page = new FrontendSamplePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
