## Zestaw startowy dla API i frontendu.#

### Przygotowanie do pracy nad nowym projektem ##
Tymczasowa wersja opisu znajduje się pod adresem (wymagana rejestracja w serwisie): http://phabricator.bling.sh/w/bling/cms/tworzenie_nowego_projektu 

### Dostępne polecenia dla composera ###

Każdą z poniższych komend wykonuje się w Bashu w przedstawiony sposób: `composer bling admin-update 7.0.0`. Wykonanie tego polecenia 
spowoduje zainstalowanie panelu admina w wersji 7.0.0. Poniżej znajduje się lista wszystkich dostępnych komend do wykorzystania w projekcie.

* **admin-update** - pobiera najnowszy build panela admina z repozytorium i nadpisuje ten obecnie istniejący w katalogu /admin. Możliwe jest podanie konkretnej wersji panelu do pobrania, podając po spacji wersję: `admin-update 1.1.2`.
* **db-update** - aktualizuje strukturę istniejącej już bazy danych projektu, strukturą z API. Przydatne przy nowych featurach.
* **generate** - generuje pliki frontu i api, nadpisując te, które są ustawione do nadpisywania.
* **generate-content** - Uzupełnienie wybranej encji wybraną liczbą rekordów za losową treścią. Przykład wywołania: `generate-content Author 20`.
* **generate-deploy** - przygotowuje paczkę z projektem do wrzucenia na serwer docelowy. Warto uruchamiać przed każdą zmianą, którą trzeba wdrożyć na produkcji.
* **update** - komenda wykonująca kilka operacji jednocześnie: aktualizacja wersji API, struktury bazy danych, panelu admina i wygenerowanie plików na nowo.
