<?php
const BASE_DIR = __DIR__ . '/../';

use BlingApi\EntryPoint;

require BASE_DIR . '/vendor/autoload.php';

$api = new EntryPoint();
$api->setErrorHandler();

if (PHP_SAPI === 'cli') {
    $api->runFromCli($argv);
} else {
    $api->run();
}
