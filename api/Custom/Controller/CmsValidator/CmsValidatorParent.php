<?php

namespace BlingCustom\Controller\CmsValidator;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\CmsValidator\CmsValidator;

class CmsValidatorParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new CmsValidator();
    }

}
