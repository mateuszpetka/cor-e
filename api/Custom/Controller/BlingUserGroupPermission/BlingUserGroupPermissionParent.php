<?php

namespace BlingCustom\Controller\BlingUserGroupPermission;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\BlingUserGroupPermission\BlingUserGroupPermission;

class BlingUserGroupPermissionParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new BlingUserGroupPermission();
    }

}
