<?php

namespace BlingCustom\Controller\BlingMetaSnippet;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Exception\InvalidArgumentException;
use BlingApi\Core\Request;
use BlingCustom\Model\BlingMetaSnippet\BlingMetaSnippet;
use Db\Manager as Q;

class BlingMetaSnippetParent extends EntityController
{

    /**
     * Lista dostępnych lokalizacji w kodzie HTML.
     *
     * @var array
     */
    private static $locations = [
        'BODY-AFTER-OPEN',
        'BODY-BEFORE-CLOSE',
    ];

    /**
     * BlingMetaSnippetParent constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        $this->module = [self::MODULE_BACKEND, self::MODULE_FRONTEND];
        $this->request = $request;
        $this->model = new BlingMetaSnippet();
        $this->actionPermissions = ['canRead' => ['positions']];
    }

    /**
     * Zwrócenie listy dostępnych lokalizacji do wyboru
     */
    public function positionsAction(): void
    {
        $this->setResponseData(self::$locations);
    }

    /**
     * Wstawienie kodu do bazy i plików.
     *
     * @throws \BlingApi\Core\Exception\InvalidArgumentException
     */
    public function insertAction(): void
    {
        if (!$this->request->get('name') || !$this->request->get('value') || !$this->request->get('location')) {
            throw new InvalidArgumentException('Missing name, value or position parameter');
        }
        if (!in_array($this->request->get('location'), self::$locations)) {
            throw new InvalidArgumentException('Given position is not supported');
        }

        parent::insertAction();

        $this->updateTagsInFiles();
    }

    /**
     * Aktualizacja kodu w bazie i w plikach.
     *
     * @param int $id
     * @throws \BlingApi\Core\Exception\InvalidArgumentException
     */
    public function updateAction(int $id): void
    {
        if ($this->request->get('location') && !in_array($this->request->get('location'), self::$locations)) {
            throw new InvalidArgumentException('Given position is not supported');
        }
        parent::updateAction($id);

        $this->updateTagsInFiles();
    }

    /**
     * Usunięcie kodu z bazy i plików.
     *
     * @param int $id
     */
    public function deleteAction(int $id): void
    {
        parent::deleteAction($id);

        $this->updateTagsInFiles();
    }

    /**
     * Dodanie znacznika do plików index.html.
     *
     * @return bool
     */
    private function updateTagsInFiles(): bool
    {
        $snippets = $this->getAllSnippets();

        foreach ($this->getFilesToUpdate() as $file) {
            $newFileContent = $file['content'];

            $regex = '/<!-- REPLACE-TAG-HEAD-AFTER-OPEN-O -->([\s\S]*?)<!-- REPLACE-TAG-HEAD-AFTER-OPEN-C -->/mi';
            $toReplace = "<!-- REPLACE-TAG-HEAD-AFTER-OPEN-O -->\n  <!-- REPLACE-TAG-HEAD-AFTER-OPEN-C -->";
            $newFileContent = preg_replace($regex, $toReplace, $newFileContent);

            $regex = '/<!-- REPLACE-TAG-HEAD-BEFORE-CLOSE-O -->([\s\S]*?)<!-- REPLACE-TAG-HEAD-BEFORE-CLOSE-C -->/mi';
            $toReplace = "<!-- REPLACE-TAG-HEAD-BEFORE-CLOSE-O -->\n  <!-- REPLACE-TAG-HEAD-BEFORE-CLOSE-C -->";
            $newFileContent = preg_replace($regex, $toReplace, $newFileContent);

            $regex = '/<!-- REPLACE-SNIPPET-BODY-AFTER-OPEN-O -->([\s\S]*?)<!-- REPLACE-SNIPPET-BODY-AFTER-OPEN-C -->/mi';
            $toReplace = "<!-- REPLACE-SNIPPET-BODY-AFTER-OPEN-O -->\n  <!-- REPLACE-SNIPPET-BODY-AFTER-OPEN-C -->";
            $newFileContent = preg_replace($regex, $toReplace, $newFileContent);

            $regex = '/<!-- REPLACE-SNIPPET-BODY-BEFORE-CLOSE-O -->([\s\S]*?)<!-- REPLACE-SNIPPET-BODY-BEFORE-CLOSE-C -->/mi';
            $toReplace = "<!-- REPLACE-SNIPPET-BODY-BEFORE-CLOSE-O -->\n  <!-- REPLACE-SNIPPET-BODY-BEFORE-CLOSE-C -->";
            $newFileContent = preg_replace($regex, $toReplace, $newFileContent);

            foreach ($snippets as $location => $insertSnippets) {
                if (empty($insertSnippets)) {
                    continue;
                }

                $regex = '/<!-- REPLACE-SNIPPET-' . $location . '-O -->([\s\S]*?)<!-- REPLACE-SNIPPET-' . $location . '-C -->/mi';
                $replace = sprintf(
                    "<!-- REPLACE-SNIPPET-%s-O -->\n  %s\n  <!-- REPLACE-SNIPPET-%s-C -->", $location, implode("\n  ", $insertSnippets), $location
                );

                $newFileContent = preg_replace($regex, $replace, $newFileContent);
            }

            $this->saveFile($file['path'], $newFileContent);
        }

        return true;
    }

    /**
     * Zapisanie zawartości do pliku.
     *
     * @param string $path
     * @param string $content
     * @return bool
     */
    private function saveFile(string $path, string $content): bool
    {
        return (bool) file_put_contents($path, $content);
    }

    /**
     * Pobranie z bazy wszystkich kodów, z podziałem na miejsce wstawienia.
     *
     * @return array
     */
    private function getAllSnippets(): array
    {
        $snippets = [];
        $snippetResults = Q::select(BlingMetaSnippet::_all)->getAssoc();

        foreach (self::$locations as $location) {
            $snippets[$location] = [];
        }

        foreach ($snippetResults as $snippetResult) {
            $snippets[$snippetResult['location']][] = $snippetResult['value'];
        }

        return $snippets;
    }

    /**
     * Pobranie plików index.html do edycji.
     *
     * @return array
     */
    private function getFilesToUpdate(): array
    {
        $files = [];

        if (file_exists(BASE_DIR . '/front/index.html')) {
            $files[] = [
                'path'    => BASE_DIR . '/front/index.html',
                'content' => file_get_contents(BASE_DIR . '/front/index.html'),
            ];
        }

        if (file_exists(BASE_DIR . '/front_src/src/index.html')) {
            $files[] = [
                'path'    => BASE_DIR . '/front_src/src/index.html',
                'content' => file_get_contents(BASE_DIR . '/front_src/src/index.html'),
            ];
        }

        return $files;
    }
}