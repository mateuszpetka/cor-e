<?php

namespace BlingCustom\Controller\BlingUser;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Exception\AccessDeniedException;
use BlingApi\Core\Request;
use BlingApi\Core\User\User;
use BlingCustom\Model\BlingUser\BlingUser;
use Symfony\Component\HttpFoundation\Response;

class BlingUserParent extends EntityController
{

    /**
     * BlingUserParent constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        $this->module = [self::MODULE_BACKEND, self::MODULE_FRONTEND];
        $this->request = $request;
        $this->model = new BlingUser();
    }

    /**
     * Wstawienie nowego obiektu do bazy danych.
     */
    public function insertAction(): void
    {
        $user = new User($this->model);

        $this->setResponseData(['id' => $user->create($this->request->getData())]);
        $this->setResponseStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Zmiana danych wskazanego użytkownika.
     *
     * @param int $id
     * @throws \BlingApi\Core\Exception\AccessDeniedException
     */
    public function updateAction(int $id): void
    {
        if ($this->request->user->getTokenData('groupId') === 2) {
            $editedUser = $this->model->entityGetOne($id, $this->request->controllerParameters);

            if (isset($editedUser['groupId']) && $editedUser['groupId'] === 1) {
                throw new AccessDeniedException("You're not allowed to modify admin group users.");
            }
        }

        $userData = $this->request->getData();
        $userData['id'] = $id;

        $user = new User($this->model);
        $user->update($userData);
    }
}
