<?php

namespace BlingCustom\Controller\BlingUser;

use BlingCustom\Controller\AuthController;

class BlingUser extends BlingUserParent
{

    public function __construct($request)
    {
      parent::__construct($request);
    }

    public function insertAction(): void
    {

      $auth = new AuthController($this->request);
      $auth->registerAction();
    }
}
