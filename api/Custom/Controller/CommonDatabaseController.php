<?php

namespace BlingCustom\Controller;

use BlingApi\Core\Controller;
use BlingApi\Core\Exception\InvalidArgumentException;
use BlingApi\Core\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommonDatabaseController extends Controller
{

    /**
     * Ścieżka do katalogu, gdzie przechowywane są pliki.
     *
     * @var string
     */
    private $storeDirectoryPath = BASE_DIR . 'tmp';

    /**
     * CommonDatabaseController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->module = [self::MODULE_BACKEND];
        $this->request = $request;
        $this->allowMethodsWithoutToken = ['insertAction', 'getItemAction'];
    }

    /**
     * Pobranie danych z pliku tymczasowego.
     *
     * @param $id
     */
    public function getItemAction($id): void
    {
        $filePath = $this->storeDirectoryPath . '/' . $id;

        if (!file_exists($filePath)) {
            throw new NotFoundHttpException('Storage file not found.');
        }

        $this->setResponseData(json_decode(file_get_contents($filePath), true));
    }

    /**
     * Zapisanie danych do tymczasowego pliku.
     *
     * @throws InvalidArgumentException
     */
    public function insertAction(): void
    {
        if (!$this->request->get('entity') || !$this->request->get('method') || !$this->request->get('data')) {
            throw new InvalidArgumentException('Entity, method and data fields are required.');
        }

        $fileName = hexdec(uniqid());
        $filePath = $this->storeDirectoryPath . '/' . $fileName;
        file_put_contents($filePath, json_encode($this->request->getData()));
        chmod($filePath, 0777);

        foreach (glob(BASE_DIR . 'tmp/*') as $file) {
            if (filemtime($file) < time() - 60) {
                unlink($file);
            }
        }

        $this->setResponseData(['storePath' => $fileName]);
        $this->setResponseStatus(Response::HTTP_CREATED);
    }
}