<?php

namespace BlingCustom\Controller\CmsEntityFieldGroupPermission;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\CmsEntityFieldGroupPermission\CmsEntityFieldGroupPermission;

class CmsEntityFieldGroupPermissionParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new CmsEntityFieldGroupPermission();
    }

}
