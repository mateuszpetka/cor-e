<?php

namespace BlingCustom\Controller\BlingCoreActionPermission;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\BlingCoreActionPermission\BlingCoreActionPermission;

class BlingCoreActionPermissionParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new BlingCoreActionPermission();
    }

}
