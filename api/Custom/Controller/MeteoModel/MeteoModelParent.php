<?php

namespace BlingCustom\Controller\MeteoModel;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\MeteoModel\MeteoModel;

class MeteoModelParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new MeteoModel();
    }

}
