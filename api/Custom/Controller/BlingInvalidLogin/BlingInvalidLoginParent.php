<?php

namespace BlingCustom\Controller\BlingInvalidLogin;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\BlingInvalidLogin\BlingInvalidLogin;

class BlingInvalidLoginParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new BlingInvalidLogin();
    }

}
