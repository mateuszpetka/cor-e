<?php

namespace BlingCustom\Controller\BlingUserGroup;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Exception\AccessDeniedException;
use BlingApi\Core\Exception\EmptyResultsException;
use BlingApi\Core\Request;
use BlingCustom\Model\BlingUserGroup\BlingUserGroup;

class BlingUserGroupParent extends EntityController
{

    /**
     * BlingUserGroupParent constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        $this->module = [self::MODULE_BACKEND];
        $this->request = $request;
        $this->model = new BlingUserGroup();
    }

    /**
     * Aktualizacja grupy o podanym ID w bazie.
     *
     * @param int $id
     * @throws \BlingApi\Core\Exception\AccessDeniedException
     */
    public function updateAction(int $id): void
    {
        if ($this->request->user->getTokenData('groupId') === 2) {
            if (in_array($id, [1, 2])) {
                throw new AccessDeniedException("You're not allowed to modify Admin and Super Admin groups.");
            }
        }

        $this->model->entityUpdate($id, $this->getEntityData());
    }

    /**
     * Usunięcie grupy o podanym ID z bazy danych.
     *
     * @param int $id
     * @throws \BlingApi\Core\Exception\EmptyResultsException
     * @throws \BlingApi\Core\Exception\AccessDeniedException
     */
    public function deleteAction(int $id): void
    {
        if ($this->request->user->getTokenData('groupId') === 2) {
            if (in_array($id, [1, 2])) {
                throw new AccessDeniedException("You're not allowed to remove Admin and Super Admin groups.");
            }
        }

        if ($this->model->entityDelete($id) === false) {
            throw new EmptyResultsException('Entity object not found.');
        }
    }

}
