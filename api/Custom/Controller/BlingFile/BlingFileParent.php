<?php

namespace BlingCustom\Controller\BlingFile;

use BlingApi\Core\Controller;
use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Config;
use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Exception\FileNotFoundException;
use BlingApi\Core\Exception\InvalidArgumentException;
use BlingApi\Core\Helper\EntityHelper;
use BlingApi\Core\Helper\FileHelper;
use BlingApi\Core\Request;
use BlingCustom\Model\BlingFile\BlingFile;
use BlingCustom\Model\BlingRecordFile\BlingRecordFile;
use BlingCustom\Model\CmsEntity\CmsEntity;
use BlingCustom\Model\CmsEntityField\CmsEntityField;
use Symfony\Component\HttpFoundation\Response;
use Db\Manager as Q;

class BlingFileParent extends EntityController
{

    /**
     * BlingFileParent constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        $this->module = [self::MODULE_BACKEND, self::MODULE_FRONTEND];
        $this->request = $request;
        $this->model = new BlingFile();
        $this->actionPermissions = ['canRead' => ['listUnused', 'uploadLimit']];
    }

    /**
     * Odebranie szczegółów pliku, przypisanie ID, zapisanie w bazie i zwrócenie ID dla wgrywanego pliku.
     *
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     * @throws \InvalidArgumentException
     * @throws \BlingApi\Core\Exception\InvalidArgumentException
     */
    public function insertAction(): void
    {
        /** @var  \Symfony\Component\HttpFoundation\File\UploadedFile $uploadedFile */
        $uploadedFile = $this->request->files->get('file');
        $fileStatus = $this->determineFileStatus();
        $data = $this->request->getData();

        if (!isset($uploadedFile) && $data['isForeign']) {
            $this->addForeignFile($data);
            return;
        }

        if ($uploadedFile->getSize() > Config::get('UPLOAD_PUBLIC_MAX_FILE_SIZE_BYTES')) {
            throw new InvalidArgumentException(
                'File is too big to upload. Max file size is: ' . Config::get('UPLOAD_PUBLIC_MAX_FILE_SIZE_BYTES') . 'bytes.'
            );
        }

        $fileName = FileHelper::getNewFileName(FileHelper::getUploadDir(), $uploadedFile->getClientOriginalName());
        $additionalParams = $this->request->getData();

        $fileDetails = [
            'name'     => $fileName,
            'path'     => FileHelper::getUploadDir(false) . '/' . $fileName,
            'size'     => $uploadedFile->getSize(),
            'mime'     => $uploadedFile->getClientMimeType(),
            'width'    => $additionalParams['width'] ?? 0,
            'height'   => $additionalParams['height'] ?? 0,
            'duration' => $additionalParams['duration'] ?? 0,
            'status'   => $fileStatus,
        ];

        if ($uploadedFile->move(FileHelper::getUploadDir(), $fileName)) {
            $file = $this->model->entityInsert($fileDetails, true);

            if ($this->request->getModule() === Controller::MODULE_BACKEND || $fileStatus !== 0) {
                $fileDetails = $this->model->getOne($file['id']);
            } else {
                $fileDetails = [];
            }
        } else {
            $fileDetails = [];
        }

        $this->setResponseData($fileDetails);
        $this->setResponseStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Usunięcie pliku z serwera i informacji o nim w bazie danych.
     *
     * @param int $id
     * @throws FileNotFoundException
     */
    public function deleteAction(int $id): void
    {
        $file = $this->model->entityGetOne($id, $this->request->controllerParameters);

        if (empty($file)) {
            throw new FileNotFoundException($id);
        }

        if (file_exists(Config::get('UPLOAD_PUBLIC_DIR_PATH') . '/' . $file['path'])) {
            unlink(Config::get('UPLOAD_PUBLIC_DIR_PATH') . '/' . $file['path']);
        }

        $this->model->entityDelete($id);
    }

    /**
     * Usunięcie wielu plików na raz.
     *
     * @throws InvalidArgumentException
     */
    public function multiDeleteAction(): void
    {
        if (!$this->request->query->has('id') && !$this->request->query->has('where')) {
            throw new InvalidArgumentException('Missing "id[]" or "where" query parameter');
        }

        if ($this->request->query->has('id')) {
            $whereConditions = [];

            foreach ($this->request->query->get('id') as $elementId) {
                if ($this->model->getDriven() > EntityDriven::none && $this->request->user->getTokenData('groupId') > 1) {
                    $whereConditions = [
                        'id'      => $elementId,
                        'addedBy' => $this->request->user->getTokenData('id'),
                    ];
                }

                $this->model->entityDelete($elementId, $whereConditions);
            }
        }

        if ($this->request->query->has('where')) {
            $whereConditions = $this->request->query->get('where');
            $filePaths = Q::select(BlingFile::path)->where(Q::whereFromArray($whereConditions))->getColumn();

            if ($this->model->getDriven() > EntityDriven::none && $this->request->user->getTokenData('groupId') > 1) {
                $whereConditions['addedBy'] = $this->request->user->getTokenData('id');
            }

            $this->model->entityDelete(0, $whereConditions);

            foreach ($filePaths as $filePath) {
                if (file_exists(Config::get('UPLOAD_PUBLIC_DIR_PATH') . '/' . $filePath)) {
                    unlink(Config::get('UPLOAD_PUBLIC_DIR_PATH') . '/' . $filePath);
                }
            }
        }
    }

    /**
     * Pobranie listy ID nieużywanych plików.
     */
    public function listUnusedAction(): void
    {
        $usedFiles = [];

        $allFiles = $this->model->entityGet($this->request->controllerParameters);
        $allFiles = array_reduce($allFiles, function($result, $item){
            $result[$item['id']] = $item['path'];

            return $result;
        });

        $singleFileFieldIds = [
          14, // file
          15, // image
          24 // video
        ];

        $multiFileFieldIds = [
          16, // multifile
          17, // multiimage
          25, // multivideo
          26 // multimedia
        ];

        $fieldsWithFiles = Q::select(CmsEntityField::_all, Q::alias(CmsEntity::name, 'entityName'))
            ->join(CmsEntity::id, CmsEntityField::entityId)
            ->where(Q::in(CmsEntityField::fieldId, array_merge($singleFileFieldIds, $multiFileFieldIds)))
            ->getAssoc();

        foreach ($fieldsWithFiles as $field) {
            $fileField = EntityHelper::getModelField($field['entityName'], $field['name']);

            if (in_array($field['fieldId'], $singleFileFieldIds)) {
                $entityFiles = Q::select($fileField)->where(Q::notNull($fileField))->getColumn();

                foreach ($entityFiles as $entityFilePath) {
                    if (in_array($entityFilePath, $allFiles)) {
                        $fileId = array_search($entityFilePath, $allFiles);

                        if ($fileId > 0 && !in_array($fileId, $usedFiles)) {
                            $usedFiles[] = $fileId;
                        }
                    }
                }
            } else {
                $entityFiles = Q::select(BlingRecordFile::fileId)->where(Q::eq(BlingRecordFile::entity, $field['entityName']))->getColumn();

                foreach ($entityFiles as $entityFileId) {
                    if (array_key_exists($entityFileId, $allFiles)) {
                        $usedFiles[] = $entityFileId;
                    }
                }
            }
        }

        $this->setResponseData(array_values(array_diff(array_keys($allFiles), $usedFiles)));
    }

    /**
     * Pobranie maksymalnego rozmiaru pliku, który może zostać wgrany na serwer.
     */
    public function uploadLimitAction(): void
    {
        $uploadMaxSize = FileHelper::sizeInBytes(ini_get('upload_max_filesize'));
        $postMaxSize = FileHelper::sizeInBytes(ini_get('post_max_size'));
        $configMaxSize = Config::get('UPLOAD_PUBLIC_MAX_FILE_SIZE_BYTES');

        $this->setResponseData(['uploadLimit' => min($uploadMaxSize, $postMaxSize, $configMaxSize)]);
    }

    /**
     * Obsługa dodania pliku z zewnętrznego źródła
     *
     * @param array $foreignFile
     */
    private function addForeignFile(array $foreignFile): void {
        preg_match("/(\w*?)\s*?$/i", $foreignFile['path'], $matches);

        $fileDetails = [
            'name'     => $matches[0],
            'path'     => $foreignFile['path'],
            'size'     => 0,
            'mime'     => '',
            'width'    => 0,
            'height'   => 0,
            'duration' => 0,
            'isForeign'=> 1
        ];

        $file = $this->model->entityInsert($fileDetails, true);
        $insertedFile = $this->model->entityGetOne($file['id'], $this->request->controllerParameters);

        $this->setResponseData($insertedFile);
        $this->setResponseStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Określa status dodawanego pliku na podstawie ustawień w configu
     * lub przesłanego statusu
     *
     * @return int
     */
    private function determineFileStatus(): int {
      $settingName = 'UPLOAD_FILE_FRONT_DEFAULT_STATUS';

      if ($this->request->getModule() === Controller::MODULE_BACKEND) {
        $settingName = 'UPLOAD_FILE_CMS_DEFAULT_STATUS';
      }

      $fileStatus = Config::get($settingName);

      if ($fileStatus === null) {
        $fileStatus = $this->request->get('status');
      }
      
      return $fileStatus ?? 0;
    }
}
