<?php

namespace BlingCustom\Controller\BlingRecordTag;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\BlingRecordTag\BlingRecordTag;

class BlingRecordTagParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new BlingRecordTag();
    }

}
