<?php

namespace BlingCustom\Controller;

use BlingApi\Core\Config;
use BlingApi\Core\Controller;
use BlingApi\Core\Exception\InvalidArgumentException;
use BlingApi\Core\Request;
use DateTime;
use Db\Config\PgsqlConnectionConfig;
use BlingApi\Core\Response\CsvResponse;
use Db\Manager as Q;

class ChartDataController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
    }

    /**
     * Pobranie danych do wykresu Availability.
     *
     * @throws InvalidArgumentException
     */
    public function availabilityAction(): void
    {
        $this->switchDataDatabase();

        $country = (string)strtoupper($this->request->get('country'));
        $kind = (string)$this->request->get('kind');
        $dateFrom = (string)$this->request->get('dateFrom');
        $dateTo = (string)$this->request->get('dateTo');

        $response = [];
        $kinds = explode(',', $kind);

        foreach ($kinds as $kind) {

            if ($country === '' || $kind === '') {
                throw new InvalidArgumentException('Missing country or kind parameter.');
            }
            if ($dateFrom === '' || $dateTo === '') {
                throw new InvalidArgumentException('Parameters: dateFrom and dateTo are required.');
            }

            $whereQuery = "flow_date BETWEEN '$dateFrom' AND '$dateTo' AND type = '$kind'";

            if ($country === 'FR') {
                $tableName = 'rte_availabilities_type_mat';
            } else {
                $tableName = 'entsoe_availabilities_type_mat';
                $whereQuery .= " AND country = '$country'";
            }
            $data =
                Q::custom("SELECT flow_date AS name, sum AS value FROM $tableName WHERE $whereQuery AND flow_date >= CURRENT_DATE + INTERVAL '1 day' ORDER BY flow_date DESC")
                    ->getAssoc();

            $data = $this->sortByDate($data);

            $response[] = [
                'name' => ucfirst($kind),
                'kind' => $kind,
                'series' => $data
            ];
        }

        $this->setResponseData($response);
    }

    /**
     * Pobranie danych do wykresu Weather/Isotherm
     *
     * @throws InvalidArgumentException
     */
    public function availabilityByRunDateAction(): void
    {
        $this->switchDataDatabase();

        $country = (string)strtoupper($this->request->get('country'));
        $kind = (string)$this->request->get('kind');
        $offset = (int)$this->request->get('offset');
        $runDate = (string)$this->request->get('runDate');

        $response = [];
        $kinds = explode(',', $kind);

        foreach ($kinds as $kind) {
            if ($country === '') {
                throw new InvalidArgumentException('Missing country parameter.');
            }

            $tableName = 'entsoe_as_of_availabilities_mat';
            $lastRunDate = '';

            $lastRunDateQuery =
                sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' AND type = '%s' ORDER BY run_date DESC LIMIT 1 OFFSET %s)",
                    $tableName, $country, $kind, $offset);
            $query =
                "SELECT flow_date as name, value FROM $tableName WHERE location = '$country' AND type = '$kind' AND run_date = $lastRunDateQuery AND flow_date >= CURRENT_DATE + INTERVAL '1 day' ORDER BY flow_date DESC";

            $data = Q::custom($query)->getAssoc();

            $lastRunDateData = Q::custom($lastRunDateQuery)->getAssoc();

            if (!empty($lastRunDateData)) {
                $lastRunDate = $lastRunDateData[0]['run_date'];
            }

            $response[] = [
                'name' => ucfirst($kind),
                'runDate' => $lastRunDate,
                'kind' => $kind,
                'series' => $data
            ];
        }

        $this->setResponseData($response);
    }

    /**
     * Pobranie run_date dla wykresu Availability.
     *
     * @throws InvalidArgumentException
     */
    public function availabilityRunDatesAction(): void
    {
        $this->switchDataDatabase();

        $country = (string)strtoupper($this->request->get('country'));
        $offset = (int)$this->request->get('offset');

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }

        $tableName = 'entsoe_as_of_availabilities_mat';
        $runDates = sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC)", $tableName, $country);

        $this->setResponseData(Q::custom($runDates)->getColumn());
    }

    /**
     * @throws InvalidArgumentException
     */
    public function boxplotAction(): void
    {
        $this->switchDataDatabase();

        $country = (string)strtoupper($this->request->get('country'));
        $model = (string)strtolower($this->request->get('model'));
        $offset = (int)$this->request->get('offset');
        $runDate = (string)$this->request->get('runDate');

        if ($country === '' || $model === '') {
            throw new InvalidArgumentException('Missing country or model parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $newModel = $this->renameModel($model);
        $tableName = $model . '_boxplot_price_mat';
        $lastRunDate = '';
        if ($offset <= 1) {
            $lastRunDateQuery =
                sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC LIMIT 1 OFFSET %s)", $tableName,
                    $country, $offset);

            $runDateQuery =
                "SELECT DISTINCT run_date FROM $tableName WHERE location = '$country' ORDER BY run_date DESC LIMIT 1 OFFSET $offset";
            $lastRunDateData = Q::custom($runDateQuery)->getAssoc();

            if (!empty($lastRunDateData)) {
                $lastRunDate = $lastRunDateData[0]['run_date'];
            }

            $query =
                "SELECT flow_date AS day, min, med, max, q_25 AS q1, q_75 AS q3 FROM $tableName WHERE location = '$country' AND run_date = $lastRunDateQuery";
        } else {
            $query =
                "SELECT flow_date AS day, min, med, max, q_25 AS q1, q_75 AS q3 FROM $tableName WHERE location = '$country' AND run_date = '$runDate'";
            $lastRunDate = $runDate;
        }

        $data = Q::custom($query)->getAssoc();
        $data = $this->sortByDate($data, true);

        $newData = [];
        foreach ($data as $row) {
            $formatData = explode(' ', $row['day']);
            $newData[] = [
                'day'  => $formatData[0],
                'min'  => $row['min'],
                'med'  => $row['med'],
                'max'  => $row['max'],
                'q1'   => $row['q1'],
                'q3'   => $row['q3']
            ];
        }

        $this->setResponseData([
            'model' => strtoupper($model),
            'run_date' => $lastRunDate,
            'model' => strtoupper($newModel),
            'series' => $newData
        ]);
    }

    /**
     * Pobranie danych z real_price_mat do wykresu boxplot Prices.
     *
     * @throws InvalidArgumentException
     */
    public function boxplotRealisedAction(): void
    {
        $this->switchDataDatabase();

        $country = (string)strtoupper($this->request->get('country'));

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }

        // TODO: ogarnąć offset tylko, w tej tabeli nie ma run_time, uwzględnić UTC
        $query = <<<SQL
SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' as name, location, ROUND(value, 2) as value 
FROM real_price_mat 
WHERE location = '$country' and flow_date >= CURRENT_DATE - INTERVAL '10 day' 
ORDER BY name
SQL;
        $data = Q::custom($query)->getAssoc();
        $data = $this->sortByDate($data);
        $prepareDate = [];

        foreach ($data as $item) {
            $itemDate = date('Y-m-d', strtotime($item['name']));

            if (!isset($prepareDate[$itemDate])) {
                $prepareDate[$itemDate] = [
                    'x' => $itemDate,
                    'y' => 0,
                    'yValue' => 0,
                    'yCount' => 0,
                ];
            }

            $prepareDate[$itemDate]['yValue'] += $item['value'];
            $prepareDate[$itemDate]['yCount']++;
        }


        $newData = [];
    
        foreach ($prepareDate as $key => &$item) {
            $item['y'] = round($item['yValue'] / $item['yCount'], 2);
            unset($item['yValue'], $item['yCount']);

            $newData[] = [
                'x'  => $item['x'],
                'y'  => $item['y'],
                'r'  => 4
            ];
        }

        $this->setResponseData([
            'name'      => 'Realised boxplot',
            'kind'      => $country,
            'series'    => $newData,
        ]);
    }

    /**
     * Pobranie danych do wykresu Ongoing outages.
     *
     * @throws InvalidArgumentException
     */
    public function ongoingOutagesAction(): void
    {
        $this->switchDataDatabase();

        $country = strtoupper($this->request->get('country'));

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }

        if ($country === 'FR') {
            $tableName = 'rte_umms_mat';
        } else {
            $tableName = 'entsoe_umms_mat';
        }

        $data = Q::custom("SELECT * FROM $tableName WHERE country = '$country' ORDER BY run_date DESC LIMIT 5")->getAssoc();

        $this->setResponseData(['name' => 'Ongoing outages', 'series' => $data]);
    }

    /**
     * Pobranie danych do wykresu Prices.
     *
     * @throws InvalidArgumentException
     */
    public function pricesAction(): void
    {
        $this->switchDataDatabase();

        // $subModel = (string) strtoupper($this->request->get('subModel'))
        $country = (string)strtoupper($this->request->get('country'));
        $model = (string)strtolower($this->request->get('model'));
        $offset = (int)$this->request->get('offset');
        $runDate = (string)$this->request->get('runDate');

        if ($country === '' || $model === '') {
            throw new InvalidArgumentException('Missing country or model parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $newModel = $this->renameModel($model);

        $tableName = $model . '_price_mat';
        if ($runDate === '') {
            $lastRunDateQuery =
                sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC LIMIT 1 OFFSET %s)", $tableName,
                    $country, $offset);

            $query =
                "SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS name, value FROM $tableName WHERE  location = '$country' AND  run_date = $lastRunDateQuery AND flow_date >= CURRENT_DATE + INTERVAL '-10 day'";

            $lastRunDateData = Q::custom($lastRunDateQuery)->getAssoc();
            $lastRunDate = '';
            if (!empty($lastRunDateData)) {
                $lastRunDate = $lastRunDateData[0]['run_date'];
            }
        } else {
            $query =
                "SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS name, value FROM $tableName WHERE location = '$country' AND run_date = '$runDate' AND flow_date >= CURRENT_DATE + INTERVAL '-10 day'";
            $lastRunDate = $runDate;
        }

        $data = Q::custom($query)->getAssoc();
        $data = $this->sortByDate($data);

        $this->setResponseData([
            'name' => 'Prices ' . strtoupper($model),
            'runDate' => $lastRunDate,
            'model' => strtoupper($newModel),
            'series' => $data,
            'kind' => $country
        ]);
    }

    /**
     * Pobranie run_date dla wykresu Prices.
     *
     * @throws InvalidArgumentException
     */
    public function pricesRunDatesAction(): void
    {
        $this->switchDataDatabase();

        $country = strtoupper($this->request->get('country'));
        $model = (string)strtolower($this->request->get('model'));

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }

        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $tableName = $model . '_price_mat';
        $runDates = sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC)", $tableName, $country);
        $this->setResponseData(Q::custom($runDates)->getColumn());
    }

    /**
     * Pobranie danych z real_price_mat do wykresu boxplot Prices.
     *
     * @throws InvalidArgumentException
     */
    public function pricesRealisedAction(): void
    {
        $this->switchDataDatabase();

        $country = (string)strtoupper($this->request->get('country'));
        $offset = (int)$this->request->get('offset');
        $runDate = (string)$this->request->get('runDate');

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }


        $tableName = 'real_price_mat';

        $query = "SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' as flow_date, location, value FROM $tableName WHERE location = '$country' AND flow_date >= CURRENT_DATE - INTERVAL '10 day' ORDER BY flow_date";

        // $query = "SELECT * FROM $tableName WHERE location = '$country'";
        $data = Q::custom($query)->getAssoc();

        $this->setResponseData([
            'kind' => $country,
            'series' => $data,
            'runDate' => $runDate,
            'name' => 'Prices Realised',
        ]);
    }

    /**
     * Pobranie danych do wykresu Renewables.
     *
     * @throws InvalidArgumentException
     */
    public function renewablesAction(): void
    {
        $this->switchDataDatabase();

        $country = (string)strtoupper($this->request->get('country'));
        $model = (string)strtolower($this->request->get('model'));
        $kind = (string)strtolower($this->request->get('kind'));
        $offset = (int)$this->request->get('offset');
        $runDate = (string)$this->request->get('runDate');

        if ($country === '' || $model === '' || $kind === '') {
            throw new InvalidArgumentException('Missing country, model or kind parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }
        if (!\in_array($kind, ['wind', 'solar', 'combined'])) {
            throw new InvalidArgumentException('Invalid kind parameter.');
        }

        $lastRunDate = '';
        $query = '';
        if ($kind === 'combined') {
            $tableNameWind = $model . '_wind_mat';
            $tableNameSolar = $model . '_solar_mat';

            if ($runDate === '') {
                $lastRunDateQuery =
                    sprintf("(SELECT DISTINCT run_date FROM (SELECT * FROM %s UNION ALL SELECT * FROM %s) Data WHERE location = '%s' ORDER BY run_date DESC LIMIT 1 OFFSET %s)",
                        $tableNameWind, $tableNameSolar, $country, $offset);
                $query = <<<SQL
SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' as name, SUM(value) AS value 
FROM (SELECT * FROM $tableNameWind UNION ALL SELECT * FROM $tableNameSolar) Data
WHERE location = '$country' AND run_date = $lastRunDateQuery AND flow_date >= CURRENT_DATE - INTERVAL '3 day'
GROUP BY flow_date 
ORDER BY flow_date DESC
SQL;
                $lastRunDateData = Q::custom($lastRunDateQuery)->getAssoc();
                if (!empty($lastRunDateData)) {
                    $lastRunDate = $lastRunDateData[0]['run_date'];
                }
            } else {

                $query = <<<SQL
SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' as name, SUM(value) AS value 
FROM (SELECT * FROM $tableNameWind UNION ALL SELECT * FROM $tableNameSolar) Data
WHERE location = '$country' AND run_date = '$runDate' AND flow_date >= CURRENT_DATE - INTERVAL '3 day'
GROUP BY flow_date 
ORDER BY flow_date DESC
SQL;

                $lastRunDate = $runDate;

            }

        } else {
            $tableName = $model . '_' . $kind . '_mat';
            if ($runDate === '') {
                $lastRunDateQuery =
                    sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC LIMIT 1 OFFSET %s)", $tableName,
                        $country, $offset);
                $query =
                    "SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS name, value FROM $tableName WHERE location = '$country' AND run_date = $lastRunDateQuery AND flow_date >= CURRENT_DATE - INTERVAL '3 day'";

                $lastRunDateData = Q::custom($lastRunDateQuery)->getAssoc();
                if (!empty($lastRunDateData)) {
                    $lastRunDate = $lastRunDateData[0]['run_date'];
                }
            } else {
                $query =
                    "SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS name, value FROM $tableName WHERE location = '$country' AND run_date = '$runDate' AND flow_date >= CURRENT_DATE - INTERVAL '3 day'";
                $lastRunDate = $runDate;
            }
        }

        $newModel = $this->renameModel($model);
        $data = Q::custom($query)->getAssoc();
        $data = $this->sortByDate($data);

        $this->setResponseData([
            'name' => ucfirst($kind) . ' ' . strtoupper($newModel),
            'kind' => ucfirst($kind),
            'runDate' => $lastRunDate,
            'model' => strtoupper($newModel),
            'series' => $data
        ]);
    }

    /**
     * Pobranie run_date dla wykresu Renewables i podanych filtrów.
     *
     * @throws InvalidArgumentException
     */
    public function renewablesRunDatesAction(): void
    {
        $this->switchDataDatabase();

        $country = (string)strtoupper($this->request->get('country'));
        $kind = (string)strtolower($this->request->get('kind'));
        $model = (string)strtolower($this->request->get('model'));

        if ($country === '' || $kind === '') {
            throw new InvalidArgumentException('Missing country, model or kind parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }
        if (!\in_array($kind, ['wind', 'solar', 'combined'])) {
            throw new InvalidArgumentException('Invalid kind parameter.');
        }

        if ($kind === 'combined') {
            $tableNameWind = $model . '_wind_mat';
            $tableNameSolar = $model . '_solar_mat';
        } else {
            $tableName = $model . '_' . $kind . '_mat';
        }

        if ($kind === 'combined') {
            $runDates =
                sprintf("(SELECT DISTINCT run_date FROM (SELECT * FROM %s UNION ALL SELECT * FROM %s) Data WHERE location = '%s' ORDER BY run_date DESC)",
                    $tableNameWind, $tableNameSolar, $country);
        } else {
            $runDates = sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC)", $tableName, $country);
        }

        $this->setResponseData(Q::custom($runDates)->getColumn());
    }

    /**
     * Pobranie run_date dla wykresu Renewables i podanych filtrów.
     *
     * @throws InvalidArgumentException
     */
    public function renewablesWindRunDatesAction(): void
    {
        $this->switchDataDatabase();

        $country = (string)strtoupper($this->request->get('country'));
        $kind = (string)strtolower($this->request->get('kind'));
        $model = (string)strtolower($this->request->get('model'));

        if ($country === '' || $kind === '') {
            throw new InvalidArgumentException('Missing country, model or kind parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }
        if (!\in_array($kind, ['wind', 'solar', 'combined'])) {
            throw new InvalidArgumentException('Invalid kind parameter.');
        }

        if ($kind === 'combined') {
            $tableNameWind = $model . '_wind_mat';
            $tableNameSolar = $model . '_solar_mat';
        } else {
            $tableName = $model . '_' . $kind . '_mat';
        }

        if ($kind === 'combined') {
            $runDates =
                sprintf("(SELECT DISTINCT run_date FROM (SELECT * FROM %s UNION ALL SELECT * FROM %s) Data WHERE location = '%s' ORDER BY run_date DESC)",
                    $tableNameWind, $tableNameSolar, $country);
        } else {
            $runDates = sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC)", $tableName, $country);
        }

        $this->setResponseData(Q::custom($runDates)->getColumn());
    }

    /**
     * Pobranie run_date dla wykresu Weather - Isotherm i podanych filtrów.
     *
     * @throws InvalidArgumentException
     */
    public function isothermRunDatesAction(): void
    {
        $this->switchDataDatabase();

        $country = strtoupper($this->request->get('country'));
        $model = (string)strtolower($this->request->get('model'));

        if ($country === '') {
            throw new InvalidArgumentException('Missing country or massif parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $tableName = $model . '_isotherm_mat';
        $runDates = sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC)", $tableName, $country);

        $this->setResponseData(Q::custom($runDates)->getColumn());
    }

    /**
     * Pobranie danych do wykresu Weather - Temperature.
     *
     * @throws InvalidArgumentException
     */
    public function temperatureAction(): void
    {
        $this->switchDataDatabase();

        $country = strtoupper($this->request->get('country'));
        $model = (string)strtolower($this->request->get('model'));
        $offset = (int)$this->request->get('offset');
        $runDate = (string)$this->request->get('runDate');

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $newModel = $this->renameModel($model);

        $tableName = $model . '_temperatures_mat';
        $lastRunDate = '';
        $query = '';
        if ($runDate === '') {
            $lastRunDateQuery =
                sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC LIMIT 1 OFFSET %s)", $tableName,
                    $country, $offset);
            $query =
                "SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS name, value FROM $tableName WHERE location = '$country' AND run_date = $lastRunDateQuery AND flow_date >= CURRENT_DATE - INTERVAL '3 day'";
            $lastRunDateData = Q::custom($lastRunDateQuery)->getAssoc();
            if (!empty($lastRunDateData)) {
                $lastRunDate = $lastRunDateData[0]['run_date'];
            }
        } else {
            $query =
                "SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS name, value FROM $tableName WHERE location = '$country' AND run_date = '$runDate' AND flow_date >= CURRENT_DATE - INTERVAL '3 day'";
            $lastRunDate = $runDate;
        }

        $data = Q::custom($query)->getAssoc();
        $data = $this->sortByDate($data);

        $this->setResponseData([
            'name' => 'Temperature' . ' ' . strtoupper($model),
            'runDate' => $lastRunDate,
            'model' => strtoupper($newModel),
            'kind' => 'Temperature',
            'series' => $data
        ]);
    }

    /**
     * Pobranie run_date dla wykresu Weather - Temperature i podanych filtrów.
     *
     * @throws InvalidArgumentException
     */
    public function temperatureRunDatesAction(): void
    {
        $this->switchDataDatabase();

        $country = strtoupper($this->request->get('country'));
        $model = (string)strtolower($this->request->get('model'));

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $tableName = $model . '_temperatures_mat';
        $runDates = sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC)", $tableName, $country);

        $this->setResponseData(Q::custom($runDates)->getColumn());
    }

    /**
     * Pobranie statycznych danych dla wykresu Weather - Temperature.
     *
     * @throws InvalidArgumentException
     */
    public function temperatureStaticCurveDataAction(): void {
        $this->switchDataDatabase();

        $country = strtoupper($this->request->get('country'));

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }

        $tableName = 'real_temperatures_norm_mat';
        $query =
            "SELECT flow_date, value, location FROM $tableName WHERE location = '$country' ORDER BY flow_date ASC";
        $data = Q::custom($query)->getAssoc();

        $this->setResponseData([
            'name' => 'Temperature Static Curve',
            'kind' => 'Temperature',
            'series' => $data
        ]);
    }

    /**
     * Pobieranie danych dla różnicy w wykresie temperatury
     *
     * @throws InvalidArgumentException
     */
    public function temperatureCalculatedDifferenceAction(): void {
        $this->switchDataDatabase();

        $country = strtoupper($this->request->get('country'));
        $model = (string)strtolower($this->request->get('model'));
        $offset = (int)$this->request->get('offset');
        $runDate = (string)$this->request->get('runDate');

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $tableNormMat= 'real_temperatures_norm_mat';
        $tableName = $model . '_temperatures_mat';
        $lastRunDate = '';

        if ($runDate === '') {
            $lastRunDateQuery =
                sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC LIMIT 1 OFFSET %s)", $tableName,
                    $country, $offset);
            $lastRunDateData = Q::custom($lastRunDateQuery)->getAssoc();

            if (!empty($lastRunDateData)) {
                $lastRunDate = $lastRunDateData[0]['run_date'];
            }

            $query = <<<SQL
SELECT (mat_temp.flow_date) as name, (mat_temp.value-real_norm.value) as value
FROM $tableName as mat_temp
JOIN $tableNormMat as real_norm ON (mat_temp.flow_date = real_norm.flow_date AND mat_temp.location = real_norm.location)
WHERE mat_temp.location = '$country' AND mat_temp.run_date = '$lastRunDate'
ORDER BY mat_temp.flow_date DESC, real_norm.flow_date DESC
SQL;

        } else {
            $query = <<<SQL
SELECT (mat_temp.flow_date) as name, (mat_temp.value-real_norm.value) as value
FROM $tableName as mat_temp
JOIN $tableNormMat as real_norm ON (mat_temp.flow_date = real_norm.flow_date AND mat_temp.location = real_norm.location)
WHERE mat_temp.location = '$country' AND mat_temp.run_date = '$runDate' 
ORDER BY mat_temp.flow_date DESC, real_norm.flow_date DESC
SQL;
            $lastRunDate = $runDate;
        }

        $data = Q::custom($query)->getAssoc();
        $data = $this->sortByDate($data);

        $this->setResponseData([
            'name' => 'Temperature Calculated Difference'. ' '. strtoupper($model),
            'runDate' => $lastRunDate,
            'model' => strtoupper($this->renameModel($model)),
            'kind' => 'Calculated Difference',
            'series' => $data
        ]);
    }

    /**
     * Pobranie run_date dla wykresu Weather - Temperature (calculated difference)_ i podanych filtrów.
     *
     * @throws InvalidArgumentException
     */
    public function temperatureCalculatedDifferenceRunDatesAction(): void
    {
        $this->switchDataDatabase();

        $country = strtoupper($this->request->get('country'));
        $model = (string)strtolower($this->request->get('model'));

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $tableName = $model . '_temperatures_mat';
        $runDates = sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC)", $tableName, $country);

        $this->setResponseData(Q::custom($runDates)->getColumn());
    }

    /**
     * Pobranie danych do wykresu Demand.
     *
     * @throws InvalidArgumentException
     */
    public function demandAction(): void
    {
        $this->switchDataDatabase();

        $country = strtoupper($this->request->get('country'));
        $model = (string)strtolower($this->request->get('model'));
        $offset = (int)$this->request->get('offset');
        $runDate = (string)$this->request->get('runDate');

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $newModel = $this->renameModel($model);

        $tableName = $model . '_load_mat';
        $lastRunDate = '';
        $query = '';
        if ($runDate === '') {
            $lastRunDateQuery =
                sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC LIMIT 1 OFFSET %s)", $tableName,
                    $country, $offset);
            $query =
                "SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS name, value FROM $tableName WHERE location = '$country' AND run_date = $lastRunDateQuery AND flow_date >= CURRENT_DATE - INTERVAL '3 day'";
            $lastRunDateData = Q::custom($lastRunDateQuery)->getAssoc();
            if (!empty($lastRunDateData)) {
                $lastRunDate = $lastRunDateData[0]['run_date'];
            }
        } else {
            $query =
                "SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS name, value FROM $tableName WHERE location = '$country' AND run_date = '$runDate' AND flow_date >= CURRENT_DATE - INTERVAL '3 day'";
            $lastRunDate = $runDate;
        }

        $data = Q::custom($query)->getAssoc();
        $data = $this->sortByDate($data);

        $this->setResponseData([
            'name' => 'Demand' . ' ' . strtoupper($model),
            'runDate' => $lastRunDate,
            'model' => strtoupper($newModel),
            'kind' => 'Demand',
            'series' => $data
        ]);
    }

    /**
     * Pobranie run_date dla wykresu Demand - demand i podanych filtrów.
     *
     * @throws InvalidArgumentException
     */
    public function demandRunDatesAction(): void
    {
        $this->switchDataDatabase();

        $country = strtoupper($this->request->get('country'));
        $model = (string)strtolower($this->request->get('model'));

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $tableName = $model . '_temperatures_mat';
        $runDates = sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC)", $tableName, $country);
        $this->setResponseData(Q::custom($runDates)->getColumn());
    }

    /**
     * Pobranie danych do wykresu Residual Demand.
     *
     * @throws InvalidArgumentException
     */
    public function residualDemandAction(): void
    {
        $this->switchDataDatabase();

        $country = strtoupper($this->request->get('country'));
        $model = (string)strtolower($this->request->get('model'));
        $offset = (int)$this->request->get('offset');
        $runDate = (string)$this->request->get('runDate');

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $demandTableName = $model . '_load_mat';
        $windTableName = $model . '_wind_mat';
        $solarTableName = $model . '_solar_mat';
        $lastRunDate = '';
        $query = '';

        if ($runDate === '') {
            $lastRunDateQuery =
                sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC LIMIT 1 OFFSET %s)", $demandTableName,
                    $country, $offset);
            $query = <<<SQL
SELECT d.flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS name, (d.value - (w.value + s.value)) AS value FROM $demandTableName d
JOIN $windTableName w ON d.flow_date = w.flow_date AND d.location = w.location AND d.run_date = w.run_date
JOIN $solarTableName s ON d.flow_date = s.flow_date AND d.location = s.location AND d.run_date = s.run_date 
WHERE d.location = '$country' AND d.run_date = $lastRunDateQuery AND d.flow_date >= CURRENT_DATE - INTERVAL '3 day'
SQL;

            $lastRunDateData = Q::custom($lastRunDateQuery)->getAssoc();

            if (!empty($lastRunDateData)) {
                $lastRunDate = $lastRunDateData[0]['run_date'];
            }

        } else {
            $query = <<<SQL
SELECT d.flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS name, (d.value - (w.value + s.value)) AS value FROM $demandTableName d
JOIN $windTableName w ON d.flow_date = w.flow_date AND d.location = w.location AND d.run_date = w.run_date
JOIN $solarTableName s ON d.flow_date = s.flow_date AND d.location = s.location AND d.run_date = s.run_date 
WHERE d.location = '$country' AND d.run_date = '$runDate' AND d.flow_date >= CURRENT_DATE - INTERVAL '3 day'
SQL;

            $lastRunDate = $runDate;
        }

        $data = Q::custom($query)->getAssoc();
        $data = $this->sortByDate($data);
        $newModel = $this->renameModel($model);

        $this->setResponseData([
            'name' => 'Residual Demand' . ' ' . strtoupper($model),
            'kind' => 'Residual Demand',
            'runDate' => $lastRunDate,
            'model' => strtoupper($newModel),
            'series' => $data
        ]);
    }

    /**
     * Pobranie run_date dla wykresu Demand - residual demand i podanych filtrów.
     *
     * @throws InvalidArgumentException
     */
    public function residualDemandRunDatesAction(): void
    {
        $this->switchDataDatabase();

        $country = strtoupper($this->request->get('country'));
        $model = (string)strtolower($this->request->get('model'));

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $tableName = $model . '_temperatures_mat';
        $runDates = sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC)", $tableName, $country);
        $this->setResponseData(Q::custom($runDates)->getColumn());
    }

    /**
     * Pobranie danych do wykresu Weather/Isotherm
     *
     * @throws InvalidArgumentException
     */
    public function getMassifsAction(): void
    {
        $this->switchDataDatabase();

        $country = (string)strtoupper($this->request->get('country'));
        $model = (string)$this->request->get('model');

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }
        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $tableName = $model . '_isotherm_mat';
        $query = "SELECT distinct massif FROM $tableName where location = '$country'";
        $data = Q::custom($query)->getAssoc();
        $newData = [];

        foreach ($data as $k => $v) {
            array_push($newData, $v['massif']);
        }

        $this->setResponseData($newData);
    }

    /**
     * Pobranie danych do wykresu Hydro/River.
     *
     * @throws InvalidArgumentException
     */
    public function hydroRiverAction(): void
    {
        $this->switchDataDatabase();

        $country = (string)strtoupper($this->request->get('country'));
        $offset = (int)$this->request->get('offset');
        $runDate = (string)$this->request->get('runDate');

        if ($country === '' | $offset === '') {
            throw new InvalidArgumentException('Missing country or offset parameter.');
        }

        $tableName = 'entsoe_run_of_river_mat';
        $lastRunDate = '';

        if ($runDate === '') {
            $lastRunDateQuery =
                sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC LIMIT 1 OFFSET %s)", $tableName,
                    $country, $offset);

            $query =
                "SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS name, value FROM $tableName WHERE  location = '$country' AND  run_date = $lastRunDateQuery AND flow_date >= CURRENT_DATE - INTERVAL '3 day'";

            $lastRunDateData = Q::custom($lastRunDateQuery)->getAssoc();

            if (!empty($lastRunDateData)) {
                $lastRunDate = $lastRunDateData[0]['run_date'];
            }
        } else {
            $query =
                "SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS name, value FROM $tableName WHERE location = '$country' AND run_date = '$runDate' AND flow_date >= CURRENT_DATE - INTERVAL '3 day'";
            $lastRunDate = $runDate;
        }

        $data = Q::custom($query)->getAssoc();
        $data = $this->sortByDate($data);

        $this->setResponseData([
            'name' => 'Run of River',
            'kind' => 'Run of River',
            'runDate' => $lastRunDate,
            'series' => $data
        ]);
    }

    /**
     * Gets run_date list for Hydro chart
     *
     * @throws InvalidArgumentException
     */
    public function hydroRunDatesAction(): void
    {
        $this->switchDataDatabase();

        $country = strtoupper($this->request->get('country'));

        if ($country === '') {
            throw new InvalidArgumentException('Missing country parameter.');
        }

        $tableName = 'entsoe_run_of_river_mat';
        $runDates = sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC)", $tableName, $country);
        $this->setResponseData(Q::custom($runDates)->getColumn());
    }

    /**
     * Pobranie danych do wykresu Weather - Isotherm.
     *
     * @throws InvalidArgumentException
     */
    public function getAllIsothermDataAction(): void
    {
        $this->switchDataDatabase();

        $country = strtoupper($this->request->get('country'));
        $model = (string)strtolower($this->request->get('model'));
        $offset = (int)$this->request->get('offset');
        $runDate = (string)$this->request->get('runDate');

        if ($country === '') {
            throw new InvalidArgumentException('Missing country or massif parameter.');
        }

        if ($offset === '' && $runDate === '') {
            throw new InvalidArgumentException('Missing run date or offset parameter.');
        }

        if (!\in_array($model, ['gfs', 'gefs', 'hres', 'hps'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $tableName = $model . '_isotherm_mat';
        $lastRunDate = '';
        if ($runDate === '') {
            $lastRunDateQuery =
                sprintf("(SELECT DISTINCT run_date FROM %s WHERE location = '%s' ORDER BY run_date DESC LIMIT 1 OFFSET %s)", $tableName,
                    $country, $offset);
            $query =
                "SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS flow_date, value, massif FROM $tableName WHERE location = '$country' AND run_date = $lastRunDateQuery AND flow_date >= CURRENT_DATE - INTERVAL '3 day'";
            $lastRunDateData = Q::custom($lastRunDateQuery)->getAssoc();
            if (!empty($lastRunDateData)) {
                $lastRunDate = $lastRunDateData[0]['run_date'];
            }
        } else {
            $query =
                "SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS flow_date, value, massif FROM $tableName WHERE location = '$country' AND run_date = '$runDate' AND flow_date >= CURRENT_DATE - INTERVAL '3 day'";
            $lastRunDate = $runDate;
        }

        $data = Q::custom($query)->getAssoc();
        $newData = [];
        $newModel = $this->renameModel($model);

        foreach ($data as $item) {
            $newData[$item['massif']][] = ['name' => $item['flow_date'], 'value' => $item['value']];
        }

        $returnData = [];
        foreach ($newData as $key => $value) {
            $returnData[] = ['kind' => 'Isotherm', 'runDate' => $lastRunDate, 'model' => $newModel, 'name' => $key, 'series' => $this->sortByDate($value)];
        }

        $this->setResponseData($returnData);
    }

    // dla wszystkiego liczymy base - czyli średnia i peak czyli średnia z godzin 9-20
    public function bigTableAction(): void
    {
        $this->switchDataDatabase();

        $model = strtolower($this->request->get('model'));
        $tables = [
            'load',
            'solar',
            'wind',
            'price'
        ];
        $data = $this->getBigTableData([$model], $tables);

        $this->setResponseData([
            'model' => $model,
            'data' => $data[$model]
        ]);
    }

    public function bigTableHourlyAction()
    {
        $model = (string)strtolower($this->request->get('model'));

        if (!\in_array($model, ['gfs', 'hres'])) {
            throw new InvalidArgumentException('Invalid model parameter.');
        }

        $tables = [
            'load',
            'solar',
            'wind',
            'price'
        ];

        $countries = [];
        $date = $this->request->get('date') ?? date('Y-m-d');
        $data = $this->getTableHourlyData([$model], $tables, $countries, $date);

        $this->setResponseData([
            'model' => $model,
            'data' => $data[$model],
            'isPrevData' => $data['isPrevDayData'],
            'isNextData' => $data['isNextDayData']
        ]);
    }

    public function hourlyDataToCSVAction(): CsvResponse
    {
        $this->switchDataDatabase();

        $models = $this->request->get('models');
        $tables = $this->request->get('tables');
        $countries = $this->request->get('countries');
        $date = $this->request->get('date') ?? date('Y-m-d');
        $data = $this->getTableHourlyData($models, $tables, $countries, $date);

        $file = fopen('php://memory', 'wb');
        $line = [];
        $line[] = 'Model';
        $line[] = 'Country';
        $line[] = 'Kind';
        $line[] = 'Hour';
        $line[] = 'Value';
        fputcsv($file, $line);

        foreach ($data as $model => $modelValues) {
            if (!is_array($modelValues)) {
                continue;
            }
            foreach ($modelValues as $country => $countryValues) {
                foreach ($countryValues as $table => $tableValues) {
                    foreach ($tableValues as $day => $value) {
                        if ($day >= 0) {
                            $line = [];
                            $line[] = $this->renameModel($model);
                            $line[] = $country;
                            $line[] = $this->renameTable($table);
                            $line[] = 'Hour ' . ($day + 1);
                            $line[] = $value['value'];
                            fputcsv($file, $line);
                        }
                    }
                }
            }
        }

        fseek($file, 0);
        $fileContent = stream_get_contents($file);
        fclose($file);

        return new CsvResponse($fileContent, 'export_' . date('Y-m-d'));
    }

    public function bigTableToCSVAction(): CsvResponse
    {
        $this->switchDataDatabase();

        $models = $this->request->get('models');
        $tables = $this->request->get('tables');
        $countries = $this->request->get('countries');

        $data = $this->getBigTableData($models, $tables, $countries);
        $todayDate = new DateTime(date('Y-m-d 00:00:00'));

        $file = fopen('php://memory', 'wb');
        $line = [];
        $line[] = 'Model';
        $line[] = 'Country';
        $line[] = 'Kind';
        $line[] = 'Day';
        $line[] = 'Base';
        $line[] = 'Peak';
        fputcsv($file, $line);

        foreach ($data as $model => $modelValues) {
            foreach ($modelValues as $country => $countryValues) {
                foreach ($countryValues as $table => $tableValues) {
                    if (!is_array($tableValues)) {
                        continue;
                    }

                    ksort($tableValues);

                    foreach ($tableValues as $day => $value) {
                        $line = [];
                        $line[] = $this->renameModel($model);
                        $line[] = $country;
                        $line[] = $this->renameTable($table);

                        if ($day === 'weekend' || $day === 'nextWeek') {
                            $line[] = ucfirst($day);
                            $line[] = $value['base'];
                            $line[] = $value['peak'];
                        } else {
                            $currentRowDate = new DateTime($value['date']);
                            $dayDiff = (int) $todayDate->diff($currentRowDate)->format('%a');

                            if ($dayDiff >= 0) {
                                if ($dayDiff === 0) {
                                    $line[] = 'D';
                                } else {
                                    $line[] = 'D+' . $dayDiff;
                                }
                                if (isset($value['base'])) {
                                    $line[] = $value['base'];
                                } else {
                                    $line[] = 'no data';
                                }
                                if (isset($value['peak'])) {
                                    $line[] = $value['peak'];
                                } else {
                                    $line[] = 'no data';
                                }
                            }
                        }

                        fputcsv($file, $line);
                    }
                }
            }
        }

        fseek($file, 0);
        $fileContent = stream_get_contents($file);
        fclose($file);

        return new CsvResponse($fileContent, 'export_' . date('Y-m-d'));
    }

    /**
     * Pobranie ścieżki mapy opadow dla wybranego modelu
     *
     * @throws InvalidArgumentException
     */
    public function weatherMapAction(): void
    {
        $this->switchDataDatabase();

        $model = strtolower($this->request->get('model'));

        if ($model === '') {
            throw new InvalidArgumentException('Missing model parameter.');
        }

        $tableName = $model . '_wind_map_mat';
        $lastRunDateQuery = sprintf('(SELECT DISTINCT run_date FROM %s ORDER BY run_date DESC LIMIT 1 OFFSET 0)', $tableName);
        $query = "SELECT file_path FROM $tableName WHERE run_date = $lastRunDateQuery";
        $data = Q::custom($query)->getOne();
        $this->setResponseData([
            'file_path' => $data,
            'model' => $model
        ]);
    }

    /**
     * Konwertuje nazwę modelu.
     *
     * @param string $modelName
     * @return string
     */
    private function renameModel(string $modelName): string
    {
        switch ($modelName) {
            case 'gfs':
                return 'GFS OP';
            case 'hres':
                return 'EC OP';
        }
    }

    private function renameTable($tableName): string
    {
        switch ($tableName) {
            case 'price':
                return 'Price';
            case 'load':
                return 'Demand';
            case 'wind':
                return 'Wind';
            case 'solar':
                return 'Solar';
        }
    }

    /**
     * Get and parse data to Data table (hourly.
     *
     * @param array $models
     * @param array $tables
     * @param array $countries
     * @param string $date
     * @return array
     */
    private function getTableHourlyData(array $models, array $tables, array $countries = [], string $date): array
    {
        $this->switchDataDatabase();

        $returnData = [];
        foreach ($models as $model) {
            $preparedData = [];
            $preparedPrevDayData = [];
            $preparedNextDayData = [];

            foreach ($tables as $table) {
                $tableName = $model . '_' . $table . '_mat';
                $query = <<<SQL
SELECT 
    flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS flow_datetime, EXTRACT(hour from flow_date at time zone 'UTC' at time zone 'Europe/Paris') as number, location, value, 
    run_date 
FROM 
    $tableName
WHERE 
    run_date = (SELECT DISTINCT run_date FROM $tableName WHERE DATE(run_date) <= DATE(to_date('$date', 'YYYY-MM-DD')) ORDER BY run_date DESC LIMIT 1)
        AND DATE(flow_date at time zone 'UTC' at time zone 'Europe/Paris') >= DATE(to_date('$date', 'YYYY-MM-DD')) - INTERVAL '0 day' 
        AND DATE(flow_date at time zone 'UTC' at time zone 'Europe/Paris') < DATE(to_date('$date', 'YYYY-MM-DD') + interval '1' day) 
ORDER BY 
    flow_date ASC
SQL;

                $data = Q::custom($query)->getAssoc();

                foreach ($data as $row) {
                    if (!empty($countries) && !\in_array($row['location'], $countries, true)) {
                        continue;
                    }

                    $preparedData[$row['location']][$table][$row['number']]['value'] = $row['value'];
                    $preparedData[$row['location']][$table][$row['number']]['date'] = $row['flow_datetime'];
                    $preparedData[$row['location']][$table][$row['number']]['hour'] = $row['number'];
                }

                // sprawdzanie dnia poprzedniego (czy istnieja dane)

                $queryPrev = <<<SQL
SELECT 
    flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS flow_datetime, EXTRACT(hour from flow_date at time zone 'UTC' at time zone 'Europe/Paris') as number, location, value, 
    run_date 
FROM 
    $tableName
WHERE 
    run_date = (SELECT DISTINCT run_date FROM $tableName WHERE DATE(run_date) <= DATE(to_date('$date', 'YYYY-MM-DD') - interval '3') ORDER BY run_date DESC LIMIT 1)
        AND DATE(flow_date at time zone 'UTC' at time zone 'Europe/Paris') > DATE(to_date('$date', 'YYYY-MM-DD') - interval '2' day) 
        AND DATE(flow_date at time zone 'UTC' at time zone 'Europe/Paris') < DATE(to_date('$date', 'YYYY-MM-DD')) 
LIMIT 1 
SQL;

                $prevDayData = Q::custom($queryPrev)->getAssoc();


                foreach ($prevDayData as $row) {
                    $preparedPrevDayData[] = $row;
                }

                // sprawdzanie dnia nastepnego (czy istnieja dane)

                $queryNext = <<<SQL
SELECT 
    flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS flow_datetime, EXTRACT(hour from flow_date at time zone 'UTC' at time zone 'Europe/Paris') as number, location, value, 
    run_date 
FROM 
    $tableName
WHERE 
    run_date = (SELECT DISTINCT run_date FROM $tableName WHERE DATE(run_date) <= DATE(to_date('$date', 'YYYY-MM-DD') + interval '1') ORDER BY run_date DESC LIMIT 1)
        AND DATE(flow_date at time zone 'UTC' at time zone 'Europe/Paris') > DATE(to_date('$date', 'YYYY-MM-DD') + interval '1' day) 
        AND DATE(flow_date at time zone 'UTC' at time zone 'Europe/Paris') < DATE(to_date('$date', 'YYYY-MM-DD') + interval '3' day) 
LIMIT 1 
SQL;

                $nextDayData = Q::custom($queryNext)->getAssoc();

                foreach ($nextDayData as $row) {
                    $preparedNextDayData[] = $row;
                }
            }

            $returnData[$model] = $preparedData;
            $returnData['isPrevDayData'] = (bool)count($preparedPrevDayData);
            $returnData['isNextDayData'] = (bool)count($preparedNextDayData);
        }

        return $returnData;
    }

    /**
     * Get and parse data to Data table (daily).
     *
     * @param array $models
     * @param array $tables
     * @param array $countries
     * @return array
     * @throws InvalidArgumentException
     */
    private function getBigTableData(array $models, array $tables, array $countries = []): array
    {
        $this->switchDataDatabase();

        $firstSaturdayDate = date('Y-m-d', strtotime('first saturday'));
        $firstSundayDate = date('Y-m-d', strtotime('first sunday'));

        $nextWeekMonday = date('Y-m-d', strtotime('next week monday'));
        $nextWeekFriday = date('Y-m-d', strtotime('next week friday'));
        $nextWeekSunday = date('Y-m-d', strtotime('next week sunday'));

        $returnData = [];
        foreach ($models as $model) {
            if (!\in_array($model, ['gfs', 'hres'])) {
                throw new InvalidArgumentException('Invalid model parameter.');
            }
            $outData = [];

            foreach ($tables as $table) {
                if (!\in_array($table, ['load', 'solar', 'wind', 'price'])) {
                    throw new InvalidArgumentException('Invalid table parameter.');
                }
                $tableName = $model . '_' . $table . '_mat';

                $lastRunDateQuery = sprintf('(SELECT DISTINCT run_date FROM %s ORDER BY run_date DESC LIMIT 1)', $tableName);
                $query = sprintf("SELECT flow_date at time zone 'UTC' at time zone 'Europe/Paris' AS flow_datetime, DATE(flow_date at time zone 'UTC' at time zone 
'Europe/Paris') as flow_date, EXTRACT(hour from flow_date at time zone 'UTC' at time zone 'Europe/Paris') as flow_time, location, value, 
run_date, DATE(flow_date at time zone 'UTC' at time zone 'Europe/Paris') - DATE(NOW()) as number FROM %s WHERE run_date = %s ORDER BY flow_datetime ASC",
                    $tableName, $lastRunDateQuery);
                $allData = Q::custom($query)->getAssoc();

                foreach ($allData as $row)
                {
                    $locKey = $row['location'];

                    if (empty($row) || empty($row['flow_date']))
                    {
                        continue;
                    }
                    if (!empty($countries) && !\in_array($locKey, $countries, true)) {
                        continue;
                    }

                    $dateKey = date('M d', strtotime($row['flow_date']));
                    $flowTime = (int)$row['flow_time'];
                    $peakCountAllowed = ($flowTime >= 9 && $flowTime <= 20);

                    if (!isset($outData[$locKey][$table]['nextWeek'])) {
                        $outData[$locKey][$table]['nextWeek'] = [
                            'base' => 0,
                            'baseCount' => 0,
                            'peak' => 0,
                            'peakCount' => 0,
                        ];
                    }
                    if (!isset($outData[$locKey][$table]['weekend'])) {
                        $outData[$locKey][$table]['weekend'] = [
                            'base' => 0,
                            'baseCount' => 0,
                            'peak' => 0,
                            'peakCount' => 0,
                        ];
                    }

                    if (!isset($outData[$locKey][$table][$dateKey])) {
                        // jeżeli to pierwszy rekord to tworzymy
                        $outData[$locKey][$table][$dateKey]['date'] = $row['flow_date'];
                        $outData[$locKey][$table][$dateKey]['count'] = 1;
                        $outData[$locKey][$table][$dateKey]['sum'] = $row['value'];
                        $outData[$locKey][$table][$dateKey]['base'] = $row['value'];
                        // szczyt
                        $outData[$locKey][$table][$dateKey]['count_peak'] = $peakCountAllowed ? 1 : 0;
                        $outData[$locKey][$table][$dateKey]['sum_peak'] = $peakCountAllowed ? $row['value'] : 0;
                        $outData[$locKey][$table][$dateKey]['peak'] = $peakCountAllowed ? $row['value'] : 0;
                    } else {
                        $outData[$locKey][$table][$dateKey]['date'] = $row['flow_date'];
                        $outData[$locKey][$table][$dateKey]['count'] += 1;
                        $outData[$locKey][$table][$dateKey]['sum'] += $row['value'];

                        if($table === 'price') {
                            $outData[$locKey][$table][$dateKey]['base'] = round($outData[$locKey][$table][$dateKey]['sum'] / $outData[$locKey][$table][$dateKey]['count'], 2);
                        } else {
                            $outData[$locKey][$table][$dateKey]['base'] = round($outData[$locKey][$table][$dateKey]['sum'] / $outData[$locKey][$table][$dateKey]['count']);
                        }

                        if ($peakCountAllowed === true) {
                            $outData[$locKey][$table][$dateKey]['count_peak'] += 1;
                            $outData[$locKey][$table][$dateKey]['sum_peak'] += $row['value'];
                            if($table === 'price') {
                                $outData[$locKey][$table][$dateKey]['peak'] = round($outData[$locKey][$table][$dateKey]['sum_peak'] / $outData[$locKey][$table][$dateKey]['count_peak'], 2);
                            } else {
                                $outData[$locKey][$table][$dateKey]['peak'] = round($outData[$locKey][$table][$dateKey]['sum_peak'] / $outData[$locKey][$table][$dateKey]['count_peak']);
                            }
                        }
                    }

                    if (strtotime($row['flow_datetime']) >= strtotime($firstSaturdayDate . ' 00:00:00') && strtotime($row['flow_datetime']) <= strtotime($firstSundayDate . ' 23:59:59')) {
                        $outData[$locKey][$table]['weekend']['base'] += $row['value'];
                        $outData[$locKey][$table]['weekend']['baseCount'] += 1;

                        if ($peakCountAllowed) {
                            $outData[$locKey][$table]['weekend']['peak'] += $row['value'];
                            $outData[$locKey][$table]['weekend']['peakCount'] += 1;
                            continue;
                        }
                    }

                    if (strtotime($row['flow_datetime']) >= strtotime($nextWeekMonday . ' 00:00:00') && strtotime($row['flow_datetime']) <= strtotime($nextWeekSunday . ' 23:59:59')) {
                        $outData[$locKey][$table]['nextWeek']['base'] += (float) $row['value'];
                        $outData[$locKey][$table]['nextWeek']['baseCount'] += 1;

                        // dla peak bierzemy tylko od poniedziałku do piątku
                        if ($peakCountAllowed && strtotime($row['flow_datetime']) <= strtotime($nextWeekFriday . ' 23:59:59')) {
                            $outData[$locKey][$table]['nextWeek']['peak'] += (float) $row['value'];
                            $outData[$locKey][$table]['nextWeek']['peakCount'] += 1;
                            continue;
                        }
                    }
                }
            }

            // zaokraglanie danych, a dla prices zostaja dwa miejsca po przecinku
            foreach ($outData as $country => &$row) {
                foreach ($row as $kind => &$item) {
                    if (isset($item['weekend'])) {
                        if ($item['weekend']['baseCount'] !== 0 && $item['weekend']['base'] !== 0) {
                            if ($kind === 'price') {
                                $item['weekend']['base'] = round($item['weekend']['base'] / $item['weekend']['baseCount'],  2);
                            } else {
                                $item['weekend']['base'] = round($item['weekend']['base'] / $item['weekend']['baseCount']);
                            }
                        }
                        if ($item['weekend']['peakCount'] !== 0 && $item['weekend']['peak'] !== 0) {
                            if ($kind === 'price') {
                                $item['weekend']['peak'] = round($item['weekend']['peak'] / $item['weekend']['peakCount'], 2);
                            } else {
                                $item['weekend']['peak'] = round($item['weekend']['peak'] / $item['weekend']['peakCount']);
                            }
                        }

                        unset($item['weekend']['baseCount'], $item['weekend']['peakCount']);
                    }

                    if (isset($item['nextWeek'])) {
                        if ($item['nextWeek']['baseCount'] !== 0 && $item['nextWeek']['base'] !== 0) {
                            if ($kind === 'price') {
                                $item['nextWeek']['base'] = round($item['nextWeek']['base'] / $item['nextWeek']['baseCount'], 2);
                            } else {
                                $item['nextWeek']['base'] = round($item['nextWeek']['base'] / $item['nextWeek']['baseCount']);
                            }
                        }
                        if ($item['nextWeek']['peakCount'] !== 0 && $item['nextWeek']['peak'] !== 0) {
                            if ($kind === 'price') {
                                $item['nextWeek']['peak'] = round($item['nextWeek']['peak'] / $item['nextWeek']['peakCount'], 2);
                            } else {
                                $item['nextWeek']['peak'] = round($item['nextWeek']['peak'] / $item['nextWeek']['peakCount']);
                            }
                        }

                        unset($item['nextWeek']['baseCount'], $item['nextWeek']['peakCount']);
                    }
                }

                unset($item);
            }
            unset($row);

            $returnData[$model] = $outData;
        }

        return $returnData;
    }

    private function switchDataDatabase(): void
    {
        $connetionSettings = [
            Config::get('DB_CHART_HOST'),
            Config::get('DB_CHART_DATABASE'),
            Config::get('DB_CHART_USERNAME'),
            Config::get('DB_CHART_PASSWORD'),
            Config::get('DB_CHART_PORT')
        ];
        $chartDbConnection = new PgsqlConnectionConfig(...$connetionSettings);

        Q::getSettings()->addConnectionConfig('chart', $chartDbConnection);
        Q::getSettings()->setCurrentConnectionName('chart');
    }


    /**
     * Sortowanie danych tablicy rosnąco po dacie
     * @param array $data
     * @param bool $sortByDay
     * @return array
     */
    private function sortByDate(array $data, bool $sortByDay = false): array
    {
        usort($data, function ($a, $b) use ($sortByDay) {
            return $sortByDay ? $a['day'] <=> $b['day'] : $a['name'] <=> $b['name'];
        });
        return $data;
    }
}
