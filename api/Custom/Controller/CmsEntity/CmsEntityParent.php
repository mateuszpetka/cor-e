<?php

namespace BlingCustom\Controller\CmsEntity;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Exception\AccessDeniedException;
use BlingApi\Core\Exception\EmptyResultsException;
use BlingApi\Core\Exception\InvalidArgumentException;
use BlingApi\Core\Field;
use BlingApi\Core\Helper\EntityHelper;
use BlingApi\Core\Model;
use BlingApi\Core\Request;
use BlingCustom\Model\BlingLanguage\BlingLanguage;
use BlingCustom\Model\CmsEntity\CmsEntity;
use BlingCustom\Model\CmsEntityField\CmsEntityField;
use BlingCustom\Model\CmsField\CmsField;
use BlingCustom\Model\CmsReservedName\CmsReservedName;
use BlingGenerator\Generator;
use Db\Exception\QueryDataException;
use Db\Manager as Q;
use Db\Query\InsertQuery;
use Db\Query\Sub\WhereQuery;
use Db\Query\UpdateQuery;
use ReflectionClass;
use Symfony\Component\HttpFoundation\Response;

class CmsEntityParent extends EntityController
{

    /**
     * CmsEntityParent constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        $this->module = [self::MODULE_BACKEND, self::MODULE_FRONTEND];
        $this->request = $request;
        $this->model = new CmsEntity();
    }

    /**
     * Wstawienie nowego obiektu do bazy danych oraz stworzenie tabeli encji.
     */
    public function insertAction(): void
    {
        /**
         * @var Field[]
         */
        $entity = $this->getEntityData();
        $insertQueryId = null;
        $generator = new Generator();
        $activeLanguages = Q::select(BlingLanguage::code)->distinct()->where(Q::eq(BlingLanguage::active, 1))->getColumn();

        if ($this->insertDataValid($entity)) {
            $entity['name']->setValue(preg_replace('/[^a-zA-z]/', '', toPascalCase($entity['name']->getValue())));
            $entity['position'] = new Field\IntegerField('posiiton');
            $entity['position']->setValue(0);

            // dodajemy nową encję
            $insertQueryId = $this->model->entityInsert($entity)['id'];

            if ($insertQueryId > 0) {
                $defaultFields = EntityHelper::getCreateStatementFields($entity, $activeLanguages);

                // tworzymy nową tabelę encji
                $createStatement = Q::create($entity['name']->getValue(), false);

                foreach ($defaultFields as $column => $field) {
                    $createStatement->addColumn($column, $field);
                }

                $createStatement->setPrimaryKey('id');
                $createStatement->execute();

                if (isset($entity['isSingle']) && $entity['isSingle']->getValue() === 1) {
                    $this->insertEmptyRecord($entity['name']->getValue());
                }

                if (isset($entity['isSluggable']) && $entity['isSluggable']->getValue() === 1) {
                    $this->insertEntityField(CmsEntityField::_tableName, [
                        'entityId'       => $insertQueryId,
                        'name'           => 'slug',
                        'displayName'    => 'Unikalny adres',
                        'fieldId'        => 1,
                        'isTranslatable' => 1,
                        'core'           => 2,
                    ]);
                }

                if (isset($entity['isHideable']) && $entity['isHideable']->getValue() === 1) {
                    $this->insertEntityField(CmsEntityField::_tableName, [
                        'entityId'           => $insertQueryId,
                        'name'               => 'status',
                        LANG . 'DisplayName' => 'Status rekordu',
                        LANG . 'Placeholder' => 'Status rekordu',
                        'fieldId'            => 11,
                    ]);
                }

                if (isset($entity['isSortable']) && $entity['isSortable']->getValue() === 1) {
                    $this->insertEntityField(CmsEntityField::_tableName, [
                        'entityId'    => $insertQueryId,
                        'name'        => 'position',
                        'displayName' => 'Pozycja rekordu na liście',
                        'fieldId'     => 10,
                    ]);

                    Q::alter($entity['name']->getValue())->addIndex('position')->execute();
                }

                if (isset($entity['driven'])) {
                    switch ($entity['driven']->getValue()) {
                        case 1:
                            $this->insertEntityField(
                                CmsEntityField::_tableName,
                                [
                                    'entityId'       => $insertQueryId,
                                    'name'           => 'userId',
                                    'displayName'    => 'ID właściciela rekordu',
                                    'fieldId'        => 8,
                                    'foreignLeft'    => $entity['name']->getValue() . '.`userId`',
                                    'foreignRight'   => 'BlingUser.`id`',
                                    'foreignColumns' => '["name", "email"]',
                                ]
                            );
                            break;
                        case 2:
                            $this->insertEntityField(
                                CmsEntityField::_tableName,
                                [
                                    'entityId'     => $insertQueryId,
                                    'name'         => 'childHash',
                                    'displayName'  => 'Hash właściciela rekordu',
                                    'fieldId'      => 4,
                                    'isSelectable' => 0,
                                ]
                            );
                            break;
                        default:
                            break;
                    }
                }

                if (isset($entity['isParentable']) && $entity['isParentable']->getValue() === 1) {
                    $this->insertEntityField(CmsEntityField::_tableName, [
                        'entityId'     => $insertQueryId,
                        'name'         => 'parentId',
                        'displayName'  => 'ID rodzica',
                        'fieldId'      => 8,
                        'foreignLeft'  => $entity['name']->getValue() . '.`parentId`',
                        'foreignRight' => $entity['name']->getValue() . '.`id`',
                    ]);
                }

                // tworzymy domyślne uprawnienia dla wszystkich grup
                EntityHelper::createDefaultPermissions($insertQueryId);

                $generator->go();
            }
        }

        $this->setResponseData(['entityId' => $insertQueryId]);
        $this->setResponseStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Aktualizacja encji o podanym ID.
     *
     * @param int $id
     * @throws \BlingApi\Core\Exception\AccessDeniedException
     */
    public function updateAction(int $id): void
    {
        $currentEntity = Q::select(CmsEntity::_all)->where(CmsEntity::id, '=', $id)->getAssocRow();
        $newEntity = $this->request->getData();
        $entityModelClassName = '\BlingCustom\Model\\' . $currentEntity['name'] . '\\' . $currentEntity['name'];
        $entityModel = new $entityModelClassName();
        $entityRow = [];

        if ($currentEntity['core'] === 1) {
            throw new AccessDeniedException('You are not allowed to edit core entities');
        }

        if (isset($newEntity['name'])) {
            $newEntity['name'] = preg_replace('/[^a-zA-z]/', '', toPascalCase($newEntity['name']));

            if ($newEntity['name'] !== $currentEntity['name'] && $this->insertDataValid($newEntity)) {
                Q::alter($currentEntity['name'])->renameTable($newEntity['name'])->execute();
            }
        } else {
            $newEntity['name'] = $currentEntity['name'];
        }

        foreach ($newEntity as $key => $param) {
            if (defined("BlingCustom\Model\CmsEntity\CmsEntity::$key")) {
                $entityRow[constant("BlingCustom\Model\CmsEntity\CmsEntity::$key")['fieldName']] = $param;
            }
        }

        // zmiana liczby rekordów: single / multi
        if (isset($newEntity['isSingle']) && $newEntity['isSingle'] !== $currentEntity['isSingle']) {
            if ($newEntity['isSingle'] === 1) {
                $isFirstRow = Q::select($entityModel::id)->from($newEntity['name'])->where(Q::eq($entityModel::id, 1))->getAssocRow();

                if ($isFirstRow === null) {
                    $this->insertEmptyRecord($newEntity['name']);
                }
            }
        }

        // zmiana sortowania rekordów
        if (isset($newEntity['isSortable']) && $newEntity['isSortable'] !== $currentEntity['isSortable']) {
            if ($newEntity['isSortable'] === 1) {
                Q::alter($newEntity['name'])->addColumn('position', 'INT(11) DEFAULT 0')->execute();
                Q::alter($newEntity['name'])->addIndex('position')->execute();
                Q::custom(sprintf('UPDATE %s SET `position` = id', $newEntity['name']))->execute();

                $this->insertEntityField(CmsEntityField::_tableName, [
                    'entityId'           => $id,
                    'name'               => 'position',
                    LANG . 'DisplayName' => 'Pozycja na liście',
                    LANG . 'Placeholder' => 'Pozycja na liście',
                    'fieldId'            => 11,
                ]);
            } else {
                Q::alter($newEntity['name'])->dropColumn('position')->execute();

                Q::delete(
                    CmsEntityField::_tableName, Q::where(CmsEntityField::entityId, '=', $id)->_and(CmsEntityField::name, '=', 'position')
                )->execute();
            }
        }

        // zmiana sposobu usuwania rekordów
        if (isset($newEntity['isSoftDelete']) && $newEntity['isSoftDelete'] !== $currentEntity['isSoftDelete']) {
            if ($newEntity['isSoftDelete'] === 1) {
                Q::alter($newEntity['name'])->addColumn('deleted', 'DATETIME DEFAULT NULL')->addColumn('deletedBy', 'INT(11) DEFAULT NULL')->execute();
            } else {
                Q::delete($entityModel::_tableName, Q::where(Q::notNull($entityModel::deleted)))->execute(false);
                Q::alter($newEntity['name'])->dropColumn('deleted')->dropColumn('deletedBy')->execute();
            }
        }

        // zmiana wersjonowania rekordów encji
        if (isset($newEntity['isVersionable']) && $newEntity['isVersionable'] !== $currentEntity['isVersionable']) {
            if ($newEntity['isVersionable'] === 1) {
                Q::alter($newEntity['name'])->addColumn('currentVersionRowId', 'INT(11) UNSIGNED NOT NULL')->execute();

                $this->insertEntityField(CmsEntityField::_tableName, [
                    'entityId'           => $id,
                    'name'               => 'currentVersionRowId',
                    LANG . 'DisplayName' => 'ID aktualnej wersji rekordu',
                    LANG . 'Placeholder' => 'ID aktualnej wersji rekordu',
                    'fieldId'            => 8,
                    'foreignLeft'        => $newEntity['name'] . '.`currentVersionRowId`',
                    'foreignRight'       => $newEntity['name'] . '.`id`',
                ]);

                if ($currentEntity['isHideable'] === 0) {
                    $newEntity['isHideable'] = 1;
                    $entityRow['isHideable'] = 1;
                }
            } else {
                $entityModelClassName = '\BlingCustom\Model\\' . $newEntity['name'] . '\\' . $newEntity['name'];
                /** @var Model $entityModel */
                $entityModel = new $entityModelClassName();

                Q::delete($newEntity['name'], Q::where($entityModel::getModelField('status'), '=', 3))->execute();
                Q::delete(CmsEntityField::_tableName, Q::where(CmsEntityField::entityId, '=', $id)->_and(CmsEntityField::name, '=', 'currentVersionRowId'))->execute();
                Q::alter($newEntity['name'])->dropColumn('currentVersionRowId')->execute();
            }
        }

        // zmiana widoczności rekordów
        if (isset($newEntity['isHideable']) && $newEntity['isHideable'] !== $currentEntity['isHideable']) {
            if ($newEntity['isHideable'] === 1) {
                Q::alter($newEntity['name'])->addColumn('status', 'SMALLINT(5) DEFAULT 0')->execute();

                $this->insertEntityField(CmsEntityField::_tableName, [
                    'entityId'           => $id,
                    'name'               => 'status',
                    LANG . 'DisplayName' => 'Status rekordu',
                    LANG . 'Placeholder' => 'Status rekordu',
                    'fieldId'            => 11,
                ]);
            } else {
                Q::alter($newEntity['name'])->dropColumn('status')->execute();

                Q::delete(
                    CmsEntityField::_tableName, Q::where(CmsEntityField::entityId, '=', $id)->_and(CmsEntityField::name, '=', 'status')
                )->execute();
            }
        }

        // zmiana obsługi sluga
        if (isset($newEntity['isSluggable']) && $newEntity['isSluggable'] !== $currentEntity['isSluggable']) {
            $activeLanguages = Q::select(BlingLanguage::code)->distinct()->where(Q::eq(BlingLanguage::active, 1))->getColumn();

            if ($newEntity['isSluggable'] === 1) {
                foreach ($activeLanguages as $lang) {
                    Q::alter($newEntity['name'])->addColumn($lang . 'Slug', 'VARCHAR(255) DEFAULT NULL')->execute();
                }

                $this->insertEntityField(CmsEntityField::_tableName, [
                    'entityId'           => $id,
                    'name'               => 'slug',
                    LANG . 'DisplayName' => 'Unikalny adres',
                    LANG . 'Placeholder' => 'Unikalny adres',
                    'fieldId'            => 1,
                    'isTranslatable'     => 1,
                    'core'               => 2,
                ]);
            } else {
                Q::delete(CmsEntityField::_tableName, Q::where(CmsEntityField::entityId, '=', $id)->_and(CmsEntityField::name, '=', 'slug'))->execute();

                foreach ($activeLanguages as $lang) {
                    Q::alter($newEntity['name'])->dropColumn($lang . 'Slug')->execute();
                }
            }
        }

        // zmiana user driven
        if (isset($newEntity['driven']) && $newEntity['driven'] !== $currentEntity['driven']) {
            $entityColumn = [
                'toAdd'    => ['name' => '', 'definition' => ''],
                'toRemove' => ['name' => ''],
            ];
            $entityField = [
                'toAdd'    => [],
                'toRemove' => '',
            ];

            if ($newEntity['driven'] === EntityDriven::user) {
                $entityColumn['toAdd']['name'] = 'userId';
                $entityColumn['toAdd']['definition'] = 'INT(11) UNSIGNED NOT NULL';

                $entityField['toAdd'] = [
                    'entityId'           => $id,
                    'name'               => 'userId',
                    LANG . 'DisplayName' => 'ID właściciela rekordu',
                    LANG . 'Placeholder' => 'ID właściciela rekordu',
                    'foreignLeft'        => $newEntity['name'] . '.`userId`',
                    'foreignRight'       => 'BlingUser.`id`',
                    'foreignColumns'     => '["name", "email"]',
                    'fieldId'            => 8,
                    'isSelectable'       => 0,
                ];
            } elseif ($newEntity['driven'] === EntityDriven::child) {
                $entityColumn['toAdd']['name'] = 'childHash';
                $entityColumn['toAdd']['definition'] = 'VARCHAR(50) NULL DEFAULT NULL';

                $entityField['toAdd'] = [
                    'entityId'           => $id,
                    'name'               => 'childHash',
                    LANG . 'DisplayName' => 'Driven hash',
                    LANG . 'Placeholder' => 'Driven hash',
                    'fieldId'            => 1,
                    'isSelectable'       => 0,
                ];
            } elseif ($newEntity['driven'] === EntityDriven::team) {
                $entityColumn['toAdd']['name'] = 'teamHash';
                $entityColumn['toAdd']['definition'] = 'VARCHAR(50) NULL DEFAULT NULL';

                $entityField['toAdd'] = [
                    'entityId'           => $id,
                    'name'               => 'teamHash',
                    LANG . 'DisplayName' => 'Driven hash',
                    LANG . 'Placeholder' => 'Driven hash',
                    'fieldId'            => 1,
                    'isSelectable'       => 0,
                ];
            } elseif ($newEntity['driven'] === EntityDriven::parent) {
                $entityColumn['toAdd']['name'] = 'parentHash';
                $entityColumn['toAdd']['definition'] = 'VARCHAR(50) NULL DEFAULT NULL';

                $entityField['toAdd'] = [
                    'entityId'           => $id,
                    'name'               => 'parentHash',
                    LANG . 'DisplayName' => 'Driven hash',
                    LANG . 'Placeholder' => 'Driven hash',
                    'fieldId'            => 1,
                    'isSelectable'       => 0,
                ];
            }

            if ($newEntity['driven'] !== EntityDriven::none) {
                Q::alter($newEntity['name'])->addColumn($entityColumn['toAdd']['name'], $entityColumn['toAdd']['definition'])->execute();
                $this->insertEntityField(CmsEntityField::_tableName, $entityField['toAdd']);
            }

            if ($currentEntity['driven'] > EntityDriven::none) {
                if ($currentEntity['driven'] === EntityDriven::user) {
                    $entityColumn['toRemove']['name'] = 'userId';
                    $entityField['toRemove'] = 'userId';
                } elseif ($currentEntity['driven'] === EntityDriven::child) {
                    $entityColumn['toRemove']['name'] = 'childHash';
                    $entityField['toRemove'] = 'childHash';
                } elseif ($currentEntity['driven'] === EntityDriven::team) {
                    $entityColumn['toRemove']['name'] = 'teamHash';
                    $entityField['toRemove'] = 'teamHash';
                } elseif ($currentEntity['driven'] === EntityDriven::parent) {
                    $entityColumn['toRemove']['name'] = 'parentHash';
                    $entityField['toRemove'] = 'parentHash';
                }

                Q::alter($newEntity['name'])->dropColumn($entityColumn['toRemove']['name'])->execute();
                Q::delete(
                    CmsEntityField::_tableName,
                    Q::where(CmsEntityField::entityId, '=', $id)->_and(CmsEntityField::name, '=', $entityField['toRemove'])
                )->execute();
            }
        }

        // zmiana parentable
        if (isset($newEntity['isParentable']) && $newEntity['isParentable'] !== $currentEntity['isParentable']) {
            if ($newEntity['isParentable'] === 1) {
                Q::alter($newEntity['name'])->addColumn('parentId', 'INT(11) UNSIGNED NOT NULL')->execute();

                $this->insertEntityField(CmsEntityField::_tableName, [
                    'entityId'           => $id,
                    'name'               => 'parentId',
                    LANG . 'DisplayName' => 'ID rodzica',
                    LANG . 'Placeholder' => 'ID rodzica',
                    'fieldId'            => 8,
                    'foreignLeft'        => $newEntity['name'] . '.`parentId`',
                    'foreignRight'       => $newEntity['name'] . '.`id`',
                ]);
            } else {
                Q::alter($newEntity['name'])->dropColumn('parentId')->execute();
                Q::delete(CmsEntityField::_tableName, Q::where(CmsEntityField::entityId, '=', $id)->_and(CmsEntityField::name, '=', 'parentId'))->execute();
            }
        }

        $updateQuery = new UpdateQuery(CmsEntity::_tableName, CmsEntity::_primaryKey, $entityRow);
        $updateQuery->setUpdateRule(new WhereQuery([CmsEntity::_primaryKey, '=', $id]));
        $updateQuery->execute();

        $generator = new Generator();
        $generator->go();
    }

    /**
     * Usunięcie tabeli encji o podanym ID z bazy danych wraz z informacją o niej.
     *
     * @param int $id
     * @throws \BlingApi\Core\Exception\EmptyResultsException
     * @throws \Db\Exception\QueryDataException
     */
    public function deleteAction(int $id): void
    {
        $entity = Q::select(CmsEntity::_all)->where(CmsEntity::id, '=', $id)->getAssocRow();

        if ($entity === null) {
            throw new EmptyResultsException('Entity object not found.');
        }

        if ($entity['core'] === 1) {
            throw new QueryDataException('Cannot delete core entity.');
        }

        $entityFieldIds = Q::select(CmsEntityField::id)->eq(CmsEntityField::entityId, $id)->getColumn();

        foreach ($entityFieldIds as $entityFieldId) {
            $cmsEntityField = Q::select(CmsEntityField::_all)->eq(CmsEntityField::id, $entityFieldId)->getRow();

            if ($cmsEntityField) {
                $fieldType = Q::select(CmsField::name)->eq(CmsField::id, $cmsEntityField[CmsEntityField::fieldId])->getOne();
                $fieldType = ucfirst($fieldType);

                $fieldClass = "\BlingApi\Core\Field\\" . $fieldType . 'Field';
                $field = new $fieldClass();
                $field->deleteColumn($cmsEntityField);

                Q::delete(CmsEntityField::_tableName, $entityFieldId)->result();
            }
        }

        // usunięcie pól, które odwoływały się do encji
        $selectFields = Q::select(CmsEntityField::id, CmsEntityField::name, CmsEntityField::entityId)
                         ->where(Q::like(CmsEntityField::foreignRight, $entity['name'] . '.', Q::LIKE_RIGHT))
                         ->getAssoc();

        foreach ($selectFields as $selectField) {
            $tableName = Q::select(CmsEntity::name)->eq(CmsEntity::id, $selectField['entityId'])->getOne();

            if ($tableName) {
                Q::alter($tableName)->dropColumn($selectField['name'])->execute();
            }

            Q::delete(CmsEntityField::_tableName, $selectField['id'])->execute();
        }

        if (Q::drop($entity['name'])->execute()) {
            $this->model->entityDelete($id);
        } else {
            throw new EmptyResultsException('Entity table not found.');
        }

        $generator = new Generator();
        $generator->go();
    }

    /**
     * Stworzenie kontrolera z danych zapisanych w szablonie encji.
     *
     * @throws InvalidArgumentException
     */
    public function createControllerAction(): void
    {
        $controllerCode = $this->request->get('content');
        $entityId = $this->request->get('entityId');

        if($controllerCode === null || $entityId === null) {
            throw new InvalidArgumentException('Invalid or missing parameters: content, entityId.');
        }

        $entityName = Q::select(CmsEntity::name)->eq(CmsEntity::id, $entityId)->getOne();

        if ($entityName === null) {
            throw new InvalidArgumentException(sprintf('Entity ID %s not found', $entityId));
        }

        EntityHelper::createController($entityName, $controllerCode);

        $this->setResponseStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Wstawienie pustego rekordu do wskazanej encji.
     *
     * @param string $entityName
     * @return bool
     */
    protected function insertEmptyRecord(string $entityName): bool
    {
        $userId = $this->request->user->getTokenData('id');

        Q::custom(sprintf('INSERT INTO %s (`id`, `added`, `addedBy`) VALUES (1, NOW(), %s)', $entityName, $userId))->execute();

        return true;
    }

    /**
     * Dodanie rekordu do CmsEntityField.
     *
     * @param string $table
     * @param array  $fieldRow
     * @return bool
     */
    protected function insertEntityField(string $table, array $fieldRow): bool
    {
        $activeLanguages = Q::select(BlingLanguage::code)->distinct()->where(Q::eq(BlingLanguage::active, 1))->getColumn();

        if (isset($fieldRow['placeholder'])) {
            foreach ($activeLanguages as $activeLanguage) {
                $fieldRow[$activeLanguage . 'Placeholder'] = $fieldRow['placeholder'];
            }

            unset($fieldRow['placeholder']);
        }

        if (isset($fieldRow['displayName'])) {
            foreach ($activeLanguages as $activeLanguage) {
                $fieldRow[$activeLanguage . 'DisplayName'] = $fieldRow['displayName'];
            }

            unset($fieldRow['displayName']);
        }

        if (isset($fieldRow['description'])) {
            foreach ($activeLanguages as $activeLanguage) {
                $fieldRow[$activeLanguage . 'Description'] = $fieldRow['description'];
            }

            unset($fieldRow['description']);
        }

        $fieldRow['foreignLeft'] = $fieldRow['foreignLeft'] ?? null;
        $fieldRow['foreignRight'] = $fieldRow['foreignRight'] ?? null;
        $fieldRow['isEditable'] = $fieldRow['isEditable'] ?? 1;
        $fieldRow['isSelectable'] = $fieldRow['isSelectable'] ?? 1;
        $fieldRow['isTranslatable'] = $fieldRow['isTranslatable'] ?? 0;
        $fieldRow['isRequired'] = $fieldRow['isRequired'] ?? 1;
        $fieldRow['position'] = $fieldRow['position'] ?? 0;

        if (!isset($fieldRow['core'])) {
            $fieldRow['core'] = 1;
        }

        $insertQuery = new InsertQuery($table, $fieldRow);
        $insertQuery->execute();

        return true;
    }

    /**
     * Walidacja danych wejściowych dla tworzonej encji.
     *
     * @param Field[] $parameters
     * @return bool
     * @throws \BlingApi\Core\Exception\InvalidArgumentException
     */
    protected function insertDataValid(array $parameters): bool
    {
        if (!isset($parameters['name'])) {
            throw new InvalidArgumentException('Entity name is mandatory');
        }

        $entityName = is_object($parameters['name']) ? $parameters['name']->getValue() : $parameters['name'];
        $driven = (isset($parameters['driven']) && is_object($parameters['driven'])) ? $parameters['driven']->getValue() : $parameters['driven'];

        $name = toPascalCase($entityName);
        $reservedKeywords = array_map('strtolower', Q::select(CmsReservedName::name)->getColumn());

        if (in_array(strtolower($name), $reservedKeywords, true)) {
            throw new InvalidArgumentException("Given entity name '$name' is reserved keyword. Please use another name.");
        }

        if (isset($parameters['driven'])) {
            $drivenReflection = new ReflectionClass(EntityDriven::class);

            if (!in_array($driven, array_values($drivenReflection->getConstants()), true)) {
                throw new InvalidArgumentException(sprintf("Given entity driven type '%s' is not supported.", $parameters['driven']));
            }
        }

        $currentEntity = Q::select(CmsEntity::id)->eq(CmsEntity::name, $name)->getOne();

        if ($currentEntity !== null) {
            throw new InvalidArgumentException('Entity with given name already exists.');
        }

        if (class_exists('BlingApi\Core\Controller\\' . $name . 'Controller')) {
            throw new InvalidArgumentException("Given name '$name' is reserved for core controllers.");
        }

        if (strlen($name) < 3) {
            throw new InvalidArgumentException('Entity name should contain min 3 characters.');
        }

        return true;
    }
}
