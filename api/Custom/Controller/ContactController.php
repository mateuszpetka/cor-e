<?php

namespace BlingCustom\Controller;

use BlingApi\Core\Config;
use BlingApi\Core\Controller;
use BlingApi\Core\Exception\ConfigException;
use BlingApi\Core\Exception\FormValidationException;
use BlingApi\Core\Mailer;
use BlingApi\Core\Request;
use BlingApi\Core\Template;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends Controller
{

    /**
     * ContactController constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        $this->module = [self::MODULE_FRONTEND, self::MODULE_BACKEND];
        $this->request = $request;
        $this->allowMethodsWithoutToken = ['mailToOwnerAction', 'mailToRecipientAction', 'templateMailAction'];
    }

    /**
     * Wysyłka email do właściciela strony.
     */
    public function mailToOwnerAction(): void
    {
        if (!$this->validDataMailToOwner()) {
            $this->setResponseStatus(false);
            $this->setResponseStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $data = $this->request->getData();
        $message = sprintf("Wiadomość od: %s (%s)\n\n", $data['contactName'], $data['contactEmail']) . $data['message'];

        $mail = new Mailer(Config::get('CONTACT_RECIPIENT_ADDRESS'), Config::get('CONTACT_SUBJECT'), $message);
        $mail->send();
    }

    /**
     * Wysyłka maila pod wskazany adres.
     */
    public function mailToRecipientAction(): void
    {
        if (!$this->validDataMailToRecipient()) {
            $this->setResponseStatus(false);
            $this->setResponseStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $content = $this->request->get('htmlContent') ?? (new Template($this->request->get('templateName'), $this->request->get('templateVars')))
                ->getHtml();

        $mail = new Mailer($this->request->get('recipientEmail'), $this->request->get('subject'), $content);

        if ($this->request->get('uploadedAttachments')) {
            $uploadDir = Config::get('UPLOAD_PUBLIC_DIR_PATH') . '/';

            foreach ($this->request->get('uploadedAttachments') as $attachmentPath) {
                if (file_exists($uploadDir . $attachmentPath)) {
                    $mail->addAttachment($uploadDir . $attachmentPath);
                }
            }
        }

        if ($this->request->files->get('incomingAttachments')) {
            /** @var UploadedFile $attachment */
            foreach ($this->request->files->get('incomingAttachments') as $attachment) {
                $mail->addAttachment($attachment->getRealPath(), $attachment->getClientOriginalName());
            }
        }

        $mail->send();
    }

    /**
     * Sprawdzenie poprawności wprowadzonych danych do wysyłki maila do właściciela.
     *
     * @return bool
     * @throws \BlingApi\Core\Exception\FormValidationException
     */
    private function validDataMailToOwner(): bool
    {
        if ((string)$this->request->get('message') === '') {
            throw new FormValidationException('Message cannot be empty');
        }
        if ((string)$this->request->get('contactName') === '') {
            throw new FormValidationException('Sender name cannot be empty');
        }
        if ((string)$this->request->get('contactEmail') === '') {
            throw new FormValidationException('Sender email address cannot be empty');
        }
        if (!filter_var($this->request->get('contactEmail'), FILTER_VALIDATE_EMAIL)) {
            throw new FormValidationException('Sender email address invalid format');
        }

        return true;
    }

    /**
     * Sprawdzenie poprawności wprowadzonych danych do wysyłki maila pod wskazany adres.
     *
     * @return bool
     * @throws FormValidationException
     * @throws ConfigException
     */
    private function validDataMailToRecipient(): bool
    {
        if (Config::get('SMTP_ENABLED') === false) {
            throw new ConfigException('SMTP protocol have to be configured in this send method.');
        }
        if ((string)$this->request->get('recipientEmail') === '') {
            throw new FormValidationException('Recipient email cannot be empty');
        }
        if ((string)$this->request->get('subject') === '') {
            throw new FormValidationException('Subject cannot be empty');
        }
        if ((string)$this->request->get('htmlContent') === '' && (string)$this->request->get('templateName') === '') {
            throw new FormValidationException('You should pass htmlContent or templateName');
        }
        if ((string)$this->request->get('htmlContent') === '' && (string)$this->request->get('templateName') === '') {
            throw new FormValidationException('HTML content cannot be empty');
        }
        if (!filter_var($this->request->get('recipientEmail'), FILTER_VALIDATE_EMAIL)) {
            throw new FormValidationException('Recipient email address invalid format');
        }

        return true;
    }
}