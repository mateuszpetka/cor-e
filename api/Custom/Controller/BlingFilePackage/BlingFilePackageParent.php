<?php

namespace BlingCustom\Controller\BlingFilePackage;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\BlingFilePackage\BlingFilePackage;

class BlingFilePackageParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new BlingFilePackage();
    }

}
