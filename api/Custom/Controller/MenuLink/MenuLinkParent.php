<?php

namespace BlingCustom\Controller\MenuLink;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\MenuLink\MenuLink;

class MenuLinkParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new MenuLink();
    }

}
