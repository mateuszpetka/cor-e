<?php

namespace BlingCustom\Controller\BlingTranslation;

use BlingApi\Core\Config;
use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Database;
use BlingApi\Core\Helper\Lang;
use BlingApi\Core\Request;
use BlingCustom\Model\BlingTranslation\BlingTranslation;
use Db\Manager as Q;

class BlingTranslationParent extends EntityController
{

    /**
     * TranslationsController constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        $this->module = [self::MODULE_BACKEND, self::MODULE_FRONTEND];
        $this->allowMethodsWithoutToken = ['getListAction'];
        $this->request = $request;
        $this->model = new BlingTranslation();
        $this->actionPermissions = ['canRead' => ['all']];

        if (Config::get('DEVELOPMENT_MODE') && $this->isCommonDatabaseRequest()) {
            Database::setCommonDatabaseConnection();
        }
    }

    /**
     * Pobranie listy tłumaczeń tylko dla języków wybranej aplikacji.
     */
    public function getListAction(): void
    {
        $appLanguages = $this->getApplicationLanguages();
        $this->request->controllerParameters->setCoreWhere(Q::in($this->model->getModelField('languageId'), array_keys($appLanguages)));
        $results = $this->model->entityGet($this->request->controllerParameters);

        $this->setResponseData($results);
    }

    /**
     * Pobranie listy tłumaczeń dla wszystkich języków.
     */
    public function allAction(): void
    {
        $otherTranslations = $this->model->entityGet($this->request->controllerParameters);

        if (Config::get('DEVELOPMENT_MODE') && $this->isCommonDatabaseRequest()) {
            Database::setCommonDatabaseConnection();
        }

        $this->request->controllerParameters->setCoreWhere(Q::in($this->model->getModelField('languageId'), Lang::getAdminLanguages()));
        $adminTranslations = $this->model->entityGet($this->request->controllerParameters);

        $this->setResponseData(array_merge($adminTranslations, $otherTranslations));
    }

    /**
     * Sprawdzenie, czy wywoływane zapytanie powinno zostać wysłane do wspólnej bazy.
     *
     * @return bool
     */
    private function isCommonDatabaseRequest(): bool
    {
        $action = $this->request->getAction();

        if ($action === 'allAction' || $action === 'getListAction') {
            $requestWhere = $this->request->get('where');

            if (isset($requestWhere['languageId']) && Lang::isAdminLanguage($requestWhere['languageId'])) {
                return true;
            }
        }

        if ($action === 'insertAction') {
            return Lang::isAdminLanguage($this->request->get('languageId'));
        }

        if ($action === 'multiInsertAction') {
            $languagesId = [];

            foreach ($this->request->getData() as $item) {
                if (!in_array($item['languageId'], $languagesId)) {
                    $languagesId[] = $item['languageId'];
                }
            }

            foreach ($languagesId as $languageId) {
                if (Lang::isAdminLanguage($languageId) === true) {
                    return true;
                }
            }
        }

        if ($action === 'updateAction') {
            return Lang::isAdminLanguage($this->request->get('languageId'));
        }

        if ($action === 'multiUpdateAction') {
            $languagesId = [];

            foreach ($this->request->getData() as $item) {
                $translation = $this->model->entityGetOne($item['id'], $this->request->controllerParameters);

                if (isset($translation['languageId']) && !in_array($translation['languageId'], $languagesId)) {
                    $languagesId[] = $translation['languageId'];
                }
            }

            foreach ($languagesId as $languageId) {
                if (Lang::isAdminLanguage($languageId) === true) {
                    return true;
                }
            }
        }

        if ($action === 'deleteAction') {
            $translation = $this->model->entityGetOne($this->request->getId(), $this->request->controllerParameters);

            if (empty($translation)) {
                return true;
            }

            if (Lang::isAdminLanguage($translation['languageId'])) {
                return true;
            }
        }

        if ($action === 'multiDeleteAction') {
            if ($this->request->query->has('id')) {
                $this->request->controllerParameters->setCoreWhere(Q::in($this->model->getModelField('id'), $this->request->query->get('id')));
                $translation = $this->model->entityGet($this->request->controllerParameters);
                $languagesId = [];

                if (empty($translation)) {
                    return true;
                }

                foreach ($translation as $item) {
                    if (!in_array($item['languageId'], $languagesId)) {
                        $languagesId[] = $item['languageId'];
                    }
                }

                foreach ($languagesId as $languageId) {
                    if (Lang::isAdminLanguage($languageId) === true) {
                        return true;
                    }
                }
            }

            if ($this->request->query->has('where')) {
                $where = $this->request->query->get('where');

                if (isset($where['languageId']) && $where['languageId']['value']) {
                    foreach ($where['languageId']['value'] as $languageId) {
                        if (Lang::isAdminLanguage($languageId) === true) {
                            return true;
                        }
                    }
                } else {
                    return true;
                }
            }
        }

        return false;
    }
}
