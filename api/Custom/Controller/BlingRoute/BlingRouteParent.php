<?php

namespace BlingCustom\Controller\BlingRoute;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;
use BlingCustom\Model\BlingRoute\BlingRoute;
use BlingGenerator\Generator;
use Symfony\Component\HttpFoundation\Response;

class BlingRouteParent extends EntityController
{

    /**
     * BlingRouteParent constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        $this->module = [self::MODULE_BACKEND, self::MODULE_FRONTEND];
        $this->request = $request;
        $this->model = new BlingRoute();
    }

    /**
     * Wstawienie nowego obiektu do bazy danych.
     */
    public function insertAction(): void
    {
        $record = $this->model->entityInsert($this->getEntityData());
        $generator = new Generator();
        $generator->go();

        $this->setResponseData(['id' => $record['id']]);
        $this->setResponseStatusCode(Response::HTTP_CREATED);
    }

}