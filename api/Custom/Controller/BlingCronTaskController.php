<?php

namespace BlingCustom\Controller;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Cache;
use BlingApi\Core\Cron;
use BlingApi\Core\Request;
use BlingCustom\Model\BlingCronTask\BlingCronTask;

class BlingCronTaskController extends EntityController
{

    /**
     * BlingCoreTaskController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->module = [self::MODULE_BACKEND];
        $this->request = $request;
        $this->model = new BlingCronTask();

        Cache::delete(Cron::CACHE_KEY);
    }

}