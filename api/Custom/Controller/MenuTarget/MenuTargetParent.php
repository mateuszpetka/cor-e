<?php

namespace BlingCustom\Controller\MenuTarget;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\MenuTarget\MenuTarget;

class MenuTargetParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new MenuTarget();
    }

}
