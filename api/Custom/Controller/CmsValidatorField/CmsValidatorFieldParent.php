<?php

namespace BlingCustom\Controller\CmsValidatorField;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\CmsValidatorField\CmsValidatorField;

class CmsValidatorFieldParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new CmsValidatorField();
    }

}
