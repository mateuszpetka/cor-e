<?php

namespace BlingCustom\Controller;

use BlingApi\Core\Controller;
use BlingApi\Core\Exception\InvalidArgumentException;
use BlingApi\Core\Request;
use BlingApi\Core\Config;
use Symfony\Component\HttpFoundation\Response;

class SettingsController extends Controller
{

    /**
     * SettingsController constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->module = [self::MODULE_BACKEND, self::MODULE_FRONTEND];
        $this->allowMethodsWithoutToken = ['getListAction', 'visibleAction'];
        $this->request = $request;
    }

    /**
     * Pobranie listy ustawień, które można edytować.
     */
    public function getListAction(): void
    {
        $settings = [];

        if ($this->request->user->getTokenData('groupId') < 10) {
            $settings = Config::getAllEditable();
        }

        $this->setResponseData($settings);
    }

    /**
     * Pobranie ustawień publicznych.
     */
    public function visibleAction(): void
    {
        $this->setResponseData(Config::getAllVisible());
    }

    /**
     * Aktualizacja wartości wskazanego ustawienia.
     *
     * @throws \BlingApi\Core\Exception\InvalidArgumentException
     */
    public function insertAction(): void
    {
        $key = $this->request->get('key');
        $value = $this->request->get('value');

        if ($key === null || $value === null) {
            throw new InvalidArgumentException('Missing key or value field.');
        }

        $currentValue = Config::get($key, true);

        if ($currentValue['level'] === 'PROTECTED') {
            throw new InvalidArgumentException('Modifying protected settings is not allowed');
        }

        if ($this->request->user->getTokenData('groupId') >= 10) {
            throw new InvalidArgumentException('You\'are not allowed to modify system settings');
        }

        $this->setResponseStatus(Config::set($key, $value));
        $this->setResponseStatusCode(Response::HTTP_CREATED);
    }

}