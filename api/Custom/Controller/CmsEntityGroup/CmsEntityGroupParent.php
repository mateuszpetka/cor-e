<?php

namespace BlingCustom\Controller\CmsEntityGroup;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\CmsEntityGroup\CmsEntityGroup;

class CmsEntityGroupParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new CmsEntityGroup();
    }

}
