<?php

namespace BlingCustom\Controller\CmsReservedName;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\CmsReservedName\CmsReservedName;

class CmsReservedNameParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new CmsReservedName();
    }

}
