<?php

namespace BlingCustom\Controller\BlingCustomization;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Exception\InvalidArgumentException;
use BlingApi\Core\Request;
use BlingCustom\Model\BlingCustomization\BlingCustomization;

class BlingCustomizationParent extends EntityController
{

    /**
     * BlingFileController constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        $this->module = [self::MODULE_BACKEND];
        $this->request = $request;
        $this->model = new BlingCustomization();
    }

    /**
     * Podmiana plików customizacji na serwerze.
     *
     * @throws InvalidArgumentException
     */
    public function logoAction(): void
    {
        /** @var  \Symfony\Component\HttpFoundation\File\UploadedFile $uploadedAsset */
        $uploadedAsset = $this->request->files->get('file');
        $uploadedAssetExt = strtolower($uploadedAsset->getClientOriginalExtension());
        $allowedExtensions = ['ico', 'png', 'gif', 'jpg', 'svg'];

        if (!in_array($uploadedAssetExt, $allowedExtensions)) {
            throw new InvalidArgumentException('Uploaded file have invalid extension.');
        }

        $filePath = BASE_DIR;
        $fileName = null;

        switch ($this->request->get('component')) {
            case 'admin/favicon':
                $filePath .= 'admin/';
                $fileName = 'favicon';
                break;
            case 'admin/big_logo':
                $filePath .= 'admin/assets/custom/';
                $fileName = 'big_logo';
                break;
            case 'admin/small_logo':
                $filePath .= 'admin/assets/custom/';
                $fileName = 'small_logo';
                break;
            case 'front/favicon':
                $filePath .= 'front/';
                $fileName = 'favicon';
                break;
            default:
                throw new InvalidArgumentException('Not supported component operation.');
        }

        foreach ($allowedExtensions as $extension) {
            $currentFilePath = $filePath . $fileName . '.' . $extension;

            if (file_exists($currentFilePath)) {
                rename($currentFilePath, $currentFilePath . '.bak');
            }
        }

        $uploadedAsset->move($filePath, $fileName . '.' . $uploadedAssetExt);
    }

}