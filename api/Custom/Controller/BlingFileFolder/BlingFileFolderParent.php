<?php

namespace BlingCustom\Controller\BlingFileFolder;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\BlingFileFolder\BlingFileFolder;

class BlingFileFolderParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new BlingFileFolder();
    }

}
