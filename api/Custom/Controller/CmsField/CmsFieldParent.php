<?php

namespace BlingCustom\Controller\CmsField;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\CmsField\CmsField;

class CmsFieldParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new CmsField();
    }

}
