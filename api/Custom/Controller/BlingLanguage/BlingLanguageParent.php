<?php

namespace BlingCustom\Controller\BlingLanguage;

use BlingApi\Core\Config;
use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Exception\InvalidArgumentException;
use BlingApi\Core\Request;
use BlingCustom\Model\BlingApp\BlingApp;
use BlingCustom\Model\BlingLanguage\BlingLanguage;
use BlingCustom\Model\CmsEntity\CmsEntity;
use BlingCustom\Model\CmsEntityField\CmsEntityField;
use BlingCustom\Model\CmsField\CmsField;
use BlingGenerator\Generator;
use Db\Manager as Q;
use Symfony\Component\HttpFoundation\Response;

class BlingLanguageParent extends EntityController
{

    /**
     * BlingTranslationController constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        $this->module = [self::MODULE_BACKEND, self::MODULE_FRONTEND];
        $this->allowMethodsWithoutToken = ['getListAction', 'allAction'];
        $this->request = $request;
        $this->model = new BlingLanguage();
        $this->actionPermissions = ['canRead' => ['all']];
    }

    /**
     * Pobranie listy języków tylko dla wybranej aplikacji.
     */
    public function getListAction(): void
    {
        $appId = Q::select(BlingApp::id)
            ->eq(BlingApp::name, strtolower($this->request->getModule()))
            ->getOne();

        $this->request->controllerParameters->setCoreWhere(Q::eq($this->model->getModelField('appId'), $appId));
        $results = $this->model->entityGet($this->request->controllerParameters);

        $this->setResponseData($results);
    }

    /**
     * Dodanie nowego języka do bazy.
     */
    public function insertAction(): void
    {
        $this->validateInsertData();

        $code = $this->request->get('code');

        $translatableFields = Q::select(CmsEntityField::name, CmsEntityField::fieldId, Q::alias(CmsEntity::name, 'entityName'))
            ->join(CmsEntity::id, CmsEntityField::entityId)
            ->eq(CmsEntityField::isTranslatable, 1)
            ->getAssoc();

        foreach ($translatableFields as $field) {
            $columnExist = Q::custom(sprintf("SHOW COLUMNS FROM %s LIKE '%s';", $field['entityName'], $code.ucfirst($field['name'])))
                ->getColumn();

            if (!$columnExist) {
                $fieldType = Q::select(CmsField::name, CmsField::nameForSql)->eq(CmsField::id, $field['fieldId'])->getRow();
                Q::alter($field['entityName'])->addColumn($code.ucfirst($field['name']), $fieldType['nameForSql'])->execute();
            }
        }

        $record = $this->model->entityInsert($this->getEntityData());

        $generator = new Generator();
        $generator->go();

        if (!in_array($record['code'], Config::get('ROUTE_LANGUAGES'))) {
            $currentLanguages = Config::get('ROUTE_LANGUAGES');
            $currentLanguages[] = $record['code'];

            Config::set('ROUTE_LANGUAGES', $currentLanguages, true);
        }

        $this->setResponseData(['id' => $record['id']]);
        $this->setResponseStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Usunięcie języka z bazy.
     *
     * @param int $id
     * @throws InvalidArgumentException
     */
    public function deleteAction(int $id): void
    {
        $code = Q::select(BlingLanguage::code)->distinct()->eq(BlingLanguage::id, $id)->getOne();

        if (!$code) {
            throw new InvalidArgumentException('Language not found.');
        }

        $languageApps = Q::select(BlingLanguage::appId)->eq(BlingLanguage::code, $code)->neq(BlingLanguage::id, $id)->getColumn();

        if (empty($languageApps)) {
            $translatableFields = Q::select(CmsEntityField::name, CmsEntityField::fieldId, Q::alias(CmsEntity::name, 'entityName'))
                ->join(CmsEntity::id, CmsEntityField::entityId)
                ->eq(CmsEntityField::isTranslatable, 1)
                ->getAssoc();

            foreach ($translatableFields as $field) {
                $columnExist = Q::custom(sprintf("SHOW COLUMNS FROM %s LIKE '%s';", $field['entityName'], $code.ucfirst($field['name'])))
                    ->getColumn();

                if ($columnExist) {
                    Q::alter($field['entityName'])->dropColumn($code.ucfirst($field['name']))->execute();
                }
            }

            if (in_array($code, Config::get('ROUTE_LANGUAGES'))) {
                $currentLanguages = array_diff(Config::get('ROUTE_LANGUAGES'), [$code]);

                Config::set('ROUTE_LANGUAGES', $currentLanguages, true);
            }
        }

        $this->model->entityDelete($id);
        $this->setResponseStatusCode(Response::HTTP_NO_CONTENT);
    }


    /**
     * Pobranie listy języków dla wszystkich aplikacji.
     */
    public function allAction(): void
    {
        $results = $this->model->entityGet($this->request->controllerParameters);

        $this->setResponseData($results);
    }

    /**
     * Walidacja danych podczas dodawania nowego języka.
     *
     * @throws \BlingApi\Core\Exception\InvalidArgumentException
     */
    protected function validateInsertData(): bool {
        $appId = $this->request->get('appId');
        $code = $this->request->get('code');

        if ((int)$appId < 1) {
            throw new InvalidArgumentException("Missing 'appId' parameter.");
        }
        if ((string)$code === '') {
            throw new InvalidArgumentException("Missing 'code' parameter.");
        }
        if ((string)$this->request->get('name') === '') {
            throw new InvalidArgumentException("Missing 'name' parameter.");
        }

        $langExists = Q::select(BlingLanguage::id)->eq(BlingLanguage::appId, $appId)->eq(BlingLanguage::code, $code)->getOne();

        if ($langExists > 0) {
            throw new InvalidArgumentException("Language with code '$code' already exists in app with ID: $appId");
        }

        return true;
    }
}