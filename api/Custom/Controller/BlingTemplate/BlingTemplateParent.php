<?php

namespace BlingCustom\Controller\BlingTemplate;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\BlingTemplate\BlingTemplate;

class BlingTemplateParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new BlingTemplate();
    }

}
