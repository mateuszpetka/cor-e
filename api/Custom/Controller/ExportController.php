<?php

namespace BlingCustom\Controller;

use BlingApi\Core\Controller;
use BlingApi\Core\Exception\EmptyResultsException;
use BlingApi\Core\Exporter;
use BlingApi\Core\Request;
use BlingApi\Core\Response\XlsResponse;

class ExportController extends Controller
{

    /**
     * ExportController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->module = [self::MODULE_BACKEND];
        $this->request = $request;

        $entityName = $this->getEntityName();
        $entityClassName = "\BlingCustom\Model\\$entityName\\$entityName";

        $this->model = new $entityClassName();
    }

    /**
     * Wyeksportowanie danych do pliku XLS.
     *
     * @throws EmptyResultsException
     */
    public function xlsAction()
    {
        $entityData = $this->model->entityGet($this->request->controllerParameters);

        if (isset($entityData['items'])) {
            $entityData = $entityData['items'];
        }

        if (empty($entityData)) {
            throw new EmptyResultsException('No data to export.');
        }

        $exporter = new Exporter($this->model, $this->getApplicationLanguages());
        $exporter->setData($entityData);
        $exporter->getFile('xls');

        return (new XlsResponse(ob_get_clean(), 'export_' . $this->model->getTableName() .'_' . date('Y-m-d')))->send();
    }

    /**
     * Pobranie nazwy encji z URL zapytania.
     *
     * @return string
     */
    private function getEntityName(): string
    {
        $pathParts = explode('/', $this->request->getPathInfo());

        return end($pathParts);
    }

}