<?php

namespace BlingCustom\Controller\BlingTemplateFile;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\BlingTemplateFile\BlingTemplateFile;

class BlingTemplateFileParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new BlingTemplateFile();
    }

}
