<?php

namespace BlingCustom\Controller\CmsEntityFieldGroup;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\CmsEntityFieldGroup\CmsEntityFieldGroup;

class CmsEntityFieldGroupParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new CmsEntityFieldGroup();
    }

}
