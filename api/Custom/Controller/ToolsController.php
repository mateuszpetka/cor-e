<?php

namespace BlingCustom\Controller;

use BlingApi\Core\Controller;
use BlingApi\Core\Generator;
use BlingApi\Core\DatabaseBackup;
use BlingApi\Core\Request;
use BlingApi\Core\Config;
use BlingCustom\Model\BlingLanguage\BlingLanguage;
use BlingCustom\Model\BlingTranslation\BlingTranslation;
use Db\Manager as Q;
use Db\Result\Row;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ToolsController extends Controller
{

    /**
     * ToolsController constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->module = [self::MODULE_BACKEND];
        $this->request = $request;
    }

    /**
     * Pobranie informacji wyświetlanych za pomocą phpinfo() jako tablica.
     */
    public function phpInfoAction(): void
    {
        ob_start();

        phpinfo();

        $s = ob_get_contents();
        ob_end_clean();

        $s = strip_tags($s, '<h2><th><td>');
        $s = preg_replace('/<th[^>]*>([^<]+)<\/th>/', '<info>\1</info>', $s);
        $s = preg_replace('/<td[^>]*>([^<]+)<\/td>/', '<info>\1</info>', $s);
        $t = preg_split('/(<h2[^>]*>[^<]+<\/h2>)/', $s, -1, PREG_SPLIT_DELIM_CAPTURE);
        $output = [];
        $count = count($t);
        $p1 = '<info>([^<]+)<\/info>';
        $p2 = '/' . $p1 . '\s*' . $p1 . '\s*' . $p1 . '/';
        $p3 = '/' . $p1 . '\s*' . $p1 . '/';

        for ($i = 1; $i < $count; $i++) {
            if (preg_match('/<h2[^>]*>([^<]+)<\/h2>/', $t[$i], $matchs)) {
                $name = trim($matchs[1]);
                $vals = explode("\n", $t[$i + 1]);

                foreach ($vals AS $val) {
                    if (preg_match($p2, $val, $matchs)) { // 3cols
                        $output[$name][trim($matchs[1])] = [trim($matchs[2]), trim($matchs[3])];
                    } elseif (preg_match($p3, $val, $matchs)) { // 2cols
                        $output[$name][trim($matchs[1])] = trim($matchs[2]);
                    }
                }
            }
        }

        $this->setResponseData($output);
    }

    /**
     * Uruchomienie wygenerowania plików.
     */
    public function generateFilesAction(): void
    {
        $generator = new Generator();
        $generator->go();
    }

    /**
     * Wyciągnięcie z plików HTML frontu fraz tłumaczeń i dodanie ich do bazy jeśli jeszcze nie istnieją.
     */
    public function updateTranslationKeysAction(): void
    {
        $htmlFiles = $this->findHtmlFiles(Config::get('GENERATOR_TS_LOCATION'));
        $fileKeywordsToInsert = $insertRows = [];
        $dbKeywords = Q::select(BlingTranslation::key)->distinct(true, true)->getAssoc();
        $languages = Q::select(BlingLanguage::id)->getColumn(0);

        if (!empty($dbKeywords)) {
            $dbKeywords = array_column($dbKeywords, 'key');
        }

        foreach ($htmlFiles as $file) {
            $fileContent = file_get_contents($file);

            preg_match_all('/{{(|\s{0,})(\'|")(.*)(\'|")(\s{0,})\|(\s{0,})translate(.*)}}/', $fileContent, $keywords);

            if (!empty($keywords[3])) {
                foreach ($keywords[3] as $keyword) {
                    if (!in_array($keyword, $dbKeywords)) {
                        $fileKeywordsToInsert[] = $keyword;
                    }
                }
            }
        }

        foreach ($fileKeywordsToInsert as $keyword) {
            foreach ($languages as $lang) {
                $row = new Row(BlingTranslation::_tableName);
                $row['languageId'] = $lang;
                $row['key'] = $keyword;
                $row->save();
            }
        }

        if (!empty($fileKeywordsToInsert)) {
            $this->setResponseData(['resultsInserted' => count($fileKeywordsToInsert)]);
            $this->setResponseStatusCode(Response::HTTP_CREATED);
        }
    }

    /**
     * Stworzenie kopii bazy danych, zapisanie jej na serwerze i zwrócenie pliku SQL do klienta.
     *
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     */
    public function getDbBackupAction(): void
    {
        $backup = new DatabaseBackup();
        $backupFilePath = $backup->create();
        $backupFilePathParts = explode('/', $backupFilePath);
        $backupFileName = end($backupFilePathParts);

        $response = new Response();
        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $backupFileName);

        $response->headers->set('Access-Control-Allow-Headers', 'Php-Auth-Digest, Php-Debug, origin, content-type, accept');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE');
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'application/sql');
        $response->setContent(file_get_contents($backup->create()));

        $response->send();
        exit;
    }

    /**
     * Przywrócenie bazy danych z wgrywanego pliku.
     *
     * @throws \RuntimeException
     */
    public function uploadDbBackupAction()
    {
        $backup = new DatabaseBackup();
        /** @var  \Symfony\Component\HttpFoundation\File\UploadedFile $uploadedFile */
        $uploadedFile = $this->request->files->get('file');

        if ($uploadedFile->getSize() > Config::get('UPLOAD_PUBLIC_MAX_FILE_SIZE_BYTES')) {
            throw new RuntimeException(
                'File is too big to upload. Max file size is: ' . Config::get('UPLOAD_PUBLIC_MAX_FILE_SIZE_BYTES') . 'bytes.'
            );
        }

        $fileContent = file_get_contents($uploadedFile->getPathname());

        $backup->recovery($fileContent);
    }

    /**
     * Odnalezienie wszystkich plików HTML z katalogu frontu.
     *
     * @param string $dir
     * @return array
     */
    private function findHtmlFiles(string $dir): array
    {
        $files = array_diff(scandir($dir, SCANDIR_SORT_ASCENDING), ['.', '..']);
        $htmlFiles = [[]];

        foreach ($files as $file) {
            if (is_dir("$dir/$file")) {
                $htmlFiles[] = $this->findHtmlFiles("$dir/$file");
            } else {
                $fileName = explode('.', $file);

                if (end($fileName) === 'html') {
                    $htmlFiles[] = [$dir . DIRECTORY_SEPARATOR . $file];
                }
            }
        }

        return array_merge(...$htmlFiles);
    }

}
