<?php

namespace BlingCustom\Controller\BlingTag;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\BlingTag\BlingTag;

class BlingTagParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new BlingTag();
    }

}
