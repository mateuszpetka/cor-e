<?php

namespace BlingCustom\Controller\BlingRouteSeo;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Exception\InvalidArgumentException;
use BlingApi\Core\Field;
use BlingApi\Core\Request;
use BlingCustom\Controller\CmsEntityField\CmsEntityField as CmsEntityFieldController;
use BlingCustom\Model\BlingRoute\BlingRoute;
use BlingCustom\Model\BlingRouteSeo\BlingRouteSeo;
use BlingCustom\Model\CmsEntityField\CmsEntityField;
use BlingCustom\Model\CmsField\CmsField;
use BlingGenerator\Generator;
use Db\Manager as Q;
use Symfony\Component\HttpFoundation\Response;

class BlingRouteSeoParent extends EntityController
{

    /**
     * BlingRouteSeoParent constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        $this->module = [self::MODULE_BACKEND];
        $this->request = $request;
        $this->model = new BlingRouteSeo();
    }

    /**
     * Wstawienie nowego obiektu do bazy danych.
     */
    public function insertAction(): void
    {
        $this->validateInsertData();

        $seoSettingsRow = $this->model->entityInsert($this->getEntityData());

        if (isset($seoSettingsRow['entityId'])) {
            $entityId = $seoSettingsRow['entityId'];

            if (!$this->entityHasSeoFields($entityId)) {
                $this->createSeoFields($entityId);
            }
        }

        $this->setResponseData($seoSettingsRow);
        $this->setResponseStatusCode(Response::HTTP_CREATED);
    }

    public function updateAction(int $id): void
    {
        $this->validateUpdateData($id);
        $this->model->entityUpdate($id, $this->getEntityData(), false, $this->request);

        if (isset($this->getEntityData()['entityId'])) {
            $entityId = $this->getEntityData()['entityId']->getValue();

            if (!$this->entityHasSeoFields($entityId)) {
                $this->createSeoFields($entityId);
            }
        }
    }

    /**
     * Sprawdzenie, czy podana encja zawiera pola SEO (title, description, image).
     *
     * @param int $entityId
     * @return bool
     */
    private function entityHasSeoFields(int $entityId): bool
    {
        $entityFields = Q::select(CmsEntityField::name)->eq(CmsEntityField::entityId, $entityId)->getColumn();

        return empty(array_diff(['seoTitle', 'seoDescription', 'seoImage'], $entityFields));
    }

    /**
     * Stwórzenie pól SEO dla wskazanej encji.
     *
     * @param int $entityId
     * @return bool
     */
    private function createSeoFields(int $entityId): bool
    {
        $fields = [
            [
                'entityId'       => $entityId,
                'fieldId'        => 1,
                'groupId'        => 1,
                'name'           => 'seoTitle',
                'displayName'    => 'SEO Title',
                'isSelectable'   => true,
                'isTranslatable' => true,
                'params'         => [],
            ],
            [
                'entityId'       => $entityId,
                'fieldId'        => 2,
                'groupId'        => 1,
                'name'           => 'seoDescription',
                'displayName'    => 'SEO Description',
                'isSelectable'   => true,
                'isTranslatable' => true,
                'params'         => [],
            ],
            [
                'entityId'    => $entityId,
                'fieldId'     => 14,
                'groupId'     => 1,
                'name'        => 'seoImage',
                'displayName' => 'SEO Image',
                'isSelectable'   => true,
                'params'         => [],
            ],
        ];

        $cmsEntityFieldController = new CmsEntityFieldController($this->request);

        foreach ($fields as $field) {
            $fieldType = ucfirst(Q::select(CmsField::name)->eq(CmsField::id, $field['fieldId'])->getOne());
            $fieldClass = "\BlingApi\Core\Field\\" . $fieldType . 'Field';
            /** @var Field $fieldObject */
            $fieldObject = new $fieldClass();

            $field = $cmsEntityFieldController->getEntityData($field);
            $field['name']->setValue(toCamelCase($field['name']->getValue()));
            $fieldObject->addColumn($field);
        }

        $generator = new Generator();
        $generator->go();

        return true;
    }

    /**
     * Walidacja danych podczas dodawania nowego rekordu.
     *
     * @throws InvalidArgumentException
     */
    protected function validateInsertData(): bool
    {
        $routeId = $this->request->get('routeId');
        $routeRow = Q::select(BlingRoute::id, BlingRoute::appId)->eq(BlingRoute::id, $routeId)->getRow();

        if ($routeRow === null) {
            throw new InvalidArgumentException(sprintf('Given route with ID: %s not found.', $routeId));
        }

        if ($routeRow[BlingRoute::appId] !== 2) {
            throw new InvalidArgumentException(sprintf('Given route with ID: %s is not exists in APP ID: 2.', $routeId));
        }

        if (Q::select(BlingRouteSeo::id)->eq(BlingRouteSeo::routeId, $routeId)->getOne() !== null) {
            throw new InvalidArgumentException(sprintf('SEO settings for route ID: %s already exists.', $routeId));
        }

        return true;
    }

    /**
     * Walidacja danych podczas aktualizowania rekordu.
     *
     * @param int $recordId
     * @throws InvalidArgumentException
     */
    protected function validateUpdateData(int $recordId): void
    {
        $routeId = $this->request->get('routeId');
        $routeRow = Q::select(BlingRoute::id, BlingRoute::appId)->eq(BlingRoute::id, $routeId)->getRow();

        if ($routeRow === null) {
            throw new InvalidArgumentException(sprintf('Given route with ID: %s not found.', $routeId));
        }

        if ($routeRow[BlingRoute::appId] !== 2) {
            throw new InvalidArgumentException(sprintf('Given route with ID: %s is not exists in APP ID: 2.', $routeId));
        }

        if (Q::select(BlingRouteSeo::id)->eq(BlingRouteSeo::id, $recordId)->getOne() === null) {
            throw new InvalidArgumentException(sprintf('Record with ID: %s not found.', $recordId));
        }
    }

}
