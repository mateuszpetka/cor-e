<?php

namespace BlingCustom\Controller\CmsEntityAction;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\CmsEntityAction\CmsEntityAction;

class CmsEntityActionParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new CmsEntityAction();
    }

}
