<?php

namespace BlingCustom\Controller\BlingRouteTranslation;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\BlingRouteTranslation\BlingRouteTranslation;

class BlingRouteTranslationParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new BlingRouteTranslation();
    }

}
