<?php

namespace BlingCustom\Controller\CmsEntityField;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Entity\EntityField;
use BlingApi\Core\Exception\EntityFieldNotFoundException;
use BlingApi\Core\Exception\EntityNotFoundException;
use BlingApi\Core\Exception\InvalidArgumentException;
use BlingApi\Core\Request;
use BlingCustom\Model\CmsEntity\CmsEntity;
use BlingCustom\Model\CmsField\CmsField;
use BlingCustom\Model\CmsEntityField\CmsEntityField;
use BlingCustom\Model\CmsReservedName\CmsReservedName;
use BlingGenerator\Generator;
use BlingGenerator\Helper;
use Db\Manager as Q;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CmsEntityFieldParent extends EntityController
{

    /**
     * CmsEntityFieldParent constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        $this->module = [self::MODULE_BACKEND, self::MODULE_FRONTEND];
        $this->request = $request;
        $this->model = new CmsEntityField();
    }

    /**
     * Wstawienie nowego pola encji do bazy danych oraz stworzenie kolumny w tabeli encji.
     *
     * @throws \BlingApi\Core\Exception\InvalidArgumentException
     * @throws \Exception
     */
    public function insertAction(): void
    {
        $fieldTypeId = $this->request->get('fieldId');
        $fieldType = Q::select(CmsField::name)->eq(CmsField::id, $fieldTypeId)->getOne();

        if (null === $fieldType) {
            throw new InvalidArgumentException('Field type not found for given fieldId param: ' . $fieldTypeId);
        }

        $fieldType = ucfirst($fieldType);
        $field = $this->getFieldObject($fieldType);

        $data = $this->getEntityData();

        if ($this->validateInsertData()) {
            $data['name']->setValue(Helper::toCamelCase($data['name']->getValue()));

            $this->setResponseData(['id' => $field->addColumn($data)]);
            $this->setResponseStatusCode(Response::HTTP_CREATED);

            $generator = new Generator();
            $generator->go();
        }
    }

    /**
     * Wstawienie jednocześnie wielu pól encji do bazy oraz stworzenie kolumn w tabelach encji.
     *
     * @throws \BlingApi\Core\Exception\InvalidArgumentException
     */
    public function multiInsertAction(): void
    {
        foreach ($this->request->getData() as $entityField) {
            $fieldTypeId = $entityField['fieldId'];
            $fieldType = Q::select(CmsField::name)->eq(CmsField::id, $fieldTypeId)->getOne();

            if ($fieldType === null) {
                throw new InvalidArgumentException('Field type not found for given fieldId param: ' . $fieldTypeId);
            }

            $fieldType = ucfirst($fieldType);
            $field = $this->getFieldObject($fieldType);

            if ($this->validateInsertData()) {
                $entityField['name']->setValue(Helper::toCamelCase($entityField['name']->getValue()));

                $field->addColumn($entityField);
            }
        }

        $generator = new Generator();
        $generator->go();
    }

    /**
     * Aktualizacja pola encji o podanym ID.
     *
     * @param int $id
     * @throws \Exception
     */
    public function updateAction(int $id): void
    {
        $entityFieldModel = new CmsEntityField();
        $entityFieldModelColumns = array_merge($entityFieldModel->publicColumns, $entityFieldModel->hiddenColumns);
        $entityFieldData = Q::select(...$entityFieldModelColumns)->eq(CmsEntityField::id, $id)->getAssocRow();

        if ($entityFieldData === null) {
            throw new EntityFieldNotFoundException($id);
        }

        $entity = Q::select(CmsEntity::_all)->eq(CmsEntity::id, $entityFieldData['entityId'])->getAssocRow();

        if ($entity === null) {
            throw new EntityNotFoundException($entityFieldData['entityId']);
        }

        if ($entityFieldData['isProtected'] === 1) {
            throw new InvalidArgumentException('You cannot modify protected core fields.');
        }

        $entityField = new EntityField($entityFieldData);
        $entityField->setEntity($entity);
        $entityField->setLanguages($this->getUniqueLanguages());
        $entityField->setNewData($this->getEntityData());
        $entityField->save();

        $generator = new Generator();
        $generator->go();
    }

    /**
     * Usunięcie pola encji o podanym ID z bazy danych oraz usunięcie kolumny w tabeli encji.
     *
     * @param int $id
     * @throws \Exception
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction(int $id): void
    {
        $cmsEntityField = Q::select(CmsEntityField::_all)->eq(CmsEntityField::id, $id)->getRow();

        if ($cmsEntityField) {
            $fieldType = Q::select(CmsField::name)->eq(CmsField::id, $cmsEntityField[CmsEntityField::fieldId])->getOne();
            $fieldType = ucfirst($fieldType);
            $field = $this->getFieldObject($fieldType);
            $field->deleteColumn($cmsEntityField);

            Q::delete(CmsEntityField::_tableName, $id)->result();
        } else {
            throw new NotFoundHttpException('Given CmsEntityField record not found.');
        }

        $generator = new Generator();
        $generator->go();
    }

    /**
     * Posortowanie pól encji
     *
     * @throws \BlingApi\Core\Exception\InvalidArgumentException
     */
    public function updatePositionsAction(): void
    {
        $elements = $this->request->getData();

        if (!isset($elements['conditions'])) {
            throw new InvalidArgumentException('Missing "conditions" parameter');
        }

        $changedRows = $this->model->changePositions($elements['rowId'], $elements['nextRowId'], $elements['conditions']);

        $this->setResponseData($changedRows);
    }

    /**
     * Walidacja danych wejściowych dla tworzonego pola encji.
     *
     * @return bool
     * @throws \BlingApi\Core\Exception\InvalidArgumentException
     */
    protected function validateInsertData(): bool
    {
        if ($this->request->get('name') === null || $this->request->get('entityId') === null) {
            throw new InvalidArgumentException('Fields name, entityId are mandatory.');
        }

        $reservedKeywords = array_map(function($keyword) {
            return strtolower($keyword);
        }, Q::select(CmsReservedName::name)->getColumn());

        if (in_array(strtolower($this->request->get('name')), $reservedKeywords)) {
            throw new InvalidArgumentException('Given field name "' . $this->request->get('name') .'" is reserved keyword. Please use another name.');
        }

        $row = Q::select(CmsEntityField::name)
            ->eq(CmsEntityField::name, $this->request->get('name'))
            ->eq(CmsEntityField::entityId, $this->request->get('entityId'))
            ->getRow();

        if (null !== $row) {
            throw new InvalidArgumentException('There must not be two fields with the same name in one entity.');
        }

        return true;
    }

    /**
     * @param string $fieldTypeName
     * @return mixed
     */
    private function getFieldObject(string $fieldTypeName)
    {
        $fieldClass = "\BlingApi\Core\Field\\" . $fieldTypeName . 'Field';

        return new $fieldClass();
    }

}
