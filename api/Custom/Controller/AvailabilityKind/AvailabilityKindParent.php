<?php

namespace BlingCustom\Controller\AvailabilityKind;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\AvailabilityKind\AvailabilityKind;

class AvailabilityKindParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new AvailabilityKind();
    }

}
