<?php

namespace BlingCustom\Controller\CmsSetting;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\CmsSetting\CmsSetting;

class CmsSettingParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new CmsSetting();
    }

}
