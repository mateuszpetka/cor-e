<?php

namespace BlingCustom\Controller\CmsEntityTemplate;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Database;
use BlingApi\Core\Request;
use BlingCustom\Model\CmsEntityTemplate\CmsEntityTemplate;

class CmsEntityTemplateParent extends EntityController
{

    /**
     * TranslationsController constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        $this->module = [self::MODULE_BACKEND, self::MODULE_FRONTEND];
        $this->request = $request;
        $this->model = new CmsEntityTemplate();

        Database::setCommonDatabaseConnection();
    }

}
