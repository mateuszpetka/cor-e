<?php

namespace BlingCustom\Controller\Maps;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\Maps\Maps;

class MapsParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new Maps();
    }

}
