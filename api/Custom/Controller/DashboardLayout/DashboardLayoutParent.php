<?php

namespace BlingCustom\Controller\DashboardLayout;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\DashboardLayout\DashboardLayout;

class DashboardLayoutParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new DashboardLayout();
    }

}
