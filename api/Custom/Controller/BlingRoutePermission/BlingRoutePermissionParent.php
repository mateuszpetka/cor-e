<?php

namespace BlingCustom\Controller\BlingRoutePermission;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\BlingRoutePermission\BlingRoutePermission;

class BlingRoutePermissionParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new BlingRoutePermission();
    }

}
