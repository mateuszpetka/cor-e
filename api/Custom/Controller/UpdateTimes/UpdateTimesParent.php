<?php

namespace BlingCustom\Controller\UpdateTimes;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\UpdateTimes\UpdateTimes;

class UpdateTimesParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new UpdateTimes();
    }

}
