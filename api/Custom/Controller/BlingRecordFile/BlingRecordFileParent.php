<?php

namespace BlingCustom\Controller\BlingRecordFile;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\BlingRecordFile\BlingRecordFile;

class BlingRecordFileParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new BlingRecordFile();
    }

}
