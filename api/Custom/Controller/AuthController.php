<?php

namespace BlingCustom\Controller;

use BlingApi\Core\Config;
use BlingApi\Core\Controller;
use BlingApi\Core\Helper\Security;
use BlingApi\Core\Helper\Url;
use BlingApi\Core\Mailer;
use BlingApi\Core\Request;
use BlingApi\Core\Template;
use BlingApi\Core\User\DatabaseProvider;
use BlingApi\Core\User\TokenProvider;
use BlingApi\Core\User\User;
use BlingCustom\Model\BlingUser\BlingUser;
use Db\Manager as Q;
use Db\Query\UpdateQuery;
use Symfony\Component\HttpFoundation\Response;


class AuthController extends Controller
{

    /**
     * Domyślne kraje do filtrowania danych.
     *
     * @var array
     */
    protected static $defaultFilterCountries = [
        [
            'name' => 'Germany',
            'iso'  => 'de',
        ],
        [
            'name' => 'France',
            'iso'  => 'fr',
        ],
        [
            'name' => 'Belgium',
            'iso'  => 'be',
        ],
        [
            'name' => 'Netherlands',
            'iso'  => 'nl',
        ],
    ];

    /**
     * Domyślne modele do filtrowania danych.
     *
     * @var array
     */
    protected static $defaultFilterModes = ['gfs', 'hres'];

    /**
     * Klasa użytkownika.
     *
     * @var User
     */
    protected $user;

    /**
     * AuthController constructor.
     *
     * @param \BlingApi\Core\Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->module = [self::MODULE_BACKEND, self::MODULE_FRONTEND];
        $this->allowWithoutToken = true;
        $this->request = $request;
        $this->user = new User();
    }

    /**
     * Aktywacja konta użytkownika.
     */
    public function activateAccountAction(): void
    {
        $email = $this->request->get('email');
        $token = $this->request->get('activateToken');

        $user = Q::select(BlingUser::id, BlingUser::name)->eq(BlingUser::email, $email)->eq(BlingUser::activateToken, $token)->getRow();

        if (!$user) {
            error('Invalid email or activateToken.', 82);
        }

        $user['active'] = 1;
        $user['activateToken'] = null;
        $user->save();

        $userName = explode(' ', $user['name']);
        $userName = $userName[0] ?? $userName;

        $templateData = [
            'baseUrl'   => Url::getFull(),
            'firstName' => $userName,
        ];
        $template = new Template('registerActivate', $templateData);

        $mailer = new Mailer($email, $template->getSubject(), $template->getHtml());
        $mailer->send();
    }

    /**
     * Usunięcie konta użytkownika.
     */
    public function deleteAccountAction(): void
    {
        $userId = $this->request->user->getTokenData('id');
        $password = $this->request->get('password');

        if ($userId < 1) {
            error('Invalid user ID.', 83);
        }

        $passwordHash = Q::select(BlingUser::password)->eq(BlingUser::id, $userId)->getOne();

        if ($passwordHash === null) {
            error('User not found.', 84);
        }

        if (!password_verify($password, $passwordHash)) {
            error('Invalid password.', 85);
        }

        Q::delete(BlingUser::_tableName, $userId)->result();

        $this->setResponseStatusCode(Response::HTTP_NO_CONTENT);
    }

    /**
     * Logowanie użytkownika - wygenerowanie tokena
     */
    public function loginAction(): void
    {
        $authIdentifier = $this->request->get('identifier');
        $authPassword = $this->request->get('password');
        $captchaToken = $this->request->get('captcha');
        $identifierField = $this->request->get('identifierField');

        if ($authIdentifier === null || $authPassword === null) {
            error('Please provide identifier and password data to get valid auth token', 86);
        }

        if ($this->user->lastInvalidLogin(Config::get('CAPTCHA_INVALID_LOGIN_TIME'), $this->request->getClientIp())) {
            if (!$captchaToken) {
                error('Captcha token required', 87, 403);
            }

            if (!Security::captchaTokenIsValid($captchaToken)) {
                error('Google said: no bots.', 88, 403);
            }
        }

        $userData = $this->user->login($authIdentifier, $authPassword, $this->request->getModule(), $this->request->getClientIp(), $identifierField);

        if ($this->request->getModule() === Controller::MODULE_BACKEND) {
            if (isset($userData['groupId']) && $userData['groupId'] >= 10) {
                error('You\'re not allowed to access %s module.', 89, 403, [Controller::MODULE_BACKEND]);
            }
        } elseif ($this->request->getModule() === Controller::MODULE_FRONTEND) {
            if (isset($userData['groupId']) && $userData['groupId'] <= 2) {
                error('You\'re not allowed to access %s module.', 89, 403, [Controller::MODULE_BACKEND]);
            }
        }

        if (!isset($userData['customFields']['filterCountries']) || $userData['customFields']['filterCountries'] === null) {
            $userData['customFields']['filterCountries'] = json_encode(self::$defaultFilterCountries);
        }
        if (!isset($userData['customFields']['filterModels']) || $userData['customFields']['filterModels'] === null) {
            $userData['customFields']['filterModels'] = json_encode(self::$defaultFilterModes);
        }

        $update = [
            'id'              => $userData['id'],
            'activeJwt'       => $userData['token'],
            'lastLogin'       => date('Y-m-d H:i:s'),
            'filterModels'    => $userData['customFields']['filterModels'],
            'filterCountries' => $userData['customFields']['filterCountries'],
        ];
        (new BlingUser())->save($update);

        $this->setResponseData($userData);
    }

    /**
     * Sprawdzenie, czy wyświetlenie captchy jest potrzebne.
     */
    public function captchaRequiredAction(): void
    {
        $response = [
            'captchaRequired' => $this->user->lastInvalidLogin(
                Config::get('CAPTCHA_INVALID_LOGIN_TIME'), $this->request->getClientIp()
            )
        ];

        $this->setResponseData($response);
    }

    /**
     * Zmiana danych użytkownika wywołana przez niego samego.
     */
    public function updateProfileAction(): void
    {
        $userData = $this->request->getData();
        $userData['id'] = $this->request->user->getTokenData('id');

        if (isset($userData['added'])) {
            unset($userData['added']);
        }
        if (isset($userData['addedBy'])) {
            unset($userData['addedBy']);
        }
        if (isset($userData['edited'])) {
            unset($userData['edited']);
        }
        if (isset($userData['editedBy'])) {
            unset($userData['editedBy']);
        }
        if (isset($userData['login'])) {
            unset($userData['login']);
        }
        if (isset($userData['active'])) {
            unset($userData['active']);
        }
        if (isset($userData['groupId'])) {
            unset($userData['groupId']);
        }

        $user = new User(new BlingUser);
        $user->update($userData);
    }

    /**
     * Pobranie tokena gościa.
     */
    public function getGuestTokenAction(): void
    {
        $guestToken = $this->user->getGuestToken();

        $this->setResponseData(['token' => $guestToken]);
    }

    /**
     * Wygenerowanie nowego tokena dla użytkownika.
     */
    public function regenerateTokenAction(): void
    {
        $tokenProvider = new TokenProvider();
        $databaseProvider = new DatabaseProvider();

        $user = $databaseProvider->getUserByField($this->request->user->getTokenData('id'), 'id');
        $userToken = $tokenProvider->getToken($user);

        $update = [
            'id'        => $user['id'],
            'activeJwt' => $userToken,
            'lastLogin' => date('Y-m-d H:i:s'),
        ];
        (new BlingUser())->save($update);

        $this->setResponseData(['newToken' => $userToken]);
    }

    /**
     * Rejestracja konta użytkownika.
     */
    public function registerAction(): void
    {
        // $captchaToken = $this->request->get('captcha');

        // if (!$captchaToken) {
        //     error('Missing captcha token.', 90);
        // }
        // if (!Security::captchaTokenIsValid($captchaToken)) {
        //     error('Google said: no bots.', 91, 403);
        // }

        $userData = [
            'groupId'       => 10,
            'login'         => (string) $this->request->get('login'),
            'name'          => (string) $this->request->get('name'),
            'email'         => (string) $this->request->get('email'),
            'password'      => $this->request->get('password'),
            'active'        => 0,
            'activateToken' => md5(time()),
            'added'         => Q::now(),
        ];

        if ($this->request->get('additionalData')) {
            $userData = array_merge($userData, $this->request->get('additionalData'));
        }

        $user = new User(new BlingUser());
        $user->create($userData);

        try {
            $templateData = [
                'name'     => $userData['name'],
                'login'    => $userData['login'],
                'password' => $userData['password'],
                'email'    =>  $userData['email'],
                'baseUrl'  => Url::getFull(),
                // 'activationUrl' => Url::getFull('') . '/accountVerify?email=' . $userData['email'] . '&activateToken=' . $userData['activateToken'],
            ];

            $template = new Template('register', $templateData);

            $mailer = new Mailer($userData['email'], $template->getSubject(), $template->getHtml());
            $mailer->send();
        } catch (\Exception $exception) {
            // dd($exception);
        }

        $this->setResponseStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Wysłanie linka do zmiany hasła na podany adres email.
     */
    public function resetPasswordAction(): void
    {
        $password = $this->request->get('password');
        $confirmPassword = $this->request->get('passwordConfirm');
        $resetToken = $this->request->get('token');

        if (!$password || !$confirmPassword || !$resetToken) {
            error('Missing password, passwordConfirm or token property.', 92);
        }

        if ($password !== $confirmPassword) {
            error('Both passwords must be equal.', 93);
        }

        $user = Q::select(BlingUser::_all)->eq(BlingUser::resetToken, $resetToken)->getAssocRow();

        if ($user === null) {
            error('User not found.', 94);
        }

        if (round((time() - strtotime($user['resetTokenTime'])) / 60) > 5) {
            error('Reset token was generated more than 5 minutes ago. Please generate new token.', 95);
        }

        $updateData = ['password' => $this->user->hash($password), 'resetToken' => null, 'resetTokenTime' => null];
        $updateCondition = Q::eq(BlingUser::id, $user['id']);
        $updateUser = new UpdateQuery(BlingUser::_tableName, BlingUser::id, $updateData, $updateCondition);
        $updateUser->execute();
    }

    /**
     * Zmiana zapomnianego hasła.
     */
    public function sendResetCodeAction(): void
    {
        $userEmail = $this->request->get('email');
        $captchaToken = $this->request->get('captcha');

        if (!$userEmail || !$captchaToken) {
            error('Missing email or captcha token.', 96);
        }

        if (Config::get('SMTP_ENABLED') === false) {
            error('SMTP protocol should be enable to allow send reset password code.', 97, 500);
        }

        if (!Security::captchaTokenIsValid($captchaToken)) {
            error('Google said: no bots.', 98, 403);
        }

        $user = $this->user->getUser($userEmail, 'email');

        if (empty($user)) {
            error('User not found.', 99, 404);
        }

        $resetToken = bin2hex(random_bytes(10));
        $updateUserTokenData = ['resetToken' => $resetToken, 'resetTokenTime' => date('Y-m-d H:i:s')];
        $updateUserTokenCondition = Q::eq(BlingUser::id, $user['id']);
        $updateUserToken = new UpdateQuery(BlingUser::_tableName, BlingUser::id, $updateUserTokenData, $updateUserTokenCondition);
        $updateUserToken->execute();

        $resetLink = Url::getFull('auth/recovery-password/' . $resetToken);
        $resetMessage = 'Click here to reset your password: <a href="' . $resetLink . '">' . $resetLink . '</a><br /><br />Link is active 5 minutes.';

        $mail = new Mailer($user['email'], 'Password change', $resetMessage);
        $mail->send();
    }
}
