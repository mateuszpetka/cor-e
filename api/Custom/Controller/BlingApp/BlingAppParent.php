<?php

namespace BlingCustom\Controller\BlingApp;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\BlingApp\BlingApp;

class BlingAppParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new BlingApp();
    }

}
