<?php

namespace BlingCustom\Controller\MenuPublicationTime;

use BlingApi\Core\Controller\EntityController;
use BlingApi\Core\Request;

use BlingCustom\Model\MenuPublicationTime\MenuPublicationTime;

class MenuPublicationTimeParent extends EntityController
{

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
        $this->model = new MenuPublicationTime();
    }

}
