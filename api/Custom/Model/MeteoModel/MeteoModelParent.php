<?php

namespace BlingCustom\Model\MeteoModel;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;

use BlingCustom\Model\Maps\Maps;

use Db\Manager as Q;

abstract class MeteoModelParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'MeteoModel', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'MeteoModel';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'MeteoModel', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'status',
        'position',
        'modelName',
        'modelDisplayedName',
        'modelAssignId',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'MeteoModel',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'MeteoModel',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'MeteoModel',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'MeteoModel',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'MeteoModel',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * status
     *
     * @var array
     */
    public const status = [
        'tableName'      => 'MeteoModel',
        'fieldName'      => 'status',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Pozycja rekordu na liście
     *
     * @var array
     */
    public const position = [
        'tableName'      => 'MeteoModel',
        'fieldName'      => 'position',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Model name
     *
     * @var array
     */
    public const modelName = [
        'tableName'      => 'MeteoModel',
        'fieldName'      => 'modelName',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
),
    ];

    /**
     * Model displayed name
     *
     * @var array
     */
    public const modelDisplayedName = [
        'tableName'      => 'MeteoModel',
        'fieldName'      => 'modelDisplayedName',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
),
    ];

    /**
     * modelAssignId
     *
     * @var array
     */
    public const modelAssignId = [
        'tableName'      => 'MeteoModel',
        'fieldName'      => 'modelAssignId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];


    /**
     * MeteoModelParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = true;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = true;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,
            self::status,
            self::position,
            self::modelName,
            self::modelDisplayedName,
            self::modelAssignId,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,

        ];
        $this->joinColumns = [
            Q::alias('SelectModelAssign.mapHtml', '__modelAssign_mapHtml'),
            Q::alias('SelectModelAssign.assignModelId', '__modelAssign_assignModelId'),
            Q::alias('SelectModelAssign.status', '__modelAssign_status'),

        ];
        $this->joinRelations = [
            ['left' => self::modelAssignId, 'right' => Maps::id, 'alias' => 'SelectModelAssign'],

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

