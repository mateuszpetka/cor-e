<?php

namespace BlingCustom\Model\BlingUser;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;

use BlingCustom\Model\BlingUserGroup\BlingUserGroup;
use BlingCustom\Model\BlingLanguage\BlingLanguage;
use BlingCustom\Model\BlingUser\BlingUser;

use Db\Manager as Q;

abstract class BlingUserParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'BlingUser', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'BlingUser';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'BlingUser', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'hydroState',
        'pricesState',
        'availabilityState',
        'tableState',
        'countryIso',
        'phonePrefix',
        'activateToken',
        'themeDisplay',
        'weatherState',
        'demandState',
        'groupId',
        'languageId',
        'parentId',
        'password',
        'resetToken',
        'resetTokenTime',
        'active',
        'street',
        'zipCode',
        'city',
        'phoneNumber',
        'filterCountries',
        'filterModels',
        'lastName',
        'firstName',
        'name',
        'login',
        'email',
        'avatar',
        'renewablesState',
        'activeJwt',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Hydro Chart State
     *
     * @var array
     */
    public const hydroState = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'hydroState',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
),
    ];

    /**
     * Prices Chart State
     *
     * @var array
     */
    public const pricesState = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'pricesState',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
),
    ];

    /**
     * Availability Chart State
     *
     * @var array
     */
    public const availabilityState = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'availabilityState',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
),
    ];

    /**
     * Data Table State
     *
     * @var array
     */
    public const tableState = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'tableState',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
),
    ];

    /**
     * Country iso 
     *
     * @var array
     */
    public const countryIso = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'countryIso',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Phone prefix
     *
     * @var array
     */
    public const phonePrefix = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'phonePrefix',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
),
    ];

    /**
     * Token do aktywowania konta
     *
     * @var array
     */
    public const activateToken = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'activateToken',
        'fieldClass'     => 'InputField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Theme Display
     *
     * @var array
     */
    public const themeDisplay = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'themeDisplay',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
  'fieldLength' => '127',
),
    ];

    /**
     * Weather Chart State
     *
     * @var array
     */
    public const weatherState = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'weatherState',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
),
    ];

    /**
     * Demand Chart State
     *
     * @var array
     */
    public const demandState = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'demandState',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
),
    ];

    /**
     * ID grupy użytkownika
     *
     * @var array
     */
    public const groupId = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'groupId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID języka użytkownika
     *
     * @var array
     */
    public const languageId = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'languageId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID rodzica
     *
     * @var array
     */
    public const parentId = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'parentId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Hasło użytkownka
     *
     * @var array
     */
    public const password = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'password',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Token do resetowania hasła
     *
     * @var array
     */
    public const resetToken = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'resetToken',
        'fieldClass'     => 'MiniinputField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czas wygenerowania tokena do resetowania hasła
     *
     * @var array
     */
    public const resetTokenTime = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'resetTokenTime',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy użytkownik jest aktywny
     *
     * @var array
     */
    public const active = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'active',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Street
     *
     * @var array
     */
    public const street = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'street',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Zip code
     *
     * @var array
     */
    public const zipCode = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'zipCode',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * City
     *
     * @var array
     */
    public const city = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'city',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Phone number
     *
     * @var array
     */
    public const phoneNumber = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'phoneNumber',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
  'fieldLength' => '127',
),
    ];

    /**
     * Lista państw dla początkowych filtrów 
     *
     * @var array
     */
    public const filterCountries = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'filterCountries',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
  'fieldLength' => '127',
),
    ];

    /**
     * Lista modeli dla początkowych filtrów 
     *
     * @var array
     */
    public const filterModels = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'filterModels',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
  'fieldLength' => '127',
),
    ];

    /**
     * Last name
     *
     * @var array
     */
    public const lastName = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'lastName',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * First name
     *
     * @var array
     */
    public const firstName = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'firstName',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Pełna nazwa użytkownika
     *
     * @var array
     */
    public const name = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'name',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Login użytkownika
     *
     * @var array
     */
    public const login = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'login',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Adres email użytkownika
     *
     * @var array
     */
    public const email = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'email',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Avatar użytkownka
     *
     * @var array
     */
    public const avatar = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'avatar',
        'fieldClass'     => 'ImageField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Renewables Chart State
     *
     * @var array
     */
    public const renewablesState = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'renewablesState',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
),
    ];

    /**
     * activeJwt
     *
     * @var array
     */
    public const activeJwt = [
        'tableName'      => 'BlingUser',
        'fieldName'      => 'activeJwt',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
  'fieldLength' => '127',
),
    ];


    /**
     * BlingUserParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = false;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = false;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,
            self::hydroState,
            self::pricesState,
            self::availabilityState,
            self::tableState,
            self::countryIso,
            self::phonePrefix,
            self::themeDisplay,
            self::weatherState,
            self::demandState,
            self::street,
            self::zipCode,
            self::city,
            self::phoneNumber,
            self::filterCountries,
            self::filterModels,
            self::lastName,
            self::firstName,
            self::renewablesState,
            self::activeJwt,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,
            self::activateToken,
            self::groupId,
            self::languageId,
            self::parentId,
            self::password,
            self::resetToken,
            self::resetTokenTime,
            self::active,
            self::name,
            self::login,
            self::email,
            self::avatar,

        ];
        $this->joinColumns = [
            Q::alias('SelectLanguage.appId', '__language_appId'),
            Q::alias('SelectLanguage.code', '__language_code'),
            Q::alias('SelectLanguage.active', '__language_active'),
            Q::alias('SelectLanguage.visible', '__language_visible'),
            Q::alias('SelectLanguage.isDefault', '__language_isDefault'),
            Q::alias('SelectLanguage.position', '__language_position'),
            Q::alias('SelectParent.firstName', '__parent_firstName'),
            Q::alias('SelectParent.lastName', '__parent_lastName'),
            Q::alias('SelectParent.street', '__parent_street'),
            Q::alias('SelectParent.zipCode', '__parent_zipCode'),
            Q::alias('SelectParent.city', '__parent_city'),
            Q::alias('SelectParent.countryIso', '__parent_countryIso'),
            Q::alias('SelectParent.phoneNumber', '__parent_phoneNumber'),
            Q::alias('SelectParent.filterCountries', '__parent_filterCountries'),
            Q::alias('SelectParent.filterModels', '__parent_filterModels'),
            Q::alias('SelectParent.phonePrefix', '__parent_phonePrefix'),
            Q::alias('SelectParent.themeDisplay', '__parent_themeDisplay'),
            Q::alias('SelectParent.renewablesState', '__parent_renewablesState'),
            Q::alias('SelectParent.weatherState', '__parent_weatherState'),
            Q::alias('SelectParent.demandState', '__parent_demandState'),
            Q::alias('SelectParent.hydroState', '__parent_hydroState'),
            Q::alias('SelectParent.pricesState', '__parent_pricesState'),
            Q::alias('SelectParent.availabilityState', '__parent_availabilityState'),
            Q::alias('SelectParent.tableState', '__parent_tableState'),
            Q::alias('SelectParent.activeJwt', '__parent_activeJwt'),

        ];
        $this->joinRelations = [
            ['left' => self::groupId, 'right' => BlingUserGroup::id, 'alias' => 'SelectGroup'],
            ['left' => self::languageId, 'right' => BlingLanguage::id, 'alias' => 'SelectLanguage'],
            ['left' => self::parentId, 'right' => BlingUser::id, 'alias' => 'SelectParent'],

        ];
        $this->autoFields = [
            self::resetTokenTime,
        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

