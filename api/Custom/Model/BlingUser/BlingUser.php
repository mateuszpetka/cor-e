<?php

namespace BlingCustom\Model\BlingUser;

use Db\Manager as Q;

class BlingUser extends BlingUserParent
{
  public function getLastToken($userId) {
    return Q::select(self::activeJwt)->eq(self::id, $userId)->getOne();
  }
}
