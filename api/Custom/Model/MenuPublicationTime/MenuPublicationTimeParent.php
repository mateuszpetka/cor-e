<?php

namespace BlingCustom\Model\MenuPublicationTime;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;


use Db\Manager as Q;

abstract class MenuPublicationTimeParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'MenuPublicationTime', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'MenuPublicationTime';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'MenuPublicationTime', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'publicationTimeTextarea',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'MenuPublicationTime',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'MenuPublicationTime',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'MenuPublicationTime',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'MenuPublicationTime',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'MenuPublicationTime',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Publication Time Textarea
     *
     * @var array
     */
    public const publicationTimeTextarea = [
        'tableName'      => 'MenuPublicationTime',
        'fieldName'      => 'publicationTimeTextarea',
        'fieldClass'     => 'EditorField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => array (
  'editorOptions' => 
  array (
  ),
),
    ];


    /**
     * MenuPublicationTimeParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = false;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = false;
        $this->isSingle      = true;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,
            self::publicationTimeTextarea,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,

        ];
        $this->joinColumns = [

        ];
        $this->joinRelations = [

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

