<?php

namespace BlingCustom\Model\BlingRouteSeo;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;

use BlingCustom\Model\BlingRoute\BlingRoute;
use BlingCustom\Model\CmsEntity\CmsEntity;

use Db\Manager as Q;

abstract class BlingRouteSeoParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'BlingRouteSeo', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'BlingRouteSeo';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'BlingRouteSeo', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'routeId',
        'entityId',
        'titleField',
        'descriptionField',
        'imageField',
        'titleValue',
        'descriptionValue',
        'imageValue',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'BlingRouteSeo',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'BlingRouteSeo',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'BlingRouteSeo',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'BlingRouteSeo',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'BlingRouteSeo',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID ścieżki routingu
     *
     * @var array
     */
    public const routeId = [
        'tableName'      => 'BlingRouteSeo',
        'fieldName'      => 'routeId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID encji przypisanej do ścieżki
     *
     * @var array
     */
    public const entityId = [
        'tableName'      => 'BlingRouteSeo',
        'fieldName'      => 'entityId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Nazwa encji i pola źródłowego dla meta title
     *
     * @var array
     */
    public const titleField = [
        'tableName'      => 'BlingRouteSeo',
        'fieldName'      => 'titleField',
        'fieldClass'     => 'TextareaField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Nazwa encji i pola źródłowego dla meta description
     *
     * @var array
     */
    public const descriptionField = [
        'tableName'      => 'BlingRouteSeo',
        'fieldName'      => 'descriptionField',
        'fieldClass'     => 'TextareaField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Nazwa encji i pola źródłowego dla meta image
     *
     * @var array
     */
    public const imageField = [
        'tableName'      => 'BlingRouteSeo',
        'fieldName'      => 'imageField',
        'fieldClass'     => 'TextareaField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Domyślna wartość dla meta title
     *
     * @var array
     */
    public const titleValue = [
        'tableName'      => 'BlingRouteSeo',
        'fieldName'      => 'titleValue',
        'fieldClass'     => 'TextareaField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Domyślna wartość dla meta description
     *
     * @var array
     */
    public const descriptionValue = [
        'tableName'      => 'BlingRouteSeo',
        'fieldName'      => 'descriptionValue',
        'fieldClass'     => 'TextareaField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Domyślna wartość dla meta image
     *
     * @var array
     */
    public const imageValue = [
        'tableName'      => 'BlingRouteSeo',
        'fieldName'      => 'imageValue',
        'fieldClass'     => 'TextareaField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];


    /**
     * BlingRouteSeoParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = false;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = false;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,
            self::routeId,
            self::entityId,
            self::titleField,
            self::descriptionField,
            self::imageField,
            self::titleValue,
            self::descriptionValue,
            self::imageValue,

        ];
        $this->joinColumns = [
            Q::alias('SelectEntity.name', '__entity_name'),
            Q::alias('SelectEntity.groupId', '__entity_groupId'),
            Q::alias('SelectEntity.position', '__entity_position'),
            Q::alias('SelectEntity.' . LANG . 'Description', '__entity_description'),
            Q::alias('SelectEntity.' . LANG . 'DisplayName', '__entity_displayName'),
            Q::alias('SelectEntity.isSortable', '__entity_isSortable'),
            Q::alias('SelectEntity.isSoftDelete', '__entity_isSoftDelete'),
            Q::alias('SelectEntity.isVersionable', '__entity_isVersionable'),
            Q::alias('SelectEntity.isHideable', '__entity_isHideable'),
            Q::alias('SelectEntity.isSingle', '__entity_isSingle'),
            Q::alias('SelectEntity.isSluggable', '__entity_isSluggable'),
            Q::alias('SelectEntity.isParentable', '__entity_isParentable'),
            Q::alias('SelectEntity.driven', '__entity_driven'),
            Q::alias('SelectEntity.type', '__entity_type'),
            Q::alias('SelectEntity.typeForm', '__entity_typeForm'),
            Q::alias('SelectEntity.typeView', '__entity_typeView'),
            Q::alias('SelectEntity.core', '__entity_core'),
            Q::alias('SelectEntity.isStreamable', '__entity_isStreamable'),

        ];
        $this->joinRelations = [
            ['left' => self::routeId, 'right' => BlingRoute::id, 'alias' => 'SelectRoute'],
            ['left' => self::entityId, 'right' => CmsEntity::id, 'alias' => 'SelectEntity'],

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

