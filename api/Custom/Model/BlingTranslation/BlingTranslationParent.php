<?php

namespace BlingCustom\Model\BlingTranslation;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;

use BlingCustom\Model\BlingLanguage\BlingLanguage;

use Db\Manager as Q;

abstract class BlingTranslationParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'BlingTranslation', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'BlingTranslation';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'BlingTranslation', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'languageId',
        'translationKey',
        'translation',
        'description',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'BlingTranslation',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'BlingTranslation',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'BlingTranslation',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'BlingTranslation',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'BlingTranslation',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID języka
     *
     * @var array
     */
    public const languageId = [
        'tableName'      => 'BlingTranslation',
        'fieldName'      => 'languageId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * String do przetłumaczenia
     *
     * @var array
     */
    public const translationKey = [
        'tableName'      => 'BlingTranslation',
        'fieldName'      => 'translationKey',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Przetłumaczony string
     *
     * @var array
     */
    public const translation = [
        'tableName'      => 'BlingTranslation',
        'fieldName'      => 'translation',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Opis tłumaczenia
     *
     * @var array
     */
    public const description = [
        'tableName'      => 'BlingTranslation',
        'fieldName'      => 'description',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];


    /**
     * BlingTranslationParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = false;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = false;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,
            self::languageId,
            self::translationKey,
            self::translation,
            self::description,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,

        ];
        $this->joinColumns = [
            Q::alias('SelectLanguage.appId', '__language_appId'),
            Q::alias('SelectLanguage.code', '__language_code'),
            Q::alias('SelectLanguage.active', '__language_active'),
            Q::alias('SelectLanguage.visible', '__language_visible'),
            Q::alias('SelectLanguage.isDefault', '__language_isDefault'),
            Q::alias('SelectLanguage.position', '__language_position'),

        ];
        $this->joinRelations = [
            ['left' => self::languageId, 'right' => BlingLanguage::id, 'alias' => 'SelectLanguage'],

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

