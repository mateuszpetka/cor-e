<?php

namespace BlingCustom\Model\BlingMetaTag;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;

use BlingCustom\Model\BlingApp\BlingApp;

use Db\Manager as Q;

abstract class BlingMetaTagParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'BlingMetaTag', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'BlingMetaTag';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'BlingMetaTag', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'appId',
        'name',
        'value',
        'location',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'BlingMetaTag',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'BlingMetaTag',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'BlingMetaTag',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'BlingMetaTag',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'BlingMetaTag',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID aplikacji
     *
     * @var array
     */
    public const appId = [
        'tableName'      => 'BlingMetaTag',
        'fieldName'      => 'appId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Nazwa znacznika
     *
     * @var array
     */
    public const name = [
        'tableName'      => 'BlingMetaTag',
        'fieldName'      => 'name',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Wartość
     *
     * @var array
     */
    public const value = [
        'tableName'      => 'BlingMetaTag',
        'fieldName'      => 'value',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Położenie
     *
     * @var array
     */
    public const location = [
        'tableName'      => 'BlingMetaTag',
        'fieldName'      => 'location',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];


    /**
     * BlingMetaTagParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = false;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = false;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,
            self::appId,
            self::name,
            self::value,
            self::location,

        ];
        $this->joinColumns = [

        ];
        $this->joinRelations = [
            ['left' => self::appId, 'right' => BlingApp::id, 'alias' => 'SelectApp'],

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

