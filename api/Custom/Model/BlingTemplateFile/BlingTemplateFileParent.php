<?php

namespace BlingCustom\Model\BlingTemplateFile;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;

use BlingCustom\Model\BlingTemplate\BlingTemplate;
use BlingCustom\Model\BlingFile\BlingFile;

use Db\Manager as Q;

abstract class BlingTemplateFileParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'BlingTemplateFile', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'BlingTemplateFile';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'BlingTemplateFile', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'templateId',
        'fileId',
        'position',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'BlingTemplateFile',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'BlingTemplateFile',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'BlingTemplateFile',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'BlingTemplateFile',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'BlingTemplateFile',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID szablonu
     *
     * @var array
     */
    public const templateId = [
        'tableName'      => 'BlingTemplateFile',
        'fieldName'      => 'templateId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID pliku
     *
     * @var array
     */
    public const fileId = [
        'tableName'      => 'BlingTemplateFile',
        'fieldName'      => 'fileId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Pozycja na liście
     *
     * @var array
     */
    public const position = [
        'tableName'      => 'BlingTemplateFile',
        'fieldName'      => 'position',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];


    /**
     * BlingTemplateFileParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = true;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = false;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,
            self::templateId,
            self::fileId,
            self::position,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,

        ];
        $this->joinColumns = [
            Q::alias('SelectTemplate.name', '__template_name'),
            Q::alias('SelectTemplate.' . LANG . 'DisplayName', '__template_displayName'),
            Q::alias('SelectTemplate.' . LANG . 'Description', '__template_description'),
            Q::alias('SelectTemplate.' . LANG . 'Tags', '__template_tags'),
            Q::alias('SelectTemplate.' . LANG . 'HtmlContent', '__template_htmlContent'),
            Q::alias('SelectTemplate.kind', '__template_kind'),
            Q::alias('SelectTemplate.parentId', '__template_parentId'),
            Q::alias('SelectTemplate.' . LANG . 'Subject', '__template_subject'),

        ];
        $this->joinRelations = [
            ['left' => self::templateId, 'right' => BlingTemplate::id, 'alias' => 'SelectTemplate'],
            ['left' => self::fileId, 'right' => BlingFile::id, 'alias' => 'SelectFile'],

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

