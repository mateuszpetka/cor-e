<?php

namespace BlingCustom\Model\CmsEntityField;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;


use Db\Manager as Q;

abstract class CmsEntityFieldParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'CmsEntityField', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'CmsEntityField';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'CmsEntityField', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'position',
        'entityId',
        'fieldId',
        'groupId',
        'name',
        'isEditable',
        'isSelectable',
        'isTranslatable',
        'isRequired',
        'isIdentifier',
        'placeholder',
        'foreignLeft',
        'foreignRight',
        'foreignColumns',
        'params',
        'displayName',
        'description',
        'core',
        'isProtected',
        'showOnList',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Pozycja na liście
     *
     * @var array
     */
    public const position = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'position',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID encji
     *
     * @var array
     */
    public const entityId = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'entityId',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID pola encji
     *
     * @var array
     */
    public const fieldId = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'fieldId',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID grupy pól
     *
     * @var array
     */
    public const groupId = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'groupId',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Nazwa pola
     *
     * @var array
     */
    public const name = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'name',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy pole jest edytowalne
     *
     * @var array
     */
    public const isEditable = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'isEditable',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy pole może zostać pobrane
     *
     * @var array
     */
    public const isSelectable = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'isSelectable',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy pole może być tłumaczone
     *
     * @var array
     */
    public const isTranslatable = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'isTranslatable',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy pole jest wymagane
     *
     * @var array
     */
    public const isRequired = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'isRequired',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy pole wchodzi w skład przyjaznego identyfikatora rekordu
     *
     * @var array
     */
    public const isIdentifier = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'isIdentifier',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Placeholder pola
     *
     * @var array
     */
    public const placeholder = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => LANG . 'Placeholder',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => true,
        'params'         => [],
    ];

    /**
     * Pole dla warunku z dołączanej encji
     *
     * @var array
     */
    public const foreignLeft = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'foreignLeft',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Pole dla warunku z encji do której jest dołączane
     *
     * @var array
     */
    public const foreignRight = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'foreignRight',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Kolumny identyfikujące złączone tabele
     *
     * @var array
     */
    public const foreignColumns = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'foreignColumns',
        'fieldClass'     => 'JsonField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Dodatkowe parametry pola
     *
     * @var array
     */
    public const params = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'params',
        'fieldClass'     => 'JsonField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Nazwa wyświetlana pola encji
     *
     * @var array
     */
    public const displayName = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => LANG . 'DisplayName',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => true,
        'params'         => [],
    ];

    /**
     * Opis pola encji
     *
     * @var array
     */
    public const description = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => LANG . 'Description',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => true,
        'params'         => [],
    ];

    /**
     * Czy jest to pole systemowe
     *
     * @var array
     */
    public const core = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'core',
        'fieldClass'     => 'CheckboxField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy pole jest chronione
     *
     * @var array
     */
    public const isProtected = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'isProtected',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy wyświetlać pole w widoku listy?
     *
     * @var array
     */
    public const showOnList = [
        'tableName'      => 'CmsEntityField',
        'fieldName'      => 'showOnList',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];


    /**
     * CmsEntityFieldParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = true;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = false;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,
            self::position,
            self::entityId,
            self::fieldId,
            self::groupId,
            self::name,
            self::isEditable,
            self::isSelectable,
            self::isTranslatable,
            self::isRequired,
            self::isIdentifier,
            Q::alias(self::placeholder, 'placeholder'),
            self::foreignLeft,
            self::foreignRight,
            self::foreignColumns,
            self::params,
            Q::alias(self::displayName, 'displayName'),
            Q::alias(self::description, 'description'),
            self::core,
            self::isProtected,
            self::showOnList,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,

        ];
        $this->joinColumns = [

        ];
        $this->joinRelations = [

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

