<?php

namespace BlingCustom\Model\CmsEntity;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;

use BlingCustom\Model\CmsEntityGroup\CmsEntityGroup;

use Db\Manager as Q;

abstract class CmsEntityParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'CmsEntity', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'CmsEntity';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'CmsEntity', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'name',
        'groupId',
        'position',
        'description',
        'displayName',
        'isSortable',
        'isSoftDelete',
        'isVersionable',
        'isHideable',
        'isSingle',
        'isSluggable',
        'isParentable',
        'driven',
        'type',
        'typeForm',
        'typeView',
        'core',
        'isStreamable',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Nazwa encji
     *
     * @var array
     */
    public const name = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'name',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID grupy
     *
     * @var array
     */
    public const groupId = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'groupId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Pozycja na liście
     *
     * @var array
     */
    public const position = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'position',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Opis encji
     *
     * @var array
     */
    public const description = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => LANG . 'Description',
        'fieldClass'     => 'EditorField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => true,
        'params'         => [],
    ];

    /**
     * Nazwa wyświetlana
     *
     * @var array
     */
    public const displayName = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => LANG . 'DisplayName',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => true,
        'params'         => [],
    ];

    /**
     * Czy encja jest sortowalna
     *
     * @var array
     */
    public const isSortable = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'isSortable',
        'fieldClass'     => 'CheckboxField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy encja usuwa jest miękko
     *
     * @var array
     */
    public const isSoftDelete = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'isSoftDelete',
        'fieldClass'     => 'CheckboxField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy archiwizować aktualizowane encje
     *
     * @var array
     */
    public const isVersionable = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'isVersionable',
        'fieldClass'     => 'CheckboxField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy encja może być ukrywana
     *
     * @var array
     */
    public const isHideable = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'isHideable',
        'fieldClass'     => 'CheckboxField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy encja ma zawierać tylko jeden rekord
     *
     * @var array
     */
    public const isSingle = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'isSingle',
        'fieldClass'     => 'CheckboxField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy encja używa sluga
     *
     * @var array
     */
    public const isSluggable = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'isSluggable',
        'fieldClass'     => 'CheckboxField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy encja zawiera strukturę drzewa
     *
     * @var array
     */
    public const isParentable = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'isParentable',
        'fieldClass'     => 'CheckboxField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy encja jest lub typ DRIVEN
     *
     * @var array
     */
    public const driven = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'driven',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Typ encji (CMS)
     *
     * @var array
     */
    public const type = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'type',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Typ widoku formularza (CMS)
     *
     * @var array
     */
    public const typeForm = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'typeForm',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Typ widoku listy (CMS)
     *
     * @var array
     */
    public const typeView = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'typeView',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy jest to encja systemowa
     *
     * @var array
     */
    public const core = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'core',
        'fieldClass'     => 'CheckboxField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy encja może emitować zmiany rekordów
     *
     * @var array
     */
    public const isStreamable = [
        'tableName'      => 'CmsEntity',
        'fieldName'      => 'isStreamable',
        'fieldClass'     => 'CheckboxField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];


    /**
     * CmsEntityParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = false;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = false;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,
            self::name,
            self::groupId,
            self::position,
            Q::alias(self::description, 'description'),
            Q::alias(self::displayName, 'displayName'),
            self::isSortable,
            self::isSoftDelete,
            self::isVersionable,
            self::isHideable,
            self::isSingle,
            self::isSluggable,
            self::isParentable,
            self::driven,
            self::type,
            self::typeForm,
            self::typeView,
            self::core,
            self::isStreamable,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,

        ];
        $this->joinColumns = [
            Q::alias('SelectGroup.name', '__group_name'),
            Q::alias('SelectGroup.' . LANG . 'DisplayName', '__group_displayName'),
            Q::alias('SelectGroup.position', '__group_position'),
            Q::alias('SelectGroup.icon', '__group_icon'),

        ];
        $this->joinRelations = [
            ['left' => self::groupId, 'right' => CmsEntityGroup::id, 'alias' => 'SelectGroup'],

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

