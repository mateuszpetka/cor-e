<?php

namespace BlingCustom\Model\CmsEntityAction;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;

use BlingCustom\Model\CmsEntity\CmsEntity;

use Db\Manager as Q;

abstract class CmsEntityActionParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'CmsEntityAction', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'CmsEntityAction';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'CmsEntityAction', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'entityId',
        'actionPath',
        'displayName',
        'description',
        'icon',
        'target',
        'params',
        'code',
        'position',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'CmsEntityAction',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'CmsEntityAction',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'CmsEntityAction',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'CmsEntityAction',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'CmsEntityAction',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID encji do której przypisana jest akcja
     *
     * @var array
     */
    public const entityId = [
        'tableName'      => 'CmsEntityAction',
        'fieldName'      => 'entityId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Ścieżka do akcji
     *
     * @var array
     */
    public const actionPath = [
        'tableName'      => 'CmsEntityAction',
        'fieldName'      => 'actionPath',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Nazwa wyświetlana
     *
     * @var array
     */
    public const displayName = [
        'tableName'      => 'CmsEntityAction',
        'fieldName'      => LANG . 'DisplayName',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => true,
        'params'         => [],
    ];

    /**
     * Opis akcji
     *
     * @var array
     */
    public const description = [
        'tableName'      => 'CmsEntityAction',
        'fieldName'      => LANG . 'Description',
        'fieldClass'     => 'EditorField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => true,
        'params'         => [],
    ];

    /**
     * Ikona akcji
     *
     * @var array
     */
    public const icon = [
        'tableName'      => 'CmsEntityAction',
        'fieldName'      => 'icon',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Przeznaczenie akcji (encja, rekord)
     *
     * @var array
     */
    public const target = [
        'tableName'      => 'CmsEntityAction',
        'fieldName'      => 'target',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Dodatkowe parametry akcji
     *
     * @var array
     */
    public const params = [
        'tableName'      => 'CmsEntityAction',
        'fieldName'      => 'params',
        'fieldClass'     => 'JsonField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Kod PHP akcji
     *
     * @var array
     */
    public const code = [
        'tableName'      => 'CmsEntityAction',
        'fieldName'      => 'code',
        'fieldClass'     => 'EditorField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Pozycja na liście
     *
     * @var array
     */
    public const position = [
        'tableName'      => 'CmsEntityAction',
        'fieldName'      => 'position',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];


    /**
     * CmsEntityActionParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = true;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = false;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,
            self::entityId,
            self::actionPath,
            Q::alias(self::displayName, 'displayName'),
            Q::alias(self::description, 'description'),
            self::icon,
            self::target,
            self::params,
            self::code,
            self::position,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,

        ];
        $this->joinColumns = [
            Q::alias('SelectEntity.name', '__entity_name'),
            Q::alias('SelectEntity.groupId', '__entity_groupId'),
            Q::alias('SelectEntity.position', '__entity_position'),
            Q::alias('SelectEntity.' . LANG . 'Description', '__entity_description'),
            Q::alias('SelectEntity.' . LANG . 'DisplayName', '__entity_displayName'),
            Q::alias('SelectEntity.isSortable', '__entity_isSortable'),
            Q::alias('SelectEntity.isSoftDelete', '__entity_isSoftDelete'),
            Q::alias('SelectEntity.isVersionable', '__entity_isVersionable'),
            Q::alias('SelectEntity.isHideable', '__entity_isHideable'),
            Q::alias('SelectEntity.isSingle', '__entity_isSingle'),
            Q::alias('SelectEntity.isSluggable', '__entity_isSluggable'),
            Q::alias('SelectEntity.isParentable', '__entity_isParentable'),
            Q::alias('SelectEntity.driven', '__entity_driven'),
            Q::alias('SelectEntity.type', '__entity_type'),
            Q::alias('SelectEntity.typeForm', '__entity_typeForm'),
            Q::alias('SelectEntity.typeView', '__entity_typeView'),
            Q::alias('SelectEntity.core', '__entity_core'),
            Q::alias('SelectEntity.isStreamable', '__entity_isStreamable'),

        ];
        $this->joinRelations = [
            ['left' => self::entityId, 'right' => CmsEntity::id, 'alias' => 'SelectEntity'],

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

