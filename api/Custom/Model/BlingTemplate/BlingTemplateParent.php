<?php

namespace BlingCustom\Model\BlingTemplate;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;

use BlingCustom\Model\BlingTemplate\BlingTemplate;

use Db\Manager as Q;

abstract class BlingTemplateParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'BlingTemplate', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'BlingTemplate';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'BlingTemplate', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'name',
        'displayName',
        'description',
        'tags',
        'htmlContent',
        'kind',
        'parentId',
        'subject',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'BlingTemplate',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'BlingTemplate',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'BlingTemplate',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'BlingTemplate',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'BlingTemplate',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Nazwa szablonu
     *
     * @var array
     */
    public const name = [
        'tableName'      => 'BlingTemplate',
        'fieldName'      => 'name',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Nazwa wyświetlana
     *
     * @var array
     */
    public const displayName = [
        'tableName'      => 'BlingTemplate',
        'fieldName'      => LANG . 'DisplayName',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => true,
        'params'         => [],
    ];

    /**
     * Opis szablonu
     *
     * @var array
     */
    public const description = [
        'tableName'      => 'BlingTemplate',
        'fieldName'      => LANG . 'Description',
        'fieldClass'     => 'TextareaField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => true,
        'params'         => [],
    ];

    /**
     * Lista i opis dostępnych zmiennych
     *
     * @var array
     */
    public const tags = [
        'tableName'      => 'BlingTemplate',
        'fieldName'      => LANG . 'Tags',
        'fieldClass'     => 'JsonField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => true,
        'params'         => [],
    ];

    /**
     * Kod HTML szablonu
     *
     * @var array
     */
    public const htmlContent = [
        'tableName'      => 'BlingTemplate',
        'fieldName'      => LANG . 'HtmlContent',
        'fieldClass'     => 'EditorField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => true,
        'params'         => [],
    ];

    /**
     * Rodzaj szablonu
     *
     * @var array
     */
    public const kind = [
        'tableName'      => 'BlingTemplate',
        'fieldName'      => 'kind',
        'fieldClass'     => 'MiniinputField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID rodzica
     *
     * @var array
     */
    public const parentId = [
        'tableName'      => 'BlingTemplate',
        'fieldName'      => 'parentId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Tytuł wysyłanej wiadomości
     *
     * @var array
     */
    public const subject = [
        'tableName'      => 'BlingTemplate',
        'fieldName'      => LANG . 'Subject',
        'fieldClass'     => 'TextareaField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => true,
        'params'         => [],
    ];


    /**
     * BlingTemplateParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = false;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = false;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,
            self::name,
            Q::alias(self::displayName, 'displayName'),
            Q::alias(self::description, 'description'),
            Q::alias(self::tags, 'tags'),
            Q::alias(self::htmlContent, 'htmlContent'),
            self::kind,
            self::parentId,
            Q::alias(self::subject, 'subject'),

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,

        ];
        $this->joinColumns = [
            Q::alias('SelectParent.name', '__parent_name'),
            Q::alias('SelectParent.' . LANG . 'DisplayName', '__parent_displayName'),
            Q::alias('SelectParent.' . LANG . 'Description', '__parent_description'),
            Q::alias('SelectParent.' . LANG . 'Tags', '__parent_tags'),
            Q::alias('SelectParent.' . LANG . 'HtmlContent', '__parent_htmlContent'),
            Q::alias('SelectParent.kind', '__parent_kind'),
            Q::alias('SelectParent.parentId', '__parent_parentId'),
            Q::alias('SelectParent.' . LANG . 'Subject', '__parent_subject'),

        ];
        $this->joinRelations = [
            ['left' => self::parentId, 'right' => BlingTemplate::id, 'alias' => 'SelectParent'],

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

