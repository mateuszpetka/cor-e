<?php

namespace BlingCustom\Model\BlingUserGroupPermission;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;


use Db\Manager as Q;

abstract class BlingUserGroupPermissionParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'BlingUserGroupPermission', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'BlingUserGroupPermission';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'BlingUserGroupPermission', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'entityId',
        'groupId',
        'canCreate',
        'canRead',
        'canUpdate',
        'canDelete',
        'canPublish',
        'canView',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'BlingUserGroupPermission',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'BlingUserGroupPermission',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'BlingUserGroupPermission',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'BlingUserGroupPermission',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'BlingUserGroupPermission',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID encji
     *
     * @var array
     */
    public const entityId = [
        'tableName'      => 'BlingUserGroupPermission',
        'fieldName'      => 'entityId',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID grupy użytkowników
     *
     * @var array
     */
    public const groupId = [
        'tableName'      => 'BlingUserGroupPermission',
        'fieldName'      => 'groupId',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy grupa może tworzyć rekord encji
     *
     * @var array
     */
    public const canCreate = [
        'tableName'      => 'BlingUserGroupPermission',
        'fieldName'      => 'canCreate',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy grupa może czytać rekord encji
     *
     * @var array
     */
    public const canRead = [
        'tableName'      => 'BlingUserGroupPermission',
        'fieldName'      => 'canRead',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy grupa może aktualizować rekord encji
     *
     * @var array
     */
    public const canUpdate = [
        'tableName'      => 'BlingUserGroupPermission',
        'fieldName'      => 'canUpdate',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy grupa może usuwać rekord encji
     *
     * @var array
     */
    public const canDelete = [
        'tableName'      => 'BlingUserGroupPermission',
        'fieldName'      => 'canDelete',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy grupa może publikować lub odpublikować rekord encji
     *
     * @var array
     */
    public const canPublish = [
        'tableName'      => 'BlingUserGroupPermission',
        'fieldName'      => 'canPublish',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy grupa może widzieć encję
     *
     * @var array
     */
    public const canView = [
        'tableName'      => 'BlingUserGroupPermission',
        'fieldName'      => 'canView',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];


    /**
     * BlingUserGroupPermissionParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = false;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = false;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,
            self::entityId,
            self::groupId,
            self::canCreate,
            self::canRead,
            self::canUpdate,
            self::canDelete,
            self::canPublish,
            self::canView,

        ];
        $this->joinColumns = [

        ];
        $this->joinRelations = [

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

