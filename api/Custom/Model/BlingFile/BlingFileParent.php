<?php

namespace BlingCustom\Model\BlingFile;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;


use Db\Manager as Q;

abstract class BlingFileParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'BlingFile', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'BlingFile';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'BlingFile', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'folderId',
        'name',
        'path',
        'alt',
        'size',
        'mime',
        'width',
        'height',
        'duration',
        'status',
        'title',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'BlingFile',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'BlingFile',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'BlingFile',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'BlingFile',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'BlingFile',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID katalogu z plikiem
     *
     * @var array
     */
    public const folderId = [
        'tableName'      => 'BlingFile',
        'fieldName'      => 'folderId',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Nazwa pliku z rozszerzeniem
     *
     * @var array
     */
    public const name = [
        'tableName'      => 'BlingFile',
        'fieldName'      => 'name',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Ścieżka do pliku na serwerze
     *
     * @var array
     */
    public const path = [
        'tableName'      => 'BlingFile',
        'fieldName'      => 'path',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Podpis pliku
     *
     * @var array
     */
    public const alt = [
        'tableName'      => 'BlingFile',
        'fieldName'      => LANG . 'Alt',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => true,
        'params'         => [],
    ];

    /**
     * Rozmiar pliku w bajtach
     *
     * @var array
     */
    public const size = [
        'tableName'      => 'BlingFile',
        'fieldName'      => 'size',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Typ mime pliku
     *
     * @var array
     */
    public const mime = [
        'tableName'      => 'BlingFile',
        'fieldName'      => 'mime',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Szerokość pliku graficznego
     *
     * @var array
     */
    public const width = [
        'tableName'      => 'BlingFile',
        'fieldName'      => 'width',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Wysokość pliku graficznego
     *
     * @var array
     */
    public const height = [
        'tableName'      => 'BlingFile',
        'fieldName'      => 'height',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Długość trwania pliku audio lub wideo
     *
     * @var array
     */
    public const duration = [
        'tableName'      => 'BlingFile',
        'fieldName'      => 'duration',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Aktualny status pliku
     *
     * @var array
     */
    public const status = [
        'tableName'      => 'BlingFile',
        'fieldName'      => 'status',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Tytuł pliku
     *
     * @var array
     */
    public const title = [
        'tableName'      => 'BlingFile',
        'fieldName'      => LANG . 'Title',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => true,
        'params'         => [],
    ];


    /**
     * BlingFileParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = false;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = true;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,
            self::folderId,
            self::name,
            self::path,
            Q::alias(self::alt, 'alt'),
            self::size,
            self::mime,
            self::width,
            self::height,
            self::duration,
            self::status,
            Q::alias(self::title, 'title'),

        ];
        $this->joinColumns = [

        ];
        $this->joinRelations = [

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

