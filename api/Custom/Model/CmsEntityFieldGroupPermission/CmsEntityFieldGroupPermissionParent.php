<?php

namespace BlingCustom\Model\CmsEntityFieldGroupPermission;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;

use BlingCustom\Model\CmsEntityFieldGroup\CmsEntityFieldGroup;
use BlingCustom\Model\BlingUserGroup\BlingUserGroup;

use Db\Manager as Q;

abstract class CmsEntityFieldGroupPermissionParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'CmsEntityFieldGroupPermission', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'CmsEntityFieldGroupPermission';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'CmsEntityFieldGroupPermission', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'groupId',
        'userGroupId',
        'canRead',
        'canUpdate',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'CmsEntityFieldGroupPermission',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'CmsEntityFieldGroupPermission',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'CmsEntityFieldGroupPermission',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'CmsEntityFieldGroupPermission',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'CmsEntityFieldGroupPermission',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID grupy pól
     *
     * @var array
     */
    public const groupId = [
        'tableName'      => 'CmsEntityFieldGroupPermission',
        'fieldName'      => 'groupId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID grupy użytkowników
     *
     * @var array
     */
    public const userGroupId = [
        'tableName'      => 'CmsEntityFieldGroupPermission',
        'fieldName'      => 'userGroupId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy grupa może czytać pola grupy
     *
     * @var array
     */
    public const canRead = [
        'tableName'      => 'CmsEntityFieldGroupPermission',
        'fieldName'      => 'canRead',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Czy grupa może edytować pola grupy
     *
     * @var array
     */
    public const canUpdate = [
        'tableName'      => 'CmsEntityFieldGroupPermission',
        'fieldName'      => 'canUpdate',
        'fieldClass'     => 'TinyintField',
        'isEditable'     => true,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];


    /**
     * CmsEntityFieldGroupPermissionParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = false;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = false;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,
            self::groupId,
            self::userGroupId,
            self::canRead,
            self::canUpdate,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,

        ];
        $this->joinColumns = [
            Q::alias('SelectGroup.name', '__group_name'),
            Q::alias('SelectGroup.position', '__group_position'),
            Q::alias('SelectGroup.isCollapsed', '__group_isCollapsed'),
            Q::alias('SelectGroup.' . LANG . 'DisplayName', '__group_displayName'),

        ];
        $this->joinRelations = [
            ['left' => self::groupId, 'right' => CmsEntityFieldGroup::id, 'alias' => 'SelectGroup'],
            ['left' => self::userGroupId, 'right' => BlingUserGroup::id, 'alias' => 'SelectUserGroup'],

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

