<?php

namespace BlingCustom\Model\BlingRecordFile;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;

use BlingCustom\Model\BlingFile\BlingFile;
use BlingCustom\Model\CmsEntityField\CmsEntityField;

use Db\Manager as Q;

abstract class BlingRecordFileParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'BlingRecordFile', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'BlingRecordFile';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'BlingRecordFile', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'recordId',
        'fileId',
        'entityFieldId',
        'entity',
        'position',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'BlingRecordFile',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'BlingRecordFile',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'BlingRecordFile',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'BlingRecordFile',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'BlingRecordFile',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID rekordu encji
     *
     * @var array
     */
    public const recordId = [
        'tableName'      => 'BlingRecordFile',
        'fieldName'      => 'recordId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID pliku
     *
     * @var array
     */
    public const fileId = [
        'tableName'      => 'BlingRecordFile',
        'fieldName'      => 'fileId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID pola encji
     *
     * @var array
     */
    public const entityFieldId = [
        'tableName'      => 'BlingRecordFile',
        'fieldName'      => 'entityFieldId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Nazwa encji
     *
     * @var array
     */
    public const entity = [
        'tableName'      => 'BlingRecordFile',
        'fieldName'      => 'entity',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Pozycja na liście
     *
     * @var array
     */
    public const position = [
        'tableName'      => 'BlingRecordFile',
        'fieldName'      => 'position',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];


    /**
     * BlingRecordFileParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = true;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = false;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,
            self::recordId,
            self::fileId,
            self::entityFieldId,
            self::entity,
            self::position,

        ];
        $this->joinColumns = [
            Q::alias('SelectEntityField.entityId', '__entityField_entityId'),
            Q::alias('SelectEntityField.fieldId', '__entityField_fieldId'),
            Q::alias('SelectEntityField.groupId', '__entityField_groupId'),
            Q::alias('SelectEntityField.name', '__entityField_name'),
            Q::alias('SelectEntityField.isEditable', '__entityField_isEditable'),
            Q::alias('SelectEntityField.isSelectable', '__entityField_isSelectable'),
            Q::alias('SelectEntityField.isTranslatable', '__entityField_isTranslatable'),
            Q::alias('SelectEntityField.isRequired', '__entityField_isRequired'),
            Q::alias('SelectEntityField.isIdentifier', '__entityField_isIdentifier'),
            Q::alias('SelectEntityField.' . LANG . 'Placeholder', '__entityField_placeholder'),
            Q::alias('SelectEntityField.foreignLeft', '__entityField_foreignLeft'),
            Q::alias('SelectEntityField.foreignRight', '__entityField_foreignRight'),
            Q::alias('SelectEntityField.foreignColumns', '__entityField_foreignColumns'),
            Q::alias('SelectEntityField.params', '__entityField_params'),
            Q::alias('SelectEntityField.' . LANG . 'DisplayName', '__entityField_displayName'),
            Q::alias('SelectEntityField.' . LANG . 'Description', '__entityField_description'),
            Q::alias('SelectEntityField.position', '__entityField_position'),
            Q::alias('SelectEntityField.core', '__entityField_core'),
            Q::alias('SelectEntityField.isProtected', '__entityField_isProtected'),
            Q::alias('SelectEntityField.showOnList', '__entityField_showOnList'),

        ];
        $this->joinRelations = [
            ['left' => self::fileId, 'right' => BlingFile::id, 'alias' => 'SelectFile'],
            ['left' => self::entityFieldId, 'right' => CmsEntityField::id, 'alias' => 'SelectEntityField'],

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

