<?php

namespace BlingCustom\Model\BlingRouteTranslation;

use BlingApi\Core\Enum\EntityDriven;
use BlingApi\Core\Model;

use BlingCustom\Model\BlingLanguage\BlingLanguage;
use BlingCustom\Model\BlingRoute\BlingRoute;

use Db\Manager as Q;

abstract class BlingRouteTranslationParent extends Model
{

    /**
     * Get all columns from table.
     *
     * @var array
     */
    public const _all = ['tableName' => 'BlingRouteTranslation', 'fieldName' => '*', 'noEscape' => true];

    /**
     * Table name.
     *
     * @var string
     */
    public const _tableName = 'BlingRouteTranslation';

    /**
     * Primary key.
     *
     * @var array
     */
    public const _primaryKey = ['tableName' => 'BlingRouteTranslation', 'fieldName' => 'id'];

    /**
     * List of all columns in table.
     *
     * @var array
     */
    public const _columns = [
        'id',
        'added',
        'addedBy',
        'edited',
        'editedBy',
        'languageId',
        'translationKey',
        'translation',
        'routeId',
        'title',
        'params',
    ];

    
    /**
     * id
     *
     * @var array
     */
    public const id = [
        'tableName'      => 'BlingRouteTranslation',
        'fieldName'      => 'id',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => true,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * added
     *
     * @var array
     */
    public const added = [
        'tableName'      => 'BlingRouteTranslation',
        'fieldName'      => 'added',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * addedBy
     *
     * @var array
     */
    public const addedBy = [
        'tableName'      => 'BlingRouteTranslation',
        'fieldName'      => 'addedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * edited
     *
     * @var array
     */
    public const edited = [
        'tableName'      => 'BlingRouteTranslation',
        'fieldName'      => 'edited',
        'fieldClass'     => 'DatetimeField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * editedBy
     *
     * @var array
     */
    public const editedBy = [
        'tableName'      => 'BlingRouteTranslation',
        'fieldName'      => 'editedBy',
        'fieldClass'     => 'IntegerField',
        'isEditable'     => false,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID języka tłumaczenia
     *
     * @var array
     */
    public const languageId = [
        'tableName'      => 'BlingRouteTranslation',
        'fieldName'      => 'languageId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Klucz tłumaczenia
     *
     * @var array
     */
    public const translationKey = [
        'tableName'      => 'BlingRouteTranslation',
        'fieldName'      => 'translationKey',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Tłumaczenie
     *
     * @var array
     */
    public const translation = [
        'tableName'      => 'BlingRouteTranslation',
        'fieldName'      => 'translation',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * ID ścieżki
     *
     * @var array
     */
    public const routeId = [
        'tableName'      => 'BlingRouteTranslation',
        'fieldName'      => 'routeId',
        'fieldClass'     => 'SelectField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Tłumaczenie tytułu strony dla podanej ścieżki
     *
     * @var array
     */
    public const title = [
        'tableName'      => 'BlingRouteTranslation',
        'fieldName'      => 'title',
        'fieldClass'     => 'InputField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];

    /**
     * Dodatkowe parametry dla tłumaczenia
     *
     * @var array
     */
    public const params = [
        'tableName'      => 'BlingRouteTranslation',
        'fieldName'      => 'params',
        'fieldClass'     => 'JsonField',
        'isEditable'     => true,
        'isSelectable'   => false,
        'isTranslatable' => false,
        'params'         => [],
    ];


    /**
     * BlingRouteTranslationParent constructor.
     */
    public function __construct()
    {
        $this->tableName     = self::_tableName;
        $this->primaryKey    = self::_primaryKey;
        $this->isSortable    = false;
        $this->isSoftDelete  = false;
        $this->isVersionable = false;
        $this->isHideable    = false;
        $this->isSingle      = false;
        $this->isSluggable   = false;
        $this->isParentable  = false;
        $this->isStreamable  = false;
        $this->driven        = EntityDriven::none;

        $this->columns       = self::_columns;
        $this->publicColumns = [
            self::id,

        ];
        $this->hiddenColumns = [
            self::added,
            self::addedBy,
            self::edited,
            self::editedBy,
            self::languageId,
            self::translationKey,
            self::translation,
            self::routeId,
            self::title,
            self::params,

        ];
        $this->joinColumns = [
            Q::alias('SelectLanguage.appId', '__language_appId'),
            Q::alias('SelectLanguage.code', '__language_code'),
            Q::alias('SelectLanguage.active', '__language_active'),
            Q::alias('SelectLanguage.visible', '__language_visible'),
            Q::alias('SelectLanguage.isDefault', '__language_isDefault'),
            Q::alias('SelectLanguage.position', '__language_position'),

        ];
        $this->joinRelations = [
            ['left' => self::languageId, 'right' => BlingLanguage::id, 'alias' => 'SelectLanguage'],
            ['left' => self::routeId, 'right' => BlingRoute::id, 'alias' => 'SelectRoute'],

        ];
        $this->autoFields = [

        ];
    }

    /**
     * Pobranie tablicy z nazwą tabeli i nazwą kolumny.
     *
     * @param string $fieldName
     * @return array
     */
    public static function getModelField(string $fieldName): array
    {
        $fieldName = str_replace('`', '', $fieldName);
        $field = explode('.', $fieldName);

        if (count($field) === 2 && $field[0] === self::_tableName && defined('self::' . $field[1])) {
            return constant('self::' . $field[1]);
        } elseif (count($field) === 1 && defined('self::' . $field[0])) {
            return constant('self::' . $field[0]);
        }

        error(sprintf('Field %s not found in %s entity', $fieldName, self::_tableName), 132, 500);
    }
}

