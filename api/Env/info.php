<?php

if (!isset($_SERVER['PHP_AUTH_USER']) || ($_SERVER['PHP_AUTH_USER'] !== 'bling')
    || (!password_verify(
        $_SERVER['PHP_AUTH_PW'],
        '$2y$10$hDbgzB0h26J.BPFwzKhQjORPSR3NMQa3MGi3yZt.NlOnCj5zgvdxW'
    ))) {
    header('WWW-Authenticate: Basic Realm="Secret Stash"');
    header('HTTP/1.0 401 Unauthorized');
    exit;
} else {
    phpinfo();
}
